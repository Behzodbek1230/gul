import { useAppContext } from "../../contexts/app/AppContext";
import Link from "next/link";
import { useRouter } from "next/router";
import React, {useCallback, useEffect, useState} from "react";
import Avatar from "../avatar/Avatar";
import Box from "../Box";
import FlexBox from "../FlexBox";
import Grid from "../grid/Grid";
import Icon from "../icon/Icon";
import Rating from "../rating/Rating";
import { H1, H2, H3, H6, SemiSpan } from "../Typography";
import {useDispatch, useSelector} from "react-redux";
import {FormattedMessage} from "react-intl";
import {Button, Modal, ModalBody, ModalHeader} from 'reactstrap';
import CheckoutForm from "../../components/checkout/CheckoutForm";
import get_current_product from "../../../Redux/Actions/get_current_product";
import InnerImageZoom  from "react-inner-image-zoom"
import ProductDescription from "../../components/products/ProductDescription";
import Cookies from "js-cookie"
import axios from "axios";
import { SITE_NAME} from "../../components/Variables";
import Currencies from "../../components/Currencies";
import {FacebookShareButton,
    FacebookIcon,
    TelegramShareButton,
    TelegramIcon,
    TwitterShareButton,
    TwitterIcon,
    WhatsappIcon,
    WhatsappShareButton
} from "react-share"


export interface ProductIntroProps {
  imgUrl?: string[];
  title: string;
  price: number;
  id?: string|number;
  rating?:number,
  total?:string,
  className?:string,
  open?:boolean,
  keyword2?:string,
  categoryKeyword?:string,
  shopName?:string,
  shopKeyword?:string,
  deliveryTime?:string,
}

const ProductIntro: React.FC<ProductIntroProps> = ({
  imgUrl,
  title,
  price,
  id,
  rating,
  className,
  open,
  categoryKeyword,
  shopName,
  keyword2,
  shopKeyword,
  deliveryTime
}) => {
  let router = useRouter()
  let info = useSelector((state:any)=>state.new.one_product_info)
  const [selectedImage, setSelectedImage] = useState(0);
  const { state, dispatch } = useAppContext();
  const { cartList } = state.cart
  const [modal, setModal] = useState(false);
  const toggle = () => setModal(!modal);
  let lang2 = router.locale
  let dispatch2 = useDispatch()
  let {keyword} = router.query
  
  useEffect(() => {
    if(!modal){
      dispatch2(get_current_product(""))
    }
  }, [modal]);
  const routerId = router.query.id as string;
  const cartItem = cartList.find(
    (item) => item.id === id || item.id === routerId
  );

  const handleImageClick = (ind) => () => {
    setSelectedImage(ind);
  };
    const handleAddCart = useCallback(
    (amount) => () => {
      const login = Cookies.get("isLoggedIn")
      if(login === "true"){
        const data = new FormData();
        data.append("keyword",id.toString())
        axios({
          method:"POST",
          url:`/orders/add-basket/${lang2}`,
          data:data
        })
            .then((res)=>{
                if(res.data.errors){
                    return null
                }
                else{
                    dispatch({
                        type: "CHANGE_CART_AMOUNT",
                        payload: {
                            id:id?.toString() || info.data.keyword,
                            name: title,
                            qty: amount,
                            price:price,
                            imgUrl:imgUrl[0],
                            categoryKeyword:categoryKeyword
                        },
                    });
                }
            })
            .catch(()=>null)
      }
      else{
          dispatch({
              type: "CHANGE_CART_AMOUNT",
              payload: {
                  id:id.toString() || info.data.keyword,
                  name: title,
                  qty: amount,
                  price:price,
                  imgUrl:imgUrl[0],
                  categoryKeyword:categoryKeyword
              },
          });
      }

    },
    []
);

const handleBuyNow = () => {
  toggle();
  dispatch2(get_current_product(keyword))
}

const handleremoveCart = useCallback(
    (amount) => () => {
      const login = Cookies.get("isLoggedIn")
      if(login === "true"){
        if(amount===0){
         const data = new FormData();
         data.append("keyword",id.toString())
         axios({
           method:"POST",
           url:`/orders/delete-basket/${lang2}`,
           data:data
         })
        }
         const data = new FormData();
         data.append("keyword",id.toString())
         axios({
           method:"POST",
           url:`/orders/remove-basket/${lang2}`,
           data:data
         })
             .then(res=>{
                 if(res.data.errors){
                     return null;
                 }
                 else{
                     dispatch({
                         type: "CHANGE_CART_AMOUNT",
                         payload: {
                             name: title,
                             qty: amount,
                             price:price,
                             imgUrl:imgUrl[0],
                             id,
                             categoryKeyword:categoryKeyword
                         },
                     });
                 }
             })
             .catch(()=>null)
       }
       else{
          dispatch({
              type: "CHANGE_CART_AMOUNT",
              payload: {
                  name: title,
                  qty: amount,
                  price:price,
                  imgUrl:imgUrl[0],
                  id,
                  categoryKeyword:categoryKeyword
              },
          });
      }

    },
    []
);


  return (
    <Box  overflow="hidden" >
      <Grid
          container
          spacing={!open ? 10 : 4}
          className={!open ?
              "mr-0 pr-0 "
              :
              ""
          }
          style={!open ?
              {padding:"0px"} :
              {padding:"20px"}
          }
      >
        <Grid item md={5} lg={5} xl={5} xs={12} style={{width:"fit-content"}}  alignItems="center" >
          <Box>
            <div>
              <FlexBox   justifyContent="center" mb="18px" >
                <div
                    className={!open ? "product_image" :"product_image_main"}
                    style={!open ?
                        {height:"420px",overflow:"hidden",width:"480px"}
                        :
                        {height:"280px",overflow:"hidden",width:"290px"}
                    }
                >
                    <InnerImageZoom

                      style={!open ?
                          {overflow:"hidden",width:"480px",objectFit:"cover",objectPosition:"center"}
                          :
                          {overflow:"hidden",width:"350px",objectFit:"cover",objectPosition:"center"}
                      }
                      height={!open ? "450px" : "350px"}
                      alt={title}
                      src={imgUrl[selectedImage]}
                      zoomSrc={imgUrl[selectedImage]}
                      zoomType="hover"
                      zoomPreload={true}
                      fullscreenOnMobile={true}
                  />
                </div>

              </FlexBox>
              <FlexBox  overflow="auto" className={!open ?  "product_image_slider mt-1" :"product_image_slider_main mt-1"} >
                {imgUrl.map((url, ind) => (
                  <Box
                    mb="7px"
                    size={70}
                    minWidth={70}

                    bg="white"
                    borderRadius="10px"
                    display="flex"
                    justifyContent="center"
                    alignItems="center"
                    cursor="pointer"
                    border="1px solid"
                    key={ind}
                    ml={ind === 0 && "auto"}
                    mr={ind === imgUrl.length - 1 ? "auto" : "10px"}
                    borderColor={
                      selectedImage === ind ? "primary.main" : "gray.400"
                    }

                    onMouseEnter={handleImageClick(ind)}
                  >
                    <Avatar  itemprop="image" href={url} src={url} title={title} borderRadius="10px"  />
                  </Box>
                ))}

              </FlexBox>
            </div>
          </Box>
        </Grid>

        <Grid className="padding-0" item lg={!open ? 5 : 4} xl={!open ? 7 : 4} md={!open ? 5 : 4} xs={12} >
          <H1
              itemprop="name"
              mb="1rem"
          >
            {title}
          </H1>


          <FlexBox alignItems="center" mb="1rem">
            <SemiSpan>
              <FormattedMessage
                  id="rating"
                  defaultMessage="Rating"
              />:
            </SemiSpan>
            <Box ml="8px" mr="8px">
              <Rating color="warn" value={rating} outof={5} />
            </Box>
          </FlexBox>

          <Box mb="24px">
            <H2 style={{fontSize:"20px",whiteSpace:"nowrap"}} color="primary.main" mb="4px" lineHeight="1">
              {price}
            </H2>
          </Box>
          {!cartItem?.qty ? (
              <div className="mr-0 mr-sm-0 mr-md-2 mr-lg-2 mr-xl-2">
            <Button
              className="mt-2 add_to_cart_detail width-sm-100 d-flex justify-content-center"
              color="success"
              variant="contained"
              size="small"
              mb="40px"
              style={{width:"327px"}}
              onClick={handleAddCart(1)}
            >
              <Icon variant="small" className="d-inline mr-2">shopping-cart2</Icon> 
              <FormattedMessage
                id="add_to_cart"
                defaultMessage="Savatchaga qo'shish"
              />
            </Button>

              </div>
          ) : (
            <FlexBox alignItems="center" mb="10px" className="mr-2 plus_minus_container_detail" >
              <Button
                p="9px"
                outline={true}
                size="small"
                color="danger"
                onClick={handleremoveCart(cartItem.qty - 1)}
              >
                <Icon variant="small">minus</Icon>
              </Button>
              <H3 fontWeight="600" mx="20px">
                {cartItem?.qty.toString().padStart(2, "0")}
              </H3>
              <Button
                p="9px"
                outline={true}
                size="small"
                color="danger"
                onClick={handleAddCart(cartItem?.qty + 1)}
              >
                <Icon variant="small">plus</Icon>
              </Button>
            </FlexBox>
          )}
          <div>
            <Button  
              className="mt-2 width-sm-100 buy_now_detail d-flex justify-content-center"
              style={{width:"327px",marginBottom:"20px"}}
              color="danger"
              onClick={()=>handleBuyNow()}>
                <Icon className="mr-1" variant="small">thunder</Icon>
                <FormattedMessage id="Buy Now" defaultMessage="Hoziroq sotib olish" />
            </Button>
            <div style={{width:"fit-content",marginBottom:"27px"}}><Currencies /></div>
            <Modal size="lg"   isOpen={modal} toggle={toggle} className={className}>
              <ModalHeader toggle={()=>{toggle();}}><FormattedMessage id="Buy Now" defaultMessage="Hoziroq sotib olish" /></ModalHeader>
              <ModalBody >
                  <CheckoutForm deliveryTime = {deliveryTime} postcard_visible={true} />
              </ModalBody>
            </Modal>
          </div>

          <FlexBox alignItems="center" mb="1rem" mt="20px" >
            <SemiSpan className="whitespace-nowrap">
              <FormattedMessage
                  id="sold_by"
              />:</SemiSpan>
            <Link href={`/shop/${shopKeyword || keyword2}` }>
              <a>
                <H6 className="whitespace-nowrap" lineHeight="1" ml="8px">
                  {shopName}
                </H6>
              </a>
            </Link>
          </FlexBox>
          <FlexBox   alignItems="center" mb="1rem" mt="20px" >
              <div className="mr-1">
                  <FormattedMessage id="share_on" />:
              </div>
                <div className="ml-1 mr-1">
                    <FacebookShareButton  url={SITE_NAME + router.asPath} quote={title} hashtag={info?.seo?.length !==0 && info?.seo ? info?.seo?.view_share_description : ""} >
                        <FacebookIcon href={SITE_NAME+router.asPath} size={20} round={true}/>
                    </FacebookShareButton>
                </div>
                <div className="ml-1 mr-1">
                    <TelegramShareButton  url={SITE_NAME + router.asPath} title={title}  >
                        <TelegramIcon href={SITE_NAME+router.asPath} size={20} round={true}/>
                    </TelegramShareButton>
                </div>
                <div className="ml-1 mr-1">
                    <TwitterShareButton  url={SITE_NAME + router.asPath} title={title}  >
                        <TwitterIcon  size={20} round={true}/>
                    </TwitterShareButton>
                </div>
                <div className="ml-1 mr-1">
                    <WhatsappShareButton  url={SITE_NAME + router.asPath}  >
                        <WhatsappIcon href={SITE_NAME+router.asPath} size={20} round={true}/>
                    </WhatsappShareButton>
                </div>
          </FlexBox>
          <br/>
          {!open ? <>

            <H2>
              <FormattedMessage id="description" />
            </H2>
            <hr className="details-hr-7 details-hr-sm-100 "/>
            <ProductDescription /></> : ""}
        </Grid>
      </Grid>
    </Box>
  );
};

ProductIntro.defaultProps = {
  imgUrl: [
    "",
    "",
    "",
  ],
  title: "",
  price: 0,
};

export default ProductIntro;
