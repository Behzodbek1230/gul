import { useAppContext } from "../contexts/app/AppContext";
import { CartItem } from "../reducers/cartReducer";
import React, {Fragment, useEffect, useState} from "react";
import Button from "../components/buttons/Button";
import { Card1 } from "../components/Card1";
import Grid from "../components/grid/Grid";
import CheckoutNavLayout from "../components/layout/CheckoutNavLayout";
import TextField from "../components/text-field/TextField";
import axios from "axios";
import {useSelector} from "react-redux";
import CheckoutForm from "../components/checkout/CheckoutForm";
import CheckoutSummary from "../components/checkout/CheckoutSummary";
import {FormattedMessage, useIntl} from "react-intl";
import Postcard from "../components/postcard";
import Cookies from "js-cookie"
import { NextSeo } from "next-seo";
import Loading from "../components/Loading";
import Accordions_Cart from "../components/accordions2";
import {useRouter} from "next/router";
const Cart = () => {
  let router = useRouter()
  const { state } = useAppContext();
  const cartList: CartItem[] = state.cart.cartList;
  let lang = router.locale
  let [loading,setloading] = useState(false)
  let change = useSelector((state:any)=>state.new.CartIsChanged)
  let loaded_cart = useSelector((state:any)=>state.new.CartLoadedFully)
  let [cartThings,setcartthings] = useState({
  data:[],
    totalProductPrice:"",
    totalDelivery:"",
    totalPrice:"",
    discount:"",
    deliveryTime:"",
    beginDeliveryTime: "",
    endDeliveryTime:""
  })
  
  let utils = useIntl()
  // const dispatch = useDispatch()
  // useEffect(()=>{
    
  // },[])
  let fetch_basket_list_panel = ()=>{
    let currency_id = Cookies.get("currency_id")
    let currency_text = typeof currency_id === "undefined" ? "" : `?currency=${currency_id}`
    if(cartList.length === 0){
     setcartthings(
       {
         data:[],
         totalDelivery:"0",
         totalPrice:"0",
         totalProductPrice:"0",
         discount:"0",
         deliveryTime:"0",
         beginDeliveryTime: "",
         endDeliveryTime:""
        }
    )
    }
    else{

     axios({
       url:`/orders/basket-list-panel/${lang}${currency_text}`,
       method:"POST",
       data:{flowers:cartList}
     })
     .then(res=>{
         setcartthings(res.data)
     })
     .catch(()=>null)
  }
}
  useEffect(()=>{
      let currency_id = Cookies.get("currency_id")
      let currency_text = typeof currency_id === "undefined" ? "" : `?currency=${currency_id}`
      if (cartList.length === 0) {
        setcartthings({
          data: [],
          totalDelivery: "0",
          totalPrice: "0",
          totalProductPrice: "0",
          discount: "0",
          deliveryTime: "0",
          beginDeliveryTime: "",
          endDeliveryTime:""
        })
      } else {
        axios({
          url: `/orders/basket-list-panel/${lang}${currency_text}`,
          method: "POST",
          data: {flowers: cartList}
        })
            .then(res => {
              setcartthings(res.data)
            })
            .catch(() => null)
      }

  },[change,loaded_cart,lang])
  

  return (
    <CheckoutNavLayout>
      <NextSeo
          title={utils.formatMessage({id:"cart"})}
      />
      {loading ? 
        <Loading />
        :
        <Fragment>
        <Grid container spacing={6}>
          <Grid item lg={8} md={8} xs={12}>
            <Accordions_Cart
              fetch_basket_list_panel={()=>fetch_basket_list_panel()}
              setcartthings = {(e)=>setcartthings(e)}
              setloading={(e)=>setloading(e)}
              data={cartThings.data}
              cartThings={cartThings.data}
            />
            <div className="mb-4 mt-4">
              <Card1 className="pb-4">
                <Postcard/>
              </Card1>

            </div>
            <div className="mb-4 rounded-3 d-block d-sm-block d-md-none d-lg-none d-xl-none " id="checkout_summary">
              <div className="row justify-content-end ">
                <div className="col-lg-6 col-xl-6 col-md-6 col-sm-12 col-12">
                    <div className="mb-4">
                          <CheckoutSummary data={cartThings}  />
                      </div>
                      <Card1>
                        <TextField placeholder={utils.formatMessage({id:"Voucher"})} readOnly={true} fullwidth />

                        <Button
                          variant="outlined"
                          color="primary"
                          mt="1rem"
                          mb="0px"
                          fullwidth
                        >
                          <FormattedMessage
                            id="Apply Voucher"
                            defaultMessage="Vaucherni ishlatish"
                          />
                        </Button>
                      </Card1>
                </div>
              </div>
            </div>


          <Grid item lg={12} md={12} xs={12}>
            <CheckoutForm 
              deliveryTime={cartThings?.deliveryTime}  
              postcard_visible={false}
              beginDeliveryTime={cartThings.beginDeliveryTime}
              endDeliveryTime={cartThings.endDeliveryTime}
            />
          </Grid>

          </Grid>
          <Grid item lg={4} md={4} xs={12} className="d-none d-sm-none d-md-block d-lg-block d-xl-block">
            <div className="mb-3">
                <CheckoutSummary  data={cartThings}  />
            </div>
            <Card1>
              <TextField placeholder={utils.formatMessage({id:"Voucher"})} readOnly={true} fullwidth />

              <Button
                variant="outlined"
                color="primary"
                mt="1rem"
                mb="0px"
                fullwidth
              >
                <FormattedMessage
                  id="Apply Voucher"
                  defaultMessage="Vaucherni ishlatish"
                />
              </Button>

            </Card1>

          </Grid>
        </Grid>
      </Fragment>  
    }
    </CheckoutNavLayout>
  );
};

export default Cart;
