"use strict";
exports.id = 695;
exports.ids = [695,4660];
exports.modules = {

/***/ 4660:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5834);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_system__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _utils_utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1972);
/* harmony import */ var _Box__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(4875);




const Card = styled_components__WEBPACK_IMPORTED_MODULE_0___default()(_Box__WEBPACK_IMPORTED_MODULE_3__["default"])`
  background-color: ${(0,_utils_utils__WEBPACK_IMPORTED_MODULE_2__/* .getTheme */ .gh)("colors.body.paper")};
  box-shadow: ${(props)=>(0,_utils_utils__WEBPACK_IMPORTED_MODULE_2__/* .getTheme */ .gh)(`shadows.${props.boxShadow}`, `shadows.${props.elevation}`)
};

  :hover {
    box-shadow: ${(props)=>props.hoverEffect && (0,_utils_utils__WEBPACK_IMPORTED_MODULE_2__/* .getTheme */ .gh)("shadows.large")
};
  }

  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.border}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.color}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.space}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.layout}
`;
Card.defaultProps = {
    boxShadow: "small",
    borderRadius: 8,
    hoverEffect: false
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Card);


/***/ }),

/***/ 3392:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ avatar_Avatar)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "styled-components"
var external_styled_components_ = __webpack_require__(7518);
var external_styled_components_default = /*#__PURE__*/__webpack_require__.n(external_styled_components_);
// EXTERNAL MODULE: external "styled-system"
var external_styled_system_ = __webpack_require__(5834);
;// CONCATENATED MODULE: ./components/avatar/AvatarStyle.tsx


const StyledAvatar = (external_styled_components_default()).div`
  display: block;
  position: relative;
  text-align: center;
  font-weight: 600;
  font-size: ${(props)=>props.size / 2
}px;
  border-radius: ${(props)=>props.size
}px;
  min-width: ${(props)=>props.size
}px;
  overflow: hidden;

  img {
    display: block;
    height: 100%;
    width: 100%;
  }
  & > * {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    line-height: 0;
  }
  ${external_styled_system_.color}
  ${external_styled_system_.space}
  ${external_styled_system_.border}
  ${external_styled_system_.layout}
`;
/* harmony default export */ const AvatarStyle = (StyledAvatar);

;// CONCATENATED MODULE: ./components/avatar/Avatar.tsx



const Avatar = ({ src , size , children , ...props })=>{
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(AvatarStyle, {
        size: size,
        ...props,
        children: [
            src && /*#__PURE__*/ jsx_runtime_.jsx("img", {
                style: {
                    objectFit: "cover",
                    objectPosition: "center"
                },
                src: src,
                alt: "avatar"
            }),
            !src && children && /*#__PURE__*/ jsx_runtime_.jsx("span", {
                children: children
            })
        ]
    }));
};
Avatar.defaultProps = {
    size: 48
};
/* harmony default export */ const avatar_Avatar = (Avatar);


/***/ })

};
;