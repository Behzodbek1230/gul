import styled from 'styled-components'
let Section3Container = styled.div`
    @media only screen and (max-width:650px){
            margin-bottom: 0rem;
            margin-top: 0rem;
            padding-right: 1rem;
            overflow-x: hidden;
    }
    @media only screen and (max-width:2000px) and(min-width:1400px){
        margin-left: 0px;
    }
    @media only screen and (max-width:1400px) and(min-width:1211px){
        margin-left: 8%;
        margin-right: 8%;
    }
    @media only screen and (max-width:1211px) and(min-width:1122px){
        margin-left: 90px;
        margin-right: 102px;
    }
    @media only screen and (max-width:1000px) and(min-width:1122px){
        margin-left: 40px;
        margin-right: 55px;
    }
    @media only screen and (max-width:937px) and(min-width:1000px){
        margin-left: 120px;
        margin-right: 150px;
    }
    @media only screen and (max-width:650px) and(min-width:835px){
        margin-left: 60px;
        margin-right: 60px;
    }
}
`;
export default Section3Container