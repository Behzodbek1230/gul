"use strict";
exports.id = 966;
exports.ids = [966,9221,9758,4875,5359,8444];
exports.modules = {

/***/ 4875:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5834);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_system__WEBPACK_IMPORTED_MODULE_1__);


const Box = styled_components__WEBPACK_IMPORTED_MODULE_0___default().div(({ shadow , cursor , transition , theme  })=>({
        boxShadow: theme.shadows[shadow],
        cursor,
        transition
    })
, (0,styled_system__WEBPACK_IMPORTED_MODULE_1__.compose)(styled_system__WEBPACK_IMPORTED_MODULE_1__.layout, styled_system__WEBPACK_IMPORTED_MODULE_1__.space, styled_system__WEBPACK_IMPORTED_MODULE_1__.color, styled_system__WEBPACK_IMPORTED_MODULE_1__.position, styled_system__WEBPACK_IMPORTED_MODULE_1__.flexbox, styled_system__WEBPACK_IMPORTED_MODULE_1__.flex, styled_system__WEBPACK_IMPORTED_MODULE_1__.border, styled_system__WEBPACK_IMPORTED_MODULE_1__.typography));
Box.defaultProps = {
    shadow: 0,
    cursor: "unset"
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Box);


/***/ }),

/***/ 9221:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5834);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_system__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _utils_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(9758);



const Container = (styled_components__WEBPACK_IMPORTED_MODULE_0___default().div)`
  max-width: ${_utils_constants__WEBPACK_IMPORTED_MODULE_2__/* .layoutConstant.containerWidth */ .P.containerWidth};
  margin-left: auto;
  margin-right: auto;

  @media only screen and (max-width: 1199px) {
    margin-left: 1rem;
    margin-right: 1rem;
  }

  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.color}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.position}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.flexbox}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.layout}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.space}
`;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Container);


/***/ }),

/***/ 5359:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5834);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_system__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Box__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4875);



const FlexBox = styled_components__WEBPACK_IMPORTED_MODULE_0___default()(_Box__WEBPACK_IMPORTED_MODULE_2__["default"])`
  display: flex;
  flex-direction: row;

  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.layout}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.color}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.flexbox}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.space}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.border}
`;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (FlexBox);


/***/ }),

/***/ 3211:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _CategorySectionCreator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(7272);
/* harmony import */ var _components_carousel_Carousel__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(9868);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _hooks_useWindowSize__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(12);
/* harmony import */ var react_intersection_observer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(9785);
/* harmony import */ var react_intersection_observer__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_intersection_observer__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var next_dynamic__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(5218);







const ProductCard1 = (0,next_dynamic__WEBPACK_IMPORTED_MODULE_6__["default"])(()=>Promise.all(/* import() */[__webpack_require__.e(2389), __webpack_require__.e(8936), __webpack_require__.e(695), __webpack_require__.e(8414), __webpack_require__.e(8167), __webpack_require__.e(8970), __webpack_require__.e(197), __webpack_require__.e(3887), __webpack_require__.e(639), __webpack_require__.e(3689), __webpack_require__.e(2599), __webpack_require__.e(903)]).then(__webpack_require__.bind(__webpack_require__, 2599))
, {
    ssr: false
});
const Box = (0,next_dynamic__WEBPACK_IMPORTED_MODULE_6__["default"])(()=>Promise.resolve(/* import() */).then(__webpack_require__.bind(__webpack_require__, 4875))
, {
    ssr: false
});
const Section2 = ({ data  })=>{
    const { 0: visibleSlides , 1: setVisibleSlides  } = (0,react__WEBPACK_IMPORTED_MODULE_3__.useState)(4);
    const width = (0,_hooks_useWindowSize__WEBPACK_IMPORTED_MODULE_4__/* ["default"] */ .Z)();
    (0,react__WEBPACK_IMPORTED_MODULE_3__.useEffect)(()=>{
        if (width < 500) setVisibleSlides(2);
        else if (width < 650) setVisibleSlides(2);
        else if (width < 1050) setVisibleSlides(3);
        else setVisibleSlides(4);
    }, [
        width
    ]);
    let left_button_style;
    let right_button_style;
    if (width < 650) {
        left_button_style = {
            marginLeft: '-20px'
        };
        right_button_style = {
            marginRight: '-20px'
        };
    }
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        children: !data && (data === null || data === void 0 ? void 0 : data.length) < 1 && typeof data !== 'object' ? "" : data === null || data === void 0 ? void 0 : data.map((category)=>{
            var ref1;
            return (category === null || category === void 0 ? void 0 : (ref1 = category.flowers) === null || ref1 === void 0 ? void 0 : ref1.length) !== 0 ? /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_intersection_observer__WEBPACK_IMPORTED_MODULE_5__.InView, {
                triggerOnce: true,
                children: (props)=>{
                    var ref;
                    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        ref: props.ref,
                        className: "mt-0 mt-sm-0 mt-md-0 mt-lg-0 mt-xl-0 ",
                        id: category === null || category === void 0 ? void 0 : category.keyword,
                        children: props.inView && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_CategorySectionCreator__WEBPACK_IMPORTED_MODULE_1__["default"], {
                            iconName: "light",
                            title: category.title,
                            seeMoreLink: `/${category === null || category === void 0 ? void 0 : category.keyword}`,
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(Box, {
                                mb: "0rem",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_carousel_Carousel__WEBPACK_IMPORTED_MODULE_2__["default"], {
                                    leftButtonStyle: left_button_style,
                                    rightButtonStyle: right_button_style,
                                    totalSlides: category.flowers.length,
                                    visibleSlides: visibleSlides,
                                    children: category === null || category === void 0 ? void 0 : (ref = category.flowers) === null || ref === void 0 ? void 0 : ref.map((item)=>{
                                        return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_intersection_observer__WEBPACK_IMPORTED_MODULE_5__.InView, {
                                            triggerOnce: true,
                                            children: (props2)=>{
                                                return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                    ref: props2.ref,
                                                    className: "index_product_container",
                                                    "data-aos-delay": width < 650 ? 1000 : 0,
                                                    children: props2.inView && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(Box, {
                                                        py: "0rem",
                                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(ProductCard1, {
                                                            id: item === null || item === void 0 ? void 0 : item.keyword,
                                                            category_keyword: item === null || item === void 0 ? void 0 : item.categoryKeyword,
                                                            imgUrl: item === null || item === void 0 ? void 0 : item.image,
                                                            title: item === null || item === void 0 ? void 0 : item.name,
                                                            rating: item === null || item === void 0 ? void 0 : item.rating,
                                                            price: item === null || item === void 0 ? void 0 : item.price,
                                                            is_favourite: item === null || item === void 0 ? void 0 : item.is_favorites,
                                                            shopName: item === null || item === void 0 ? void 0 : item.shopName,
                                                            shopKeyword: item === null || item === void 0 ? void 0 : item.shopKeyword
                                                        }, item === null || item === void 0 ? void 0 : item.keyword)
                                                    })
                                                }, item === null || item === void 0 ? void 0 : item.keyword));
                                            }
                                        }));
                                    })
                                })
                            })
                        })
                    }));
                }
            }, category === null || category === void 0 ? void 0 : category.id) : "";
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Section2);


/***/ }),

/***/ 9758:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "J": () => (/* binding */ deviceSize),
/* harmony export */   "P": () => (/* binding */ layoutConstant)
/* harmony export */ });
const deviceSize = {
    xs: 425,
    sm: 768,
    md: 1024,
    lg: 1440
};
const layoutConstant = {
    grocerySidenavWidth: "280px",
    containerWidth: "1200px",
    mobileNavHeight: "64px",
    headerHeight: "80px",
    mobileHeaderHeight: "64px"
};


/***/ })

};
;