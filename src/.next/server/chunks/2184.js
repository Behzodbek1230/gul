"use strict";
exports.id = 2184;
exports.ids = [2184,4875,5359];
exports.modules = {

/***/ 4875:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5834);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_system__WEBPACK_IMPORTED_MODULE_1__);


const Box = styled_components__WEBPACK_IMPORTED_MODULE_0___default().div(({ shadow , cursor , transition , theme  })=>({
        boxShadow: theme.shadows[shadow],
        cursor,
        transition
    })
, (0,styled_system__WEBPACK_IMPORTED_MODULE_1__.compose)(styled_system__WEBPACK_IMPORTED_MODULE_1__.layout, styled_system__WEBPACK_IMPORTED_MODULE_1__.space, styled_system__WEBPACK_IMPORTED_MODULE_1__.color, styled_system__WEBPACK_IMPORTED_MODULE_1__.position, styled_system__WEBPACK_IMPORTED_MODULE_1__.flexbox, styled_system__WEBPACK_IMPORTED_MODULE_1__.flex, styled_system__WEBPACK_IMPORTED_MODULE_1__.border, styled_system__WEBPACK_IMPORTED_MODULE_1__.typography));
Box.defaultProps = {
    shadow: 0,
    cursor: "unset"
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Box);


/***/ }),

/***/ 5359:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5834);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_system__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Box__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4875);



const FlexBox = styled_components__WEBPACK_IMPORTED_MODULE_0___default()(_Box__WEBPACK_IMPORTED_MODULE_2__["default"])`
  display: flex;
  flex-direction: row;

  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.layout}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.color}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.flexbox}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.space}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.border}
`;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (FlexBox);


/***/ }),

/***/ 2184:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_lazy_load_image_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(9252);
/* harmony import */ var react_lazy_load_image_component__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_lazy_load_image_component__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(1853);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(2167);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var next_dynamic__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(5218);
/* harmony import */ var _FlexBox__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(5359);







const Card = (0,next_dynamic__WEBPACK_IMPORTED_MODULE_5__["default"])(()=>Promise.all(/* import() */[__webpack_require__.e(1972), __webpack_require__.e(4660)]).then(__webpack_require__.bind(__webpack_require__, 4660))
, {
    ssr: false
});
const Container = (0,next_dynamic__WEBPACK_IMPORTED_MODULE_5__["default"])(()=>__webpack_require__.e(/* import() */ 9221).then(__webpack_require__.bind(__webpack_require__, 9221))
, {
    ssr: false
});
const Grid = (0,next_dynamic__WEBPACK_IMPORTED_MODULE_5__["default"])(()=>__webpack_require__.e(/* import() */ 8167).then(__webpack_require__.bind(__webpack_require__, 8167))
);
const H4 = (0,next_dynamic__WEBPACK_IMPORTED_MODULE_5__["default"])(()=>__webpack_require__.e(/* import() */ 9374).then(__webpack_require__.bind(__webpack_require__, 9374)).then((res)=>res.H4
    )
, {
    ssr: false
});
const SemiSpan = (0,next_dynamic__WEBPACK_IMPORTED_MODULE_5__["default"])(()=>__webpack_require__.e(/* import() */ 9374).then(__webpack_require__.bind(__webpack_require__, 9374)).then((res)=>res.SemiSpan
    )
, {
    ssr: false
});
const Section12 = ()=>{
    let { 0: serviceList , 1: setservice  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)([]);
    let lang = (0,next_router__WEBPACK_IMPORTED_MODULE_3__.useRouter)().locale;
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        axios__WEBPACK_IMPORTED_MODULE_4___default().get(`/references/services/${lang}`).then((res)=>{
            setservice(res.data);
        }).catch(()=>null
        );
    }, [
        lang
    ]);
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(Container, {
        "data-aos": "zoom-in",
        mb: "70px",
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(Grid, {
            container: true,
            className: "p-0 m-0 justify-content-between",
            spacing: 2,
            children: serviceList === null || serviceList === void 0 ? void 0 : serviceList.map((item, ind)=>{
                return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(Grid, {
                    item: true,
                    lg: 3,
                    sm: 6,
                    xs: 6,
                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        "data-aos": ind % 2 == 0 ? 'fade-right' : "fade-left",
                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_FlexBox__WEBPACK_IMPORTED_MODULE_6__["default"], {
                            as: Card,
                            flexDirection: "column",
                            alignItems: "center",
                            p: "0rem",
                            className: "py-3",
                            height: "170px",
                            borderRadius: 8,
                            boxShadow: "border",
                            hoverEffect: true,
                            children: [
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_FlexBox__WEBPACK_IMPORTED_MODULE_6__["default"], {
                                    justifyContent: "center",
                                    alignItems: "center",
                                    borderRadius: "300px",
                                    // bg="gray.200"
                                    size: "64px",
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_lazy_load_image_component__WEBPACK_IMPORTED_MODULE_2__.LazyLoadImage, {
                                        src: item.icon_name,
                                        alt: item === null || item === void 0 ? void 0 : item.title,
                                        width: 28,
                                        height: 28,
                                        visibleByDefault: false,
                                        threshold: -10,
                                        effect: "blur",
                                        placeholderSrc: ""
                                    })
                                }),
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(H4, {
                                    mt: "10px",
                                    mb: "15px",
                                    fontSize: "14px",
                                    textAlign: "center",
                                    height: "20px",
                                    children: item.title
                                }),
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(SemiSpan, {
                                    className: "p-0 m-0",
                                    fontSize: "12px",
                                    textAlign: "center",
                                    children: item.description
                                })
                            ]
                        })
                    })
                }, ind));
            })
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Section12);


/***/ })

};
;