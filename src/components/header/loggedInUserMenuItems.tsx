import Icon from "../../components/icon/Icon"
import axios from "axios"
import { FC } from "react"
import { FormattedMessage } from "react-intl"
import Cookies from 'js-cookie'
import { useDispatch, useSelector } from "react-redux"
import logout from "../../../Redux/Actions/LogoutAction"
import { fetch_user_info } from "../../../Redux/Actions/Action"
import get_cart_products from "../../../Redux/Actions/get_cart_products"
import { useRouter } from "next/router"
import { useAppContext } from "../../contexts/app/AppContext"
import { MenuList } from "@material-ui/core"
import { MenuItem } from "@material-ui/core"
interface LoggedInUserMenuList{
    shop2:any,
    setOpen2:any,
    setloggedin:any
}

let LoggedInUserMenuList:FC<LoggedInUserMenuList>= ({
    shop2,
    setOpen2,
    setloggedin
}) =>{
    let user = useSelector((state:any)=>state.token.user)
    let router = useRouter()
    let lang = router.locale
    let dispatch2 = useDispatch()
    const { state, dispatch } = useAppContext();
    const { cartList } = state.cart;
    function handleListKeyDown(event) {
        if (event.key === 'Tab') {
          event.preventDefault();
          setOpen2(false);
        }
    }

    const handleclick = () =>{
        axios({
            method:"POST",
            url:`/login/logout`
        })
            .then(()=>{
                    Cookies.remove("isLoggedIn")
                    Cookies.remove("token")
                    dispatch2(logout())
                    dispatch2(fetch_user_info({}))
                    //--------------------- Clearing data from carts---------------------------
                    handleremovecart()
                    handleremovecart()
                    setloggedin("false")
                    if(router.pathname.startsWith("/profile") ||router.pathname.startsWith("/vendor") || router.pathname.startsWith("/wish-list")){
                      router.push('/')
                    }
            })
            .catch(()=>{
                return  null;
            })
  
  
    }


    const handleCartAmountChange2 = (amount,product) =>  {
        dispatch({
            type: "CHANGE_CART_AMOUNT",
            payload: {
                ...product,
                qty: amount,
            },
        });
    }


    const handleremovecart = () =>{
        try{
            cartList.forEach((value)=>{handleCartAmountChange2(0,value)})
        }
        catch{
            return;
        }
        let currency_text = typeof Cookies.get("currency_id") === "undefined" ? "" : `?currency=${Cookies.get("currency_id")}`
        if(Cookies.get("isLoggedIn") === "true"){
            axios(`/orders/basket-list/${lang}${currency_text}`)
                .then(response =>{
                    dispatch2(get_cart_products(response.data))
                })
                .catch(()=>null)
        }
    }

    
    


    return (
        <MenuList
            id="menu-list-grow"
            onKeyDown={handleListKeyDown}
        >
            <MenuItem
                style={{
                    padding:"0px",
                    margin:"0px"
                }}
            >
                <div className="fs-small w-100 padding-20">
                    <label
                        className="fs-large"
                    >
                        <FormattedMessage
                            id="hi"
                            defaultMessage="Salom"
                        />  {user?.data?.fullname
                                ?
                                        user.data.fullname
                                :
                                        user?.data?.phone
                                }
                    </label><br/>
                    <FormattedMessage
                        id="balance"
                        defaultMessage="Balans"
                    /> {user?.data?.balance
                        ?
                            user?.data?.balance
                        :   0
                    } <FormattedMessage id="sum" />
                </div>
            </MenuItem>
            <MenuItem
                onClick={()=>router.push("/profile/edit")}
            >
                <Icon
                    variant="small"
                    defaultcolor="currentColor"
                    mr="10px"
                >
                    user-profile
                </Icon>
                    <FormattedMessage
                        id="my_profile"
                        defaultMessage="Mening profilim"
                    />
            </MenuItem>
            <MenuItem
                onClick={()=>router.push(shop2 !== null
                            ?
                                "/vendor/products/page/1"
                            :
                                "/vendor/create"
                        )}
            >
                <Icon
                    variant="small"
                    defaultcolor="currentColor"
                    mr="10px"
                >
                    crown
                </Icon>
                {shop2 ?
                    <FormattedMessage
                        id="shop"
                        defaultMessage="Mening dokonlarim"
                    />
                :
                    <FormattedMessage
                        id="create_shop"
                        defaultMessage="Magazin Yaratish"
                    />
                }
            </MenuItem>
            <MenuItem
                onClick={handleclick}
            >
            <Icon
                variant="small"
                defaultcolor="currentColor"
                mr="10px"
            >
                logout
            </Icon>
                <FormattedMessage
                    id="logout"
                    defaultMessage="Chiqish"
                />
            </MenuItem>
        </MenuList>
    )
}

export default LoggedInUserMenuList
