const Footer = dynamic(()=>import('../../components/footer/Footer'))
const MobileNavigationBar = dynamic(()=>import('../../components/mobile-navigation/MobileNavigationBar'),{ssr:false})
import Header from "../../components/header/Header";
import Sticky from "../../components/sticky/Sticky";
import Topbar from "../../components/topbar/Topbar";
import Head from "next/head";
import React, {useEffect, useState,useCallback} from "react";
import StyledAppLayout from "./AppLayoutStyle";
import Navbar from "../../components/navbar/Navbar";
import {Helmet} from "react-helmet"
import dynamic from "next/dynamic";
type Props = {
  title?: string;
  navbar?: React.ReactChild;
  description?:string;
  keyword?:string;
  seoRelated?:any
};

const AppLayout: React.FC<Props> = ({
  children,
  navbar,
  title,
}) => {
    let [y,setY] = useState(0)
    let [up,setup] = useState(true)
    const handleNavigation = useCallback(
    (e) => {
      const window = e.currentTarget;
      if (y > window.scrollY) {
        setup(true)
      } else if (y < window.scrollY) {
        setup(false)
      }
      setY(window.scrollY);
    },
    [y]
  );
    useEffect(()=>{
      setY(window.scrollY);
      window.addEventListener("scroll", handleNavigation);
      return ()=>{
        window.removeEventListener("scroll", handleNavigation);
    }},[handleNavigation])
    return(<StyledAppLayout>
        <Helmet>
            <script type="application/ld+json">
                {JSON.stringify({
                    "@context": "http://schema.org/",
                    "@type": "Organization",
                    "name": "Dana.uz",
                    "logo": "https://api.dana.uz/storage/about_company/1servicesu8KfDpsNUReDaTeebv4i.png",
                    "url": "https://dana.uz",
                    "address": {
                        "@type": "PostalAddress",
                        "streetAddress": "Chilanzar district, 1st quarter, 4",
                        "addressLocality": "Tashkent",
                        "addressRegion": "Tashkent",
                        "postalCode": "100000",
                        "addressCountry": "Uzbekistan"
                    },
                    "sameAs": ["https://facebook.com/danauzbekistan", "https://instagram.com/Dana_uzbekistan/", "https://www.pinterest.com/dana_uzbekistan/", "https://www.youtube.com/c/DanaUzbekistan/"]
                })}
            </script>
        </Helmet>
        <Head>
            <title>{title}</title>
            <meta charSet="utf-8"/>
            <meta name="viewport" content="initial-scale=1.0, width=device-width"/>
        </Head>
        <Sticky fixedOn={0}>
            <>
                <Topbar />
                <div className="index_header_container_style2">
                  <Header />
                </div>
               
                {up ? <Navbar navListOpen={false} /> : ""}
            </>
        </Sticky>

        {navbar && <div className="section-after-sticky">{navbar}</div>}
        {!navbar ? (
            <div className="section-after-sticky">{children}</div>
        ) : (
            children
        )}
       <MobileNavigationBar/>
       <Footer/>
    </StyledAppLayout>
    )
};

export default AppLayout;
