"use strict";
(() => {
var exports = {};
exports.id = 9415;
exports.ids = [9415];
exports.modules = {

/***/ 8890:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__),
/* harmony export */   "getStaticPaths": () => (/* binding */ getStaticPaths),
/* harmony export */   "getStaticProps": () => (/* binding */ getStaticProps)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_buttons_Button__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2389);
/* harmony import */ var _components_Card__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4660);
/* harmony import */ var _components_DropZone__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(1005);
/* harmony import */ var _components_grid_Grid__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(8167);
/* harmony import */ var _components_layout_DashboardPageHeader2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(1251);
/* harmony import */ var _components_layout_VendorDashboardLayout__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(9330);
/* harmony import */ var _material_ui_icons_HighlightOff__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(9175);
/* harmony import */ var _material_ui_icons_HighlightOff__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_HighlightOff__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(9894);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(2167);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(6022);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(1853);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(358);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var _hooks_useWindowSize__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(12);
/* harmony import */ var react_intl__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(3126);
/* harmony import */ var react_intl__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(react_intl__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var _components_Variables__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(5153);
/* harmony import */ var next_seo__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(6641);
/* harmony import */ var next_seo__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(next_seo__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var _components_CurrencyFormatter__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(1016);
/* harmony import */ var _material_ui_icons__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(2105);
/* harmony import */ var _material_ui_icons__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons__WEBPACK_IMPORTED_MODULE_17__);




















const EditProduct = ({ currency2 , material2 , units2 , one  })=>{
    var ref4, ref1, ref2, ref3;
    let intl = (0,react_intl__WEBPACK_IMPORTED_MODULE_15__.useIntl)();
    let router = (0,next_router__WEBPACK_IMPORTED_MODULE_12__.useRouter)();
    let lang = router.locale;
    // -------States----------
    const { 0: name , 1: setname  } = (0,react__WEBPACK_IMPORTED_MODULE_9__.useState)(one.data.name);
    const { 0: desc , 1: setdesc  } = (0,react__WEBPACK_IMPORTED_MODULE_9__.useState)(one.data.description);
    const { 0: cat_id , 1: setcat_id  } = (0,react__WEBPACK_IMPORTED_MODULE_9__.useState)(one.data.categoryId);
    const { 0: cat_name , 1: setcat  } = (0,react__WEBPACK_IMPORTED_MODULE_9__.useState)(one.data.categoryName);
    const { 0: cat_er , 1: setcat_er  } = (0,react__WEBPACK_IMPORTED_MODULE_9__.useState)("");
    const { 0: price , 1: setprice  } = (0,react__WEBPACK_IMPORTED_MODULE_9__.useState)(one === null || one === void 0 ? void 0 : (ref4 = one.data) === null || ref4 === void 0 ? void 0 : ref4.priceFlowers);
    const { 0: price_c , 1: setprice_c  } = (0,react__WEBPACK_IMPORTED_MODULE_9__.useState)(one === null || one === void 0 ? void 0 : (ref1 = one.data) === null || ref1 === void 0 ? void 0 : ref1.currency_id);
    const { 0: price_er , 1: setprice_er  } = (0,react__WEBPACK_IMPORTED_MODULE_9__.useState)("");
    const { 0: width , 1: setwidth  } = (0,react__WEBPACK_IMPORTED_MODULE_9__.useState)(one.data.width);
    const { 0: width_unit , 1: setwidth_unit  } = (0,react__WEBPACK_IMPORTED_MODULE_9__.useState)(one.data.width_unit_id);
    const { 0: width_er , 1: setwidth_er  } = (0,react__WEBPACK_IMPORTED_MODULE_9__.useState)("");
    const { 0: height , 1: setheight  } = (0,react__WEBPACK_IMPORTED_MODULE_9__.useState)(one.data.height);
    const { 0: height_unit , 1: setheight_unit  } = (0,react__WEBPACK_IMPORTED_MODULE_9__.useState)(one.data.height_unit_id);
    const { 0: height_er , 1: setheight_er  } = (0,react__WEBPACK_IMPORTED_MODULE_9__.useState)("");
    const { 0: count , 1: setcount  } = (0,react__WEBPACK_IMPORTED_MODULE_9__.useState)(one.data.count);
    const { 0: booked , 1: setbooked  } = (0,react__WEBPACK_IMPORTED_MODULE_9__.useState)(one.data.booked);
    const { 0: discount , 1: setdiscount  } = (0,react__WEBPACK_IMPORTED_MODULE_9__.useState)(one.data.discount || 0);
    const { 0: photo , 1: setphoto  } = (0,react__WEBPACK_IMPORTED_MODULE_9__.useState)((one === null || one === void 0 ? void 0 : (ref2 = one.data) === null || ref2 === void 0 ? void 0 : ref2.image) ? one === null || one === void 0 ? void 0 : (ref3 = one.data) === null || ref3 === void 0 ? void 0 : ref3.image : []);
    let { 0: photo_err , 1: setphoto_err  } = (0,react__WEBPACK_IMPORTED_MODULE_9__.useState)("");
    const { 0: structure , 1: setstructure  } = (0,react__WEBPACK_IMPORTED_MODULE_9__.useState)(one.data.structure);
    let width2 = (0,_hooks_useWindowSize__WEBPACK_IMPORTED_MODULE_14__/* ["default"] */ .Z)();
    let { 0: currency , 1: setcurrency  } = (0,react__WEBPACK_IMPORTED_MODULE_9__.useState)(currency2);
    let { 0: material , 1: setmaterial  } = (0,react__WEBPACK_IMPORTED_MODULE_9__.useState)(material2);
    let { 0: units , 1: setunits  } = (0,react__WEBPACK_IMPORTED_MODULE_9__.useState)(units2);
    const { 0: delivery_time , 1: setdelivery  } = (0,react__WEBPACK_IMPORTED_MODULE_9__.useState)(0);
    const handleCategoryClick = (id, title)=>{
        setcat_id(id), setcat(title);
    };
    (0,react__WEBPACK_IMPORTED_MODULE_9__.useEffect)(()=>{
        axios__WEBPACK_IMPORTED_MODULE_10___default()(`/references/currency-flowers/${lang}`).then((res)=>{
            setcurrency(res.data);
        }).catch(()=>null
        );
        axios__WEBPACK_IMPORTED_MODULE_10___default()(`/flowers/material/list/${lang}`).then((res)=>{
            setmaterial(res.data);
        }).catch(()=>null
        );
        axios__WEBPACK_IMPORTED_MODULE_10___default()(`/flowers/units/list/${lang}`).then((res)=>{
            setunits(res.data);
        }).catch(()=>null
        );
    }, [
        lang
    ]);
    // -------States----------
    const category_dropdown = (items)=>{
        return items.map((item, ind)=>item.children.length !== 0 ? /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                className: "mb-2",
                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_13__.DropdownButton, {
                    as: react_bootstrap__WEBPACK_IMPORTED_MODULE_13__.ButtonGroup,
                    id: `dropdown-button-drop-bottom`,
                    drop: "down",
                    variant: "light",
                    title: item.title,
                    children: category_dropdown(item.children)
                }, "end")
            }, ind) : /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_13__.Dropdown.Item, {
                onClick: ()=>handleCategoryClick(item.id, item.title)
                ,
                eventKey: item.id,
                children: item.title
            })
        );
    };
    const updatematerial2 = (e, id)=>{
        e.preventDefault();
        let array = [
            ...structure
        ];
        array[id].material_id = e.target.value;
        setstructure(array);
    };
    const updatecount2 = (t, id)=>{
        t.preventDefault();
        let array = [
            ...structure
        ];
        array[id].count = t.target.value;
        setstructure(array);
    };
    const updateunit2 = (x, id)=>{
        x.preventDefault();
        let array = [
            ...structure
        ];
        array[id].unit_id = x.target.value;
        setstructure(array);
    };
    const addstructure = ()=>{
        let array = [
            ...structure
        ];
        array.push({
            material_id: undefined,
            count: 0,
            unit_id: undefined
        });
        setstructure(array);
    };
    const handleremove = (id)=>{
        let array = [
            ...structure
        ];
        array.splice(id - 1, 1);
        setstructure(array);
    };
    const category = (0,react_redux__WEBPACK_IMPORTED_MODULE_11__.useSelector)((state)=>state.new.category
    );
    const handlePhotoAdd = (photos)=>{
        setphoto((prevState)=>[
                ...prevState,
                ...photos
            ]
        );
    };
    const handleRemoveImage = (ind)=>{
        let array = [
            ...photo
        ];
        array.splice(ind, 1);
        setphoto(array);
    };
    const handlePriceChange = (t)=>{
        if (t.target.value !== "") setprice(parseInt(t.target.value.replace(/\s+/g, "")));
        else setprice(0);
    };
    const handleFormSubmit = async (values)=>{
        var ref;
        values.preventDefault();
        let data = new FormData();
        data.append('name', name);
        data.append('description', desc);
        data.append('cat_id', cat_id);
        data.append('shop_id', one === null || one === void 0 ? void 0 : (ref = one.data) === null || ref === void 0 ? void 0 : ref.shop_id);
        data.append('price', price.toString());
        data.append('currency_id', price_c);
        data.append('width', width);
        data.append('width_unit_id', width_unit);
        data.append('height', height);
        data.append('height_unit_id', height_unit);
        data.append('count', count || 0);
        data.append('booked', booked || 0);
        data.append('discount', discount || 0);
        data.append('delivery_time', delivery_time.toString());
        photo.forEach(function(value, index) {
            if (typeof value !== "string") {
                data.append(`photo[${index}]`, value);
            }
        });
        structure.forEach(function(value, index) {
            data.append(`structure[${index}][material_id]`, value.material_id);
            data.append(`structure[${index}][count]`, value.count.toString());
            data.append(`structure[${index}][unit_id]`, value.unit_id);
        });
        setcat_er("");
        setprice_er("");
        setwidth_er("");
        setheight_er("");
        let r = router.asPath.split("/")[3];
        if (cat_id === undefined) {
            setcat_er(intl.formatMessage({
                id: "choose_category"
            }));
            window.scrollTo({
                top: 100
            });
        } else if (price_c === undefined) {
            setprice_er(intl.formatMessage({
                id: "currency_unit"
            }));
        } else if (width_unit === undefined) {
            setwidth_er(intl.formatMessage({
                id: "units_error"
            }));
        } else if (height_unit === undefined) {
            setheight_er(intl.formatMessage({
                id: "units_error"
            }));
        } else if (photo === [] || photo.length === 0 || typeof photo === "undefined") {
            window.scrollTo({
                top: 90
            });
            setphoto_err(intl.formatMessage({
                id: "select_image"
            }));
        } else {
            axios__WEBPACK_IMPORTED_MODULE_10___default()({
                method: "POST",
                url: `/flowers/update/${r}/${lang}`,
                data: data
            }).then(()=>{
                router.push("/vendor/products/page/1");
            }).catch(()=>{
                return null;
            });
        }
    };
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_components_layout_VendorDashboardLayout__WEBPACK_IMPORTED_MODULE_6__/* ["default"] */ .Z, {
        children: [
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_seo__WEBPACK_IMPORTED_MODULE_16__.NextSeo, {
                title: intl.formatMessage({
                    id: "edit_product"
                })
            }),
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_layout_DashboardPageHeader2__WEBPACK_IMPORTED_MODULE_5__/* ["default"] */ .Z, {
                        title: intl.formatMessage({
                            id: "edit_product",
                            defaultMessage: "Mahsulotni tahrirlash"
                        }),
                        iconName: "delivery-box",
                        button: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_link__WEBPACK_IMPORTED_MODULE_8__["default"], {
                            href: "/vendor/products/page/1",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_buttons_Button__WEBPACK_IMPORTED_MODULE_1__/* ["default"] */ .Z, {
                                color: "primary",
                                bg: "primary.light",
                                px: "2rem",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_intl__WEBPACK_IMPORTED_MODULE_15__.FormattedMessage, {
                                    id: "back_to_product_list",
                                    defaultMessage: "Mahsulot listiga qaytish"
                                })
                            })
                        })
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_Card__WEBPACK_IMPORTED_MODULE_2__["default"], {
                        p: "30px",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("form", {
                            onSubmit: handleFormSubmit,
                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_components_grid_Grid__WEBPACK_IMPORTED_MODULE_4__["default"], {
                                container: true,
                                spacing: 6,
                                children: [
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_components_grid_Grid__WEBPACK_IMPORTED_MODULE_4__["default"], {
                                        item: true,
                                        sm: 12,
                                        xs: 12,
                                        md: 6,
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("label", {
                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_intl__WEBPACK_IMPORTED_MODULE_15__.FormattedMessage, {
                                                    id: "Product Name",
                                                    defaultMessage: "Nomi"
                                                })
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                type: "text",
                                                required: true,
                                                value: name,
                                                onChange: (r)=>setname(r.target.value)
                                                ,
                                                className: "form-control"
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_components_grid_Grid__WEBPACK_IMPORTED_MODULE_4__["default"], {
                                        item: true,
                                        sm: 12,
                                        xs: 12,
                                        md: 6,
                                        children: [
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("label", {
                                                htmlFor: "sel6",
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_intl__WEBPACK_IMPORTED_MODULE_15__.FormattedMessage, {
                                                        id: "mobile_navigation_category",
                                                        defaultMessage: "Kategoriyalar"
                                                    }),
                                                    ":"
                                                ]
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_13__.DropdownButton, {
                                                style: {
                                                    border: "1px solid #c7cbd1",
                                                    borderRadius: "5px",
                                                    width: "100%"
                                                },
                                                border: "secondary",
                                                as: react_bootstrap__WEBPACK_IMPORTED_MODULE_13__.ButtonGroup,
                                                id: `dropdown-button-drop-down`,
                                                drop: "down",
                                                variant: "light",
                                                title: cat_name === "" ? "PLease choose one of category for your product" : cat_name,
                                                children: category_dropdown(category)
                                            }, "end"),
                                            cat_er !== "" ? /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                className: "text-danger",
                                                children: cat_er
                                            }) : ""
                                        ]
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_grid_Grid__WEBPACK_IMPORTED_MODULE_4__["default"], {
                                        item: true,
                                        xs: 12,
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_DropZone__WEBPACK_IMPORTED_MODULE_3__/* ["default"] */ .Z, {
                                            onChange: (files)=>{
                                                handlePhotoAdd(files);
                                            }
                                        })
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        children: photo_err !== "" ? /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "col-12 text-danger",
                                            children: photo_err
                                        }) : ""
                                    }),
                                    photo.map((img, ind)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_grid_Grid__WEBPACK_IMPORTED_MODULE_4__["default"], {
                                            item: true,
                                            sm: 3,
                                            xs: 6,
                                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                style: {
                                                    position: "relative"
                                                },
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                        src: typeof img === "string" ? img : URL.createObjectURL(img),
                                                        alt: name,
                                                        width: "50px",
                                                        height: "50px"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        style: {
                                                            position: "absolute",
                                                            zIndex: 10,
                                                            top: "-10px",
                                                            left: "38px"
                                                        },
                                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((_material_ui_icons_HighlightOff__WEBPACK_IMPORTED_MODULE_7___default()), {
                                                            className: "text-danger",
                                                            onClick: ()=>handleRemoveImage(ind)
                                                        })
                                                    })
                                                ]
                                            })
                                        }, ind)
                                    ),
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_components_grid_Grid__WEBPACK_IMPORTED_MODULE_4__["default"], {
                                        item: true,
                                        xs: 12,
                                        md: 12,
                                        sm: 12,
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("label", {
                                                className: "mt-1 mb-1",
                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_intl__WEBPACK_IMPORTED_MODULE_15__.FormattedMessage, {
                                                    id: "description",
                                                    defaultMessage: "Tavsif"
                                                })
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("textarea", {
                                                onChange: (e)=>setdesc(e.target.value)
                                                ,
                                                cols: 20,
                                                rows: 7,
                                                required: true,
                                                value: desc,
                                                className: "form-control"
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_components_grid_Grid__WEBPACK_IMPORTED_MODULE_4__["default"], {
                                        item: true,
                                        sm: 12,
                                        xs: 12,
                                        md: 6,
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("label", {
                                                className: "mt-1 mb-1",
                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_intl__WEBPACK_IMPORTED_MODULE_15__.FormattedMessage, {
                                                    id: "Amount",
                                                    defaultMessage: "Soni"
                                                })
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                type: "number",
                                                className: "form-control form-control-color-danger",
                                                value: count,
                                                placeholder: intl.formatMessage({
                                                    id: "Amount"
                                                }),
                                                onChange: (e)=>setcount(e.target.value)
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_components_grid_Grid__WEBPACK_IMPORTED_MODULE_4__["default"], {
                                        item: true,
                                        sm: 8,
                                        xs: 9,
                                        md: 3,
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("label", {
                                                className: "mt-1 mb-1",
                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_intl__WEBPACK_IMPORTED_MODULE_15__.FormattedMessage, {
                                                    id: "Regular Price",
                                                    defaultMessage: "Narxi"
                                                })
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                value: typeof price !== "undefined" ? (0,_components_CurrencyFormatter__WEBPACK_IMPORTED_MODULE_18__/* ["default"] */ .Z)(price) : undefined,
                                                type: "text",
                                                required: true,
                                                className: "form-control",
                                                placeholder: intl.formatMessage({
                                                    id: "Regular Price",
                                                    defaultMessage: "Narx"
                                                }),
                                                onChange: (t)=>handlePriceChange(t)
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_components_grid_Grid__WEBPACK_IMPORTED_MODULE_4__["default"], {
                                        item: true,
                                        sm: 4,
                                        xs: 3,
                                        md: 3,
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("label", {
                                                children: intl.formatMessage({
                                                    id: 'currency',
                                                    defaultMessage: "Pul birligi"
                                                })
                                            }),
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("select", {
                                                required: true,
                                                onChange: (e)=>setprice_c(e.target.value)
                                                ,
                                                className: "form-control",
                                                value: price_c,
                                                id: "sel2",
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("option", {
                                                        value: "",
                                                        hidden: true,
                                                        children: intl.formatMessage({
                                                            id: 'currency',
                                                            defaultMessage: "Pul birligi"
                                                        })
                                                    }),
                                                    currency.map((curr)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("option", {
                                                            value: curr.id,
                                                            children: curr.code
                                                        }, curr.id)
                                                    )
                                                ]
                                            }),
                                            price_er !== "" ? /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                className: "text-danger",
                                                children: price_er
                                            }) : ""
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_components_grid_Grid__WEBPACK_IMPORTED_MODULE_4__["default"], {
                                        item: true,
                                        sm: 12,
                                        xs: 12,
                                        md: 12,
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("label", {
                                                className: "mt-1 mb-1",
                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_intl__WEBPACK_IMPORTED_MODULE_15__.FormattedMessage, {
                                                    id: "Discount(without percentage)",
                                                    defaultMessage: "Chegirma(foiz yozish shart emas)"
                                                })
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                type: "number",
                                                onChange: (r)=>setdiscount(r.target.value)
                                                ,
                                                placeholder: intl.formatMessage({
                                                    id: "Discount(without percentage)"
                                                }),
                                                className: "form-control form-control-color-danger",
                                                value: discount
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_components_grid_Grid__WEBPACK_IMPORTED_MODULE_4__["default"], {
                                        item: true,
                                        sm: 6,
                                        xs: 6,
                                        md: 3,
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("label", {
                                                className: "mt-1 mb-1",
                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_intl__WEBPACK_IMPORTED_MODULE_15__.FormattedMessage, {
                                                    id: "width",
                                                    defaultMessage: "Eni"
                                                })
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                type: "number",
                                                placeholder: intl.formatMessage({
                                                    id: "width"
                                                }),
                                                className: "form-control form-control-color-danger",
                                                required: true,
                                                value: width,
                                                onChange: (y)=>setwidth(y.target.value)
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_components_grid_Grid__WEBPACK_IMPORTED_MODULE_4__["default"], {
                                        item: true,
                                        sm: 6,
                                        xs: 6,
                                        md: 3,
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("label", {
                                                children: intl.formatMessage({
                                                    id: "width_unit"
                                                })
                                            }),
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("select", {
                                                required: true,
                                                onChange: (r)=>setwidth_unit(r.target.value)
                                                ,
                                                className: "form-control",
                                                id: "sel3",
                                                value: width_unit,
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("option", {
                                                        value: "",
                                                        hidden: true,
                                                        children: intl.formatMessage({
                                                            id: "width_unit"
                                                        })
                                                    }),
                                                    units.map((categor)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("option", {
                                                            value: categor.id,
                                                            children: categor.name
                                                        }, categor.id)
                                                    )
                                                ]
                                            }),
                                            width_er !== "" ? /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                className: "text-danger",
                                                children: width_er
                                            }) : ""
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_components_grid_Grid__WEBPACK_IMPORTED_MODULE_4__["default"], {
                                        item: true,
                                        sm: 6,
                                        xs: 6,
                                        md: 3,
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("label", {
                                                className: "mt-1 mb-1",
                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_intl__WEBPACK_IMPORTED_MODULE_15__.FormattedMessage, {
                                                    id: "height",
                                                    defaultMessage: "Bo'yi"
                                                })
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                type: "number",
                                                placeholder: intl.formatMessage({
                                                    id: "height"
                                                }),
                                                className: "form-control form-control-color-danger",
                                                required: true,
                                                value: height,
                                                onChange: (t)=>setheight(t.target.value)
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_components_grid_Grid__WEBPACK_IMPORTED_MODULE_4__["default"], {
                                        item: true,
                                        sm: 6,
                                        xs: 6,
                                        md: 3,
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("label", {
                                                children: intl.formatMessage({
                                                    id: "height_unit",
                                                    defaultMessage: "Bo'yi O'lchovi"
                                                })
                                            }),
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("select", {
                                                required: true,
                                                onChange: (y)=>setheight_unit(y.target.value)
                                                ,
                                                className: "form-control",
                                                id: "sel1",
                                                value: height_unit,
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("option", {
                                                        value: "",
                                                        hidden: true,
                                                        children: intl.formatMessage({
                                                            id: "height_unit",
                                                            defaultMessage: "Bo'yi O'lchovi"
                                                        })
                                                    }),
                                                    units.map((categor)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("option", {
                                                            value: categor.id,
                                                            children: categor.name
                                                        }, categor.id)
                                                    )
                                                ]
                                            }),
                                            height_er !== "" ? /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                className: "text-danger",
                                                children: height_er
                                            }) : ""
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_components_grid_Grid__WEBPACK_IMPORTED_MODULE_4__["default"], {
                                        item: true,
                                        sm: 12,
                                        md: 6,
                                        lg: 6,
                                        xl: 6,
                                        xs: 12,
                                        children: [
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("label", {
                                                className: "mt-1 mb-1",
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_intl__WEBPACK_IMPORTED_MODULE_15__.FormattedMessage, {
                                                        id: "delivery_time"
                                                    }),
                                                    "(m)"
                                                ]
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                type: "number",
                                                required: true,
                                                placeholder: intl.formatMessage({
                                                    id: "delivery_time"
                                                }),
                                                className: "form-control form-control-color-danger",
                                                value: delivery_time,
                                                onChange: (e)=>setdelivery(parseInt(e.target.value))
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_components_grid_Grid__WEBPACK_IMPORTED_MODULE_4__["default"], {
                                        item: true,
                                        sm: 12,
                                        md: 6,
                                        lg: 6,
                                        xl: 6,
                                        xs: 12,
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("label", {
                                                className: "mt-1 mb-1",
                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_intl__WEBPACK_IMPORTED_MODULE_15__.FormattedMessage, {
                                                    id: "Booked",
                                                    defaultMessage: "Oldindan band qilib qoyilganlar soni"
                                                })
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                type: "number",
                                                placeholder: intl.formatMessage({
                                                    id: "Booked",
                                                    defaultMessage: "Oldindan band qilib qoyilganlar soni"
                                                }),
                                                className: "form-control form-control-color-danger",
                                                value: booked,
                                                onChange: (e)=>setbooked(e.target.value)
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_grid_Grid__WEBPACK_IMPORTED_MODULE_4__["default"], {
                                        item: true,
                                        sm: 12,
                                        md: 12,
                                        style: {
                                            textAlign: "center",
                                            fontSize: "large"
                                        },
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_intl__WEBPACK_IMPORTED_MODULE_15__.FormattedMessage, {
                                            id: "Consumed_products",
                                            defaultMessage: "Ishlatilgan maxsulotlar"
                                        })
                                    }),
                                    structure.map((struct, index)=>{
                                        return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_grid_Grid__WEBPACK_IMPORTED_MODULE_4__["default"], {
                                                    item: true,
                                                    sm: 12,
                                                    xs: 12,
                                                    md: 12,
                                                    alignContent: "center",
                                                    className: "justify-content-center mb-2",
                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: " border-muted justify-content-center pt-3",
                                                        style: {
                                                            borderBottom: "3px solid lavender"
                                                        }
                                                    })
                                                }, index),
                                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_components_grid_Grid__WEBPACK_IMPORTED_MODULE_4__["default"], {
                                                    item: true,
                                                    sm: 7,
                                                    xs: 7,
                                                    md: 9,
                                                    className: "ml-3",
                                                    style: {
                                                        marginRight: "5%"
                                                    },
                                                    children: [
                                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("label", {
                                                            htmlFor: "sel4",
                                                            children: [
                                                                "Material",
                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_intl__WEBPACK_IMPORTED_MODULE_15__.FormattedMessage, {
                                                                    id: "Used Products",
                                                                    defaultMessage: "Mahsulot"
                                                                })
                                                            ]
                                                        }),
                                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("select", {
                                                            value: struct.material_id,
                                                            className: "vendor_add_product_select_Style form-control",
                                                            required: true,
                                                            onChange: (t)=>updatematerial2(t, index)
                                                            ,
                                                            id: "sel4",
                                                            children: [
                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("option", {
                                                                    children: intl.formatMessage({
                                                                        id: 'material_select'
                                                                    })
                                                                }),
                                                                material.map((categor)=>{
                                                                    let r = 0;
                                                                    structure === null || structure === void 0 ? void 0 : structure.map((structu)=>{
                                                                        return (structu === null || structu === void 0 ? void 0 : structu.material_id) === parseInt(categor === null || categor === void 0 ? void 0 : categor.id) ? r++ : "";
                                                                    });
                                                                    if (r < 1) {
                                                                        return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("option", {
                                                                            value: categor === null || categor === void 0 ? void 0 : categor.id,
                                                                            children: categor === null || categor === void 0 ? void 0 : categor.name
                                                                        }, categor === null || categor === void 0 ? void 0 : categor.id));
                                                                    } else {
                                                                        return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("option", {
                                                                            hidden: true,
                                                                            value: categor === null || categor === void 0 ? void 0 : categor.id,
                                                                            children: categor === null || categor === void 0 ? void 0 : categor.name
                                                                        }, categor === null || categor === void 0 ? void 0 : categor.id));
                                                                    }
                                                                })
                                                            ]
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_components_grid_Grid__WEBPACK_IMPORTED_MODULE_4__["default"], {
                                                    item: true,
                                                    sm: 3,
                                                    xs: 3,
                                                    md: 2,
                                                    className: "float-right justify-content-right align-items-right ml-2 ml-sm-2 ml-md-0 ml-lg-0 ml-xl-0",
                                                    children: [
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("label", {
                                                            className: "mt-1 mb-1",
                                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_intl__WEBPACK_IMPORTED_MODULE_15__.FormattedMessage, {
                                                                id: "Amount",
                                                                defaultMessage: "Soni"
                                                            })
                                                        }),
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                            type: "number",
                                                            className: "form-control form-control-color-danger",
                                                            value: struct.count || "",
                                                            placeholder: intl.formatMessage({
                                                                id: "Amount"
                                                            }),
                                                            onChange: (e)=>updatecount2(e, index)
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_components_grid_Grid__WEBPACK_IMPORTED_MODULE_4__["default"], {
                                                    item: true,
                                                    sm: 8,
                                                    xs: 8,
                                                    md: 9,
                                                    className: "ml-3",
                                                    style: {
                                                        marginRight: "5%"
                                                    },
                                                    children: [
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("label", {
                                                            htmlFor: "sel5",
                                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_intl__WEBPACK_IMPORTED_MODULE_15__.FormattedMessage, {
                                                                id: "unit",
                                                                defaultMessage: "Birligi"
                                                            })
                                                        }),
                                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("select", {
                                                            value: struct.unit_id,
                                                            onChange: (t)=>updateunit2(t, index)
                                                            ,
                                                            className: "form-control vendor_add_product_select_Style",
                                                            id: "sel5",
                                                            children: [
                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("option", {
                                                                    style: {
                                                                        textOverflow: "ellipsis",
                                                                        overflow: "hidden",
                                                                        width: "90%",
                                                                        whiteSpace: "nowrap"
                                                                    },
                                                                    children: intl.formatMessage({
                                                                        id: 'unit_select',
                                                                        defaultMessage: "iltimos birligini tanlang"
                                                                    })
                                                                }),
                                                                units.map((categor)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("option", {
                                                                        value: categor.id,
                                                                        children: categor.name
                                                                    }, categor.id)
                                                                )
                                                            ]
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_grid_Grid__WEBPACK_IMPORTED_MODULE_4__["default"], {
                                                    item: true,
                                                    sm: 2,
                                                    xs: 2,
                                                    md: 2,
                                                    className: "text-right float-right justify-content-right",
                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                        onClick: ()=>handleremove(index)
                                                        ,
                                                        className: "btn btn-danger w-100 ml-2 ml-sm-2 ml-md-0 ml-lg-0 ml-xl-0 pl-1 pr-1 vendor_product_card_delete_style1",
                                                        children: width2 < 650 ? /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_material_ui_icons__WEBPACK_IMPORTED_MODULE_17__.Delete, {
                                                        }) : intl.formatMessage({
                                                            id: "delete"
                                                        })
                                                    })
                                                })
                                            ]
                                        }));
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_grid_Grid__WEBPACK_IMPORTED_MODULE_4__["default"], {
                                        item: true,
                                        md: 12,
                                        xs: 12,
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "btn btn-light",
                                            onClick: addstructure,
                                            style: {
                                                textAlign: "center",
                                                width: "100%",
                                                cursor: "pointer"
                                            },
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_intl__WEBPACK_IMPORTED_MODULE_15__.FormattedMessage, {
                                                id: "add_used_products",
                                                defaultMessage: "Ishlatilgan material qo'shish +"
                                            })
                                        })
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_buttons_Button__WEBPACK_IMPORTED_MODULE_1__/* ["default"] */ .Z, {
                                        mt: "25px",
                                        variant: "contained",
                                        color: "primary",
                                        type: "submit",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_intl__WEBPACK_IMPORTED_MODULE_15__.FormattedMessage, {
                                            id: "send",
                                            defaultMessage: "Saqlash"
                                        })
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col-12 text-danger",
                                        children: cat_er === "" ? "" : cat_er
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col-12 text-danger",
                                        children: price_er === "" ? "" : price_er
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col-12 text-danger",
                                        children: height_er === "" ? "" : height_er
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col-12 text-danger",
                                        children: width_er === "" ? "" : width_er
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col-12 text-danger",
                                        children: photo_err === "" ? "" : photo_err
                                    })
                                ]
                            })
                        })
                    })
                ]
            })
        ]
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (EditProduct);
async function getStaticPaths() {
    return {
        paths: [],
        fallback: 'blocking'
    };
}
async function getStaticProps(ctx) {
    try {
        let { id  } = ctx.params;
        let lang = ctx.locale;
        const request2_4 = await axios__WEBPACK_IMPORTED_MODULE_10___default()(`${_components_Variables__WEBPACK_IMPORTED_MODULE_19__/* .BASE_URL */ ._n}/flowers/show/${id}/${lang}`);
        const answer_4 = request2_4.data;
        return {
            props: {
                currency2: [],
                material2: [],
                units2: [],
                one: answer_4
            }
        };
    } catch  {
        return {
            notFound: true
        };
    }
}


/***/ }),

/***/ 8130:
/***/ ((module) => {

module.exports = require("@material-ui/core");

/***/ }),

/***/ 7730:
/***/ ((module) => {

module.exports = require("@material-ui/core/ClickAwayListener");

/***/ }),

/***/ 6491:
/***/ ((module) => {

module.exports = require("@material-ui/core/Grow");

/***/ }),

/***/ 640:
/***/ ((module) => {

module.exports = require("@material-ui/core/Paper");

/***/ }),

/***/ 2767:
/***/ ((module) => {

module.exports = require("@material-ui/core/Popper");

/***/ }),

/***/ 8308:
/***/ ((module) => {

module.exports = require("@material-ui/core/styles");

/***/ }),

/***/ 2105:
/***/ ((module) => {

module.exports = require("@material-ui/icons");

/***/ }),

/***/ 9175:
/***/ ((module) => {

module.exports = require("@material-ui/icons/HighlightOff");

/***/ }),

/***/ 9409:
/***/ ((module) => {

module.exports = require("@mui/material/Accordion");

/***/ }),

/***/ 8279:
/***/ ((module) => {

module.exports = require("@mui/material/AccordionDetails");

/***/ }),

/***/ 4604:
/***/ ((module) => {

module.exports = require("@mui/material/AccordionSummary");

/***/ }),

/***/ 8442:
/***/ ((module) => {

module.exports = require("@mui/material/styles");

/***/ }),

/***/ 2038:
/***/ ((module) => {

module.exports = require("@styled-system/css");

/***/ }),

/***/ 9099:
/***/ ((module) => {

module.exports = require("@styled-system/theme-get");

/***/ }),

/***/ 2167:
/***/ ((module) => {

module.exports = require("axios");

/***/ }),

/***/ 4146:
/***/ ((module) => {

module.exports = require("date-fns");

/***/ }),

/***/ 2296:
/***/ ((module) => {

module.exports = require("formik");

/***/ }),

/***/ 6734:
/***/ ((module) => {

module.exports = require("js-cookie");

/***/ }),

/***/ 6517:
/***/ ((module) => {

module.exports = require("lodash");

/***/ }),

/***/ 6641:
/***/ ((module) => {

module.exports = require("next-seo");

/***/ }),

/***/ 562:
/***/ ((module) => {

module.exports = require("next/dist/server/denormalize-page-path.js");

/***/ }),

/***/ 8028:
/***/ ((module) => {

module.exports = require("next/dist/server/image-config.js");

/***/ }),

/***/ 4957:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/head.js");

/***/ }),

/***/ 3539:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/i18n/detect-domain-locale.js");

/***/ }),

/***/ 4014:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/i18n/normalize-locale-path.js");

/***/ }),

/***/ 5832:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/loadable.js");

/***/ }),

/***/ 8020:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/mitt.js");

/***/ }),

/***/ 4964:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router-context.js");

/***/ }),

/***/ 9565:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/get-asset-path-from-route.js");

/***/ }),

/***/ 4365:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/get-middleware-regex.js");

/***/ }),

/***/ 1428:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/is-dynamic.js");

/***/ }),

/***/ 1292:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/parse-relative-url.js");

/***/ }),

/***/ 979:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/querystring.js");

/***/ }),

/***/ 6052:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/resolve-rewrites.js");

/***/ }),

/***/ 4226:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/route-matcher.js");

/***/ }),

/***/ 5052:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/route-regex.js");

/***/ }),

/***/ 3018:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/to-base-64.js");

/***/ }),

/***/ 9232:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ 968:
/***/ ((module) => {

module.exports = require("next/head");

/***/ }),

/***/ 1853:
/***/ ((module) => {

module.exports = require("next/router");

/***/ }),

/***/ 6689:
/***/ ((module) => {

module.exports = require("react");

/***/ }),

/***/ 358:
/***/ ((module) => {

module.exports = require("react-bootstrap");

/***/ }),

/***/ 6405:
/***/ ((module) => {

module.exports = require("react-dom");

/***/ }),

/***/ 6358:
/***/ ((module) => {

module.exports = require("react-dropzone");

/***/ }),

/***/ 6804:
/***/ ((module) => {

module.exports = require("react-facebook-login/dist/facebook-login-render-props");

/***/ }),

/***/ 67:
/***/ ((module) => {

module.exports = require("react-google-login");

/***/ }),

/***/ 2791:
/***/ ((module) => {

module.exports = require("react-helmet");

/***/ }),

/***/ 4648:
/***/ ((module) => {

module.exports = require("react-input-mask");

/***/ }),

/***/ 3126:
/***/ ((module) => {

module.exports = require("react-intl");

/***/ }),

/***/ 9252:
/***/ ((module) => {

module.exports = require("react-lazy-load-image-component");

/***/ }),

/***/ 6022:
/***/ ((module) => {

module.exports = require("react-redux");

/***/ }),

/***/ 7128:
/***/ ((module) => {

module.exports = require("react-svg");

/***/ }),

/***/ 997:
/***/ ((module) => {

module.exports = require("react/jsx-runtime");

/***/ }),

/***/ 6981:
/***/ ((module) => {

module.exports = require("reactstrap");

/***/ }),

/***/ 7518:
/***/ ((module) => {

module.exports = require("styled-components");

/***/ }),

/***/ 5834:
/***/ ((module) => {

module.exports = require("styled-system");

/***/ }),

/***/ 5609:
/***/ ((module) => {

module.exports = require("yup");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [2652,9894,8579,5218,1972,9374,4154,2389,8936,8661,695,4396,8414,3756,4790,12,7198,8167,3518,612,5129,1251,9330,1005], () => (__webpack_exec__(8890)));
module.exports = __webpack_exports__;

})();