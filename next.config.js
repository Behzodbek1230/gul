const withBundleAnalyzer = require("@next/bundle-analyzer")({
  enabled: process.env.ANALYZE === "true",
});
// @ts-check

/**
 * @type {import('next').NextConfig}
 **/
module.exports = withBundleAnalyzer({
  experimental: {
    externalDir: true,
  },
  images: {
    domains: ["api.dana.uz", "api.wolt.uz"],
  },
  devIndicators: {
    autoPrerender: false,
  },
  i18n: {
    locales: ["uz", "ru", "en"],
    defaultLocale: "uz",
  },
  async redirects() {
    return [
      {
        source: "/shop/:id/page/1",
        destination: "/shop/:id/",
        permanent: true,
      },
      {
        source: "/:id/page/1",
        destination: "/:id/",
        permanent: true,
      },
      {
        source: "/blogs/page/1",
        destination: "/blogs",
        permanent: true,
      },
      {
        source: "/product/search/:id/page/1",
        destination: "/product/search/:id",
        permanent: true,
      },
      // {
      //     source:'/product/search/:id/page/1',
      //     has:[
      //         type:'',
      //     ],
      //     destination:'/product/search/:id',
      //     permanent:true
      // }
    ];
  },
  async rewrites() {
    return [
      {
        source: "/en/ru",
        destination: "/ru",
      },
      {
        source: "/en/uz",
        destination: "/",
      },
      {
        source: "/ru/en",
        destination: "/en",
      },
      {
        source: "/ru/uz",
        destination: "/",
      },
    ];
  },
  transpileModules: ["@app/config"],
});
