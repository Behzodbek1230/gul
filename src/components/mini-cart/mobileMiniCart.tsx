import Avatar from "../../components/avatar/Avatar";
import FlexBox from "../../components/FlexBox";
import LazyImage from "../../components/LazyImage";
import { useAppContext } from "../../contexts/app/AppContext";
import { CartItem } from "../../reducers/cartReducer";
import Link from "next/link";
import React, {Fragment, useCallback, useEffect, useState} from "react";
import Button from "../buttons/Button";
import Divider from "../Divider";
import Icon from "../icon/Icon";
import Typography, { H5, Paragraph, Tiny } from "../Typography";
import { StyledMiniCart } from "./MiniCartStyle";
import Cookies from "js-cookie"
import axios from "axios";
import {useDispatch, useSelector} from "react-redux";
import {FormattedMessage} from "react-intl";
import change_modal_status from "../../../Redux/Actions/change_modal_status";
import { useRouter } from "next/router";
type MiniCartProps = {
  toggleSidenav?: () => void;
  mobile?:boolean
};

const MiniCart2: React.FC<MiniCartProps> = ({ toggleSidenav ,mobile}) => {
  const { state, dispatch } = useAppContext();
  const { cartList } = state.cart
  let dispatch2 = useDispatch()
  let modal = useSelector((state:any)=>state.new.modal)
  let [currency_name,setcurrency_name] = useState([{name:"so'm"}])
  let currencies = useSelector((state:any)=>state.token.currencies);
  let lang = useRouter().locale
  useEffect(() => {
    let current_currency = Cookies.get("currency_id");
    setcurrency_name(currencies.filter(currency=>currency.id === parseInt(current_currency)))
  }, [Cookies.get("currency_id")]);
  const handleAddCart = useCallback(
      (amount, product) => () => {
        const login = Cookies.get("isLoggedIn")
        if(login === "true"){
          const data = new FormData();
          data.append("keyword",product.id.toString())
          axios({
            method:"POST",
            url:`/orders/add-basket/${lang}`,
            data:data
          })
        }
        dispatch({
          type: "CHANGE_CART_AMOUNT",
          payload: {
            ...product,
            id:product.keyword,
            imgUrl:product.image,
            qty: amount,
          },
        });
      },
      []
  );

  const handleremove_1 = useCallback(
      (amount, product) => () => {
        const login = Cookies.get("isLoggedIn")
        if(login === "true"){
          const data = new FormData();
          data.append("keyword",product.id.toString())
          axios({
            method:"POST",
            url:`/orders/remove-basket/${lang}`,
            data:data
          })
        }
        dispatch({
          type: "CHANGE_CART_AMOUNT",
          payload: {
            id:product.keyword,
            name:product.name,
            price:product.price,
            imgUrl:product.image,
            qty: amount,
            categoryKeyword:product.categoryKeyword
          },
        });
      },
      []
  );

  const handleremovefull = useCallback(
      (amount, product) => () => {
        const login = Cookies.get("isLoggedIn")
        if(login === "true"){
          const data = new FormData();
          data.append("keyword",product.id.toString())
          axios({
            method:"POST",
            url:`/orders/delete-basket/${lang}`,
            data:data
          })
        }
        dispatch({
          type: "CHANGE_CART_AMOUNT",
          payload: {
            ...product,
            qty: amount,
          },
        });
      },
      []
  );



  const getTotalPrice = () => {
    return (
      cartList.reduce(
        (accumulator, item) => accumulator + Number.parseFloat(item.price.replace(" ","")) * item.qty,
        0
      ) || 0
    );
  };

  return (
      <StyledMiniCart style={mobile&& cartList?.length ? {height:"fit-content"} : {height:"70vh"}} >
        <div className={`cart-list ${mobile && cartList?.length ? `h-50vh-scroll` : `h-100vh`}`} >
          <FlexBox alignItems="center" m="0px 20px" height="74px">
            <Icon size="1.75rem">cart</Icon>
            <Typography fontWeight={600} fontSize="16px" ml="0.5rem">
              {cartList.length} <FormattedMessage id="item" defaultMessage="ta" />
            </Typography>
          </FlexBox>

          <Divider/>

          {!!!cartList.length && (
            <>
              <FlexBox
                  flexDirection="column"
                  alignItems="center"
                  justifyContent="center"
                  height={mobile ? "50vh" :"calc(100% - 180px)"  }
              >
                <LazyImage
                    quality={100}
                    src="/assets/images/logos/shopping-bag.svg"
                    width="90px"
                    height="100%"
                />
                <Paragraph
                    mt="1rem"
                    color="text.muted"
                    textAlign="center"
                    maxWidth="200px"
                >
                  <FormattedMessage id="empty_cart" />
              
                </Paragraph>
               
              </FlexBox>
              <Fragment  >
              <Button
                  variant="contained"
                  className="mb-5 w-95"
                  color="primary"
                  m="1rem  0.75rem"
                  onClick={()=>dispatch2(change_modal_status(!modal))}
              >
                <Typography fontWeight={600}>
                  <FormattedMessage id="order_status" defaultMessage="Buyurtma xolati" /> 
                </Typography>
              </Button>
              </Fragment>
              </>   
          )}
          <div >
            {cartList.map((item: CartItem) => (
                <Fragment key={item.id}>
                  <div className="cart-item">
                    <FlexBox alignItems="center" flexDirection="column">
                      <Button
                          variant="outlined"
                          color="primary"
                          padding="5px"
                          size="none"
                          borderColor="primary.light"
                          borderRadius="300px"
                          onClick={handleAddCart(item.qty + 1,item)}
                      >
                        <Icon variant="small">plus</Icon>
                      </Button>
                      <Typography fontWeight={600} fontSize="15px" my="3px">
                        {item.qty}
                      </Typography>
                      <Button
                          variant="outlined"
                          color="primary"
                          padding="5px"
                          size="none"
                          borderColor="primary.light"
                          borderRadius="300px"
                          onClick={handleremove_1(item.qty - 1, item)}
                          disabled={item.qty === 1}
                      >
                        <Icon variant="small">minus</Icon>
                      </Button>
                    </FlexBox>

                    <Link href={`${item.categoryKeyword}/${item.id}`}>
                      <a>
                        <Avatar
                            src={item.imgUrl || "/assets/images/products/iphone-x.png"}
                            mx="1rem"
                            alt={item.name}
                            size={76}
                        />
                      </a>
                    </Link>

                    <div className="product-details">
                      <Link href={`${item.categoryKeyword}/${item.id}`}>
                        <a>
                          <H5 className="title py-1" fontSize="14px">
                            {item.name}
                          </H5>
                        </a>
                      </Link>
                      <Tiny color="text.muted">
                        {item.price} x {item.qty}
                      </Tiny>
                      <Typography
                          fontWeight={600}
                          fontSize="14px"
                          color="primary.main"
                          mt="4px"
                      >
                        {(item.qty * Number.parseFloat(item.price.toString().replace(/ /g,"")))} {currency_name.length >= 1 && currency_name ? currency_name[0].name : currencies.filter(currency=>currency.code === "UZS")[0].name}
                      </Typography>
                    </div>

                    <Icon
                        className="clear-icon"
                        size="1rem"
                        ml="1.25rem"
                        onClick={handleremovefull(0, item)}
                    >
                      close
                    </Icon>
                  </div>
                  <Divider/>
                </Fragment>
            ))}
          </div>
        </div>

        {!!cartList.length && (

              <Fragment>
              <Link href="/cart">
                <Button
                    variant="contained"
                    className="mb-0"
                    color="primary"
                    m="1rem 1rem 0.75rem"
                    onClick={toggleSidenav}
                >
                  <Typography fontWeight={600} >
                    <FormattedMessage id="Checkout Now" defaultMessage="Hoziroq sotib olish" /> ({getTotalPrice().toFixed(2)} {currency_name.length >= 1 && currency_name ? currency_name[0].name : currencies.filter(currency=>currency.code === "UZS")[0].name})
                  </Typography>
                </Button>
              </Link>
              
              <Button
                  variant="contained"
                  className="mb-5"
                  color="primary"
                  m="1rem 1rem 0.75rem"
                  onClick={()=>dispatch2(change_modal_status(!modal))}
              >
                <Typography fontWeight={600} >
                  <FormattedMessage id="order_status" defaultMessage="Buyurtma xolati" /> 
                </Typography>
              </Button>
            </Fragment>

        )}
      </StyledMiniCart>
  );
};

MiniCart2.defaultProps = {
  toggleSidenav: () => {},
};

export default MiniCart2;
