import { useFormik } from "formik";
import axios from "axios"
import { useRouter } from "next/router";
import React, { useState } from "react";
import * as yup from "yup";
import { StyledSessionCard } from "./SessionStyle";
import {useDispatch} from "react-redux"
import { get_token} from "../../../Redux/Actions/Action"
import {close_login} from "../../../Redux/Actions/LoginModel";
import Cookies from "js-cookie"
import get_cart_products from "../../../Redux/Actions/get_cart_products";
import {useAppContext} from "../../contexts/app/AppContext";
import {useIntl} from "react-intl";
import Section1 from "../../components/login/Section1";
import Section2 from "../../components/login/Section2";


const Login2: React.FC = () => {
  const router = useRouter()
  const [telephonenumber, Settelephonenumber] = useState(" ");
  const [errors2, seterrors2] = useState(" ")
  const [errors3, seterrors3] = useState(" ")
  const [change,setchange] = useState(false)
  const [button,setbutton] = useState(true)
  const [code,setcode] = useState(undefined)
  const [error,seterror] = useState(" ")
  const [send_button,setsend_button] = useState(false)
  let intl = useIntl()
  let lang = router.locale

const responseFacebook = (response) => {
  let data  = new FormData()
  data.append("phone",response?.phone)
  data.append("id",response?.id)
  data.append("fio",response?.name)
  data.append("email",response?.email)
  data.append("type","2")
  axios({
      method:"POST",
      url:`/login/social-set`,
      data:data
  })
      .then(response=>{
            let today = new Date()
            let day = today.getUTCDate()
            today.setDate(day+30)
            Cookies.remove("token")
            Cookies.remove("isLoggedIn")
            document.cookie =`token=${response.data.access_token}; path=/; expires=${today}`;
            document.cookie = `isLoggedIn=true; path=/; expires=${today}`;
            dispatch2(get_token(response.data.access_token))
            dispatch2(close_login())
            if(response.data.errors){
                return null;
            }
            else{
                //--------------------- Clearing data from carts---------------------------
                handleremovecart()
                router.push(router.asPath)
            }
      })
      .catch(()=>{
          return null;
      })
}
 const responseGoogle = (response) => {
  let data  = new FormData()
  data.append("phone",response?.profileObj?.phone)
  data.append("id",response?.googleId)
  data.append("fio",response?.profileObj?.name)
  data.append("email",response?.profileObj?.email)
  data.append("type","1")
  axios({
      method:"POST",
      url:`/login/social-set`,
      data:data
  })
      .then(response=>{
            let today = new Date()
            let day = today.getUTCDate()
            today.setDate(day+30)
            Cookies.remove("token")
            Cookies.remove("isLoggedIn")
            document.cookie =`token=${response.data.access_token}; path=/; expires=${today}`;
            document.cookie = `isLoggedIn=true; path=/; expires=${today}`;
            dispatch2(get_token(response.data.access_token))
            dispatch2(close_login())
            if(response.data.errors){
                return null;
            }
            else{
                //--------------------- Clearing data from carts---------------------------
                handleremovecart()
                router.push(router.asPath)
            }
      })
      .catch(()=>{
          return null;
      })


 }
  const dispatch2 = useDispatch()
  const handle222222 = (event) => {
    event.preventDefault()
    const phone = event.target.value.slice(1,17);
    const x = phone.replace(/-/g,"")
    const x2 = x.replace(/_/g,"")
    const isValid = x2.length === 12 ? true : false
    if(isValid) {
      const phone_number = event.target.value.toString().replace(/-/g,"")

      Settelephonenumber(phone_number)
      seterrors2("")
    }
    else {
      if (event.target.value === "") {

        seterrors2(intl.formatMessage({id:"input_phone"}))
      }
      else {
        seterrors2(intl.formatMessage({id:"correct_phone"}))
      }
    }
  }


  const handlecode = (event) => {
    event.preventDefault();
    if(event.target.value.length === 4){
        seterrors3("")
        setcode(event.target.value)

    }
    else{
      seterrors3(intl.formatMessage({id:"correct_password"}))
    }
  }


    const { state, dispatch } = useAppContext();
    const { cartList } = state.cart;
    const handleCartAmountChange2 = (amount,product) =>  {
        dispatch({
            type: "CHANGE_CART_AMOUNT",
            payload: {
                ...product,
                qty: amount,
            },
        });
    }
    const handleCartAmountChange = (amount,product) =>  {
        dispatch({
            type: "CHANGE_CART_AMOUNT",
            payload: {
                ...product,
                imgUrl:product.image,
                id:product.keyword,
                qty: amount,
            },
        });
    }
    const handleremovecart = () =>{
        try{
            if(Cookies.get("isLoggedIn") === true){
                cartList.forEach((value)=>{handleCartAmountChange2(0,value)})
            }
        }
        catch{
            return null;
        }
        let currency_text = typeof Cookies.get("currency_id") === "undefined" ? "" : `?currency=${Cookies.get("currency_id")}`
        if(Cookies.get("isLoggedIn") === "true"){
            axios(`/orders/basket-list/${lang}${currency_text}`)
                .then(response =>{
                    dispatch2(get_cart_products(response.data));
                    try{
                        cartList.forEach(product=>handleCartAmountChange2(0,product))
                        response.data.products.map(product=>handleCartAmountChange(product.count,product))
                    }
                    catch{
                        return null;
                    }
                })
                .catch(()=>null)
        }
    }
  const handleSubmit4 = (e) =>{
    e.preventDefault()
    axios({
      url: `/login/auth`,
      method: "POST",
      data: { "phone": telephonenumber,"code": code},
    })
        .then(response=>{
            if(response.data.errors){
                seterror(intl.formatMessage({id:"correct_password"}))
                return null;
            }
            else{
                let today = new Date()
                let day = today.getUTCDate()
                today.setDate(day+30)
                Cookies.remove("token")
                Cookies.remove("isLoggedIn")
                document.cookie =`token=${response.data.access_token}; path=/; expires=${today}`;
                document.cookie = `isLoggedIn=true; path=/; expires=${today}`;
                dispatch2(get_token(response.data.access_token))
                dispatch2(close_login())
                //--------------------- Clearing data from carts---------------------------
                handleremovecart()
                router.push(router.asPath)
            }
        })
        .catch(() =>{
          seterror(intl.formatMessage({id:"correct_password"}))
        })


  }
  const handleSubmit5 = (e) => {
    setsend_button(true)
    e.preventDefault();
    axios({
      url: `/login/verify`,
      method: "POST",
      data: { "phone": telephonenumber },
    })
      .then(() => {
        setchange(true)
        setbutton(true)
        setsend_button(false)
        setTimeout(()=>{
            setbutton(false)
        },60 * 1000)

      })
  }

  const handleFormSubmit = async (values) => {
    console.log(values)
    router.push("/profile");
  };

  const {
  } = useFormik({
    onSubmit: handleFormSubmit,
    initialValues,
    validationSchema: formSchema,
  });

  return (

      <StyledSessionCard
          mx="auto"
          my="2rem"
          boxShadow="large"
      >
      {change ? (
            <Section1
                button={button}
                code={code}
                error={error}
                errors3={errors3}
                handleSubmit4={(e)=>handleSubmit4(e)}
                handleSubmit5={(e)=>handleSubmit5(e)}
                handlecode={(r)=>handlecode(r)}
                setchange={(e)=>setchange(e)}
            />
          )
          :
          (
            <Section2
                errors2={errors2}
                handle222222={(r)=>handle222222(r)}
                handleSubmit5={(e)=>handleSubmit5(e)}
                responseFacebook={(e)=>responseFacebook(e)}
                responseGoogle = {(e)=>responseGoogle(e)}
                send_button={send_button}
            />
          )
      }

      <br />
    </StyledSessionCard>

  );
};

const initialValues = {
  email: "",
  password: "",
};

const formSchema = yup.object().shape({
  email: yup.string().email("invalid email").required("${path} is required"),
  password: yup.string().required("${path} is required"),
});

export default Login2;
