"use strict";
exports.id = 3518;
exports.ids = [3518];
exports.modules = {

/***/ 3518:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(5834);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(styled_system__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _utils_constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(9758);





const StyledHidden = styled_components__WEBPACK_IMPORTED_MODULE_2___default().div(({ up , down  })=>{
    const upDeviceSize = _utils_constants__WEBPACK_IMPORTED_MODULE_4__/* .deviceSize */ .J[up] || up;
    const downDeviceSize = _utils_constants__WEBPACK_IMPORTED_MODULE_4__/* .deviceSize */ .J[down] || down;
    if (up) return {
        [`@media only screen and (min-width: ${upDeviceSize + 1}px)`]: {
            display: "none"
        }
    };
    else if (down) return {
        [`@media only screen and (max-width: ${downDeviceSize}px)`]: {
            display: "none"
        }
    };
    else return {
        display: "none"
    };
}, (0,styled_system__WEBPACK_IMPORTED_MODULE_3__.compose)(styled_system__WEBPACK_IMPORTED_MODULE_3__.space, styled_system__WEBPACK_IMPORTED_MODULE_3__.flex));
const Hidden = ({ children , ...props })=>{
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(StyledHidden, {
        ...props,
        children: children
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Hidden);


/***/ })

};
;