import Box from "../../components/Box";
import IconButton from "../../components/buttons/IconButton";
import Card from "../../components/Card";
import FlexBox from "../../components/FlexBox";
import Grid from "../../components/grid/Grid";
import Hidden from "../../components/hidden/Hidden";
import Icon from "../../components/icon/Icon";
import NavbarLayout from "../../components/layout/NavbarLayout";
import ProductCard2List from "../../components/products/ProductCard2List";
import ProductFilterCard2 from "../../components/products/ProductFilterCard2";
import Sidenav from "../../components/sidenav/Sidenav";
import { H1, Paragraph } from "../../components/Typography";
import React, {useCallback, useEffect, useState} from "react";
import axios from "axios";
import {useDispatch, useSelector} from "react-redux";
import {useRouter} from "next/router";
import useWindowSize from "../../hooks/useWindowSize";
import {FormattedMessage, useIntl} from "react-intl";
import Cookies from "js-cookie";
import { NextSeo } from "next-seo";
import get_search_results from "../../../Redux/Actions/get_search_results";
import SeoText from "../../components/SeoText";
import { BASE_URL } from "../../components/Variables";
const Category_items_page = ({info2}) => {
    let intl = useIntl()
    let currency_id = Cookies.get("currency_id");
    const router = useRouter()
    let lang = router.locale
    const view ="grid";
    const { id,page } = router.query
    const width = useWindowSize();
    const isTablet = width < 1025;
    const [from_price,setfrom_price] = useState("")
    const [to_price,setto_price] = useState("")
    const [cat_id,setcat_id] = useState("")
    const [sort,setsort] = useState(null)
    const dispatch = useDispatch()
    let whenCategoryChange = useCallback(()=>{
       dispatch(get_search_results(info2))
    },[])
    useEffect(()=>{
        whenCategoryChange()
    },[])
    let info = useSelector((state:any)=>state.new.search_results)
    useEffect(()=>{
        let sort_type = sort && typeof sort !== "undefined"? sort : "";
        let url = `/flowers/search/${lang}${typeof page === "undefined" ? "?" : `?page=${page}&`}from_price=${from_price}&to_price=${to_price}&keyword=${cat_id || id}&sort_type=${sort_type}&currency=${currency_id}`
        axios(url)
            .then(res=>{
                dispatch(get_search_results(res.data))
            })
            .catch(()=>{
                return null;
            })
    },[from_price,to_price,cat_id,id,sort,page,lang,currency_id])
    const sortOptions = [
        {label:intl.formatMessage({ id:"Sort By"}),value: ""},
        { label: intl.formatMessage({id:"low_high"}), value: 1 },
        { label: intl.formatMessage({id:"high_low"}), value: 2 },
    ];
    return (
        <NavbarLayout>
            <NextSeo
                title={info?.seo?.length !==0 && info?.seo ?  info?.seo?.mtitle : ""}
                description={info?.seo?.length !==0 && info?.seo ? info?.seo?.mdescription : ""}
                additionalMetaTags={[{
                    name: 'keyword',
                    content: info?.seo?.length !==0 && info?.seo ? info?.seo.mkeywords :""
                }, ]}
                openGraph={{
                    type:"product",
                }}
            />
            <Box pt="20px" pb="20px">
                <FlexBox
                    p="1.25rem"
                    flexWrap="wrap"
                    justifyContent="space-between"
                    alignItems="center"
                    mb="55px"
                    elevation={5}
                    as={Card}
                >
                    <div>
                        <H1 className="fontSize-small" >

                            {info?.categoryName|| id}

                        </H1>
                        <Paragraph
                            color="text.muted"
                        >
                            {info?.datas?.total} <FormattedMessage id="results_found" defaultMessage="natija topildi" />
                        </Paragraph>
                    </div>
                    <FlexBox
                        alignItems="center"
                        flexWrap="wrap"
                    >
                        <Box style={{marginTop:"10px"}} className="w-100 " flex="1 1 0" mr="0.8rem" mb="0.4rem" marginRight="-2px" minWidth="190px">
                            <select
                                value={sort || sortOptions[0]}
                                className="form-control"
                                id="exampleFormControlSelect1"
                                onChange={(r)=>setsort(r.target.value)}
                            >
                                {sortOptions.map(option=> <option key={option.value} value={option.value} >{option.label}</option>)}


                            </select>
                        </Box>
                        {isTablet && (
                            <Sidenav
                                position="right"

                                scroll={true}
                                handle={
                                    <IconButton className="ml-2 margin-top-optionadminkadaa edit qilishda bor, Alohida nomer kerak sababi mijoz o'zini shaxsiy nomerida ro'yxatdan o'tishi mumkin. Mijozlari uchun boshqa tel nomer qo'yishi mumkin,s" size="small">
                                        <Icon>options</Icon>
                                    </IconButton>
                                }
                            >
                                <ProductFilterCard2
                                    setfrom_price={(e)=>setfrom_price(e)}
                                    setto_price={(e)=>setto_price(e)}
                                    setcat_id={(g)=>setcat_id(g)}
                                />
                            </Sidenav>
                        )}
                    </FlexBox>
                </FlexBox>

                <Grid container spacing={6}>
                    <Hidden
                        as={Grid}
                        item
                        lg={3}
                const        xs={12}
                        down={1024}
                    >
                        <ProductFilterCard2
                            setfrom_price={(e)=>setfrom_price(e)}
                            setto_price={(e)=>setto_price(e)}
                            setcat_id={(g)=>setcat_id(g)}
                        />
                    </Hidden>

                    <Grid item lg={9} xs={12}>
                        {view === "grid" ? <ProductCard2List   /> :  <ProductCard2List  />}
                    </Grid>
                </Grid>
            </Box>
            <SeoText text={info?.seo?.seotext} />
        </NavbarLayout>
    );
};


export async function getStaticPaths(){
    return {paths:[],fallback:'blocking'}
}


export async function getStaticProps(ctx){
    let {id} = ctx.params
    let x = ctx.locale
    try{
        const request3 = await axios( `${BASE_URL}/flowers/category-search/${x}?keyword=${id}`)
        const answer2 =  request3.data;
        return {
            props:{
                info2:answer2
            },
            revalidate:2
        }
    }
    catch{
        return {
            notFound:true
        }
    }
}

export default Category_items_page;
