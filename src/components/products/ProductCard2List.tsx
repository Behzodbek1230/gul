import React from "react";
import FlexBox from "../FlexBox";
import Grid from "../grid/Grid";
import ProductCard1 from "../product-cards/ProductCard1";
import {StyledPagination} from "../../components/pagination/PaginationStyle";
import Button from "../../components/buttons/Button";
import Icon from "../../components/icon/Icon";
import ReactPaginate from "react-paginate";

import { useSelector} from "react-redux";

import { useRouter } from "next/router";



const ProductCard2List = () => {

    const data = useSelector((state:any)=>state.new.search_results)
    const wishlist = data||[]
    const router = useRouter()
    const { page} = router.query

    return (
        <div>
                {wishlist?.datas && wishlist?.datas?.data.length !==0 ?
                    <>
                         <Grid container spacing={6}>
                            {wishlist?.datas?.data?.map((item, ind) => (
                                <Grid item lg={4} sm={4} xs={6} key={ind}>
                                    <ProductCard1
                                        title={item.name}
                                        category_keyword = {item.categoryKeyword}
                                        imgUrl={item.image}
                                        price={item.price}
                                        id={item.keyword}
                                        rating={item.rating}
                                        is_favourite={item.is_favorites}
                                        shopName={item.shopName}
                                        shopKeyword={item?.shopKeyword}
                                    />
                                </Grid>
                            ))}
                         </Grid>
                        {wishlist?.datas?.last_page!==1
                            ?
                            <FlexBox
                            flexWrap="wrap"
                            justifyContent="space-between"
                            alignItems="center"
                            mt="32px"
                        >
                            <StyledPagination>
                                {wishlist.datas.current_page !== 1?
                                    <ReactPaginate
                                        forcePage={typeof page === "undefined" ? 0 : parseInt(data?.datas?.current_page)-1}
                                        previousLabel={
                                            <Button
                                                style={{cursor: "pointer"}}
                                                className="control-button"
                                                color="primary"
                                                overflow="hidden"
                                                height="auto"
                                                padding="6px"
                                                borderRadius="50%"
                                            >
                                                <Icon defaultcolor="currentColor" variant="small">
                                                    chevron-left
                                                </Icon>
                                            </Button>

                                        }
                                        nextLabel={
                                            <Button
                                                style={{cursor: "pointer"}}
                                                className="control-button"
                                                color="primary"
                                                overflow="hidden"
                                                height="auto"
                                                padding="6px"
                                                borderRadius="50%"
                                            >
                                                <Icon defaultcolor="currentColor" variant="small">
                                                    chevron-right
                                                </Icon>
                                            </Button>
                                        }
                                        breakLabel={
                                            <Icon defaultcolor="currentColor" variant="small">
                                                triple-dot
                                            </Icon>
                                        }
                                        pageCount={wishlist.lastPage}
                                        marginPagesDisplayed={true}
                                        pageRangeDisplayed={false}
                                        onPageChange={(r)=>{
                                            if(router.pathname.includes("page")){
                                                if(r.selected === 0){
                                                    let query = {...router.query}
                                                    delete query.page
                                                    router.push({pathname:router.pathname.replace("/page/[page]",""),query:query})
                                                }
                                                else{
                                                    let query = {...router.query}
                                                    query.page = r.selected+1
                                                    router.push({pathname:router.pathname,query:query})
                                                }
                                            }
                                            else{
                                                router.push( {pathname:router.asPath +`/page/${r.selected + 1}`})
                                            }

                                        }
                                        }
                                        containerClassName="pagination"
                                        subContainerClassName="pages pagination"
                                        activeClassName="active"
                                        disabledClassName="disabled"
                                    />
                                    :
                                    <ReactPaginate
                                        previousLabel={
                                            <Button
                                                style={{cursor: "pointer"}}
                                                className="control-button"
                                                color="primary"
                                                overflow="hidden"
                                                height="auto"
                                                padding="6px"
                                                borderRadius="50%"
                                            >
                                                <Icon defaultcolor="currentColor" variant="small">
                                                    chevron-left
                                                </Icon>
                                            </Button>

                                        }
                                        nextLabel={
                                            <Button
                                                style={{cursor: "pointer"}}
                                                className="control-button"
                                                color="primary"
                                                overflow="hidden"
                                                height="auto"
                                                padding="6px"
                                                borderRadius="50%"
                                            >
                                                <Icon defaultcolor="currentColor" variant="small">
                                                    chevron-right
                                                </Icon>
                                            </Button>
                                        }
                                        breakLabel={
                                            <Icon defaultcolor="currentColor" variant="small">
                                                triple-dot
                                            </Icon>
                                        }
                                        pageCount={wishlist.lastPage}
                                        marginPagesDisplayed={true}
                                        pageRangeDisplayed={false}
                                        onPageChange={(r)=>{
                                            if(router.pathname.includes("page")){
                                                let query = {...router.query}
                                                query.page = r.selected+1
                                                router.push({pathname:router.pathname,query:query})
                                            }
                                            else{
                                                router.push( {pathname:router.asPath +`/page/${r.selected + 1}`})
                                            }

                                        }
                                        }
                                        containerClassName="pagination"
                                        subContainerClassName="pages pagination"
                                        activeClassName="active"
                                        disabledClassName="disabled"
                                    />
                                }
                            </StyledPagination>
                        </FlexBox>
                            :
                            ""
                        }
                    </>
                    : ""}



        </div>
    );
};

export default ProductCard2List;
