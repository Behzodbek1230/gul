// your_nextjs_sitemap_generator.js

const sitemap = require("nextjs-sitemap-generator");

sitemap({
    alternateUrls: {
        uz: "https://dana.uz",
        ru: "https://dana.uz/ru",
        en: "https://dana.uz/en",
    },
    baseUrl: "https://dana.uz",
    ignoredPaths: [
        '/vendor/account-settings',
        '/vendor/add-product',
        '/vendor/orders',
        '/vendor/products',
        '/wish-list',
        '/profile/edit',
        '/profile/budget',
        '/server-sitemap.xml',
        "/review",
        "/vendor/create",
        "/review",
        "/cart",
        "/blogs/page/[page]",
        '/blogs/[keyword]',
        '/company/[keyword]',
        '/mobile-cart',
        '/orders/page/[page]',
        '/orders/[id]',
        '/product/search/[id]',
        '/product/search/[id]/page/[page]',
        '/shop/[id]',
        '/shop/[id]/page/[page]',
        '/shopList/page/[page]',
        '/[id]',
        '/[id]/page/[page]',
        '/[id]/[keyword]'

    ],
    ignoreIndexFiles:true,
    pagesDirectory: __dirname + "\\src\\pages",
    targetDirectory: "public/",
    sitemapFilename: "sitemap.xml",
    nextConfigPath: __dirname + "\\next.config.js",
    sitemapStylesheet: [
        {
            type: "text/css",
            styleFile: "/test/styles.css",
        },
        {
            type: "text/xsl",
            styleFile: "test/test/styles.xls",
        },
    ],
});

console.log(`✅ sitemap.xml generated!`);