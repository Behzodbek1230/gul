import Card from "../../components/Card";
import {ChipwithoutRounded } from "../../components/Chip";
import HoverBox from "../../components/HoverBox";
// import LazyImage from "../../components/LazyImage";
import useWindowSize from "../../hooks/useWindowSize";
import React from "react";
export interface ProductCard6Props {
  imgUrl: string;
  s_imgUrl:string;
  title: string;
  subtitle: string;
}

const ProductCard6: React.FC<ProductCard6Props> = ({
  title,
  imgUrl,
  s_imgUrl
}) => {
  const size = useWindowSize();
  return (
    <Card 
      position="relative" 
      style={{marginLeft:"20px",cursor:"pointer",}}
      className='d-flex flex-column align-items-center bg-transparent shadow-none'
      >
        <HoverBox position="relative" className="productcard6_product_Style" >
        <img
          className="category_image "

          src={size < 750 ? s_imgUrl : imgUrl}

          alt={title}

        />
      </HoverBox>
      <div style={{textAlign:"center"}}>
      <ChipwithoutRounded
        className="productcard6_chip text-wrap font-weight-normal"
        color="white"
        fontWeight="600"
        p="7px 20px"
        top="6.375rem"
        left="1.5rem"
        zIndex={2}
      >
        {title}
      </ChipwithoutRounded></div>
    </Card>
  );
};

export default ProductCard6;
