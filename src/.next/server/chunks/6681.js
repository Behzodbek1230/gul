"use strict";
exports.id = 6681;
exports.ids = [6681];
exports.modules = {

/***/ 6681:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _hooks_useWindowSize__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(12);
/* harmony import */ var react_intl__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3126);
/* harmony import */ var react_intl__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_intl__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(1853);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(2167);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var next_dynamic__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(5218);







const ShopCard1 = (0,next_dynamic__WEBPACK_IMPORTED_MODULE_6__["default"])(()=>Promise.all(/* import() */[__webpack_require__.e(1972), __webpack_require__.e(9374), __webpack_require__.e(4154), __webpack_require__.e(8661), __webpack_require__.e(695), __webpack_require__.e(8970), __webpack_require__.e(197), __webpack_require__.e(9636), __webpack_require__.e(5359)]).then(__webpack_require__.bind(__webpack_require__, 9636))
, {
    ssr: false
});
const Box = (0,next_dynamic__WEBPACK_IMPORTED_MODULE_6__["default"])(()=>__webpack_require__.e(/* import() */ 4875).then(__webpack_require__.bind(__webpack_require__, 4875))
, {
    ssr: false
});
const Carousel = (0,next_dynamic__WEBPACK_IMPORTED_MODULE_6__["default"])(()=>Promise.all(/* import() */[__webpack_require__.e(1972), __webpack_require__.e(4154), __webpack_require__.e(8661), __webpack_require__.e(9868), __webpack_require__.e(9758)]).then(__webpack_require__.bind(__webpack_require__, 9868))
, {
    ssr: false
});
const CategorySectionCreator = (0,next_dynamic__WEBPACK_IMPORTED_MODULE_6__["default"])(()=>Promise.all(/* import() */[__webpack_require__.e(2652), __webpack_require__.e(9894), __webpack_require__.e(9374), __webpack_require__.e(4154), __webpack_require__.e(7272), __webpack_require__.e(8444)]).then(__webpack_require__.bind(__webpack_require__, 7272))
, {
    ssr: false
});
const Grid = (0,next_dynamic__WEBPACK_IMPORTED_MODULE_6__["default"])(()=>__webpack_require__.e(/* import() */ 8167).then(__webpack_require__.bind(__webpack_require__, 8167))
, {
    ssr: false
});
const Section2 = ()=>{
    const { 0: visibleSlides , 1: setVisibleSlides  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(3);
    const width = (0,_hooks_useWindowSize__WEBPACK_IMPORTED_MODULE_2__/* ["default"] */ .Z)();
    const { 0: shops_list , 1: setshop_list  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)([]);
    let lang = (0,next_router__WEBPACK_IMPORTED_MODULE_4__.useRouter)().locale;
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        axios__WEBPACK_IMPORTED_MODULE_5___default().get(`/shops/list-home-page/${lang}`).then((res)=>{
            setshop_list(res.data);
        }).catch(()=>null
        );
    }, [
        lang
    ]);
    let intl = (0,react_intl__WEBPACK_IMPORTED_MODULE_3__.useIntl)();
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        if (width < 500) setVisibleSlides(1);
        else if (width < 650) setVisibleSlides(2);
        else if (width < 1050) setVisibleSlides(2);
        else setVisibleSlides(3);
    }, [
        width
    ]);
    let left_button_style;
    let right_button_style;
    let shop_style;
    if (width < 650) {
        left_button_style = {
            marginLeft: '-20px',
            marginTop: "-10px"
        };
        right_button_style = {
            marginRight: '-20px',
            marginTop: "-10px"
        };
        shop_style = {
            marginLeft: "40px"
        };
    } else {
        left_button_style = {
        };
    }
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        "data-aos": "zoom-in",
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
            style: {
                textTransform: "capitalize"
            },
            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(CategorySectionCreator, {
                iconName: "light",
                title: intl.formatMessage({
                    id: "Shops"
                }),
                seeMoreLink: "/shopList/page/1",
                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(Box, {
                    mt: "-0.25rem",
                    mb: "0rem",
                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(Carousel, {
                        leftButtonStyle: left_button_style,
                        rightButtonStyle: right_button_style,
                        totalSlides: shops_list === null || shops_list === void 0 ? void 0 : shops_list.length,
                        visibleSlides: visibleSlides,
                        infinite: false,
                        autoPlay: true,
                        showArrow: true,
                        spacing: "15px",
                        interval: 4000,
                        children: shops_list.length !== 0 && (shops_list === null || shops_list === void 0 ? void 0 : shops_list.map((item, ind)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(Grid, {
                                item: true,
                                lg: 12,
                                sm: 12,
                                xs: 12,
                                style: shop_style,
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(ShopCard1, {
                                    ...item
                                })
                            }, ind)
                        ))
                    })
                })
            })
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Section2);


/***/ })

};
;