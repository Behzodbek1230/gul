import React, {Fragment, useEffect, useState} from "react";
import FlexBox from "../FlexBox";
import Hidden from "../hidden/Hidden";
import  { H5 } from "../Typography";
import OrderRow from "./OrderRow";
import {useDispatch, useSelector} from "react-redux";
import {StyledPagination} from "../../components/pagination/PaginationStyle";
import Button from "../../components/buttons/Button";
import Icon from "../../components/icon/Icon";
import axios from "axios";
import Cookies from "js-cookie"
import ReactPaginate from "react-paginate";
import {useRouter} from "next/router";
import one_shop_orders from "../../../Redux/Actions/one_shop_orders";
import {FormattedMessage} from "react-intl";
import useWindowSize from "../../hooks/useWindowSize";

export interface VendorOrderListProps {}


const VendorOrderList: React.FC<VendorOrderListProps> = () => {
  let orders = useSelector((state:any)=>state.token.one_shop_orders);
  let user = useSelector((state:any)=>state.token.user)
  const [data2,setdata2] = useState(orders)
  const width = useWindowSize()
  const dispatch = useDispatch()
  let router =  useRouter()
  let {page} =router.query
  let lang = router.locale
  let OrderFetch = () =>{
    if(user?.data?.is_shops){
        let currency_text = typeof Cookies.get("currency_id") === "undefined" ? "" : `currency=${Cookies.get("currency_id")}`
        axios(`/shops/orders-list/${user?.data?.is_shops ? user?.data?.is_shops : "" }/${lang}${typeof page=== "undefined" ? "?" : `?page=${page}&`}${currency_text}`)
            .then(response=>{
                dispatch(one_shop_orders(response.data))
                setdata2(response.data)
            })
            .catch(()=>{
                return null;
            })
    }
  }
  useEffect(()=>OrderFetch(),[user])
  useEffect(() => {
    OrderFetch()
  }, [page,lang,Cookies.get("currency_id")]);
  return (
    <Fragment>
      {orders?.length === 0 ? <div style={{textAlign: "center"}}>
          <FormattedMessage
            id="none_orders"
          />
          </div> 
          :
          <>
        {data2?.datas?.data?.length !==0 ?
            <>
                {width < 770 ?
                    <div className='vendor_order_container'>
                        <div style={{width:"640px"}}>
                            <div className="d-flex">
                                <H5 color="text.muted"  ml="30px" fontSize="13px"  textAlign="left">
                                    <FormattedMessage
                                        id="Order_#"
                                        defaultMessage="Buyurtma #"
                                    />
                                </H5>
                                <H5 color="text.muted" flex="0 0 33%" className="text-truncate" fontSize="13px" ml="40px" textAlign="left">
                                    <FormattedMessage
                                        id="Status"
                                        defaultMessage="Xolati"
                                    />
                                </H5>
                                <H5 color="text.muted" flex="0 0 23% !important" fontSize="13px"   textAlign="left">
                                    <FormattedMessage
                                        id="Date Purchased"
                                        defaultMessage="Sotib Olingan"
                                    />
                                </H5>
                                <H5 color="text.muted" flex="0 0 10% !important" fontSize="13px"   textAlign="left">
                                    <FormattedMessage
                                        id="Total"
                                        defaultMessage="Umumiy"
                                    />
                                </H5>
                                <H5
                                    fontSize="13px"
                                    color="text.muted"
                                    px="22px"
                                    ml="45px"

                                >
                                    <FormattedMessage
                                        id="actions"
                                    />
                                </H5>

                            </div>
                            {data2?.datas?.data?.map((item, ind) => (
                                <OrderRow setdata={(e)=>setdata2(e)} item={item} key={ind} detail={true} />
                            ))}
                        </div>
                    </div>
                    :
                    <div className="" >
                    <Hidden down={769}>
                        <div className="d-flex px-2 py-1" >
                            <H5 color="text.muted"  ml="10px" textAlign="left">
                                <FormattedMessage
                                    id="Order_#"
                                    defaultMessage="Buyurtma #"
                                />
                            </H5>
                            <H5
                                color="text.muted"
                                className="text-truncate"
                                flex="0 0 25% !important"
                                ml="120px"
                            >
                                <FormattedMessage
                                    id="Status"
                                    defaultMessage="Xolati"
                                />
                            </H5>
                            <H5 color="text.muted" flex="0 0 23%" textAlign="left">
                                <FormattedMessage
                                    id="Date Purchased"
                                    defaultMessage="Sotib Olingan"
                                />
                            </H5>
                            <H5 flex="0 0 24%" color="text.muted"  textAlign="left">
                                <FormattedMessage
                                    id="Total"
                                    defaultMessage="Umumiy"
                                />
                            </H5>
                            <H5
                                className=""
                                flex="0 0 0 !important"
                                color="text.muted"
                                px="0px"
                                my="0px"
                                ml="30px"
                            >
                                <FormattedMessage
                                    id="actions"
                                />
                            </H5>

                        </div>
                    </Hidden>
                    <div className="">
                        {data2?.datas?.data?.map((item, ind) => (
                            <OrderRow setdata={(e)=>setdata2(e)} item={item} key={ind} detail={true} />
                        ))}
                    </div>
                    </div>
                }
                {data2?.datas?.last_page !== 1
                  ?
                  <FlexBox justifyContent="center" mt="2.5rem">
                    <StyledPagination>
                      <ReactPaginate
                          initialPage={typeof page !== "undefined" ? parseInt(page.toString())-1 : 0 }
                          previousLabel={
                            <Button
                                style={{cursor: "pointer"}}
                                className="control-button"
                                color="primary"
                                overflow="hidden"
                                height="auto"
                                padding="6px"
                                borderRadius="50%"
                            >
                              <Icon defaultcolor="currentColor" variant="small">
                                chevron-left
                              </Icon>
                            </Button>

                          }
                          nextLabel={
                            <Button
                                style={{cursor: "pointer"}}
                                className="control-button"
                                color="primary"
                                overflow="hidden"
                                height="auto"
                                padding="6px"
                                borderRadius="50%"
                            >
                              <Icon defaultcolor="currentColor" variant="small">
                                chevron-right
                              </Icon>
                            </Button>
                          }
                          breakLabel={
                            <Icon defaultcolor="currentColor" variant="small">
                              triple-dot
                            </Icon>
                          }
                          pageCount={data2.lastPage}
                          marginPagesDisplayed={true}
                          pageRangeDisplayed={false}
                          onPageChange={(r)=>{
                             let query2 = {...router.query}
                             query2.page = r.selected+1
                             router.push( {pathname:router.pathname,query:query2})
                            }
                        }
                          containerClassName="pagination"
                          subContainerClassName="pages pagination"
                          activeClassName="active"
                          disabledClassName="disabled"
                      />
                    </StyledPagination>
                </FlexBox>
                :
                ""

                }
            </>
        :
            ""
        }


      </>
      }
    </Fragment>
  );
};


export default VendorOrderList;
