"use strict";
exports.id = 9330;
exports.ids = [9330];
exports.modules = {

/***/ 9330:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Container__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(9221);
/* harmony import */ var _grid_Grid__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(8167);
/* harmony import */ var _hidden_Hidden__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3518);
/* harmony import */ var _VendorDashboardNavigation__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(1851);
/* harmony import */ var _AppLayout__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(7198);
/* harmony import */ var _CompanyDashboardNavigation__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(5273);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(1853);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_8__);









const VendorDashboardLayout = ({ title , description , keyword , children  })=>{
    let router = (0,next_router__WEBPACK_IMPORTED_MODULE_8__.useRouter)();
    let pathname = router.pathname;
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_AppLayout__WEBPACK_IMPORTED_MODULE_6__["default"], {
        title: title,
        description: description,
        keyword: keyword,
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_Container__WEBPACK_IMPORTED_MODULE_2__["default"], {
            my: "2rem",
            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_grid_Grid__WEBPACK_IMPORTED_MODULE_3__["default"], {
                container: true,
                spacing: 6,
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_hidden_Hidden__WEBPACK_IMPORTED_MODULE_4__/* ["default"] */ .Z, {
                        as: _grid_Grid__WEBPACK_IMPORTED_MODULE_3__["default"],
                        item: true,
                        lg: 3,
                        xs: 12,
                        down: 1024,
                        children: pathname.startsWith("/company") || pathname === "/faq" || pathname === "/connectUs" || pathname === "/sitemap" || pathname.includes("/carriers") ? /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_CompanyDashboardNavigation__WEBPACK_IMPORTED_MODULE_7__/* ["default"] */ .Z, {
                        }) : /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_VendorDashboardNavigation__WEBPACK_IMPORTED_MODULE_5__/* ["default"] */ .Z, {
                        })
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_grid_Grid__WEBPACK_IMPORTED_MODULE_3__["default"], {
                        item: true,
                        lg: 9,
                        xs: 12,
                        children: children
                    })
                ]
            })
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (VendorDashboardLayout);


/***/ })

};
;