const ShopCard1 = dynamic(()=>import("../../components/shop/ShopCard1"),{ssr:false})
const Box = dynamic(()=>import("../../components/Box"),{ssr:false})
const Carousel = dynamic(()=>import("../carousel/Carousel"),{ssr:false})
const CategorySectionCreator = dynamic(()=>import("../CategorySectionCreator"),{ssr:false})
const Grid = dynamic(()=>import("../../components/grid/Grid"),{ssr:false})
import React, { useEffect, useState } from "react";
import useWindowSize from "../../hooks/useWindowSize";
import {useIntl} from "react-intl";
import { useRouter } from "next/router";
import axios from "axios";
import dynamic from "next/dynamic";

const Section2= () => {
    const [visibleSlides, setVisibleSlides] = useState(3);
    const width = useWindowSize();
    const [shops_list,setshop_list] = useState([])
    let lang = useRouter().locale
    useEffect(()=>{
        axios.get(`/shops/list-home-page/${lang}`)
        .then((res)=>{
            setshop_list(res.data)
        })
        .catch(()=>null)
    },[lang])
    let intl = useIntl()
    useEffect(() => {
        if (width < 500) setVisibleSlides(1);
        else if (width < 650) setVisibleSlides(2);
        else if (width < 1050) setVisibleSlides(2);
        else setVisibleSlides(3);
    }, [width]);
    let left_button_style;
    let right_button_style;
    let shop_style
    if(width < 650) {
        left_button_style = {
        marginLeft:'-20px',
        marginTop:"-10px"
        }
        right_button_style = {
        marginRight:'-20px',
        marginTop:"-10px"
        }
        shop_style = {
            marginLeft:"40px"
        }
    }
    else{
        left_button_style = {

        }
    }
    return (
        <div data-aos="zoom-in">

                    <div
                        style={{textTransform:"capitalize"}}
                    >
                        <CategorySectionCreator
                            iconName="light"
                            title={intl.formatMessage({
                                id:"Shops"
                            })}
                            seeMoreLink="/shopList/page/1"
                        >
                            <Box
                                mt="-0.25rem"
                                mb="0rem"
                            >
                                <Carousel
                                    leftButtonStyle={left_button_style}
                                    rightButtonStyle={right_button_style}
                                    totalSlides={shops_list?.length}
                                    visibleSlides={visibleSlides}
                                    infinite={false}
                                    autoPlay={true}
                                    showArrow={true}
                                    spacing="15px"
                                    interval={4000}
                                >
                                    {shops_list.length !== 0 && shops_list?.map((item, ind) => (
                                        <Grid
                                            item
                                            lg={12}
                                            sm={12}
                                            xs={12}
                                            key={ind}
                                            style={shop_style}
                                        >
                                            <ShopCard1 {...item} />
                                        </Grid>
                                    ))}
                                </Carousel>
                            </Box>
                        </CategorySectionCreator>
                    </div>


        </div>
    );
};

export default Section2;
