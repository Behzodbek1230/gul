"use strict";
exports.id = 4154;
exports.ids = [4154];
exports.modules = {

/***/ 4154:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ icon_Icon)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "react-svg"
var external_react_svg_ = __webpack_require__(7128);
// EXTERNAL MODULE: external "styled-components"
var external_styled_components_ = __webpack_require__(7518);
var external_styled_components_default = /*#__PURE__*/__webpack_require__.n(external_styled_components_);
// EXTERNAL MODULE: external "styled-system"
var external_styled_system_ = __webpack_require__(5834);
// EXTERNAL MODULE: external "@styled-system/css"
var css_ = __webpack_require__(2038);
var css_default = /*#__PURE__*/__webpack_require__.n(css_);
;// CONCATENATED MODULE: ./components/icon/IconStyle.tsx




const StyledIcon = external_styled_components_default()(external_react_svg_.ReactSVG)(({ color , size , transform , defaultcolor  })=>css_default()({
        svg: {
            width: "100%",
            height: "100%",
            transform,
            path: {
                fill: color ? `${color}.main` : defaultcolor
            },
            polyline: {
                color: color ? `${color}.main` : defaultcolor
            },
            polygon: {
                color: color ? `${color}.main` : defaultcolor
            }
        },
        div: {
            display: "flex",
            width: size,
            height: size
        }
    })
, ({ size  })=>(0,external_styled_system_.variant)({
        prop: "variant",
        variants: {
            small: {
                div: {
                    width: size || "1.25rem",
                    height: size || "1.25rem"
                }
            },
            medium: {
                div: {
                    width: size || "1.5rem",
                    height: size || "1.5rem"
                }
            },
            large: {
                div: {
                    width: size || "2rem",
                    height: size || "2rem"
                }
            }
        }
    })
, (0,external_styled_system_.compose)(external_styled_system_.color, external_styled_system_.space));
/* harmony default export */ const IconStyle = (StyledIcon);

;// CONCATENATED MODULE: ./components/icon/Icon.tsx



const Icon = ({ children , ...props })=>{
    return(/*#__PURE__*/ jsx_runtime_.jsx(IconStyle, {
        src: `/assets/images/icons/${children}.svg`,
        fallback: ()=>{
            return(/*#__PURE__*/ jsx_runtime_.jsx("span", {
                children: children === null || children === void 0 ? void 0 : children.trim()
            }));
        },
        ...props
    }));
};
Icon.defaultProps = {
    variant: "medium",
    defaultcolor: "currentColor"
};
/* harmony default export */ const icon_Icon = (Icon);


/***/ })

};
;