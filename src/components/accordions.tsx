// import { Accordion } from "react-bootstrap";
import { styled } from '@mui/material/styles';
// import ArrowForwardIosSharpIcon from '@mui/icons-material/ArrowForwardIosSharp';
import MuiAccordion, { AccordionProps } from '@mui/material/Accordion';
import MuiAccordionSummary, {
  AccordionSummaryProps,
} from '@mui/material/AccordionSummary';
import MuiAccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import React, { useState } from 'react';
import {ArrowDownward, ArrowForward, BusinessCenter} from '@material-ui/icons';
import {FormattedMessage} from "react-intl";
import {Modal, ModalBody, ModalHeader} from "reactstrap";
import VacancyForm from "../components/VacancyForm";


const Accordion = styled((props: AccordionProps) => (
    <MuiAccordion disableGutters elevation={0} square {...props} />
  ))(({  }) => ({
    paddingTop: "6px",
    paddingBottom:"6px",
    marginBottom:"0px",
    // '&:not(:last-child)': {
    //   borderBottom: 0,
    // },
    '&:before': {
      display: 'none',
    },
  }));
  
  const AccordionSummary = styled((props: AccordionSummaryProps) => (
    <MuiAccordionSummary
      {...props}
    />
  ))(({ theme }) => ({
    backgroundColor:
      theme.palette.mode === 'dark'
        ? 'rgba(255, 255, 255, .05)'
        : 'white',
    flexDirection: 'row-reverse',
    '& .MuiAccordionSummary-expandIconWrapper.Mui-expanded': {
      transform: 'rotate(90deg)',
    },
    '& .MuiAccordionSummary-content': {
      marginLeft: "0px",
    },
  }));
  
  const AccordionDetails = styled(MuiAccordionDetails)(({ theme }) => ({
    padding: theme.spacing(2),
    borderTop: '1px solid rgba(0, 0, 0, .125)',
  }));

let Accordions = ({data,accordionClass,carrier=false})=>{
  const [expanded, setExpanded] = useState([]);
  const [modal,setmodal] = useState(false)
  const handleChange  =(panel: string) =>{
      let i:any
      expanded.map((one,ind)=>{
        if(one === panel){
          i =  ind
        }
      })
      let array = [...expanded]
      expanded?.includes(panel) ? delete array[i] : array?.push(panel)
      setExpanded(array);
  };
  
    return( 
        <div>
        {data.map((one,ind)=>
        <Accordion className={accordionClass} expanded={expanded?.includes(`panel${ind}`)} onChange={()=>handleChange(`panel${ind}`)}>
          <AccordionSummary aria-controls="panel1d-content" id="panel1d-header">
            <Typography>
              {carrier ?
                    <div className="d-inline ml-3 mr-2"><BusinessCenter /></div>
                  : <div className="d-inline mr-2">{expanded?.includes(`panel${ind}`) ? <ArrowForward/> : <ArrowDownward/>}</div>
              }
              {one.name}</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography>
                <div className="whitespace-break" dangerouslySetInnerHTML={{__html: one.text}} />
                {carrier ?
                    <button onClick={()=>setmodal(true)} className="btn btn-warning" >
                      <FormattedMessage id="apply_now" />
                    </button>
                :
                  ""}
              <Modal  isOpen={modal} toggle={()=>setmodal(!modal)} className="modal-dialog-centered">
                <ModalHeader toggle={()=>setmodal(!modal)}><FormattedMessage id="apply_now"  /></ModalHeader>
                <ModalBody >
                  <VacancyForm close={(e)=>setmodal(e)} id={one.id} />
                </ModalBody>
              </Modal>
            </Typography>
          </AccordionDetails>
        </Accordion>
       )}
      </div>
    )  
}
export default Accordions;


// <Accordion>
//           {data.map((one,ind)=>{
//               return (  <Accordion.Item className={accordionClass} eventKey={ind}>
//                             <Accordion.Header>{one.name}</Accordion.Header>
//                             <Accordion.Body>
//                                 <div className="whitespace-break" dangerouslySetInnerHTML={{__html: one.text}} />
//                             </Accordion.Body>
//                         </Accordion.Item>
//               )
//           })}
//         </Accordion>
