"use strict";
exports.id = 5129;
exports.ids = [5129];
exports.modules = {

/***/ 5129:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "nX": () => (/* binding */ DashboardNavigationWrapper),
/* harmony export */   "Wt": () => (/* binding */ StyledDashboardNav)
/* harmony export */ });
/* unused harmony export StyledDashboardPageTitle */
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _utils_utils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1972);
/* harmony import */ var _Card__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4660);
/* harmony import */ var _FlexBox__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(5359);
/* harmony import */ var _nav_link_NavLink__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(612);





const DashboardNavigationWrapper = styled_components__WEBPACK_IMPORTED_MODULE_0___default()(_Card__WEBPACK_IMPORTED_MODULE_2__["default"])`
  @media only screen and (max-width: 768px) {
    height: calc(100vh - 64px);
    box-shadow: none;
    overflow-y: auto;
  }
`;
const StyledDashboardNav = styled_components__WEBPACK_IMPORTED_MODULE_0___default()(_nav_link_NavLink__WEBPACK_IMPORTED_MODULE_4__["default"])`
  display: flex;
  justify-content: space-between;
  align-items: center;
  border-left: 4px solid;
  color: ${({ isCurrentPath  })=>isCurrentPath ? (0,_utils_utils__WEBPACK_IMPORTED_MODULE_1__/* .getTheme */ .gh)("colors.primary.main") : "inherit"
};
  border-left-color: ${({ isCurrentPath  })=>isCurrentPath ? (0,_utils_utils__WEBPACK_IMPORTED_MODULE_1__/* .getTheme */ .gh)("colors.primary.main") : "transparent"
};

  .dashboard-nav-icon-holder {
    color: ${(0,_utils_utils__WEBPACK_IMPORTED_MODULE_1__/* .getTheme */ .gh)("colors.gray.600")};
  }

  :hover {
    border-left-color: ${(0,_utils_utils__WEBPACK_IMPORTED_MODULE_1__/* .getTheme */ .gh)("colors.primary.main")};

    .dashboard-nav-icon-holder {
      color: ${(0,_utils_utils__WEBPACK_IMPORTED_MODULE_1__/* .getTheme */ .gh)("colors.primary.main")};
    }
  }
`;
const StyledDashboardPageTitle = (/* unused pure expression or super */ null && (styled(FlexBox)``));


/***/ })

};
;