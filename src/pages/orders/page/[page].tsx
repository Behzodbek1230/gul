import CustomerDashboardLayout from "../../../components/layout/CustomerDashboardLayout";
import CustomerOrderList from "../../../components/orders/CustomerOrderList";
import React from "react";
import { useIntl } from "react-intl";
import { NextSeo } from "next-seo";

const Orders = () => {
  let intl = useIntl()
  return (
  <CustomerDashboardLayout >
    <NextSeo
      title={intl.formatMessage({id:"My Orders"})} 
    />
    <CustomerOrderList />
  </CustomerDashboardLayout>
  );
};


export default Orders;
