import NavbarLayout from "../../../components/layout/NavbarLayout";
import {Card1} from "../../../components/Card1";
import Grid from "../../../components/grid/Grid";
import React, {useEffect, useState} from "react";
import Loading from "../../../components/Loading";
import {NextSeo} from "next-seo";
import axios from "axios";
import {useRouter} from "next/router";

let BlogsByKeyword = () =>{
    let [loading,setloading] = useState(true)
    let [data,setdata] = useState(undefined)
    let router = useRouter()
    let {keyword} = router.query
    let lang = router.locale
    useEffect(()=>{
        if(keyword){
            axios(`/blog/show/${keyword}/${lang}`)
                .then((res)=>{
                    setdata(res.data)
                    setloading(false)
                })
                .catch(()=>{router.push("/404")})
        }
    },[keyword,lang])
   if(loading){
       return(<>
           <NextSeo
               title={data?.title}
               description={data?.text}
               additionalMetaTags={[{
                   name: 'keyword',
                   content:data?.title
               }, ]}

           />
               <Loading />
       </>
       )
   }
   else{
       return(
           <NavbarLayout>
               <NextSeo
                   title={data?.title}
                   description={data?.text}
                   additionalMetaTags={[{
                       name: 'keyword',
                       content:data?.title
                   }, ]}

               />
               <div  style={{color:"#f7961"}} className="mt-4">
                   <Card1 style={{marginLeft:"13px",marginRight:"13px"}}>
                       <Grid  style={{padding:"5px",paddingBottom:"0px",paddingTop:"8px"}}>
                           <h1 className="text-center mb-4 fs-large" >{data.title}</h1>
                           <div style={{width:'100%',height:'400px',overflow:'hidden'}}>
                               <img
                                   style={{width:'100%',height:'100%'}}
                                   src={data?.image}
                                   className="w-100"
                               />
                           </div>
                           <div className="whitespace-break mt-4" dangerouslySetInnerHTML={{__html:data.text}} ></div>
                           <div className="text-right fs-12 " >{data.data_cr}</div>
                       </Grid>
                   </Card1>

               </div>
           </NavbarLayout>
       )
   }
}
export default BlogsByKeyword;
