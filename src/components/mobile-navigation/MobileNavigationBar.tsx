import { Chip } from "../../components/Chip";
import { useAppContext } from "../../contexts/app/AppContext";
import React from "react";
import useWindowSize from "../../hooks/useWindowSize";
import Icon from "../icon/Icon";
import NavLink from "../nav-link/NavLink";
import StyledMobileNavigationBar from "./MobileNavigationBar.style";
import Cookies from  "js-cookie"
import {useIntl} from "react-intl";
import {FormattedMessage} from "react-intl";
import {useSelector} from "react-redux";
const MobileNavigationBar: React.FC = () => {
  const width = useWindowSize();
  const { state } = useAppContext();
  const { cartList } = state.cart;
  const href2 = Cookies.get("isLoggedIn") === "true" ? "/profile/edit" : "/register"
  let intl = useIntl()
  let wishlist = useSelector((state:any)=>state.new.wishlist)
  let isloggedin = Cookies.get("isLoggedIn")
  let user2 = useSelector((state:any)=>state.token.user)
    const list = [
  {
    title: intl.formatMessage({id:"mobile_navigation_home"}),
    icon: "home",
    href: "/",
  },
  {
    title: intl.formatMessage({id:"mobile_navigation_category2"}),
    icon: "category",
    href: "/mobile-category-nav",
  },
  {
    title: intl.formatMessage({id:"cart"}),
    icon: "cart",
    href: "/mobile-cart",
  },
];
  const wishlisthandle = (
      <NavLink
          className="link"
          href="/wish-list"
          key="Wishlist"
      >
          <Icon
              className="icon"
              variant="small"
          >
              heart
          </Icon>
          <FormattedMessage
              id="wishlist"
              defaultMessage="Sevimlilar"
          />
          {isloggedin === "true" && (
              <Chip
                  bg="primary.main"
                  position="absolute"
                  color="primary.text"
                  fontWeight="600"
                  px="0.25rem"
                  top="4px"
                  data-test='mobile_navigation_authenticated_wishlist_count'
                  left="calc(50% + 8px)"
                  className="rounded-circle"
              >
                  {user2?.data?.favoritesCount}
              </Chip>
          )}
          {isloggedin !== "true"  && (
              <Chip
                  bg="primary.main"
                  position="absolute"
                  color="primary.text"
                  fontWeight="600"
                  px="0.25rem"
                  top="4px"
                  data-test='mobile_navigation_unauthenticated_wishlist_count'
                  left="calc(50% + 8px)"
                  className="rounded-circle"
              >
                  {wishlist.length}
              </Chip>
          )}
      </NavLink>

  );
  return (
    width <= 900 && (
      <StyledMobileNavigationBar>
        {list.map((item) => (
          <NavLink className="link" href={item.href} key={item.title}>
            <Icon className="icon" variant="small">
              {item.icon}
            </Icon>
            {item.title}
            {item.icon === "cart" && !!cartList.length && (
              <Chip
                bg="primary.main"
                position="absolute"
                color="primary.text"
                fontWeight="600"
                px="0.25rem"
                top="4px"
                left="calc(50% + 8px)"
                className="rounded-circle"
              >
                {cartList.length}
              </Chip>
            )}
          </NavLink>
        ))}
          {wishlisthandle}
          <NavLink
              className="link"
              href={href2}
              key="Account"
          >
            <Icon
                className="icon"
                variant="small"
            >
              user-2
            </Icon>
              <FormattedMessage
                  id="mobile_navigation_account"
                  defaultMessage="Akkount"
              />
          </NavLink>
      </StyledMobileNavigationBar>
    )
  );
};



export default MobileNavigationBar;
