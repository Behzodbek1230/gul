import React, {useEffect, useState} from "react";
import Button from "../components/buttons/Button";
import FlexBox from "../components/FlexBox";
import Grid from "../components/grid/Grid";
import ProductCard1 from "../components/product-cards/ProductCard1";
import {useDispatch, useSelector} from "react-redux";
import axios from "axios";
import {StyledPagination} from "../components/pagination/PaginationStyle";
import Icon from "../components/icon/Icon";
import get_shop_products from "../../Redux/Actions/get_product_shop";
import ReactPaginate from "react-paginate";
import { useRouter } from "next/router";
import Cookies from "js-cookie"
import {FormattedMessage, useIntl} from "react-intl";
import { NextSeo } from "next-seo";
import NavbarLayout from "../components/layout/NavbarLayout";
import {H1} from "../components/Typography";
const WishList = () => {
    let intl = useIntl()
    const dispatch = useDispatch()
    const router = useRouter()
    const wishlistProducts = useSelector((state:any)=>state.new.wishlist)
    const loggedIn = Cookies.get("isLoggedIn")
    const [wishlist,setdata2] = useState({datas:{data:[],last_page:1},lastPage:1})
    let {page} = router.query
    let user = useSelector((state:any)=>state.token.user)
    let currency_id = Cookies.get("currency_id")
    useEffect(() => {
      if(loggedIn === "true"){
          let lang = Cookies.get("lang")
          let currency_text = typeof currency_id === "undefined" ? "" : `&currency=${currency_id}`;
          axios(`/flowers/get-favorites/${lang}?page=${page||1}${currency_text}`)
              .then(response=>{
                  dispatch(get_shop_products(response.data))
                  setdata2(response.data)
              })
              .catch(()=>{
                  return null;
              })
      }
      else{
          setdata2({datas:{data:wishlistProducts,last_page:1},lastPage:1})
      }
    }, [page,user?.data?.favoritesCount]);

  return (
    <NavbarLayout>
        <NextSeo
            title={intl.formatMessage({id:"wishlist"})}
            description={intl.formatMessage({id:"wishlist"})}
            additionalMetaTags={[{
            name: 'keyword',
            content: intl.formatMessage({id:"wishlist"})
            }, 
        ]}
        />
        <div>
        <FlexBox mb="11px" alignItems="center">
            <Icon
                color="primary"
                mr="0.5rem"
            >
                heart-filled
            </Icon>
            <H1>
                <FormattedMessage
                    id="wishlist"
                    defaultMessage="wishlist"
                />
            </H1>
        </FlexBox>



                {wishlist?.datas?.data.length !==0 ?
                <>
                <Grid container spacing={6}>
                    {wishlist?.datas?.data?.map((item) => (
                    <Grid item lg={3} sm={3} xs={6} key={item}>
                    <ProductCard1
                        is_favourite={item.is_favorites}
                        title={item.name}
                        imgUrl={item.image}
                        price={item.price}
                        id={item.keyword}
                        rating={item.ratings}
                        category_keyword = {item.categoryKeyword}
                        shopName={item.shopName}
                        shopKeyword={item?.shopKeyword}
                        />
                    </Grid>
                    ))}
                </Grid>
                <br/>
                {wishlist?.datas?.last_page !==1
                    ?
                    <FlexBox>
                    <StyledPagination >
                        <ReactPaginate
                            initialPage={typeof page !== "undefined" ? parseInt(page.toString())-1 : 0 }
                            previousLabel={
                                <Button
                                    style={{cursor: "pointer"}}
                                    className="control-button"
                                    color="primary"
                                    overflow="hidden"
                                    height="auto"
                                    padding="6px"
                                    borderRadius="50%"
                                >
                                    <Icon defaultcolor="currentColor" variant="small">
                                        chevron-left
                                    </Icon>
                                </Button>

                            }
                            nextLabel={
                                <Button
                                    style={{cursor: "pointer"}}
                                    className="control-button"
                                    color="primary"
                                    overflow="hidden"
                                    height="auto"
                                    padding="6px"
                                    borderRadius="50%"
                                >
                                    <Icon defaultcolor="currentColor" variant="small">
                                        chevron-right
                                    </Icon>
                                </Button>
                            }
                            breakLabel={
                                <Icon defaultcolor="currentColor" variant="small">
                                    triple-dot
                                </Icon>
                            }
                            pageCount={wishlist?.lastPage}
                            marginPagesDisplayed={true}
                            pageRangeDisplayed={false}
                            onPageChange={(r)=>{
                                let query2 = {...router.query}
                                query2.page = r.selected+1
                                router.push( {pathname:router.pathname,query:query2})
                                }
                            }
                            containerClassName="pagination"
                            subContainerClassName="pages pagination"
                            activeClassName="active"
                            disabledClassName="disabled"
                        />
                    </StyledPagination>
                    </FlexBox>
                :
                ""
                }
                </>
            :
                intl.formatMessage({id:"empty"})}


        </div>
    </NavbarLayout>
  );
};


export default WishList;
