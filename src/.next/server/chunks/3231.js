"use strict";
exports.id = 3231;
exports.ids = [3231];
exports.modules = {

/***/ 3231:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ accordions)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "@mui/material/styles"
var styles_ = __webpack_require__(8442);
// EXTERNAL MODULE: external "@mui/material/Accordion"
var Accordion_ = __webpack_require__(9409);
var Accordion_default = /*#__PURE__*/__webpack_require__.n(Accordion_);
// EXTERNAL MODULE: external "@mui/material/AccordionSummary"
var AccordionSummary_ = __webpack_require__(4604);
var AccordionSummary_default = /*#__PURE__*/__webpack_require__.n(AccordionSummary_);
// EXTERNAL MODULE: external "@mui/material/AccordionDetails"
var AccordionDetails_ = __webpack_require__(8279);
var AccordionDetails_default = /*#__PURE__*/__webpack_require__.n(AccordionDetails_);
// EXTERNAL MODULE: external "@mui/material/Typography"
var Typography_ = __webpack_require__(7163);
var Typography_default = /*#__PURE__*/__webpack_require__.n(Typography_);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "@material-ui/icons"
var icons_ = __webpack_require__(2105);
// EXTERNAL MODULE: external "react-intl"
var external_react_intl_ = __webpack_require__(3126);
// EXTERNAL MODULE: external "reactstrap"
var external_reactstrap_ = __webpack_require__(6981);
// EXTERNAL MODULE: ./components/Box.tsx
var Box = __webpack_require__(4875);
// EXTERNAL MODULE: external "react-input-mask"
var external_react_input_mask_ = __webpack_require__(4648);
var external_react_input_mask_default = /*#__PURE__*/__webpack_require__.n(external_react_input_mask_);
// EXTERNAL MODULE: external "react-dropzone"
var external_react_dropzone_ = __webpack_require__(6358);
// EXTERNAL MODULE: ./components/buttons/Button.tsx
var Button = __webpack_require__(2389);
// EXTERNAL MODULE: ./components/Divider.tsx
var Divider = __webpack_require__(7650);
// EXTERNAL MODULE: ./components/Typography.tsx
var Typography = __webpack_require__(9374);
;// CONCATENATED MODULE: ./components/DefaultDropzone.tsx








const DropZoneDefault = ({ onChange  })=>{
    const onDrop = (0,external_react_.useCallback)((acceptedFiles)=>{
        if (onChange) onChange(acceptedFiles);
    }, []);
    const { getRootProps , getInputProps , isDragActive  } = (0,external_react_dropzone_.useDropzone)({
        onDrop,
        multiple: true,
        accept: ".jpeg,.jpg,.png,.gif,.doc,.docx,.pdf",
        maxFiles: 10
    });
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(Box["default"], {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        minHeight: "200px",
        border: "1px dashed",
        borderColor: "gray.400",
        borderRadius: "10px",
        bg: isDragActive && "gray.200",
        transition: "all 250ms ease-in-out",
        style: {
            outline: "none"
        },
        ...getRootProps(),
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx("input", {
                ...getInputProps()
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(Typography.H5, {
                mb: "18px",
                ml: 3,
                color: "text.muted",
                children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                    id: "drag_drop2"
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(Divider/* default */.Z, {
                width: "200px",
                mx: "auto"
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(Typography["default"], {
                color: "text.muted",
                bg: isDragActive ? "gray.200" : "body.paper",
                lineHeight: "1",
                px: "1rem",
                mt: "-10px",
                mb: "18px"
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(Button/* default */.Z, {
                color: "primary",
                bg: "primary.light",
                px: "2rem",
                mb: "22px",
                type: "button",
                children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                    id: "select_files"
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(Typography.Small, {
                color: "text.muted",
                children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                    id: "upload_images2"
                })
            })
        ]
    }));
};
/* harmony default export */ const DefaultDropzone = (DropZoneDefault);

// EXTERNAL MODULE: external "axios"
var external_axios_ = __webpack_require__(2167);
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_);
;// CONCATENATED MODULE: ./components/VacancyForm.tsx







let VacancyForm = ({ id , close  })=>{
    const { 0: file , 1: setfile  } = (0,external_react_.useState)([]);
    const { 0: name , 1: setname  } = (0,external_react_.useState)("");
    const { 0: phone , 1: setphone  } = (0,external_react_.useState)("");
    const { 0: msg , 1: setmsg  } = (0,external_react_.useState)("");
    const { 0: err , 1: seterr  } = (0,external_react_.useState)("");
    let intl = (0,external_react_intl_.useIntl)();
    const handleSubmit = (event)=>{
        event.preventDefault();
        const data = new FormData();
        data.append("name", name);
        data.append("phone", phone);
        data.append("description", msg);
        data.append("file", file[0]);
        data.append("vacancy_id", id);
        if ((file === null || file === void 0 ? void 0 : file.length) === 0 || typeof file === "undefined") {
            seterr(intl.formatMessage({
                id: "select_files"
            }));
        } else {
            external_axios_default()({
                method: "POST",
                url: `/vacancy/store`,
                data: data
            }).then(()=>{
                seterr(intl.formatMessage({
                    id: "corporate_send_sucess"
                }));
                setTimeout(()=>{
                    close(false);
                }, 2000);
            }).catch(()=>seterr(intl.formatMessage({
                    id: "error_booking"
                }))
            );
        }
    };
    // @ts-ignore
    return(/*#__PURE__*/ jsx_runtime_.jsx(Box["default"], {
        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("form", {
                onSubmit: handleSubmit,
                encType: "multipart/form-data",
                className: "row ml-0 pl-0",
                children: [
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: "col-md-12",
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                                    id: "full_name",
                                    defaultMessage: "Sizning smsingiz"
                                })
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                className: "form-control",
                                required: true,
                                value: name,
                                onChange: (r)=>setname(r.target.value)
                            })
                        ]
                    }),
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: "col-12",
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                className: "mt-1",
                                children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                                    id: "phone_number",
                                    defaultMessage: ""
                                })
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx((external_react_input_mask_default()), {
                                mask: "+\\9\\9\\8-99-999-99-99",
                                onChange: (e)=>setphone(e.target.value)
                                ,
                                children: ()=>/*#__PURE__*/ jsx_runtime_.jsx("input", {
                                        required: true,
                                        type: "text",
                                        className: "form-control",
                                        placeholder: "+998-__-___-__-__",
                                        name: "number"
                                    })
                            })
                        ]
                    }),
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: "col-md-12",
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                className: "mt-1 mb-0",
                                children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                                    id: "Your Message",
                                    defaultMessage: "Sizning xabaringiz"
                                })
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("textarea", {
                                className: "form-control",
                                onChange: (r)=>setmsg(r.target.value)
                                ,
                                required: true
                            })
                        ]
                    }),
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: "col-md-12 h-100",
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                className: "mt-1",
                                children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                                    id: "Any Document",
                                    defaultMessage: "Biror Dokument"
                                })
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx(DefaultDropzone, {
                                onChange: (files)=>setfile(files)
                            }),
                            file.length === 0 || typeof file === "undefined" ? "" : /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                className: "text-primary",
                                children: file[0].name
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("br", {
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                className: "btn btn-primary col-md-12 mt-3",
                                children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                                    id: "submit"
                                })
                            }),
                            err === "" ? "" : /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                className: "text-danger",
                                children: err
                            })
                        ]
                    })
                ]
            })
        })
    }));
};
/* harmony default export */ const components_VacancyForm = (VacancyForm);

;// CONCATENATED MODULE: ./components/accordions.tsx

// import { Accordion } from "react-bootstrap";

// import ArrowForwardIosSharpIcon from '@mui/icons-material/ArrowForwardIosSharp';









const Accordion = (0,styles_.styled)((props)=>/*#__PURE__*/ jsx_runtime_.jsx((Accordion_default()), {
        disableGutters: true,
        elevation: 0,
        square: true,
        ...props
    })
)(({})=>({
        paddingTop: "6px",
        paddingBottom: "6px",
        marginBottom: "0px",
        // '&:not(:last-child)': {
        //   borderBottom: 0,
        // },
        '&:before': {
            display: 'none'
        }
    })
);
const AccordionSummary = (0,styles_.styled)((props)=>/*#__PURE__*/ jsx_runtime_.jsx((AccordionSummary_default()), {
        ...props
    })
)(({ theme  })=>({
        backgroundColor: theme.palette.mode === 'dark' ? 'rgba(255, 255, 255, .05)' : 'white',
        flexDirection: 'row-reverse',
        '& .MuiAccordionSummary-expandIconWrapper.Mui-expanded': {
            transform: 'rotate(90deg)'
        },
        '& .MuiAccordionSummary-content': {
            marginLeft: "0px"
        }
    })
);
const AccordionDetails = (0,styles_.styled)((AccordionDetails_default()))(({ theme  })=>({
        padding: theme.spacing(2),
        borderTop: '1px solid rgba(0, 0, 0, .125)'
    })
);
let Accordions = ({ data , accordionClass , carrier =false  })=>{
    const { 0: expanded , 1: setExpanded  } = (0,external_react_.useState)([]);
    const { 0: modal , 1: setmodal  } = (0,external_react_.useState)(false);
    const handleChange = (panel)=>{
        let i;
        expanded.map((one, ind)=>{
            if (one === panel) {
                i = ind;
            }
        });
        let array = [
            ...expanded
        ];
        (expanded === null || expanded === void 0 ? void 0 : expanded.includes(panel)) ? delete array[i] : array === null || array === void 0 ? void 0 : array.push(panel);
        setExpanded(array);
    };
    return(/*#__PURE__*/ jsx_runtime_.jsx("div", {
        children: data.map((one, ind)=>{
            return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(Accordion, {
                className: accordionClass,
                expanded: expanded === null || expanded === void 0 ? void 0 : expanded.includes(`panel${ind}`),
                onChange: ()=>handleChange(`panel${ind}`)
                ,
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx(AccordionSummary, {
                        "aria-controls": "panel1d-content",
                        id: "panel1d-header",
                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)((Typography_default()), {
                            children: [
                                carrier ? /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                    className: "d-inline ml-3 mr-2",
                                    children: /*#__PURE__*/ jsx_runtime_.jsx(icons_.BusinessCenter, {
                                    })
                                }) : /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                    className: "d-inline mr-2",
                                    children: (expanded === null || expanded === void 0 ? void 0 : expanded.includes(`panel${ind}`)) ? /*#__PURE__*/ jsx_runtime_.jsx(icons_.ArrowForward, {
                                    }) : /*#__PURE__*/ jsx_runtime_.jsx(icons_.ArrowDownward, {
                                    })
                                }),
                                one.name
                            ]
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx(AccordionDetails, {
                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)((Typography_default()), {
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                    className: "whitespace-break",
                                    dangerouslySetInnerHTML: {
                                        __html: one.text
                                    }
                                }),
                                carrier ? /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                    onClick: ()=>setmodal(true)
                                    ,
                                    className: "btn btn-warning",
                                    children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                                        id: "apply_now"
                                    })
                                }) : "",
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)(external_reactstrap_.Modal, {
                                    isOpen: modal,
                                    toggle: ()=>setmodal(!modal)
                                    ,
                                    className: "modal-dialog-centered",
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx(external_reactstrap_.ModalHeader, {
                                            toggle: ()=>setmodal(!modal)
                                            ,
                                            children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                                                id: "apply_now"
                                            })
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx(external_reactstrap_.ModalBody, {
                                            children: /*#__PURE__*/ jsx_runtime_.jsx(components_VacancyForm, {
                                                close: (e)=>setmodal(e)
                                                ,
                                                id: one.id
                                            })
                                        })
                                    ]
                                })
                            ]
                        })
                    })
                ]
            }));
        })
    }));
};
/* harmony default export */ const accordions = (Accordions); // <Accordion>
 //           {data.map((one,ind)=>{
 //               return (  <Accordion.Item className={accordionClass} eventKey={ind}>
 //                             <Accordion.Header>{one.name}</Accordion.Header>
 //                             <Accordion.Body>
 //                                 <div className="whitespace-break" dangerouslySetInnerHTML={{__html: one.text}} />
 //                             </Accordion.Body>
 //                         </Accordion.Item>
 //               )
 //           })}
 //         </Accordion>


/***/ })

};
;