
import React from "react";
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Link from "next/link"
import Typography from '@mui/material/Typography';
export interface BlogCardProps {
    id?:string
    short_text?: string;
    title?: string;
    slug?:string
    image?:string,
    data_cr?:string,
    ind:string

}

const BlogCard: React.FC<BlogCardProps> = ({
    short_text,
    title,
    slug,
    image,
    data_cr,
     }) => {
    return (
        <>
            <Card  className="mt-2" sx={{ maxWidth: 345 }}>
                <Link href={`/blogs/${slug}`}>
                    <CardMedia
                        className="cursor-pointer2"
                        component="img"
                        alt="green iguana"
                        height="200"
                        image={image}
                    />
                </Link>
                <CardContent className="pb-0 blog_short_text">
                    <Link href={`/blogs/${slug}`}>
                        <Typography fontSize="16px" maxHeight="75px" overflow="hidden" className="text-center cursor-pointer2" gutterBottom variant="h6" component="div">
                            {title.length > 118 ? title.slice(0,115)+ "..." : title}
                        </Typography>
                    </Link>
                    <Typography fontSize="13px" className={title.length/38 >=3 ? "blog_short_text5 text-center " : `blog_short_text${Math.floor(title.length/38)} text-center `} variant="body2" color="text.secondary">
                        {short_text}
                    </Typography>
                </CardContent>
                <CardActions>
                    <div className="ml-auto fs-12">{data_cr}</div>

                </CardActions>
            </Card>



            {/*<FlexBox pl="30px" pr="18px" justifyContent="space-between" height="170px">*/}
            {/*    <div style={{position:"relative"}}>*/}
            {/*        <div className="my-2">*/}
            {/*            <H3 className="d-flex justify-content-center" fontWeight="600" mb="8px" style={{width:'370px',overflow:"hidden",textOverflow:"ellipsis",paddingRight:"50px"}}>*/}
            {/*                {title}*/}
            {/*            </H3>*/}
            {/*            <div className="mb-2 justify-content-center">*/}
            {/*                <div className="d-flex">*/}
            {/*                    <Icon size="14px" className="align-items-end blog_clock">*/}
            {/*                        clock-circular-outline*/}
            {/*                    </Icon>*/}
            {/*                    <div className="d-inline ml-2 align-items-center">*/}
            {/*                        {data_cr.toLocaleString()}*/}
            {/*                    </div>*/}
            {/*                </div>*/}


            {/*            </div>*/}
            {/*            <SemiSpan className="d-flex ">*/}
            {/*                {short_text}*/}
            {/*            </SemiSpan>*/}
            {/*        </div>*/}
            {/*        <label*/}
            {/*            className="blog_visit_style"*/}
            {/*        >*/}
            {/*            <FormattedMessage*/}
            {/*                id="full"*/}
            {/*            />*/}
            {/*        </label>*/}
            {/*    </div>*/}
            {/*    <a>*/}
            {/*        <IconButton size="small" my="0.25rem">*/}
            {/*            <Icon defaultcolor="auto">arrow-long-right</Icon>*/}
            {/*        </IconButton>*/}
            {/*    </a>*/}
            {/*</FlexBox>*/}
        </>
    );
};

export default BlogCard;
