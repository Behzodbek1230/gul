"use strict";
exports.id = 9868;
exports.ids = [9868];
exports.modules = {

/***/ 9868:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ carousel_Carousel)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "clsx"
var external_clsx_ = __webpack_require__(8103);
var external_clsx_default = /*#__PURE__*/__webpack_require__.n(external_clsx_);
// EXTERNAL MODULE: external "pure-react-carousel"
var external_pure_react_carousel_ = __webpack_require__(2947);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);
// EXTERNAL MODULE: ./components/buttons/IconButton.tsx
var IconButton = __webpack_require__(8661);
// EXTERNAL MODULE: ./components/icon/Icon.tsx + 1 modules
var Icon = __webpack_require__(4154);
// EXTERNAL MODULE: ./utils/constants.ts
var constants = __webpack_require__(9758);
// EXTERNAL MODULE: external "styled-components"
var external_styled_components_ = __webpack_require__(7518);
var external_styled_components_default = /*#__PURE__*/__webpack_require__.n(external_styled_components_);
// EXTERNAL MODULE: ./utils/utils.ts
var utils = __webpack_require__(1972);
;// CONCATENATED MODULE: ./components/carousel/CarouselStyle.tsx





const StyledCarousel = external_styled_components_default()(({ spacing , showDots , showArrowOnHover , dotGroupMarginTop , dotColor , ...props })=>/*#__PURE__*/ jsx_runtime_.jsx(external_pure_react_carousel_.CarouselProvider, {
        ...props
    })
)`
  position: relative;
  min-width: 0px;
  .custom-slider {
    margin-left: calc(-1 * ${({ spacing  })=>spacing || "0px"
} / 2);
    margin-right: calc(-1 * ${({ spacing  })=>spacing || "0px"
} / 2);
  }

  .carousel__inner-slide {
    margin: auto;
    width: calc(100% - ${({ spacing  })=>spacing || "0px"
});
  }

  .arrow-button {
    position: absolute;
    top: calc(
      40% - ${(props)=>props.showDots ? props.dotGroupMarginTop : "0px"
}
    );
    transform: translateY(-50%);
    box-shadow: 0px 10px 30px rgba(0, 0, 0, 0.1);
  }

  .right-arrow-class {
    right: -22px;
  }

  .left-arrow-class {
    left: -22px;
  }

  ${(props)=>props.showArrowOnHover ? `
  [class*="arrow-class"] {
    display: none;
  }

  :hover {
    [class*="arrow-class"] {
      display: unset;
    }
  }

  @media only screen and (max-width: ${constants/* deviceSize.sm */.J.sm}px) {
    [class*="arrow-class"] {
      display: block;
    }
  }
  ` : ""
}

  @media only screen and (max-width: 1330px) {
    .right-arrow-class {
      right: 0px;
    }
    .left-arrow-class {
      left: 0px;
    }
  }

  .custom-dot {
    display: flex;
    justify-content: center;
    margin-top: ${(props)=>props.dotGroupMarginTop || "0px"
};
  }

  .dot {
    position: relative;
    height: 16px;
    width: 16px;
    border-radius: 300px;
    margin: 0.25rem;
    cursor: pointer;
    border: 1px solid
      ${({ dotColor  })=>dotColor || (0,utils/* getTheme */.gh)("colors.secondary.main")
};
  }
  .dot:after {
    position: absolute;
    content: " ";
    height: 9px;
    width: 9px;
    top: 50%;
    left: 50%;
    border-radius: 300px;
    transform: translate(-50%, -50%) scaleX(0);
    background: ${({ dotColor  })=>dotColor || (0,utils/* getTheme */.gh)("colors.secondary.main")
};
  }
  .dot-active:after {
    transform: translate(-50%, -50%) scaleX(1);
  }
`;

;// CONCATENATED MODULE: ./components/carousel/Carousel.tsx








const Carousel = ({ children , naturalSlideWidth , naturalSlideHeight , totalSlides , visibleSlides , currentSlide , isIntrinsicHeight , hasMasterSpinner , infinite , autoPlay , step , interval , showDots , showArrow , showArrowOnHover , dotClass , dotColor , dotGroupMarginTop , spacing , arrowButtonClass , arrowButtonColor , leftButtonClass , rightButtonClass , leftButtonStyle , rightButtonStyle ,  })=>{
    let inputRef = (0,external_react_.useRef)(null);
    let InputRefFuncton = ()=>{
        inputRef.current.focus;
    };
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(StyledCarousel, {
        naturalSlideWidth: naturalSlideWidth,
        naturalSlideHeight: naturalSlideHeight,
        totalSlides: totalSlides,
        visibleSlides: visibleSlides,
        isIntrinsicHeight: isIntrinsicHeight,
        hasMasterSpinner: hasMasterSpinner,
        infinite: infinite,
        isPlaying: autoPlay,
        step: step,
        interval: interval,
        dotColor: dotColor,
        dotGroupMarginTop: dotGroupMarginTop,
        spacing: spacing,
        showDots: showDots,
        currentSlide: currentSlide,
        showArrowOnHover: showArrowOnHover,
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx(external_pure_react_carousel_.Slider, {
                onMouseLeave: ()=>InputRefFuncton()
                ,
                ref: inputRef,
                className: "custom-slider",
                children: external_react_default().Children.map(children, (child, ind)=>/*#__PURE__*/ jsx_runtime_.jsx(external_pure_react_carousel_.Slide, {
                        index: ind,
                        children: child
                    }, ind)
                )
            }),
            showDots && /*#__PURE__*/ jsx_runtime_.jsx(external_pure_react_carousel_.DotGroup, {
                className: `custom-dot ${dotClass}`,
                renderDots: (props)=>renderDots({
                        ...props,
                        step
                    })
            }),
            showArrow && /*#__PURE__*/ (0,jsx_runtime_.jsxs)(external_react_.Fragment, {
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx(IconButton/* default */.Z, {
                        className: `arrow-button left-arrow-class ${arrowButtonClass} ${leftButtonClass}`,
                        as: external_pure_react_carousel_.ButtonBack,
                        variant: "contained",
                        color: arrowButtonColor,
                        style: leftButtonStyle || {
                        },
                        children: /*#__PURE__*/ jsx_runtime_.jsx(Icon["default"], {
                            variant: "small",
                            defaultcolor: "currentColor",
                            children: "arrow-left"
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx(IconButton/* default */.Z, {
                        className: `arrow-button right-arrow-class ${arrowButtonClass} ${rightButtonClass}`,
                        as: external_pure_react_carousel_.ButtonNext,
                        variant: "contained",
                        color: arrowButtonColor,
                        style: rightButtonStyle || {
                        },
                        children: /*#__PURE__*/ jsx_runtime_.jsx(Icon["default"], {
                            variant: "small",
                            defaultcolor: "currentColor",
                            children: "arrow-right"
                        })
                    })
                ]
            })
        ]
    }));
};
const renderDots = ({ step , currentSlide , visibleSlides , totalSlides , carouselStore ,  })=>{
    const dots = [];
    const total = totalSlides - visibleSlides + 1;
    const handleCarouselStateChange = (i)=>{
        carouselStore.setStoreState({
            currentSlide: i,
            autoPlay: false
        });
    };
    for(let i1 = 0; i1 < total; i1 += step){
        dots.push(/*#__PURE__*/ jsx_runtime_.jsx("div", {
            className: external_clsx_default()({
                dot: true,
                "dot-active": currentSlide === i1
            }),
            onClick: ()=>handleCarouselStateChange(i1)
        }, i1));
        if (total - i1 - 1 < step && total - i1 - 1 !== 0) {
            dots.push(/*#__PURE__*/ jsx_runtime_.jsx("div", {
                className: external_clsx_default()({
                    dot: true,
                    "dot-active": currentSlide === totalSlides - visibleSlides
                }),
                onClick: ()=>carouselStore.setStoreState({
                        currentSlide: totalSlides - visibleSlides,
                        autoPlay: false
                    })
            }, i1 + total));
        }
    }
    return dots;
};
Carousel.defaultProps = {
    naturalSlideWidth: 100,
    naturalSlideHeight: 125,
    totalSlides: 10,
    visibleSlides: 5,
    isIntrinsicHeight: true,
    hasMasterSpinner: false,
    infinite: false,
    autoPlay: false,
    step: 1,
    interval: 2000,
    showDots: false,
    showArrow: true,
    dotGroupMarginTop: "2rem",
    spacing: "1.5rem",
    arrowButtonColor: "secondary"
};
/* harmony default export */ const carousel_Carousel = (Carousel);


/***/ })

};
;