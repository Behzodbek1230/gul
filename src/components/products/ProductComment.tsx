import React from "react";
import Avatar from "../avatar/Avatar";
import Box from "../Box";
import FlexBox from "../FlexBox";
import Rating from "../rating/Rating";
import { H5, H6, Paragraph, SemiSpan } from "../Typography";

export interface ProductCommentProps {
  id?:any,
  name;
  imgUrl: string;
  rating: number;
  date: string;
  comment: any;
  is_shop?:boolean,
  extended?:boolean,
  reviews?:any,
  setreviews?:any
}

const ProductComment: React.FC<ProductCommentProps> = ({
  name,
  imgUrl,
  rating,
  date,
  comment,
  is_shop,
}) => {
  return (
   <>
       <Box mb="0px"  maxWidth="100vw">
        <FlexBox alignItems="center" mb="0.5rem">
          <Avatar size={35} src={imgUrl} />
          <Box ml="1rem">
            <H5 fontSize={is_shop  ? 13 : 15} mb="4px">{name}</H5>
            <FlexBox alignItems="center">
              <Rating value={rating} color="warn" readonly />
              <H6 fontSize={is_shop && 12} mx="10px">{rating}</H6>
              <SemiSpan fontSize={is_shop && 12}>{date}</SemiSpan>
            </FlexBox>
          </Box>
        </FlexBox>

        <Paragraph className="d-inline"  fontSize={is_shop && 12} mb="0.5rem" color="gray.700"> <div dangerouslySetInnerHTML={{__html:comment}}/></Paragraph>
      </Box>
  </>
  );
};

export default ProductComment;
