import Box from "../../../components/Box";
import FlexBox from "../../../components/FlexBox";
import NavbarLayout from "../../../components/layout/NavbarLayout";
import ProductIntro from "../../../components/products/ProductIntro";
import ProductReview from "../../../components/products/ProductReview";
import RelatedProducts from "../../../components/products/RelatedProducts";
import { H5 } from "../../../components/Typography";
import React, {useCallback, useEffect, useState} from "react";
import axios from "axios";
import {useDispatch, useSelector} from "react-redux";
import get_one_product_info from "../../../../Redux/Actions/get_one_product_info";
import {FormattedMessage} from "react-intl";
import Cookies from "js-cookie"
import {useRouter} from "next/router";
import { BASE_URL } from "../../../components/Variables";
import { NextSeo } from "next-seo";
import Breadcrumbs from "../../../components/breadcrumb";
import {Helmet} from "react-helmet"

const ProductDetails = ({info2}) => {
  let router = useRouter()
  let {keyword} = router.query
  let lang = router.locale
  const dispatch = useDispatch()
  const [flower,setflower] = useState(info2)
  console.log(info2)
  let info = useSelector((state:any)=>state.new.one_product_info)
  useEffect(()=>{
    dispatch(get_one_product_info(info2))
  },[])
  let current_currency = useSelector((state:any)=>state.token.current_currency)
  let InfoChange = useCallback(()=>{
          let currency_id = Cookies.get("currency_id");
          let currency_text = typeof currency_id !== "undefined" ?  `?currency=${currency_id}` : ""
          axios(`/flowers/show/${keyword}/${lang}${currency_text}`)
              .then(res=>{
                  setflower(res.data)
                  dispatch(get_one_product_info(res.data))
              })
              .catch(()=>{
                  return null;
              })
      },
      [current_currency,lang,keyword])
  useEffect(() => {
        InfoChange()
  }, [current_currency,lang,keyword]);
  const [selectedOption, setSelectedOption] = useState("review");

  const handleOptionClick = (opt) => () => {
    setSelectedOption(opt);
  };
  let ratingArray = () =>{
      let array=[]
      flower?.data?.ratingList?.map(one=>{
          array?.push({
              "@type": "Review",
              "name":'Product Review',
              "author": {
                  "@type": "Person",
                  "name": one?.userFio,
              },
              "datePublished": one?.date,
              "reviewBody" : one.comment,
              "reviewRating": {
                  "@type": "Rating",
                  "ratingValue" : one.ball,
                  "worstRating" : "0",
                  "bestRating" : "5"
              }
          })
      })
      return array
  }

  return (
    <NavbarLayout >
        <Helmet>
        <script type="application/ld+json">
            {JSON.stringify( {
                "@contexts": "http://schema.org/",
                "@type": "Product",
                "name": info2?.data?.name,
                "image": info2?.data?.image?.length >= 1 ?
                    info2?.data?.image[0]
                    :
                    "https://api.dana.uz/storage/images/noimg.jpg",
                "description": info2?.data?.description,
                "offers": {
                    "@type": "Offer",
                    "priceCurrency": "UZS",
                    "price": info2?.data?.priceFlowers,
                    "availability": "https://schema.org/InStock",
                    "itemCondition": "https://schema.org/NewCondition",
                },
                "aggregateRating": {
                    "@type": "AggregateRating",
                    "ratingValue" : info2?.data?.rating,
                },
                "review": ratingArray()
            })}
        </script>
        </Helmet>
      <NextSeo
        title={flower?.seo?.view_mtitle }
        description={flower?.seo?.view_mdescription}
        additionalMetaTags={[{
          name: 'keyword',
          content:flower?.seo?.view_mkeywords
        }, ]}
         openGraph={{
               type:"product",
               title: info2?.seo?.view_share_title ,
               site_name: info2?.seo?.view_share_sitename,
               description: info2?.seo?.view_share_description,
               images:[
                 {
                   url:info2?.data?.image[0],
                   alt:info2?.data?.name,
                   width:400,
                   height:400
                 }
               ]
             }}
      />
      <div>

        <Breadcrumbs  place={flower.data.bredcrumbs}/>
        <ProductIntro
            rating={flower?.data?.rating}
            shopName={flower?.data?.shop_name}
            imgUrl={flower?.data?.image?.length >= 1 ?
                  flower?.data?.image
                :
                  ["https://api.dana.uz/storage/images/noimg.jpg",]
            }
            title={flower?.data?.name}
            price={flower?.data?.price}
            id={flower?.data?.keyword}
            keyword2={flower?.data?.shop_keyword}
            categoryKeyword={flower?.data?.categoryKeyword}
            deliveryTime = {flower?.data?.shopDeliveryTime}
        />

        <FlexBox
          id="scroll_to_detail"
          borderBottom="1px solid"
          borderColor="gray.400"
          mt="20px"
          mb="26px"
        >
          <H5
            className="cursor-pointer"
            p="4px 10px"
            color={selectedOption === "review" ? "primary.main" : "text.muted"}
            onClick={handleOptionClick("review")}
            borderBottom={selectedOption === "review" && "2px solid"}
            borderColor="primary.main"
          >
            <FormattedMessage id="Review" />
          </H5>
          {/*  <H5*/}
          {/*  className="cursor-pointer"*/}
          {/*  p="4px 10px"*/}
          {/*  color={selectedOption === "contact" ? "primary.main" : "text.muted"}*/}
          {/*  onClick={handleOptionClick("contact")}*/}
          {/*  borderBottom={selectedOption === "contact" && "2px solid"}*/}
          {/*  borderColor="primary.main"*/}
          {/*>*/}
          {/*  <FormattedMessage*/}
          {/*      id="contact"*/}
          {/*  />*/}
          {/*</H5>*/}
        </FlexBox>



        <Box mb="50px">
          {/*{selectedOption === "description" && <ProductDescription />}*/}
          {selectedOption === "review" && <ProductReview/>}
          {/*{selectedOption === "contact" && <ContactForm />}*/}
        </Box>

          {info?.smilarFlowers?.length !== 0 ? <RelatedProducts /> : ""}
      </div>
    </NavbarLayout>
  );
};




export async function getStaticPaths(){
  const paths =[
    {params: { id:'roses',keyword:'plotno-sobrannaya-kompoziciya-iz-krasnyh-i-belyh-roz-268' }},
    {params: { id:'rosebestseller-flowers',keyword:'nebolshoy-buket-iz-9-belyh-roz-266'}}
  ]
  return { paths, fallback: 'blocking' }
}




export async function getStaticProps(ctx){
    try {
        let lang = ctx.locale
        let {keyword} = ctx.params
        const request3 = await axios(`${BASE_URL}/flowers/show/${keyword}/${lang}`)
        const answer2 =  request3.data;
        return {
            props:{
                info2:answer2,
                redirect:false
            },
            revalidate:2
        }
    }
    catch{
        return {
            notFound:true
        }
    }
}

export default ProductDetails;
