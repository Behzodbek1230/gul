import VendorDashboardLayout from "../components/layout/VendorDashboardLayout";
import DashboardPageHeader2 from "../components/layout/DashboardPageHeader2";
import Accordions from "../components/accordions";
import { NextSeo } from "next-seo";
import { useEffect, useState } from "react";
import axios from "axios";
import Loading from "../components/Loading";
import Grid from "../components/grid/Grid";
import { useRouter } from "next/router";
import {useIntl} from "react-intl";
let Carriers = ()=>{
    let [loading,setloading] = useState(true)
    let [data,setdata] = useState([])
    let intl = useIntl()
    let router = useRouter()
    let lang = router.locale
    useEffect(()=>{
        axios(`/vacancy/list/${lang}`)
        .then((res)=>{
            setdata(res.data)
            setloading(false)
        })
        .catch(()=>setloading(false))
    },[lang])
    if(loading){
        return<>
             <NextSeo
                title={intl.formatMessage({id:"Carriers"})}
                description={intl.formatMessage({id:"Carriers"})}
                additionalMetaTags={[{
                name: 'keyword',
                content:  intl.formatMessage({id:"Carriers"})
                }, ]}
                openGraph={{
                    type:"website",
                  }}
            />
            <Loading/>
        </>
    }
    return(
        <VendorDashboardLayout>
            <NextSeo
                title={intl.formatMessage({id:"Carriers"})}
                description={intl.formatMessage({id:"Carriers"})}
                additionalMetaTags={[{
                name: 'keyword',
                content: intl.formatMessage({id:"Carriers"})
                }, ]}
                openGraph={{
                    type:"website",
                  }}
            />
            <div style={{marginBottom:"50px"}}>
                <div  style={{color:"#f7961"}}>
                    <DashboardPageHeader2
                    title={intl.formatMessage({id:"Carriers"})}
                    />
                            {data.length !==0 ?
                            <Grid  style={{padding:"5px",paddingBottom:"0px",paddingTop:"0px",marginLeft:"7px"}}>
                                <Accordions carrier={true} data={data} accordionClass="faq_accordions" />
                            </Grid>
                                :
                                intl.formatMessage({id:"empty_carrier"})
                                }


                </div>
            </div>
        </VendorDashboardLayout>
    )
}
export default Carriers;
