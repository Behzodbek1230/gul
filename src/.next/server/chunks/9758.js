"use strict";
exports.id = 9758;
exports.ids = [9758];
exports.modules = {

/***/ 9758:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "J": () => (/* binding */ deviceSize),
/* harmony export */   "P": () => (/* binding */ layoutConstant)
/* harmony export */ });
const deviceSize = {
    xs: 425,
    sm: 768,
    md: 1024,
    lg: 1440
};
const layoutConstant = {
    grocerySidenavWidth: "280px",
    containerWidth: "1200px",
    mobileNavHeight: "64px",
    headerHeight: "80px",
    mobileHeaderHeight: "64px"
};


/***/ })

};
;