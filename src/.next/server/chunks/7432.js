"use strict";
exports.id = 7432;
exports.ids = [7432];
exports.modules = {

/***/ 7432:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_yandex_maps__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3742);
/* harmony import */ var react_yandex_maps__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_yandex_maps__WEBPACK_IMPORTED_MODULE_2__);


// import ReactDOM from "react-dom";

// import {useDispatch, useSelector} from "react-redux";
// import getcoordinates from "../../Redux/Actions/MapCoordinates"
// import {log} from "util";
const Map_Shop = (props)=>{
    const coordinate = props.coordinate === null ? [
        41.316441,
        69.294861
    ] : props.coordinate;
    const mapState = {
        center: coordinate,
        zoom: 15
    };
    const mapRef = (0,react__WEBPACK_IMPORTED_MODULE_1__.useRef)(null);
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        className: "App",
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_yandex_maps__WEBPACK_IMPORTED_MODULE_2__.YMaps, {
            version: "2.1.77",
            query: {
                lang: 'en_RU',
                apikey: '7acd9319-78e1-4abe-a6af-d64c62828777'
            },
            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_yandex_maps__WEBPACK_IMPORTED_MODULE_2__.Map, {
                width: "100%",
                height: props.height || 450,
                modules: [
                    "multiRouter.MultiRoute",
                    "coordSystem.geo",
                    "geocode",
                    "util.bounds"
                ],
                state: mapState,
                instanceRef: (ref)=>{
                    if (ref) {
                        mapRef.current = ref;
                    }
                },
                children: [
                    coordinate !== undefined ? /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_yandex_maps__WEBPACK_IMPORTED_MODULE_2__.Placemark, {
                        geometry: coordinate
                    }) : "",
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_yandex_maps__WEBPACK_IMPORTED_MODULE_2__.ZoomControl, {
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_yandex_maps__WEBPACK_IMPORTED_MODULE_2__.GeolocationControl, {
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_yandex_maps__WEBPACK_IMPORTED_MODULE_2__.FullscreenControl, {
                    })
                ]
            })
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Map_Shop);


/***/ })

};
;