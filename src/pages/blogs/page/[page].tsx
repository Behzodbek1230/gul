import Container from "../../../components/Container";
import React, {useEffect, useState} from "react";
import Loading from "../../../components/Loading";
import axios from "axios";
import {useRouter} from "next/router";
import BlogCard from "../../../components/BlogCard";
import Grid from "../../../components/grid/Grid";
import FlexBox from "../../../components/FlexBox";
import {StyledPagination} from "../../../components/pagination/PaginationStyle";
import Button from "../../../components/buttons/Button";
import Icon from "../../../components/icon/Icon";
import ReactPaginate from "react-paginate"
import NavbarLayout from "../../../components/layout/NavbarLayout";
import {FormattedMessage, useIntl} from "react-intl";
import {H1} from "../../../components/Typography";
import {NextSeo} from "next-seo";
let MainBlogs = () =>{
    let [blogs,setblogs] = useState({datas:{current_page:1,data:[]},lastPage:1})
    let [loading,setloading] = useState(true)
    let router = useRouter()
    let intl = useIntl()
    let {page} = router.query
    let lang = router.locale
    useEffect(()=>{
        axios({
            method:"GET",
            url:`/blog/blog-list/${lang}?page=${page}`
        })
            .then((res)=>{
                setblogs(res.data)
                setloading(false)
            })
            .catch(()=>setloading(false))
    },[lang])
    if(loading){
        return (
            <>
                <NextSeo
                    title={intl.formatMessage({id:"blogs"})}
                    description={intl.formatMessage({id:"blogs"})}
                    additionalMetaTags={[{
                        name: 'keyword',
                        content:intl.formatMessage({id:"blogs"})
                    }, ]}

                />
                <Loading />
            </>
        )
    }
    else{
        return(
            <NavbarLayout>
                <NextSeo
                    title={intl.formatMessage({id:"blogs"})}
                    description={intl.formatMessage({id:"blogs"})}
                    additionalMetaTags={[{
                        name: 'keyword',
                        content:intl.formatMessage({id:"blogs"})
                    }, ]}

                />
                <Container>
                    <H1 mb="24px">
                        <FormattedMessage
                            id="blogs"
                            defaultMessage="Bloglar"
                        />
                    </H1>
                    {blogs?.datas?.data.length !==0 && blogs
                        ?
                        <>

                            <Grid container spacing={6}>
                                {blogs?.datas.data?.map((item, ind) => (
                                    <Grid className="mt-4 mt-sm-4 mt-md-0 mt-lg-0 mt-xl-0" item lg={3} sm={6} md={4} xs={12} key={ind}>
                                        <BlogCard {...item}/>
                                    </Grid>
                                ))}
                            </Grid>
                            {blogs?.lastPage!==1
                                ?
                                <FlexBox
                                    flexWrap="wrap"
                                    justifyContent="space-between"
                                    alignItems="center"
                                    mt="32px"
                                >
                                    <StyledPagination>
                                        {blogs?.datas.current_page !== 1?
                                            <ReactPaginate
                                                forcePage={blogs?.datas?.current_page-1}
                                                previousLabel={
                                                    <Button
                                                        style={{cursor: "pointer"}}
                                                        className="control-button"
                                                        color="primary"
                                                        overflow="hidden"
                                                        height="auto"
                                                        padding="6px"
                                                        borderRadius="50%"
                                                    >
                                                        <Icon defaultcolor="currentColor" variant="small">
                                                            chevron-left
                                                        </Icon>
                                                    </Button>

                                                }
                                                nextLabel={
                                                    <Button
                                                        style={{cursor: "pointer"}}
                                                        className="control-button"
                                                        color="primary"
                                                        overflow="hidden"
                                                        height="auto"
                                                        padding="6px"
                                                        borderRadius="50%"
                                                    >
                                                        <Icon defaultcolor="currentColor" variant="small">
                                                            chevron-right
                                                        </Icon>
                                                    </Button>
                                                }
                                                breakLabel={
                                                    <Icon defaultcolor="currentColor" variant="small">
                                                        triple-dot
                                                    </Icon>
                                                }
                                                pageCount={blogs.lastPage}
                                                marginPagesDisplayed={true}
                                                pageRangeDisplayed={false}
                                                onPageChange={(r)=>{
                                                    if(router.pathname.includes("page")){
                                                        if(r.selected === 0){
                                                            let query = {...router.query}
                                                            delete query.page
                                                            router.push({pathname:router.pathname.replace("/page/[page]",""),query:query})
                                                        }
                                                        else{
                                                            let query = {...router.query}
                                                            query.page = r.selected+1
                                                            router.push({pathname:router.pathname,query:query})
                                                        }
                                                    }
                                                    else{
                                                        router.push( {pathname:router.asPath +`/page/${r.selected + 1}`})
                                                    }

                                                }
                                                }
                                                containerClassName="pagination"
                                                subContainerClassName="pages pagination"
                                                activeClassName="active"
                                                disabledClassName="disabled"
                                            />
                                            :
                                            <ReactPaginate
                                                previousLabel={
                                                    <Button
                                                        style={{cursor: "pointer"}}
                                                        className="control-button"
                                                        color="primary"
                                                        overflow="hidden"
                                                        height="auto"
                                                        padding="6px"
                                                        borderRadius="50%"
                                                    >
                                                        <Icon defaultcolor="currentColor" variant="small">
                                                            chevron-left
                                                        </Icon>
                                                    </Button>

                                                }
                                                nextLabel={
                                                    <Button
                                                        style={{cursor: "pointer"}}
                                                        className="control-button"
                                                        color="primary"
                                                        overflow="hidden"
                                                        height="auto"
                                                        padding="6px"
                                                        borderRadius="50%"
                                                    >
                                                        <Icon defaultcolor="currentColor" variant="small">
                                                            chevron-right
                                                        </Icon>
                                                    </Button>
                                                }
                                                breakLabel={
                                                    <Icon defaultcolor="currentColor" variant="small">
                                                        triple-dot
                                                    </Icon>
                                                }
                                                pageCount={blogs?.lastPage}
                                                marginPagesDisplayed={true}
                                                pageRangeDisplayed={false}
                                                onPageChange={(r)=>{
                                                    if(router.pathname.includes("page")){
                                                        let query = {...router.query}
                                                        query.page = r.selected+1
                                                        router.push({pathname:router.pathname,query:query})
                                                    }
                                                    else{
                                                        router.push( {pathname:router.asPath +`/page/${r.selected + 1}`})
                                                    }

                                                }
                                                }
                                                containerClassName="pagination"
                                                subContainerClassName="pages pagination"
                                                activeClassName="active"
                                                disabledClassName="disabled"
                                            />
                                        }
                                    </StyledPagination>
                                </FlexBox>
                                :
                                ""
                            }
                        </>
                        :
                        ""}
                </Container>
            </NavbarLayout>
        )
    }
}
export default MainBlogs
