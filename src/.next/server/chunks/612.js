"use strict";
exports.id = 612;
exports.ids = [612];
exports.modules = {

/***/ 612:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ nav_link_NavLink)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: ../node_modules/next/link.js
var next_link = __webpack_require__(9894);
// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(1853);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "@styled-system/css"
var css_ = __webpack_require__(2038);
var css_default = /*#__PURE__*/__webpack_require__.n(css_);
// EXTERNAL MODULE: external "styled-components"
var external_styled_components_ = __webpack_require__(7518);
var external_styled_components_default = /*#__PURE__*/__webpack_require__.n(external_styled_components_);
// EXTERNAL MODULE: external "styled-system"
var external_styled_system_ = __webpack_require__(5834);
;// CONCATENATED MODULE: ./components/nav-link/NavLinkStyle.tsx



const StyledNavLink = external_styled_components_default().a(({ isCurrentRoute , theme  })=>css_default()({
        position: "relative",
        color: isCurrentRoute ? theme.colors.primary.main : "auto",
        transition: "all 150ms ease-in-out",
        "&:hover": {
            color: `${theme.colors.primary.main} !important`
        },
        "& svg path": {
            fill: isCurrentRoute ? theme.colors.primary.main : "auto"
        },
        "& svg polyline, svg polygon": {
            color: isCurrentRoute ? theme.colors.primary.main : "auto"
        }
    })
, (0,external_styled_system_.compose)(external_styled_system_.space, external_styled_system_.color));
/* harmony default export */ const NavLinkStyle = (StyledNavLink);

;// CONCATENATED MODULE: ./components/nav-link/NavLink.tsx





const NavLink = ({ href , as , children , style , className , ...props })=>{
    let { pathname  } = (0,router_.useRouter)();
    const checkRouteMatch = ()=>{
        if (href === "/") return pathname === href;
        return pathname.includes(href);
    };
    return(/*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
        href: href,
        children: /*#__PURE__*/ jsx_runtime_.jsx(NavLinkStyle, {
            className: className,
            href: href,
            isCurrentRoute: checkRouteMatch(),
            style: style,
            ...props,
            children: children
        })
    }));
};
/* harmony default export */ const nav_link_NavLink = (NavLink);


/***/ })

};
;