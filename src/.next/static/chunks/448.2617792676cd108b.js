"use strict";(self.webpackChunk_N_E=self.webpackChunk_N_E||[]).push([[448],{60448:function(e,t,n){n.r(t);var i=n(24246),l=n(30012),s=n(27378),r=n(86677),o=n(14206),c=n.n(o),u=n(65218),a=(0,u.default)((function(){return Promise.all([n.e(9351),n.e(1601)]).then(n.bind(n,1601))}),{ssr:!1}),d=(0,u.default)((function(){return Promise.all([n.e(9774),n.e(9351),n.e(3387),n.e(5003),n.e(9673)]).then(n.bind(n,9868))}),{ssr:!1}),h=(0,u.default)((function(){return Promise.all([n.e(9351),n.e(8300)]).then(n.bind(n,19221))}),{ssr:!1}),f=(0,u.default)((function(){return n.e(9351).then(n.bind(n,84875))}),{ssr:!1});t.default=function(){var e=(0,l.Z)(),t=(0,r.useRouter)(),n=(0,s.useState)(0),o=n[0],u=n[1],g=t.locale;(0,s.useEffect)((function(){u(e<1050&&e>950?7:e<835&&e>650?5:e<650?4:8)}),[e]);var m=(0,s.useState)([]),b=m[0],_=m[1];return(0,s.useEffect)((function(){c().get("/flowers/general-category/list/".concat(g)).then((function(e){_(e.data)})).catch((function(){return null}))}),[g]),(0,i.jsx)("div",{className:"margin-top-25 margin-bottom-5 container section3_height",children:e>650?(0,i.jsx)("div",{className:"carousel_margin_top margin-bottom-5",children:(0,i.jsx)("div",{children:(0,i.jsx)(f,{children:(0,i.jsx)(h,{children:(0,i.jsx)(d,{leftButtonClass:"ml-lg-2",leftButtonStyle:{position:"absolute"},rightButtonStyle:{position:"absolute"},showArrow:!(e<1150||0===b.length),totalSlides:b.length,visibleSlides:o,children:0!==b.length||b?null===b||void 0===b?void 0:b.map((function(e){return(0,i.jsx)("div",{onClick:function(){return t.push("/".concat(e.keyword))},className:"index_category_image_height",children:(0,i.jsx)(a,{title:e.title,subtitle:e.keyword,imgUrl:e.icon_b,s_imgUrl:e.icon_s})},e)})):""})})})})}):(0,i.jsx)("div",{className:"carouselStyle ",children:(0,i.jsx)(d,{showArrow:!1,totalSlides:b.length,visibleSlides:o,children:0!==b.length||b?null===b||void 0===b?void 0:b.map((function(e){return(0,i.jsx)("div",{className:"cursor-pointer2",onClick:function(){return t.push("/".concat(e.keyword))},children:(0,i.jsx)(a,{title:e.title,subtitle:e.keyword,imgUrl:e.icon_b,s_imgUrl:e.icon_s})},e)})):""})})})}}}]);