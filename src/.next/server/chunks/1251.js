"use strict";
exports.id = 1251;
exports.ids = [1251];
exports.modules = {

/***/ 5273:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_Box__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4875);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1853);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _FlexBox__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(5359);
/* harmony import */ var _icon_Icon__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(4154);
/* harmony import */ var _DashboardStyle__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(5129);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(6022);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var react_intl__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(3126);
/* harmony import */ var react_intl__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react_intl__WEBPACK_IMPORTED_MODULE_8__);









const CompanyDashboardNavigation = ()=>{
    let router = (0,next_router__WEBPACK_IMPORTED_MODULE_2__.useRouter)();
    let { asPath  } = router;
    let footer_sections = (0,react_redux__WEBPACK_IMPORTED_MODULE_7__.useSelector)((state)=>state.new.footer
    );
    const linkList = [];
    let intl = (0,react_intl__WEBPACK_IMPORTED_MODULE_8__.useIntl)();
    if (footer_sections.length !== 0) {
        var ref, ref1;
        (ref = footer_sections.about_company) === null || ref === void 0 ? void 0 : ref.map((one)=>linkList.push({
                href: `/company/${one.keyword}`,
                title: one.name
            })
        );
        (ref1 = footer_sections.services) === null || ref1 === void 0 ? void 0 : ref1.map((one)=>linkList.push({
                href: `/company/${one.keyword}`,
                title: one.name
            })
        );
    }
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_DashboardStyle__WEBPACK_IMPORTED_MODULE_6__/* .DashboardNavigationWrapper */ .nX, {
        px: "0px",
        py: "1.5rem",
        color: "gray.900",
        children: [
            linkList.map((item)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_DashboardStyle__WEBPACK_IMPORTED_MODULE_6__/* .StyledDashboardNav */ .Wt, {
                    isCurrentPath: asPath === item.href,
                    href: item.href,
                    px: "1.5rem",
                    mb: "1.25rem",
                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_FlexBox__WEBPACK_IMPORTED_MODULE_4__["default"], {
                        alignItems: "center",
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_Box__WEBPACK_IMPORTED_MODULE_1__["default"], {
                                className: "dashboard-nav-icon-holder",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_icon_Icon__WEBPACK_IMPORTED_MODULE_5__["default"], {
                                    variant: "small",
                                    defaultcolor: "currentColor",
                                    mr: "10px",
                                    children: item.iconName
                                })
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                children: item.title
                            })
                        ]
                    })
                }, item.title)
            ),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_DashboardStyle__WEBPACK_IMPORTED_MODULE_6__/* .StyledDashboardNav */ .Wt, {
                isCurrentPath: asPath === "/sitemap",
                href: "/sitemap",
                px: "2rem",
                mb: "1.25rem",
                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_FlexBox__WEBPACK_IMPORTED_MODULE_4__["default"], {
                    alignItems: "center",
                    children: [
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_Box__WEBPACK_IMPORTED_MODULE_1__["default"], {
                            className: "dashboard-nav-icon-holder"
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                            children: intl.formatMessage({
                                id: "Sitemap"
                            })
                        })
                    ]
                })
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_DashboardStyle__WEBPACK_IMPORTED_MODULE_6__/* .StyledDashboardNav */ .Wt, {
                isCurrentPath: asPath === "/corporate",
                href: "/corporate",
                px: "2rem",
                mb: "1.25rem",
                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_FlexBox__WEBPACK_IMPORTED_MODULE_4__["default"], {
                    alignItems: "center",
                    children: [
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_Box__WEBPACK_IMPORTED_MODULE_1__["default"], {
                            className: "dashboard-nav-icon-holder"
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                            children: intl.formatMessage({
                                id: "Corparative Sales"
                            })
                        })
                    ]
                })
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_DashboardStyle__WEBPACK_IMPORTED_MODULE_6__/* .StyledDashboardNav */ .Wt, {
                isCurrentPath: asPath === "/carriers",
                href: "/carriers",
                px: "2rem",
                mb: "1.25rem",
                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_FlexBox__WEBPACK_IMPORTED_MODULE_4__["default"], {
                    alignItems: "center",
                    children: [
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_Box__WEBPACK_IMPORTED_MODULE_1__["default"], {
                            className: "dashboard-nav-icon-holder"
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                            children: intl.formatMessage({
                                id: "Carriers"
                            })
                        })
                    ]
                })
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_DashboardStyle__WEBPACK_IMPORTED_MODULE_6__/* .StyledDashboardNav */ .Wt, {
                isCurrentPath: asPath === "/faq",
                href: "/faq",
                px: "2rem",
                mb: "1.25rem",
                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_FlexBox__WEBPACK_IMPORTED_MODULE_4__["default"], {
                    alignItems: "center",
                    children: [
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_Box__WEBPACK_IMPORTED_MODULE_1__["default"], {
                            className: "dashboard-nav-icon-holder"
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                            children: "FAQ"
                        })
                    ]
                })
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_DashboardStyle__WEBPACK_IMPORTED_MODULE_6__/* .StyledDashboardNav */ .Wt, {
                isCurrentPath: asPath === "/blogs",
                href: "/blogs",
                px: "2rem",
                mb: "1.25rem",
                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_FlexBox__WEBPACK_IMPORTED_MODULE_4__["default"], {
                    alignItems: "center",
                    children: [
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_Box__WEBPACK_IMPORTED_MODULE_1__["default"], {
                            className: "dashboard-nav-icon-holder"
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_intl__WEBPACK_IMPORTED_MODULE_8__.FormattedMessage, {
                                id: "blogs"
                            })
                        })
                    ]
                })
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_DashboardStyle__WEBPACK_IMPORTED_MODULE_6__/* .StyledDashboardNav */ .Wt, {
                isCurrentPath: asPath === "/help",
                href: "/help",
                px: "2rem",
                mb: "1.25rem",
                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_FlexBox__WEBPACK_IMPORTED_MODULE_4__["default"], {
                    alignItems: "center",
                    children: [
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_Box__WEBPACK_IMPORTED_MODULE_1__["default"], {
                            className: "dashboard-nav-icon-holder"
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                            children: intl.formatMessage({
                                id: "help"
                            })
                        })
                    ]
                })
            })
        ]
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CompanyDashboardNavigation);


/***/ }),

/***/ 1251:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _hooks_useWindowSize__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(12);
/* harmony import */ var _Box__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(4875);
/* harmony import */ var _FlexBox__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(5359);
/* harmony import */ var _icon_Icon__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(4154);
/* harmony import */ var _sidenav_Sidenav__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(1211);
/* harmony import */ var _Typography__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(9374);
/* harmony import */ var _components_layout_VendorDashboardNavigation__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(1851);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(1853);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _CompanyDashboardNavigation__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(5273);











const DashboardPageHeader2 = ({ iconName , title , button ,  })=>{
    let { pathname  } = (0,next_router__WEBPACK_IMPORTED_MODULE_9__.useRouter)();
    const width = (0,_hooks_useWindowSize__WEBPACK_IMPORTED_MODULE_2__/* ["default"] */ .Z)();
    const isTablet = width < 1025;
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_Box__WEBPACK_IMPORTED_MODULE_3__["default"], {
        mb: "1.5rem",
        mt: "-1rem",
        children: [
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_FlexBox__WEBPACK_IMPORTED_MODULE_4__["default"], {
                justifyContent: "space-between",
                alignItems: "center",
                mt: "1rem",
                children: [
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_FlexBox__WEBPACK_IMPORTED_MODULE_4__["default"], {
                        alignItems: "center",
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_icon_Icon__WEBPACK_IMPORTED_MODULE_5__["default"], {
                                color: "primary",
                                children: iconName
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_Typography__WEBPACK_IMPORTED_MODULE_7__.H1, {
                                ml: "12px",
                                my: "0px",
                                lineHeight: "1",
                                whitespace: "pre",
                                children: title
                            })
                        ]
                    }),
                    isTablet && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_sidenav_Sidenav__WEBPACK_IMPORTED_MODULE_6__/* ["default"] */ .Z, {
                        position: "left",
                        handle: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_icon_Icon__WEBPACK_IMPORTED_MODULE_5__["default"], {
                            mx: "1rem",
                            children: "menu"
                        }),
                        children: pathname === "/company" || pathname === "/faq" || pathname === "/connectUs" || pathname === "/sitemap" || pathname.includes("/carriers") ? /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_CompanyDashboardNavigation__WEBPACK_IMPORTED_MODULE_10__/* ["default"] */ .Z, {
                        }) : /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_layout_VendorDashboardNavigation__WEBPACK_IMPORTED_MODULE_8__/* ["default"] */ .Z, {
                        })
                    }),
                    !isTablet && button
                ]
            }),
            isTablet && !!button && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_Box__WEBPACK_IMPORTED_MODULE_3__["default"], {
                mt: "1rem",
                children: button
            })
        ]
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (DashboardPageHeader2);


/***/ }),

/***/ 1851:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_Box__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4875);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1853);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _FlexBox__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(5359);
/* harmony import */ var _icon_Icon__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(4154);
/* harmony import */ var _DashboardStyle__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(5129);
/* harmony import */ var react_intl__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(3126);
/* harmony import */ var react_intl__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_intl__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _components_avatar_Avatar__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(3392);
/* harmony import */ var _components_Typography__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(9374);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(6022);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_10__);











const VendorDashboardNavigation = ()=>{
    var ref, ref1, ref2;
    let userInfo = (0,react_redux__WEBPACK_IMPORTED_MODULE_10__.useSelector)((state)=>state.token.user
    );
    const { pathname  } = (0,next_router__WEBPACK_IMPORTED_MODULE_2__.useRouter)();
    let intl = (0,react_intl__WEBPACK_IMPORTED_MODULE_7__.useIntl)();
    const linkList = [
        {
            href: "/vendor/products/page/1",
            title: intl.formatMessage({
                id: "Products"
            }),
            iconName: "box"
        },
        {
            href: "/vendor/add-product",
            title: intl.formatMessage({
                id: "add_new_product"
            }),
            iconName: "upload"
        },
        {
            href: "/vendor/orders/page/1",
            title: intl.formatMessage({
                id: "orders"
            }),
            iconName: "shopping-cart"
        },
        {
            href: "/vendor/account-settings",
            title: intl.formatMessage({
                id: "account_settings"
            }),
            iconName: "gear-2"
        }, 
    ];
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_DashboardStyle__WEBPACK_IMPORTED_MODULE_6__/* .DashboardNavigationWrapper */ .nX, {
        px: "0px",
        py: "1.5rem",
        color: "gray.900",
        children: [
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_FlexBox__WEBPACK_IMPORTED_MODULE_4__["default"], {
                p: "14px 32px",
                alignItems: "center",
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_avatar_Avatar__WEBPACK_IMPORTED_MODULE_8__/* ["default"] */ .Z, {
                        src: (userInfo === null || userInfo === void 0 ? void 0 : (ref = userInfo.data) === null || ref === void 0 ? void 0 : ref.avatar) ? userInfo === null || userInfo === void 0 ? void 0 : (ref1 = userInfo.data) === null || ref1 === void 0 ? void 0 : ref1.shopImage : "https://api.dana.uz/storage/images/noimg.jpg",
                        size: 64
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_Box__WEBPACK_IMPORTED_MODULE_1__["default"], {
                        ml: "12px",
                        flex: "1 1 0",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_FlexBox__WEBPACK_IMPORTED_MODULE_4__["default"], {
                            flexWrap: "wrap",
                            justifyContent: "space-between",
                            alignItems: "center",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_Typography__WEBPACK_IMPORTED_MODULE_9__.H5, {
                                    my: "0px",
                                    children: userInfo === null || userInfo === void 0 ? void 0 : (ref2 = userInfo.data) === null || ref2 === void 0 ? void 0 : ref2.shopName
                                })
                            })
                        })
                    })
                ]
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {
            }),
            linkList.map((item)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_DashboardStyle__WEBPACK_IMPORTED_MODULE_6__/* .StyledDashboardNav */ .Wt, {
                    isCurrentPath: pathname.includes(item.href),
                    href: item.href,
                    px: "1.5rem",
                    mb: "1.25rem",
                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_FlexBox__WEBPACK_IMPORTED_MODULE_4__["default"], {
                        alignItems: "center",
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_Box__WEBPACK_IMPORTED_MODULE_1__["default"], {
                                className: "dashboard-nav-icon-holder",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_icon_Icon__WEBPACK_IMPORTED_MODULE_5__["default"], {
                                    variant: "small",
                                    defaultcolor: "currentColor",
                                    mr: "10px",
                                    children: item.iconName
                                })
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                children: item.title
                            })
                        ]
                    })
                }, item.title)
            )
        ]
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (VendorDashboardNavigation);


/***/ })

};
;