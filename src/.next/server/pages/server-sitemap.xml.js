"use strict";
(() => {
var exports = {};
exports.id = 7773;
exports.ids = [7773];
exports.modules = {

/***/ 5153:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "_n": () => (/* binding */ BASE_URL),
/* harmony export */   "wL": () => (/* binding */ GOOGLE_CLIENT_ID),
/* harmony export */   "lZ": () => (/* binding */ FACEBOOK_APP_ID),
/* harmony export */   "px": () => (/* binding */ SITE_NAME)
/* harmony export */ });
const BASE_URL = "https://api.dana.uz/api/v1";
const GOOGLE_CLIENT_ID = "160068010891-2mnlkm5lmf3lka7ntnqcvs6jjn6ckr3p.apps.googleusercontent.com";
const FACEBOOK_APP_ID = "861976684507312";
const SITE_NAME = "https://dana.uz";


/***/ }),

/***/ 580:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ Site),
  "getServerSideProps": () => (/* binding */ getServerSideProps)
});

// EXTERNAL MODULE: ./components/Variables.tsx
var Variables = __webpack_require__(5153);
// EXTERNAL MODULE: external "axios"
var external_axios_ = __webpack_require__(2167);
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_);
;// CONCATENATED MODULE: external "next-sitemap"
const external_next_sitemap_namespaceObject = require("next-sitemap");
;// CONCATENATED MODULE: ./pages/server-sitemap.xml/index.tsx



const getServerSideProps = async (ctx)=>{
    let fields = [];
    /*
        Category Sitemap
    */ let categories = await external_axios_default()({
        url: `${Variables/* BASE_URL */._n}/sitemap/category-list`,
        method: "GET"
    });
    let category_field = categories.data.map((one)=>({
            loc: `https://dana.uz/${one.keyword}`,
            lastmod: new Date().toISOString()
        })
    );
    /*
        Flowers Sitemap
    */ let flowers = await external_axios_default()({
        url: `${Variables/* BASE_URL */._n}/sitemap/flowers-list`,
        method: "GET"
    });
    let flowers_field = flowers.data.map((one)=>({
            loc: `https://dana.uz/${one.categoryKeyword}/${one.flowerKeyword}`,
            lastmod: new Date().toISOString()
        })
    );
    /*
        Shops Sitemap
    */ let shops = await external_axios_default()({
        url: `${Variables/* BASE_URL */._n}/sitemap/shops-list`,
        method: "GET"
    });
    let shops_field = shops.data.map((one)=>({
            loc: `https://dana.uz/shop/${one.keyword}`,
            lastmod: new Date().toISOString()
        })
    );
    /*
        Footer Sitemap
    */ let footer = await external_axios_default()({
        url: `${Variables/* BASE_URL */._n}/sitemap/help-list`,
        method: "GET"
    });
    let footer_things = footer.data.map((one)=>({
            loc: `https://dana.uz/company/${one.keyword}`,
            lastmod: new Date().toISOString()
        })
    );
    fields = [
        ...category_field,
        ...flowers_field,
        ...shops_field,
        ...footer_things
    ];
    return (0,external_next_sitemap_namespaceObject.getServerSideSitemap)(ctx, fields);
};
function Site() {
};


/***/ }),

/***/ 2167:
/***/ ((module) => {

module.exports = require("axios");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__(580));
module.exports = __webpack_exports__;

})();