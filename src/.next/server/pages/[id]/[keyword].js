"use strict";
(() => {
var exports = {};
exports.id = 8916;
exports.ids = [8916];
exports.modules = {

/***/ 3447:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Container__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(9221);
/* harmony import */ var _AppLayout__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(7198);




const NavbarLayout = ({ title , description , keyword , children , seoRelated  })=>{
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_AppLayout__WEBPACK_IMPORTED_MODULE_3__["default"], {
        title: title,
        description: description,
        keyword: keyword,
        seoRelated: seoRelated,
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_Container__WEBPACK_IMPORTED_MODULE_2__["default"], {
            my: "2rem",
            children: children
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (NavbarLayout);


/***/ }),

/***/ 2932:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ _keyword_),
  "getStaticPaths": () => (/* binding */ getStaticPaths),
  "getStaticProps": () => (/* binding */ getStaticProps)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: ./components/Box.tsx
var Box = __webpack_require__(4875);
// EXTERNAL MODULE: ./components/FlexBox.tsx
var FlexBox = __webpack_require__(5359);
// EXTERNAL MODULE: ./components/layout/NavbarLayout.tsx
var NavbarLayout = __webpack_require__(3447);
// EXTERNAL MODULE: ./components/products/ProductIntro.tsx + 1 modules
var ProductIntro = __webpack_require__(4406);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: ./components/buttons/Button.tsx
var Button = __webpack_require__(2389);
// EXTERNAL MODULE: ./components/rating/Rating.tsx + 2 modules
var Rating = __webpack_require__(197);
// EXTERNAL MODULE: ./components/Typography.tsx
var Typography = __webpack_require__(9374);
// EXTERNAL MODULE: ./components/products/ProductComment.tsx
var ProductComment = __webpack_require__(2900);
// EXTERNAL MODULE: external "formik"
var external_formik_ = __webpack_require__(2296);
// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(1853);
// EXTERNAL MODULE: external "js-cookie"
var external_js_cookie_ = __webpack_require__(6734);
var external_js_cookie_default = /*#__PURE__*/__webpack_require__.n(external_js_cookie_);
// EXTERNAL MODULE: external "axios"
var external_axios_ = __webpack_require__(2167);
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_);
// EXTERNAL MODULE: external "react-intl"
var external_react_intl_ = __webpack_require__(3126);
// EXTERNAL MODULE: ./components/pagination/PaginationStyle.tsx
var PaginationStyle = __webpack_require__(7788);
// EXTERNAL MODULE: ./components/icon/Icon.tsx + 1 modules
var Icon = __webpack_require__(4154);
// EXTERNAL MODULE: external "react-paginate"
var external_react_paginate_ = __webpack_require__(9700);
var external_react_paginate_default = /*#__PURE__*/__webpack_require__.n(external_react_paginate_);
// EXTERNAL MODULE: ../node_modules/next/dynamic.js
var dynamic = __webpack_require__(5218);
;// CONCATENATED MODULE: ./components/products/ProductReview.tsx

















if (false) {}
const ReactQuill = (0,dynamic["default"])(()=>Promise.resolve(/* import() */).then(__webpack_require__.t.bind(__webpack_require__, 2586, 23))
, {
    loadableGenerated: {
        webpack: ()=>[
                /*require.resolve*/(2586)
            ]
        ,
        modules: [
            "..\\components\\products\\ProductReview.tsx -> " + "react-quill"
        ]
    },
    ssr: false
});
const modules = {
    toolbar: [
        [
            {
                header: '1'
            },
            {
                header: '2'
            },
            {
                font: []
            }
        ],
        [
            {
                size: []
            }
        ],
        [
            'bold',
            'italic',
            'underline',
            'strike',
            'blockquote'
        ],
        [
            {
                list: 'ordered'
            },
            {
                list: 'bullet'
            },
            {
                indent: '-1'
            },
            {
                indent: '+1'
            }, 
        ],
        [
            'link',
            'image',
            'video'
        ],
        [
            'clean'
        ], 
    ],
    clipboard: {
        // toggle to add extra line breaks when pasting HTML:
        matchVisual: false
    }
};
const ProductReview = ()=>{
    var ref, ref1;
    const router = (0,router_.useRouter)();
    let intl = (0,external_react_intl_.useIntl)();
    const { 0: msg , 1: setmsg  } = (0,external_react_.useState)("");
    const { 0: rating_error , 1: setrating_error  } = (0,external_react_.useState)("");
    const { 0: comment2 , 1: setcomment2  } = (0,external_react_.useState)("");
    let lang = router.locale;
    const handleFormSubmit = async (values, { resetForm  })=>{
        const { keyword  } = router.query;
        //FormData
        const formData = new FormData();
        formData.append("keyword", keyword.toString());
        formData.append("ball", values.rating);
        formData.append("comment", comment2);
        let logged_in = external_js_cookie_default().get("isLoggedIn");
        setrating_error("");
        setmsg("");
        if (!(values === null || values === void 0 ? void 0 : values.rating)) {
            setrating_error(intl.formatMessage({
                id: "rating_required"
            }));
        } else if (comment2 === "" && comment2) {
            setmsg(intl.formatMessage({
                id: "text_required"
            }));
        } else if (logged_in === "true") {
            external_axios_default()({
                method: "POST",
                url: `/flowers/rate/${lang}`,
                data: formData
            }).then((response)=>{
                if (response.data.errors) {
                    setmsg(intl.formatMessage({
                        id: "error_booking"
                    }));
                } else {
                    setmsg(intl.formatMessage({
                        id: "add_comment_success"
                    }));
                    setTimeout(()=>{
                        setmsg("");
                    }, 2000);
                    resetForm();
                    setcomment2("");
                }
            });
        } else {
            setmsg("Iltimos komment yozish saytdan ro'yhatdan o'ting");
        }
    };
    const { values: values1 , handleSubmit , setFieldValue ,  } = (0,external_formik_.useFormik)({
        initialValues: initialValues,
        onSubmit: handleFormSubmit
    });
    let { 0: ReviewPage , 1: setReviewPage  } = (0,external_react_.useState)(1);
    let { 0: reviews , 1: setreviews  } = (0,external_react_.useState)({
        data: [],
        last_page: 1
    });
    (0,external_react_.useEffect)(()=>{
        external_axios_default()({
            method: "GET",
            url: `/flowers/rating-list/${router.query.keyword}?page=${ReviewPage}`
        }).then((res)=>{
            setreviews(res.data);
        }).catch(()=>null
        );
    }, [
        msg,
        ReviewPage
    ]);
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(Box["default"], {
        children: [
            (reviews === null || reviews === void 0 ? void 0 : (ref = reviews.data) === null || ref === void 0 ? void 0 : ref.length) !== 0 ? /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                className: "p-1 mb-4",
                children: [
                    reviews === null || reviews === void 0 ? void 0 : (ref1 = reviews.data) === null || ref1 === void 0 ? void 0 : ref1.map((rating, ind)=>{
                        return(/*#__PURE__*/ jsx_runtime_.jsx(ProductComment/* default */.Z, {
                            id: ind,
                            imgUrl: rating.userAvatar,
                            date: rating.date,
                            name: rating.userFio,
                            rating: rating.ball,
                            comment: rating.comment,
                            is_shop: true,
                            extended: rating === null || rating === void 0 ? void 0 : rating.extend,
                            reviews: reviews,
                            setreviews: (e)=>setreviews(e)
                        }, rating.id));
                    }),
                    reviews.last_page !== 1 ? /*#__PURE__*/ jsx_runtime_.jsx(FlexBox["default"], {
                        flexWrap: "wrap",
                        justifyContent: "space-between",
                        alignItems: "center",
                        mt: "32px",
                        children: /*#__PURE__*/ jsx_runtime_.jsx(PaginationStyle/* StyledPagination */.I, {
                            children: /*#__PURE__*/ jsx_runtime_.jsx((external_react_paginate_default()), {
                                initialPage: ReviewPage - 1,
                                previousLabel: /*#__PURE__*/ jsx_runtime_.jsx(Button/* default */.Z, {
                                    style: {
                                        cursor: "pointer"
                                    },
                                    className: "control-button",
                                    color: "primary",
                                    overflow: "hidden",
                                    height: "auto",
                                    padding: "6px",
                                    borderRadius: "50%",
                                    children: /*#__PURE__*/ jsx_runtime_.jsx(Icon["default"], {
                                        defaultcolor: "currentColor",
                                        variant: "small",
                                        children: "chevron-left"
                                    })
                                }),
                                nextLabel: /*#__PURE__*/ jsx_runtime_.jsx(Button/* default */.Z, {
                                    style: {
                                        cursor: "pointer"
                                    },
                                    className: "control-button",
                                    color: "primary",
                                    overflow: "hidden",
                                    height: "auto",
                                    padding: "6px",
                                    borderRadius: "50%",
                                    children: /*#__PURE__*/ jsx_runtime_.jsx(Icon["default"], {
                                        defaultcolor: "currentColor",
                                        variant: "small",
                                        children: "chevron-right"
                                    })
                                }),
                                breakLabel: /*#__PURE__*/ jsx_runtime_.jsx(Icon["default"], {
                                    defaultcolor: "currentColor",
                                    variant: "small",
                                    children: "triple-dot"
                                }),
                                pageCount: reviews.last_page,
                                marginPagesDisplayed: true,
                                pageRangeDisplayed: false,
                                onPageChange: (r)=>{
                                    setReviewPage(r.selected + 1);
                                },
                                containerClassName: "pagination",
                                subContainerClassName: "pages pagination",
                                activeClassName: "active",
                                disabledClassName: "disabled"
                            })
                        })
                    }) : ""
                ]
            }) : "",
            /*#__PURE__*/ jsx_runtime_.jsx(Typography.H3, {
                fontWeight: "700",
                mt: "5px",
                mb: "20",
                children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                    id: "write_review",
                    defaultMessage: "Izoh qoldiring"
                })
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("form", {
                onSubmit: handleSubmit,
                children: [
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)(Box["default"], {
                        mb: "20px",
                        children: [
                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(FlexBox["default"], {
                                mb: "12px",
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                        style: {
                                            fontWeight: "bolder"
                                        },
                                        children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                                            id: "Your Rating",
                                            defaultMessage: "Your Rating"
                                        })
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx(Typography.H5, {
                                        color: "error.main",
                                        children: "*"
                                    })
                                ]
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx(Rating/* default */.Z, {
                                outof: 5,
                                color: "warn",
                                size: "medium",
                                readonly: false,
                                value: values1.rating || 0,
                                onChange: (value)=>setFieldValue("rating", value)
                            }),
                            rating_error === "" ? "" : /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                style: {
                                    color: "red",
                                    fontSize: "small"
                                },
                                children: rating_error
                            })
                        ]
                    }),
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)(Box["default"], {
                        mb: "24px",
                        children: [
                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(FlexBox["default"], {
                                mb: "12px",
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                        style: {
                                            fontWeight: "bolder"
                                        },
                                        children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                                            id: "Your Review",
                                            defaultMessage: "Sizning izohingiz"
                                        })
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx(Typography.H5, {
                                        color: "error.main",
                                        children: "*"
                                    })
                                ]
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                className: "react-quil-container432",
                                children: /*#__PURE__*/ jsx_runtime_.jsx(ReactQuill, {
                                    modules: modules,
                                    className: "reactquill-height2 my-editing-area",
                                    value: comment2,
                                    onChange: (e)=>setcomment2(e)
                                })
                            }),
                            msg === "" ? "" : /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                style: {
                                    color: "red",
                                    fontSize: "small"
                                },
                                children: msg
                            })
                        ]
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx(Button/* default */.Z, {
                        variant: "contained",
                        color: "primary",
                        size: "small",
                        type: "submit",
                        children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                            id: "submit",
                            defaultMessage: "Saqlash"
                        })
                    })
                ]
            })
        ]
    }));
};
const initialValues = {
    rating: "",
    comment: "",
    date: new Date().toISOString()
};
/* harmony default export */ const products_ProductReview = (ProductReview);

// EXTERNAL MODULE: ./components/carousel/Carousel.tsx + 1 modules
var Carousel = __webpack_require__(9868);
// EXTERNAL MODULE: ./components/product-cards/ProductCard1.tsx + 1 modules
var ProductCard1 = __webpack_require__(2599);
// EXTERNAL MODULE: external "react-redux"
var external_react_redux_ = __webpack_require__(6022);
// EXTERNAL MODULE: ./hooks/useWindowSize.js
var useWindowSize = __webpack_require__(12);
;// CONCATENATED MODULE: ./components/products/RelatedProducts.tsx

// import productDatabase from "@data/product-database";








const RelatedProducts = ()=>{
    var ref;
    const info = (0,external_react_redux_.useSelector)((state)=>state.new.one_product_info
    );
    const width = (0,useWindowSize/* default */.Z)();
    const { 0: visibleSlides , 1: setVisibleSlides  } = (0,external_react_.useState)(width < 650 ? 2 : 4);
    (0,external_react_.useEffect)(()=>{
        if (width < 650) setVisibleSlides(2);
        else if (width < 950) setVisibleSlides(3);
        else setVisibleSlides(4);
    }, [
        width
    ]);
    let slides_count;
    if (width < 650) {
        var ref1;
        slides_count = info === null || info === void 0 ? void 0 : (ref1 = info.smilarFlowers) === null || ref1 === void 0 ? void 0 : ref1.length;
    } else {
        var ref2;
        slides_count = info === null || info === void 0 ? void 0 : (ref2 = info.smilarFlowers) === null || ref2 === void 0 ? void 0 : ref2.length;
    }
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(Box["default"], {
        mb: "3.75rem",
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx(Typography.H3, {
                className: "fw-bold",
                mb: "1.5rem",
                children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                    id: "related_product"
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(Carousel["default"], {
                showArrow: width < 650 ? false : true,
                totalSlides: slides_count,
                visibleSlides: visibleSlides,
                children: info === null || info === void 0 ? void 0 : (ref = info.smilarFlowers) === null || ref === void 0 ? void 0 : ref.map((item, ind)=>{
                    return(/*#__PURE__*/ jsx_runtime_.jsx(ProductCard1["default"], {
                        id: item === null || item === void 0 ? void 0 : item.keyword,
                        imgUrl: item === null || item === void 0 ? void 0 : item.image,
                        title: item === null || item === void 0 ? void 0 : item.name,
                        price: item === null || item === void 0 ? void 0 : item.price,
                        rating: item === null || item === void 0 ? void 0 : item.rating,
                        category_keyword: item === null || item === void 0 ? void 0 : item.categoryKeyword,
                        is_favourite: item === null || item === void 0 ? void 0 : item.is_favorites,
                        shopName: item === null || item === void 0 ? void 0 : item.shopName,
                        shopKeyword: item === null || item === void 0 ? void 0 : item.shopKeyword,
                        hoverEffect: true
                    }, ind));
                })
            })
        ]
    }));
};
/* harmony default export */ const products_RelatedProducts = (RelatedProducts);

;// CONCATENATED MODULE: ../Redux/Actions/get_one_product_info.js
const get_one_product_info = (info)=>{
    return {
        type: "GET_ONE_PRODUCT_INFO",
        payload: info
    };
};
/* harmony default export */ const Actions_get_one_product_info = (get_one_product_info);

// EXTERNAL MODULE: ./components/Variables.tsx
var Variables = __webpack_require__(5153);
// EXTERNAL MODULE: external "next-seo"
var external_next_seo_ = __webpack_require__(6641);
// EXTERNAL MODULE: ../node_modules/next/link.js
var next_link = __webpack_require__(9894);
;// CONCATENATED MODULE: ./components/breadcrumb.tsx




let Breadcrumb = ({ place  })=>{
    return(/*#__PURE__*/ jsx_runtime_.jsx("nav", {
        "aria-label": "breadcrumb",
        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ol", {
            className: "breadcrumb-main",
            children: [
                /*#__PURE__*/ jsx_runtime_.jsx("li", {
                    className: "breadcrumb-main-item text-capitalize cursor-pointer2 ",
                    children: /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                        href: "/",
                        passHref: true,
                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                            className: "cursor-pointer2 text-secondary fw-bold",
                            children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                                id: "mobile_navigation_home"
                            })
                        })
                    })
                }),
                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: "mx-1",
                    children: [
                        " ",
                        /*#__PURE__*/ jsx_runtime_.jsx(Icon["default"], {
                            size: "12px",
                            children: "next2"
                        })
                    ]
                }),
                place.map((one, ind)=>{
                    if (ind === place.length - 1) {
                        return(/*#__PURE__*/ jsx_runtime_.jsx("li", {
                            className: "breadcrumb-main-item text-wrap fs-bold text-dark active2",
                            "aria-current": "page",
                            children: one.title
                        }));
                    } else {
                        return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                    className: "breadcrumb-main-item text-secondary text-capitalize text-dark",
                                    children: /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                                        href: `/${one.keyword}`,
                                        passHref: true,
                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                            className: "cursor-pointer2 fw-bold text-secondary",
                                            children: one.title
                                        })
                                    })
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                    className: "mx-1",
                                    children: [
                                        " ",
                                        /*#__PURE__*/ jsx_runtime_.jsx(Icon["default"], {
                                            size: "12px",
                                            children: "next2"
                                        })
                                    ]
                                })
                            ]
                        }));
                    }
                })
            ]
        })
    }));
};
/* harmony default export */ const breadcrumb = (Breadcrumb);

// EXTERNAL MODULE: external "react-helmet"
var external_react_helmet_ = __webpack_require__(2791);
;// CONCATENATED MODULE: ./pages/[id]/[keyword]/index.tsx



















const ProductDetails = ({ info2  })=>{
    var ref28, ref1, ref2, ref3, ref4, ref5, ref6, ref7, ref8, ref9, ref10, ref11, ref12, ref13, ref14, ref15, ref16, ref17, ref18, ref19, ref20, ref21, ref22, ref23, ref24, ref25, ref26;
    let router = (0,router_.useRouter)();
    let { keyword  } = router.query;
    let lang = router.locale;
    const dispatch = (0,external_react_redux_.useDispatch)();
    const { 0: flower , 1: setflower  } = (0,external_react_.useState)(info2);
    console.log(info2);
    let info = (0,external_react_redux_.useSelector)((state)=>state.new.one_product_info
    );
    (0,external_react_.useEffect)(()=>{
        dispatch(Actions_get_one_product_info(info2));
    }, []);
    let current_currency = (0,external_react_redux_.useSelector)((state)=>state.token.current_currency
    );
    let InfoChange = (0,external_react_.useCallback)(()=>{
        let currency_id = external_js_cookie_default().get("currency_id");
        let currency_text = typeof currency_id !== "undefined" ? `?currency=${currency_id}` : "";
        external_axios_default()(`/flowers/show/${keyword}/${lang}${currency_text}`).then((res)=>{
            setflower(res.data);
            dispatch(Actions_get_one_product_info(res.data));
        }).catch(()=>{
            return null;
        });
    }, [
        current_currency,
        lang,
        keyword
    ]);
    (0,external_react_.useEffect)(()=>{
        InfoChange();
    }, [
        current_currency,
        lang,
        keyword
    ]);
    const { 0: selectedOption , 1: setSelectedOption  } = (0,external_react_.useState)("review");
    const handleOptionClick = (opt)=>()=>{
            setSelectedOption(opt);
        }
    ;
    let ratingArray = ()=>{
        var ref, ref27;
        let array = [];
        flower === null || flower === void 0 ? void 0 : (ref = flower.data) === null || ref === void 0 ? void 0 : (ref27 = ref.ratingList) === null || ref27 === void 0 ? void 0 : ref27.map((one)=>{
            array === null || array === void 0 ? void 0 : array.push({
                "@type": "Review",
                "name": 'Product Review',
                "author": {
                    "@type": "Person",
                    "name": one === null || one === void 0 ? void 0 : one.userFio
                },
                "datePublished": one === null || one === void 0 ? void 0 : one.date,
                "reviewBody": one.comment,
                "reviewRating": {
                    "@type": "Rating",
                    "ratingValue": one.ball,
                    "worstRating": "0",
                    "bestRating": "5"
                }
            });
        });
        return array;
    };
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(NavbarLayout/* default */.Z, {
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx(external_react_helmet_.Helmet, {
                children: /*#__PURE__*/ jsx_runtime_.jsx("script", {
                    type: "application/ld+json",
                    children: JSON.stringify({
                        "@contexts": "http://schema.org/",
                        "@type": "Product",
                        "name": info2 === null || info2 === void 0 ? void 0 : (ref28 = info2.data) === null || ref28 === void 0 ? void 0 : ref28.name,
                        "image": (info2 === null || info2 === void 0 ? void 0 : (ref1 = info2.data) === null || ref1 === void 0 ? void 0 : (ref2 = ref1.image) === null || ref2 === void 0 ? void 0 : ref2.length) >= 1 ? info2 === null || info2 === void 0 ? void 0 : (ref3 = info2.data) === null || ref3 === void 0 ? void 0 : ref3.image[0] : "https://api.dana.uz/storage/images/noimg.jpg",
                        "description": info2 === null || info2 === void 0 ? void 0 : (ref4 = info2.data) === null || ref4 === void 0 ? void 0 : ref4.description,
                        "offers": {
                            "@type": "Offer",
                            "priceCurrency": "UZS",
                            "price": info2 === null || info2 === void 0 ? void 0 : (ref5 = info2.data) === null || ref5 === void 0 ? void 0 : ref5.priceFlowers,
                            "availability": "https://schema.org/InStock",
                            "itemCondition": "https://schema.org/NewCondition"
                        },
                        "aggregateRating": {
                            "@type": "AggregateRating",
                            "ratingValue": info2 === null || info2 === void 0 ? void 0 : (ref6 = info2.data) === null || ref6 === void 0 ? void 0 : ref6.rating
                        },
                        "review": ratingArray()
                    })
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(external_next_seo_.NextSeo, {
                title: flower === null || flower === void 0 ? void 0 : (ref7 = flower.seo) === null || ref7 === void 0 ? void 0 : ref7.view_mtitle,
                description: flower === null || flower === void 0 ? void 0 : (ref8 = flower.seo) === null || ref8 === void 0 ? void 0 : ref8.view_mdescription,
                additionalMetaTags: [
                    {
                        name: 'keyword',
                        content: flower === null || flower === void 0 ? void 0 : (ref9 = flower.seo) === null || ref9 === void 0 ? void 0 : ref9.view_mkeywords
                    }, 
                ],
                openGraph: {
                    type: "product",
                    title: info2 === null || info2 === void 0 ? void 0 : (ref10 = info2.seo) === null || ref10 === void 0 ? void 0 : ref10.view_share_title,
                    site_name: info2 === null || info2 === void 0 ? void 0 : (ref11 = info2.seo) === null || ref11 === void 0 ? void 0 : ref11.view_share_sitename,
                    description: info2 === null || info2 === void 0 ? void 0 : (ref12 = info2.seo) === null || ref12 === void 0 ? void 0 : ref12.view_share_description,
                    images: [
                        {
                            url: info2 === null || info2 === void 0 ? void 0 : (ref13 = info2.data) === null || ref13 === void 0 ? void 0 : ref13.image[0],
                            alt: info2 === null || info2 === void 0 ? void 0 : (ref14 = info2.data) === null || ref14 === void 0 ? void 0 : ref14.name,
                            width: 400,
                            height: 400
                        }
                    ]
                }
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx(breadcrumb, {
                        place: flower.data.bredcrumbs
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx(ProductIntro/* default */.Z, {
                        rating: flower === null || flower === void 0 ? void 0 : (ref15 = flower.data) === null || ref15 === void 0 ? void 0 : ref15.rating,
                        shopName: flower === null || flower === void 0 ? void 0 : (ref16 = flower.data) === null || ref16 === void 0 ? void 0 : ref16.shop_name,
                        imgUrl: (flower === null || flower === void 0 ? void 0 : (ref17 = flower.data) === null || ref17 === void 0 ? void 0 : (ref18 = ref17.image) === null || ref18 === void 0 ? void 0 : ref18.length) >= 1 ? flower === null || flower === void 0 ? void 0 : (ref19 = flower.data) === null || ref19 === void 0 ? void 0 : ref19.image : [
                            "https://api.dana.uz/storage/images/noimg.jpg", 
                        ],
                        title: flower === null || flower === void 0 ? void 0 : (ref20 = flower.data) === null || ref20 === void 0 ? void 0 : ref20.name,
                        price: flower === null || flower === void 0 ? void 0 : (ref21 = flower.data) === null || ref21 === void 0 ? void 0 : ref21.price,
                        id: flower === null || flower === void 0 ? void 0 : (ref22 = flower.data) === null || ref22 === void 0 ? void 0 : ref22.keyword,
                        keyword2: flower === null || flower === void 0 ? void 0 : (ref23 = flower.data) === null || ref23 === void 0 ? void 0 : ref23.shop_keyword,
                        categoryKeyword: flower === null || flower === void 0 ? void 0 : (ref24 = flower.data) === null || ref24 === void 0 ? void 0 : ref24.categoryKeyword,
                        deliveryTime: flower === null || flower === void 0 ? void 0 : (ref25 = flower.data) === null || ref25 === void 0 ? void 0 : ref25.shopDeliveryTime
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx(FlexBox["default"], {
                        id: "scroll_to_detail",
                        borderBottom: "1px solid",
                        borderColor: "gray.400",
                        mt: "20px",
                        mb: "26px",
                        children: /*#__PURE__*/ jsx_runtime_.jsx(Typography.H5, {
                            className: "cursor-pointer",
                            p: "4px 10px",
                            color: selectedOption === "review" ? "primary.main" : "text.muted",
                            onClick: handleOptionClick("review"),
                            borderBottom: selectedOption === "review" && "2px solid",
                            borderColor: "primary.main",
                            children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                                id: "Review"
                            })
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx(Box["default"], {
                        mb: "50px",
                        children: selectedOption === "review" && /*#__PURE__*/ jsx_runtime_.jsx(products_ProductReview, {
                        })
                    }),
                    (info === null || info === void 0 ? void 0 : (ref26 = info.smilarFlowers) === null || ref26 === void 0 ? void 0 : ref26.length) !== 0 ? /*#__PURE__*/ jsx_runtime_.jsx(products_RelatedProducts, {
                    }) : ""
                ]
            })
        ]
    }));
};
async function getStaticPaths() {
    const paths = [
        {
            params: {
                id: 'roses',
                keyword: 'plotno-sobrannaya-kompoziciya-iz-krasnyh-i-belyh-roz-268'
            }
        },
        {
            params: {
                id: 'rosebestseller-flowers',
                keyword: 'nebolshoy-buket-iz-9-belyh-roz-266'
            }
        }
    ];
    return {
        paths,
        fallback: 'blocking'
    };
}
async function getStaticProps(ctx) {
    try {
        let lang = ctx.locale;
        let { keyword  } = ctx.params;
        const request3 = await external_axios_default()(`${Variables/* BASE_URL */._n}/flowers/show/${keyword}/${lang}`);
        const answer2 = request3.data;
        return {
            props: {
                info2: answer2,
                redirect: false
            },
            revalidate: 2
        };
    } catch  {
        return {
            notFound: true
        };
    }
}
/* harmony default export */ const _keyword_ = (ProductDetails);


/***/ }),

/***/ 8130:
/***/ ((module) => {

module.exports = require("@material-ui/core");

/***/ }),

/***/ 7730:
/***/ ((module) => {

module.exports = require("@material-ui/core/ClickAwayListener");

/***/ }),

/***/ 6491:
/***/ ((module) => {

module.exports = require("@material-ui/core/Grow");

/***/ }),

/***/ 640:
/***/ ((module) => {

module.exports = require("@material-ui/core/Paper");

/***/ }),

/***/ 2767:
/***/ ((module) => {

module.exports = require("@material-ui/core/Popper");

/***/ }),

/***/ 8308:
/***/ ((module) => {

module.exports = require("@material-ui/core/styles");

/***/ }),

/***/ 2105:
/***/ ((module) => {

module.exports = require("@material-ui/icons");

/***/ }),

/***/ 3935:
/***/ ((module) => {

module.exports = require("@material-ui/icons/Add");

/***/ }),

/***/ 66:
/***/ ((module) => {

module.exports = require("@material-ui/icons/AddShoppingCart");

/***/ }),

/***/ 7149:
/***/ ((module) => {

module.exports = require("@material-ui/icons/Remove");

/***/ }),

/***/ 6072:
/***/ ((module) => {

module.exports = require("@mui/lab");

/***/ }),

/***/ 6715:
/***/ ((module) => {

module.exports = require("@mui/lab/AdapterDateFns");

/***/ }),

/***/ 9904:
/***/ ((module) => {

module.exports = require("@mui/lab/LocalizationProvider");

/***/ }),

/***/ 9409:
/***/ ((module) => {

module.exports = require("@mui/material/Accordion");

/***/ }),

/***/ 8279:
/***/ ((module) => {

module.exports = require("@mui/material/AccordionDetails");

/***/ }),

/***/ 4604:
/***/ ((module) => {

module.exports = require("@mui/material/AccordionSummary");

/***/ }),

/***/ 6042:
/***/ ((module) => {

module.exports = require("@mui/material/TextField");

/***/ }),

/***/ 8442:
/***/ ((module) => {

module.exports = require("@mui/material/styles");

/***/ }),

/***/ 2038:
/***/ ((module) => {

module.exports = require("@styled-system/css");

/***/ }),

/***/ 9099:
/***/ ((module) => {

module.exports = require("@styled-system/theme-get");

/***/ }),

/***/ 2167:
/***/ ((module) => {

module.exports = require("axios");

/***/ }),

/***/ 8103:
/***/ ((module) => {

module.exports = require("clsx");

/***/ }),

/***/ 4146:
/***/ ((module) => {

module.exports = require("date-fns");

/***/ }),

/***/ 2296:
/***/ ((module) => {

module.exports = require("formik");

/***/ }),

/***/ 6734:
/***/ ((module) => {

module.exports = require("js-cookie");

/***/ }),

/***/ 6517:
/***/ ((module) => {

module.exports = require("lodash");

/***/ }),

/***/ 6641:
/***/ ((module) => {

module.exports = require("next-seo");

/***/ }),

/***/ 562:
/***/ ((module) => {

module.exports = require("next/dist/server/denormalize-page-path.js");

/***/ }),

/***/ 8028:
/***/ ((module) => {

module.exports = require("next/dist/server/image-config.js");

/***/ }),

/***/ 4957:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/head.js");

/***/ }),

/***/ 3539:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/i18n/detect-domain-locale.js");

/***/ }),

/***/ 4014:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/i18n/normalize-locale-path.js");

/***/ }),

/***/ 5832:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/loadable.js");

/***/ }),

/***/ 8020:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/mitt.js");

/***/ }),

/***/ 4964:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router-context.js");

/***/ }),

/***/ 9565:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/get-asset-path-from-route.js");

/***/ }),

/***/ 4365:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/get-middleware-regex.js");

/***/ }),

/***/ 1428:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/is-dynamic.js");

/***/ }),

/***/ 1292:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/parse-relative-url.js");

/***/ }),

/***/ 979:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/querystring.js");

/***/ }),

/***/ 6052:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/resolve-rewrites.js");

/***/ }),

/***/ 4226:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/route-matcher.js");

/***/ }),

/***/ 5052:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/route-regex.js");

/***/ }),

/***/ 3018:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/to-base-64.js");

/***/ }),

/***/ 9232:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ 968:
/***/ ((module) => {

module.exports = require("next/head");

/***/ }),

/***/ 1853:
/***/ ((module) => {

module.exports = require("next/router");

/***/ }),

/***/ 2947:
/***/ ((module) => {

module.exports = require("pure-react-carousel");

/***/ }),

/***/ 6689:
/***/ ((module) => {

module.exports = require("react");

/***/ }),

/***/ 6405:
/***/ ((module) => {

module.exports = require("react-dom");

/***/ }),

/***/ 6804:
/***/ ((module) => {

module.exports = require("react-facebook-login/dist/facebook-login-render-props");

/***/ }),

/***/ 67:
/***/ ((module) => {

module.exports = require("react-google-login");

/***/ }),

/***/ 2791:
/***/ ((module) => {

module.exports = require("react-helmet");

/***/ }),

/***/ 3967:
/***/ ((module) => {

module.exports = require("react-inner-image-zoom");

/***/ }),

/***/ 4648:
/***/ ((module) => {

module.exports = require("react-input-mask");

/***/ }),

/***/ 3126:
/***/ ((module) => {

module.exports = require("react-intl");

/***/ }),

/***/ 9252:
/***/ ((module) => {

module.exports = require("react-lazy-load-image-component");

/***/ }),

/***/ 9700:
/***/ ((module) => {

module.exports = require("react-paginate");

/***/ }),

/***/ 2586:
/***/ ((module) => {

module.exports = require("react-quill");

/***/ }),

/***/ 6022:
/***/ ((module) => {

module.exports = require("react-redux");

/***/ }),

/***/ 6158:
/***/ ((module) => {

module.exports = require("react-share");

/***/ }),

/***/ 7128:
/***/ ((module) => {

module.exports = require("react-svg");

/***/ }),

/***/ 3742:
/***/ ((module) => {

module.exports = require("react-yandex-maps");

/***/ }),

/***/ 997:
/***/ ((module) => {

module.exports = require("react/jsx-runtime");

/***/ }),

/***/ 6981:
/***/ ((module) => {

module.exports = require("reactstrap");

/***/ }),

/***/ 7518:
/***/ ((module) => {

module.exports = require("styled-components");

/***/ }),

/***/ 5834:
/***/ ((module) => {

module.exports = require("styled-system");

/***/ }),

/***/ 5609:
/***/ ((module) => {

module.exports = require("yup");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [2652,9894,8579,5218,1972,9374,4154,2389,8936,8661,695,4396,8414,3756,4790,12,7198,8167,7788,8970,197,3887,639,3689,2599,9868,2900], () => (__webpack_exec__(2932)));
module.exports = __webpack_exports__;

})();