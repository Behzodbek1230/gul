import axios from "axios"
let fetchData = url => axios(url).then((res)=>res.data)

export default fetchData