import Link from "next/link"
import {NextSeo} from "next-seo";
import DashboardPageHeader2 from "../components/layout/DashboardPageHeader2";
import {Card1} from "../components/Card1";
import Grid from "../components/grid/Grid";
import VendorDashboardLayout from "../components/layout/VendorDashboardLayout";
import {useIntl} from "react-intl";
import {useEffect, useState} from "react";
import axios from "axios";
import {useRouter} from "next/router";
import Loading from "../components/Loading";
let Sitemap = ()=>{
    let intl = useIntl()
    let router = useRouter()
    let [loading,setloading] = useState(true)
    let lang = useRouter().locale
    let [sitemap,setsitemap] = useState([])
    useEffect(()=>{
        axios.get(`/sitemap/map-of-site/${lang}`)
            .then((res)=>{
                setsitemap(res.data)
                setloading(false)
            })
            .catch(()=>null)
    },[useRouter().locale])
    if(loading){
        return<>
            <NextSeo
                title={intl.formatMessage({id:"Sitemap"})}
                description={intl.formatMessage({id:"Sitemap"})}
                additionalMetaTags={[{
                    name: 'keyword',
                    content:intl.formatMessage({id:"Sitemap"})
                }, ]}
            />
            <Loading />
        </>
    }
    else{
        return(

            <VendorDashboardLayout >
                <NextSeo
                    title={intl.formatMessage({id:"Sitemap"})}
                    description={intl.formatMessage({id:"Sitemap"})}
                    additionalMetaTags={[{
                        name: 'keyword',
                        content:intl.formatMessage({id:"Sitemap"})
                    }, ]}
                />
                <div style={{marginBottom:"50px"}}>
                    <div  style={{color:"#f7961"}}>
                        <DashboardPageHeader2
                            title={intl.formatMessage({id:"Sitemap"})}
                        />
                        <Card1 style={{marginLeft:"13px",marginRight:"13px"}}>
                            <Grid  style={{padding:"5px",paddingBottom:"5px",paddingTop:"8px"}}>
                                <div>
                                    <div className="row">
                                        {sitemap?.map(one=><div className="col-md-6 mb-2 mt-2 ">
                                                <h6>
                                                    {one?.title}
                                                </h6>
                                                {one?.children?.map(cat=><div><Link href={`/${cat?.keyword}`} locale={router?.locale}>{cat?.title}</Link><br/></div>)}
                                            </div>
                                        )}
                                    </div>

                                </div>
                            </Grid>
                        </Card1>

                    </div>
                </div>
            </VendorDashboardLayout>
        )
    }
}
export default Sitemap;
