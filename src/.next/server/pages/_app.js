(() => {
var exports = {};
exports.id = 2888;
exports.ids = [2888,9758];
exports.modules = {

/***/ 8794:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
const categoryfetch_success = (data)=>{
    return {
        type: "GET_CATEGORY",
        payload: data
    };
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (categoryfetch_success);


/***/ }),

/***/ 5153:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "_n": () => (/* binding */ BASE_URL),
/* harmony export */   "wL": () => (/* binding */ GOOGLE_CLIENT_ID),
/* harmony export */   "lZ": () => (/* binding */ FACEBOOK_APP_ID),
/* harmony export */   "px": () => (/* binding */ SITE_NAME)
/* harmony export */ });
const BASE_URL = "https://api.dana.uz/api/v1";
const GOOGLE_CLIENT_ID = "160068010891-2mnlkm5lmf3lka7ntnqcvs6jjn6ckr3p.apps.googleusercontent.com";
const FACEBOOK_APP_ID = "861976684507312";
const SITE_NAME = "https://dana.uz";


/***/ }),

/***/ 3000:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ _app)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "next/head"
var head_ = __webpack_require__(968);
var head_default = /*#__PURE__*/__webpack_require__.n(head_);
;// CONCATENATED MODULE: external "react-scroll-to-top"
const external_react_scroll_to_top_namespaceObject = require("react-scroll-to-top");
var external_react_scroll_to_top_default = /*#__PURE__*/__webpack_require__.n(external_react_scroll_to_top_namespaceObject);
;// CONCATENATED MODULE: external "aos"
const external_aos_namespaceObject = require("aos");
var external_aos_default = /*#__PURE__*/__webpack_require__.n(external_aos_namespaceObject);
// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(1853);
var router_default = /*#__PURE__*/__webpack_require__.n(router_);
;// CONCATENATED MODULE: external "nprogress"
const external_nprogress_namespaceObject = require("nprogress");
var external_nprogress_default = /*#__PURE__*/__webpack_require__.n(external_nprogress_namespaceObject);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "styled-components"
var external_styled_components_ = __webpack_require__(7518);
// EXTERNAL MODULE: ./contexts/app/AppContext.tsx + 4 modules
var AppContext = __webpack_require__(8936);
;// CONCATENATED MODULE: ./utils/globalStyles.ts

const GlobalStyles = external_styled_components_.createGlobalStyle`
  html,
  body {
    padding: 0;
    margin: 0;
    font-size: 14px;
    background: ${({ theme  })=>theme.colors.body.default
};
    color: ${({ theme  })=>theme.colors.body.text
};
    transition: all 0.50s linear;
    font-family: Open Sans, Roboto, -apple-system, BlinkMacSystemFont, Segoe UI,
    Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
    sans-serif;
    line-height: 1.5;
  }

  html {
    font-size: 16px;
  }

  * {
    box-sizing: border-box;
  }
  a {
    text-decoration: none;
    color: ${({ theme  })=>theme.colors.body.text
};
  }
  .cursor-pointer {
    cursor: pointer;
  }

#nprogress {
  pointer-events: none;
}

#nprogress .bar {
  background: ${({ theme  })=>theme.colors.primary.main
};

  position: fixed;
  z-index: 1031;
  top: 0;
  left: 0;

  width: 100%;
  height: 3px;
  border-radius: 0px 4px 4px 0px;
  overflow: hidden;
}

/* Fancy blur effect */
#nprogress .peg {
  display: block;
  position: absolute;
  right: 0px;
  width: 100px;
  height: 100%;
  box-shadow: 0 0 10px ${({ theme  })=>theme.colors.primary.main
}, 0 0 5px ${({ theme  })=>theme.colors.primary.main
};
  opacity: 1.0;

  -webkit-transform: rotate(3deg) translate(0px, -4px);
      -ms-transform: rotate(3deg) translate(0px, -4px);
          transform: rotate(3deg) translate(0px, -4px);
}

/* Remove these to get rid of the spinner */
#nprogress .spinner {
  display: block;
  position: fixed;
  z-index: 1031;
  top: 15px;
  right: 15px;
}

#nprogress .spinner-icon {
  width: 18px;
  height: 18px;
  box-sizing: border-box;

  border: solid 2px transparent;
  border-top-color: ${({ theme  })=>theme.colors.primary.main
};
  border-left-color: ${({ theme  })=>theme.colors.primary.main
};
  border-radius: 50%;

  -webkit-animation: nprogress-spinner 400ms linear infinite;
          animation: nprogress-spinner 400ms linear infinite;
}

.nprogress-custom-parent {
  overflow: hidden;
  position: relative;
}

.nprogress-custom-parent #nprogress .spinner,
.nprogress-custom-parent #nprogress .bar {
  position: absolute;
}

@-webkit-keyframes nprogress-spinner {
  0%   { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}
@keyframes nprogress-spinner {
  0%   { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
`;

// EXTERNAL MODULE: ./utils/constants.ts
var constants = __webpack_require__(9758);
// EXTERNAL MODULE: ./utils/themeColors.ts
var themeColors = __webpack_require__(8970);
;// CONCATENATED MODULE: ./utils/themeShadows.ts
const shadows = [
    "none",
    "0px 2px 1px -1px rgba(0, 0, 0, 0.06),0px 1px 1px 0px rgba(0, 0, 0, 0.042),0px 1px 3px 0px rgba(0, 0, 0, 0.036)",
    "0px 3px 1px -2px rgba(0, 0, 0, 0.06),0px 2px 2px 0px rgba(0, 0, 0, 0.042),0px 1px 5px 0px rgba(0, 0, 0, 0.036)",
    "0px 3px 3px -2px rgba(0, 0, 0, 0.06),0px 3px 4px 0px rgba(0, 0, 0, 0.042),0px 1px 8px 0px rgba(0, 0, 0, 0.036)",
    "0px 6px 26px rgba(0, 0, 0, 0.05)",
    "0px 15px 50px rgba(0, 0, 0, 0.06)",
    "0px 3px 5px -1px rgba(0, 0, 0, 0.06),0px 5px 8px 0px rgba(0, 0, 0, 0.042),0px 1px 14px 0px rgba(0, 0, 0, 0.036)",
    "0px 3px 5px -1px rgba(0, 0, 0, 0.06),0px 6px 10px 0px rgba(0, 0, 0, 0.042),0px 1px 18px 0px rgba(0, 0, 0, 0.036)",
    "0px 4px 5px -2px rgba(0, 0, 0, 0.06),0px 7px 10px 1px rgba(0, 0, 0, 0.042),0px 2px 16px 1px rgba(0, 0, 0, 0.036)",
    "0px 5px 5px -3px rgba(0, 0, 0, 0.06),0px 8px 10px 1px rgba(0, 0, 0, 0.042),0px 3px 14px 2px rgba(0, 0, 0, 0.036)",
    "0px 5px 6px -3px rgba(0, 0, 0, 0.06),0px 9px 12px 1px rgba(0, 0, 0, 0.042),0px 3px 16px 2px rgba(0, 0, 0, 0.036)",
    "0px 6px 6px -3px rgba(0, 0, 0, 0.06),0px 10px 14px 1px rgba(0, 0, 0, 0.042),0px 4px 18px 3px rgba(0, 0, 0, 0.036)",
    "0px 6px 7px -4px rgba(0, 0, 0, 0.06),0px 11px 15px 1px rgba(0, 0, 0, 0.042),0px 4px 20px 3px rgba(0, 0, 0, 0.036)",
    "0px 7px 8px -4px rgba(0, 0, 0, 0.06),0px 12px 17px 2px rgba(0, 0, 0, 0.042),0px 5px 22px 4px rgba(0, 0, 0, 0.036)",
    "0px 7px 8px -4px rgba(0, 0, 0, 0.06),0px 13px 19px 2px rgba(0, 0, 0, 0.042),0px 5px 24px 4px rgba(0, 0, 0, 0.036)",
    "0px 7px 9px -4px rgba(0, 0, 0, 0.06),0px 14px 21px 2px rgba(0, 0, 0, 0.042),0px 5px 26px 4px rgba(0, 0, 0, 0.036)",
    "0px 8px 9px -5px rgba(0, 0, 0, 0.06),0px 15px 22px 2px rgba(0, 0, 0, 0.042),0px 6px 28px 5px rgba(0, 0, 0, 0.036)",
    "0px 8px 10px -5px rgba(0, 0, 0, 0.06),0px 16px 24px 2px rgba(0, 0, 0, 0.042),0px 6px 30px 5px rgba(0, 0, 0, 0.036)",
    "0px 8px 11px -5px rgba(0, 0, 0, 0.06),0px 17px 26px 2px rgba(0, 0, 0, 0.042),0px 6px 32px 5px rgba(0, 0, 0, 0.036)",
    "0px 9px 11px -5px rgba(0, 0, 0, 0.06),0px 18px 28px 2px rgba(0, 0, 0, 0.042),0px 7px 34px 6px rgba(0, 0, 0, 0.036)",
    "0px 9px 12px -6px rgba(0, 0, 0, 0.06),0px 19px 29px 2px rgba(0, 0, 0, 0.042),0px 7px 36px 6px rgba(0, 0, 0, 0.036)",
    "0px 10px 13px -6px rgba(0, 0, 0, 0.06),0px 20px 31px 3px rgba(0, 0, 0, 0.042),0px 8px 38px 7px rgba(0, 0, 0, 0.036)",
    "0px 10px 13px -6px rgba(0, 0, 0, 0.06),0px 20px 31px 3px rgba(0, 0, 0, 0.042),0px 8px 38px 7px rgba(0, 0, 0, 0.036)",
    "0px 10px 13px -6px rgba(0, 0, 0, 0.06),0px 20px 31px 3px rgba(0, 0, 0, 0.042),0px 8px 38px 7px rgba(0, 0, 0, 0.036)",
    "0px 10px 13px -6px rgba(0, 0, 0, 0.06),0px 20px 31px 3px rgba(0, 0, 0, 0.042),0px 8px 38px 7px rgba(0, 0, 0, 0.036)",
    "0px 10px 13px -6px rgba(0, 0, 0, 0.06),0px 20px 31px 3px rgba(0, 0, 0, 0.042),0px 8px 38px 7px rgba(0, 0, 0, 0.036)", 
];
shadows.small = "0px 1px 3px rgba(3, 0, 71, 0.09)";
shadows.regular = "0px 4px 16px rgba(43, 52, 69, 0.1)";
shadows.large = "0px 8px 45px rgba(3, 0, 71, 0.09)";
shadows.border = "0px 0px 28px rgba(3, 0, 71, 0.04)";
shadows.badge = "0px 8px 20px -5px rgba(255, 103, 128, 0.9)";
/* harmony default export */ const themeShadows = (shadows);

;// CONCATENATED MODULE: ./utils/theme.ts



const breakpoints = Object.keys(constants/* deviceSize */.J).map((key)=>constants/* deviceSize */.J[key] + "px"
);
breakpoints.sm = breakpoints[0];
breakpoints.md = breakpoints[1];
breakpoints.lg = breakpoints[2];
breakpoints.xl = breakpoints[3];
const theme = {
    colors: themeColors/* colors */.O,
    shadows: themeShadows,
    breakpoints
};

// EXTERNAL MODULE: external "react-redux"
var external_react_redux_ = __webpack_require__(6022);
;// CONCATENATED MODULE: external "redux"
const external_redux_namespaceObject = require("redux");
;// CONCATENATED MODULE: ../Redux/Reducers/TokenReducer.js
const initialState = {
    token: null,
    user: {
    },
    isLoading: true,
    error: "",
    isLoggedIn: false,
    open: false,
    current_language: "uz",
    one_shop_orders: [],
    personal_orders: [],
    order_id: null,
    cards2: [],
    currencies: [],
    current_currency: [],
    postcard_text: "",
    current_product: "",
    services: []
};
const TokenReducer = (state = initialState, action)=>{
    switch(action.type){
        case "GET_TOKEN":
            return {
                ...state,
                token: action.payload,
                isLoggedIn: true
            };
        case "GET_USER_INFO":
            return {
                ...state,
                isLoading: false,
                user: action.payload
            };
        case "GET_USER_INFO_FAILURE":
            return {
                ...state,
                isLoading: false,
                error: action.error
            };
        case "OPEN_LOGIN":
            return {
                ...state,
                open: true
            };
        case "CLOSE_LOGIN":
            return {
                ...state,
                open: false
            };
        case "LOG_OUT":
            return {
                ...state,
                isLoggedIn: false
            };
        case "ONE_SHOP_ORDERS":
            return {
                ...state,
                one_shop_orders: action.payload
            };
        case "GET_PERSONAL_ORDERS":
            return {
                ...state,
                personal_orders: action.payload
            };
        case "GET_ORDER_ID":
            return {
                ...state,
                order_id: action.payload
            };
        case "GET_CARDS2":
            return {
                ...state,
                cards2: action.payload
            };
        case "GET_CURRENCY_LIST":
            return {
                ...state,
                currencies: action.payload
            };
        case "GET_CURRENT_CURRENCY":
            return {
                ...state,
                current_currency: action.payload
            };
        case "GET_POSTCARD_TEXT":
            return {
                ...state,
                postcard_text: action.payload
            };
        case "GET_CURRENT_PRODUCT_KEYWORD":
            return {
                ...state,
                current_product: action.payload
            };
        case "GET_SERVICES":
            return {
                ...state,
                services: action.payload
            };
        default:
            return state;
    }
};

;// CONCATENATED MODULE: external "redux-thunk"
const external_redux_thunk_namespaceObject = require("redux-thunk");
var external_redux_thunk_default = /*#__PURE__*/__webpack_require__.n(external_redux_thunk_namespaceObject);
// EXTERNAL MODULE: external "axios"
var external_axios_ = __webpack_require__(2167);
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_);
// EXTERNAL MODULE: ../Redux/Actions/CategoryFetching.js
var CategoryFetching = __webpack_require__(8794);
// EXTERNAL MODULE: external "js-cookie"
var external_js_cookie_ = __webpack_require__(6734);
var external_js_cookie_default = /*#__PURE__*/__webpack_require__.n(external_js_cookie_);
;// CONCATENATED MODULE: ../Redux/Actions/fetch_language_list.js
const fetch_language_list = (data)=>{
    return {
        type: "FETCH_LANGUAGE_LIST",
        payload: data
    };
};
/* harmony default export */ const Actions_fetch_language_list = (fetch_language_list);

// EXTERNAL MODULE: ./components/Variables.tsx
var Variables = __webpack_require__(5153);
;// CONCATENATED MODULE: ../Redux/Reducers/New Reducer.js







const New_Reducer_initialState = {
    lang: external_js_cookie_default().get("lang") || "uz",
    coordinates: [],
    category: [],
    category_loading: true,
    shop: [],
    update_user_info: false,
    language_list: [],
    banner: null,
    shop_products: [],
    company_info: [],
    category_product: [],
    one_shop_products: [],
    one_product_info: [],
    cart_products: [],
    search_results: [],
    category_id: null,
    modal: false,
    footer: [],
    shops: [],
    CartIsChanged: false,
    CartLoadedFully: "",
    seoText: "",
    wishlist: []
};
const languagereducer = (state = New_Reducer_initialState, action)=>{
    switch(action.type){
        case "CHANGE_LANGUAGE":
            return {
                ...state,
                lang: action.payload
            };
        case "GET_COORDINATES":
            return {
                ...state,
                coordinates: action.payload
            };
        case "GET_CATEGORY":
            return {
                ...state,
                category: action.payload,
                category_loading: false
            };
        case "GET_SHOP":
            return {
                ...state,
                shop: action.payload
            };
        case "UPDATE_USER_INFO":
            return {
                ...state,
                update_user_info: update_user_info ? false : true
            };
        case "FETCH_LANGUAGE_LIST":
            return {
                ...state,
                language_list: action.payload
            };
        case "GET_BANNER":
            return {
                ...state,
                banner: action.payload
            };
        case "GET_SHOP_PRODUCTS":
            return {
                ...state,
                shop_products: action.payload
            };
        case "GET_COMPANY_INFO":
            return {
                ...state,
                company_info: action.payload
            };
        case "GET_CATEGORY_PRODUCTS":
            return {
                ...state,
                category_product: action.payload
            };
        case "GET_ONE_SHOP_PRODUCTS":
            return {
                ...state,
                one_shop_products: action.payload
            };
        case "GET_ONE_PRODUCT_INFO":
            return {
                ...state,
                one_product_info: action.payload
            };
        case "GET_CART_PRODUCTS":
            return {
                ...state,
                cart_products: action.payload
            };
        case "GET_SEARCH_RESULTS":
            return {
                ...state,
                search_results: action.payload
            };
        case "GET_CATEGORY_ID":
            return {
                ...state,
                category_id: action.payload
            };
        case "CHANGE_MODAL_STATUS":
            return {
                ...state,
                modal: action.payload
            };
        case "GET_FOOTER_ACTIONS":
            return {
                ...state,
                footer: action.payload
            };
        case "GET_SHOP_LIST":
            return {
                ...state,
                shops: action.payload
            };
        case "CHANGE_CART":
            return {
                ...state,
                CartIsChanged: action.payload
            };
        case "CART_LOADED_FULLY":
            return {
                ...state,
                CartLoadedFully: action.payload
            };
        case "GET_SEO_TEXT":
            return {
                ...state,
                seoText: action.payload
            };
        case "GET_WISHLIST":
            return {
                ...state,
                wishlist: action.payload
            };
        default:
            return state;
    }
};
const fetch_category_request = ()=>{
    return function(dispatch) {
        let lang;
        const lang2 = external_js_cookie_default().get("lang") || "uz";
        if (typeof lang2 !== "undefined") {
            lang = lang2;
        } else {
            lang = "uz";
        }
        external_axios_default()(`${Variables/* BASE_URL */._n}/flowers/main-category/list/${lang}`).then((response)=>{
            dispatch((0,CategoryFetching/* default */.Z)(response.data));
        }).catch(()=>null
        );
    };
};
const fetch_language_request = ()=>{
    return function(dispatch) {
        let lang = external_js_cookie_default().get("lang");
        external_axios_default()(`${Variables/* BASE_URL */._n}/references/language/${lang}`).then((response)=>{
            dispatch(Actions_fetch_language_list(response.data));
        }).catch(()=>null
        );
    };
};
const fetch_company_info = ()=>{
    return function() {
    };
};
/* harmony default export */ const New_Reducer = (languagereducer);

;// CONCATENATED MODULE: ../Redux/stores.js




// import {persistStore,persistReducer} from "redux-persist"
// import storage from "redux-persist/lib/storage"

// const persistConfig = {
//     key:"root",
//     storage,
//     whitelist:[
//         "token",
//         'new',
//     ]
// }
const rootreducer3 = (0,external_redux_namespaceObject.combineReducers)({
    token: TokenReducer,
    new: New_Reducer
});
const store = (0,external_redux_namespaceObject.createStore)(rootreducer3, (0,external_redux_namespaceObject.applyMiddleware)((external_redux_thunk_default())));
// store.subscribe(()=>{
//     console.log(store.getState())
// })
store.dispatch(fetch_category_request());
store.dispatch(fetch_language_request());
store.dispatch(fetch_company_info());

// EXTERNAL MODULE: ../node_modules/react-datepicker/dist/react-datepicker.css
var react_datepicker = __webpack_require__(191);
// EXTERNAL MODULE: ../node_modules/react-phone-number-input/style.css
var style = __webpack_require__(5664);
;// CONCATENATED MODULE: ../content/compiled-locales/ru.json
const ru_namespaceObject = JSON.parse('{"Add PostCard":[{"type":0,"value":"Добавить открытку"}],"All":[{"type":0,"value":"Все"}],"Amount":[{"type":0,"value":"Количество"}],"Any Document":[{"type":0,"value":"Любой документ"}],"Apply Voucher":[{"type":0,"value":"Применить ваучер"}],"Back To Order List":[{"type":0,"value":"Вернуться к списку заказов"}],"Back to Product List":[{"type":0,"value":"Вернуться к списку продуктов"}],"Back to cart":[{"type":0,"value":"Вернуться в корзину"}],"Booked":[{"type":0,"value":"Количество забронированных продуктов заранее"}],"Buy Now":[{"type":0,"value":"Kупить сейчас"}],"Cards":[{"type":0,"value":"Карты"}],"Carriers":[{"type":0,"value":"Вакансии"}],"Cash":[{"type":0,"value":"Наличные"}],"Checkout Now":[{"type":0,"value":"Kупить cейчас"}],"Choose Your Gender":[{"type":0,"value":"Ваш пол"}],"Consumed_products":[{"type":0,"value":"Подержанные продукты"}],"Corparative Sales":[{"type":0,"value":"Корпоративное обслуживание"}],"Date Purchased":[{"type":0,"value":"Дата покупки"}],"Delivery Time":[{"type":0,"value":"Срок поставки"}],"Details":[{"type":0,"value":"Подробности"}],"Discount":[{"type":0,"value":"Скидка"}],"Discount(without percentage)":[{"type":0,"value":"Скидка % (необязательно)"}],"Female":[{"type":0,"value":"Женский"}],"Free":[{"type":0,"value":"бесплатно"}],"Full Description":[{"type":0,"value":"Полное описание"}],"Male":[{"type":0,"value":"Мужчина"}],"My Orders":[{"type":0,"value":"Мои заказы"}],"Name":[{"type":0,"value":"Имя"}],"Order":[{"type":0,"value":"Заказы"}],"Order Details":[{"type":0,"value":"Информация для заказа"}],"Order ID":[{"type":0,"value":"Hомер заказа"}],"Order_#":[{"type":0,"value":"Id"}],"Placed On":[{"type":0,"value":"Размещены нa"}],"Price Range":[{"type":0,"value":"Ценовой диапазон"}],"Product Name":[{"type":0,"value":"Hаименование товара"}],"Products":[{"type":0,"value":"Продукты"}],"Regular Price":[{"type":0,"value":"Цена"}],"Review":[{"type":0,"value":"Рассмотрение"}],"Select List":[{"type":0,"value":"Выбрать список"}],"Shipping":[{"type":0,"value":"Перевозки"}],"Shops":[{"type":0,"value":"Магазины"}],"Sitemap":[{"type":0,"value":"Карта сайта"}],"Sort By":[{"type":0,"value":"Сортировать по цене"}],"Status":[{"type":0,"value":"Положение дел"}],"Subscribe":[{"type":0,"value":"Подписывайся"}],"Subscribed":[{"type":0,"value":"Отписаться"}],"Subtotal":[{"type":0,"value":"Промежуточный итог"}],"Total":[{"type":0,"value":"Общий"}],"Type":[{"type":0,"value":"Тип"}],"Used Products":[{"type":0,"value":"Материалы"}],"Validity Period":[{"type":0,"value":"Срок годности"}],"Voucher":[{"type":0,"value":"Ваучер"}],"Your Message":[{"type":0,"value":"Твое сообщение"}],"Your Rating":[{"type":0,"value":"Ваш рейтинг"}],"Your Review":[{"type":0,"value":"Ваш обзор"}],"Your_order":[{"type":0,"value":"Твоя очередь"}],"acceptable_link":[{"type":0,"value":"Пожалуйста, введите правильную форму ссылки"}],"accepted_convenient_payment":[{"type":0,"value":"принято. Пожалуйста, выберите тип оплаты"}],"account_settings":[{"type":0,"value":"Настройки профиля"}],"actions":[{"type":0,"value":"Действия"}],"active":[{"type":0,"value":"Активный"}],"add_all_cart":[{"type":0,"value":"Добавить все в корзину"}],"add_comment_success":[{"type":0,"value":"Вы успешно прокомментировали"}],"add_links":[{"type":0,"value":"Добавить ссылку +"}],"add_new_product":[{"type":0,"value":"Добавить новый продукт"}],"add_product":[{"type":0,"value":"Добавить товар"}],"add_to_cart":[{"type":0,"value":"Добавить в корзину"}],"add_used_products":[{"type":0,"value":"Добавить использованный продукт +"}],"address":[{"type":0,"value":"Адреса"}],"all_rights":[{"type":0,"value":"Dana.uz - Все права защищены"}],"apply_now":[{"type":0,"value":"Применить сейчас"}],"back_to_product_list":[{"type":0,"value":"Вернуться к списку продуктов"}],"balance":[{"type":0,"value":"Остаток средств:"}],"banner_button":[{"type":0,"value":"Посетить"}],"birthdate":[{"type":0,"value":"Дата рождения"}],"blocked":[{"type":0,"value":"Заблокировано"}],"blogs":[{"type":0,"value":"Блоги"}],"bolim":[{"type":0,"value":"Раздел категории"}],"bonus":[{"type":0,"value":"Бонус"}],"book":[{"type":0,"value":"Покупка товаров"}],"budget":[{"type":0,"value":"Cчет"}],"budget_fulfill":[{"type":0,"value":"Пожалуйста, введите сумму денег"}],"business_account":[{"type":0,"value":"Бизнес аккаунт"}],"button_check_status":[{"type":0,"value":"проверьте cтатус заказа."}],"cancel":[{"type":0,"value":"Отмена"}],"card_type":[{"type":0,"value":"Тип карты"}],"cart":[{"type":0,"value":"Корзина"}],"cash_or_card":[{"type":0,"value":"Денежные средства/Карта"}],"cashback":[{"type":0,"value":"Возврат"}],"categories":[{"type":0,"value":"Категории"}],"category_name":[{"type":0,"value":"Название категории"}],"changed_info_success":[{"type":0,"value":"Информация вашего профиля успешно изменена"}],"check_button_status":[{"type":0,"value":"Cтатус заказа"}],"check_order_status":[{"type":0,"value":"Чтобы узнать статус заказа и оплатить введите его номер и "}],"choose_category":[{"type":0,"value":"Пожалуйста, выберите категорию"}],"choose_delivery_place":[{"type":0,"value":"Пожалуйста, выберите место доставки"}],"choose_delivery_time":[{"type":0,"value":"Выберите срок доставки"}],"choose_length_scales":[{"type":0,"value":"Пожалуйста, обратитесь к одной из шкал длины"}],"choose_payment":[{"type":0,"value":"Пожалуйста, выберите тип оплаты"}],"choose_place_map":[{"type":0,"value":"Пожалуйста, выберите место на карте"}],"company_name":[{"type":0,"value":"Название компании"}],"connectUs":[{"type":0,"value":"Подключите нас"}],"connectUsText":[{"type":0,"value":"Скоро придет"}],"connect_A":[{"type":0,"value":"Связаться с администратором"}],"contact":[{"type":0,"value":"Контакт"}],"corporate_send_sucess":[{"type":0,"value":"Сообщение успешно отправлено"}],"corporative_page_text1":[{"type":0,"value":"У нас есть букеты, подарки, сладости и другие товары для корпоративных клиентов на любой вкус и кошелек. Наши подарки созданы специально, чтобы произвести неизгладимое впечатление. При особом индивидуальном подходе к корпоративным клиентам мы предоставляем вашему клиенту или коллеге фирменные букеты с символикой организаций и предприятий, а также благодарим клиента, поздравляем коллег, украшаем офис, украшаем свадьбы, конференц-залы и другие необходимые условия."}],"corporative_page_text2":[{"type":0,"value":"Оплата может производиться наличными или банковским переводом."}],"correct_password":[{"type":0,"value":"Введите правильный пароль"}],"correct_phone":[{"type":0,"value":"Введите номер телефона в правильном формате"}],"create_shop":[{"type":0,"value":"Создать магазин"}],"create_shop_success":[{"type":0,"value":"Магазин успешно создан"}],"currency":[{"type":0,"value":"Валюта"}],"currency_required":[{"type":0,"value":"Пожалуйста, выберите валюту"}],"currency_unit":[{"type":0,"value":"Пожалуйста, выберите одну из валют"}],"dashboard":[{"type":0,"value":"ДРУГИЕ"}],"date":[{"type":0,"value":"Дата"}],"delete":[{"type":0,"value":"Удалить"}],"deliver":[{"type":0,"value":"Условия доставки"}],"delivered":[{"type":0,"value":"Успешно доставлено"}],"delivering":[{"type":0,"value":"В процессе доставки"}],"delivery_price":[{"type":0,"value":"Стоимость доставки"}],"delivery_time":[{"type":0,"value":"Срок доставки"}],"description":[{"type":0,"value":"Описание"}],"desktop_navigation_cabinet":[{"type":0,"value":"Кабинет"}],"document":[{"type":0,"value":"Законные документы"}],"drag_drop":[{"type":0,"value":"Перетащите изображение продукта сюда"}],"drag_drop2":[{"type":0,"value":"Перетащите файлы сюда"}],"edit_p":[{"type":0,"value":"Редактировать профиль"}],"edit_product":[{"type":0,"value":"Редактировать продукт"}],"email":[{"type":0,"value":"Эл. адрес"}],"empty":[{"type":0,"value":"Пустой"}],"empty_carrier":[{"type":0,"value":"Вакансии отсутствуют"}],"empty_cart":[{"type":0,"value":"Твоя корзина покупок пуста. Начать покупки"}],"error_booking":[{"type":0,"value":"Что-то пошло не так"}],"estimated_delivery":[{"type":0,"value":"Предполагаемый день доставки"}],"footer_header_link1":[{"type":0,"value":"О компании"}],"footer_header_link1_sublink1":[{"type":0,"value":"О нас"}],"footer_header_link1_sublink2":[{"type":0,"value":"Карьера"}],"footer_header_link1_sublink3":[{"type":0,"value":"Вакансии"}],"footer_header_link1_sublink4":[{"type":0,"value":"Сотрудничество"}],"footer_header_link2":[{"type":0,"value":"Обслуживание клиентов"}],"footer_header_link2_sublink1":[{"type":0,"value":"Центр помощи"}],"footer_header_link2_sublink2":[{"type":0,"value":"Как купить"}],"footer_header_link2_sublink3":[{"type":0,"value":"Возврат"}],"footer_header_link2_sublink4":[{"type":0,"value":"Услуги"}],"footer_header_link2_sublink5":[{"type":0,"value":"Операционные услуги"}],"footer_header_link3":[{"type":0,"value":"Магазины"}],"full":[{"type":0,"value":"Подробнее"}],"full_info_order":[{"type":0,"value":"Подробная информация о покупке"}],"full_name":[{"type":0,"value":"ФИО"}],"go_back":[{"type":0,"value":"Вернитесь назад"}],"go_home":[{"type":0,"value":"Идти домой"}],"height":[{"type":0,"value":"Bысоты"}],"height_unit":[{"type":0,"value":"Bысоты eдиница"}],"help":[{"type":0,"value":"Помощь"}],"help_text":[{"type":0,"value":"Если у вас есть какие-либо вопросы, связанные с веб-сайтом, свяжитесь с нами"}],"hi":[{"type":0,"value":"Привет "}],"high_low":[{"type":0,"value":"Цена сверху вниз"}],"hisob":[{"type":0,"value":"Мой баланс"}],"hour":[{"type":0,"value":"час"}],"in_moderation":[{"type":0,"value":"Умеренно"}],"inactive":[{"type":0,"value":"Неактивный"}],"info_about_delivery_price":[{"type":0,"value":"Информация об адресе"}],"input_phone":[{"type":0,"value":"Пожалуйста введите ваш номер телефона!"}],"input_sms":[{"type":0,"value":"Пожалуйста, введите смс, которое будет отправлено на введенный"}],"item":[{"type":0,"value":"шт."}],"kod":[{"type":0,"value":"Код"}],"less":[{"type":0,"value":"Читать меньше"}],"login_phone":[{"type":0,"value":"Войти по номеру телефона"}],"logout":[{"type":0,"value":"Выход"}],"low_high":[{"type":0,"value":"Цена высока снизу вверх"}],"material_select":[{"type":0,"value":"Выберите один материал, связанный с вашим продуктом."}],"materials":[{"type":0,"value":"Материал"}],"me":[{"type":0,"value":"Я"}],"min":[{"type":0,"value":"минут"}],"mobile_navigation_account":[{"type":0,"value":"Аккоунт"}],"mobile_navigation_category":[{"type":0,"value":"Категория"}],"mobile_navigation_category2":[{"type":0,"value":"Разделы"}],"mobile_navigation_home":[{"type":0,"value":"Главная"}],"more_than_1000":[{"type":0,"value":"Цена должна быть больше 1000 сум."}],"my_profile":[{"type":0,"value":"Мой профиль"}],"need_help":[{"type":0,"value":"Нужна помощь?"}],"need_today":[{"type":0,"value":"Это нужно сегодня?"}],"none_orders":[{"type":0,"value":"В вашем магазине ничего не заказано"}],"not_found":[{"type":0,"value":" 404 Не найден"}],"off(chegirma)":[{"type":0,"value":"Скидка"}],"order_again":[{"type":0,"value":"Заказать снова"}],"order_not_found":[{"type":0,"value":" заказ не найден."}],"order_status":[{"type":0,"value":"Статус заказа"}],"orders":[{"type":0,"value":"Мои заказы"}],"other":[{"type":0,"value":"Другой"}],"packaging":[{"type":0,"value":"В процессе упаковки"}],"pay":[{"type":0,"value":"Оплата"}],"payment":[{"type":0,"value":"Оплата"}],"payment_methods":[{"type":0,"value":"Способы оплаты"}],"phone_number":[{"type":0,"value":"Телефонный номер"}],"postcard text":[{"type":0,"value":"Текст открытки"}],"price_with_delivery":[{"type":0,"value":"Цена с доставкой"}],"products_total_price":[{"type":0,"value":"Итоговая цена продуктов"}],"profile_info":[{"type":0,"value":"Информация профиля"}],"rating":[{"type":0,"value":"Оценено"}],"rating_required":[{"type":0,"value":"Рейтинг обязателен"}],"recaptcha_verify":[{"type":0,"value":"Убедитесь, что вы не бот с верификацией"}],"recipent":[{"type":0,"value":"Получатель"}],"recipent_name":[{"type":0,"value":"Имя получателя"}],"recipent_phone":[{"type":0,"value":"Номер телефона получателя"}],"register":[{"type":0,"value":"Регистрация"}],"related_product":[{"type":0,"value":"Cопутствующие товары"}],"replenish":[{"type":0,"value":"Наполните свой кошелек"}],"replenish_account":[{"type":0,"value":"Пополнить счет"}],"required_description":[{"type":0,"value":"Требуется описание"}],"results_found":[{"type":0,"value":"результаты найдены"}],"review_placeholder":[{"type":0,"value":"Напишите здесь отзыв ..."}],"rows_per_page":[{"type":0,"value":" Cтроки на каждой странице"}],"sale_price":[{"type":0,"value":"Цена продажи"}],"search":[{"type":0,"value":"Найдите и нажмите Enter  ..."}],"search_for":[{"type":0,"value":"Поиск по ключевому слову:"}],"section12_item1_text":[{"type":0,"value":"Мы предлагаем конкурентоспособные цены на нашу более 100 миллионов продуктов в любом ассортименте."}],"section12_item1_title":[{"type":0,"value":"Доставка по всему миру"}],"section12_item2_text":[{"type":0,"value":"Мы предлагаем конкурентоспособные цены на нашу более 100 миллионов продуктов в любом ассортименте."}],"section12_item2_title":[{"type":0,"value":"Доставка по всему миру"}],"section12_item3_text":[{"type":0,"value":"Мы предлагаем конкурентоспособные цены на нашу более 100 миллионов продуктов в любом ассортименте."}],"section12_item3_title":[{"type":0,"value":"Доставка по всему миру"}],"section12_item4_text":[{"type":0,"value":"Мы предлагаем конкурентоспособные цены на нашу более 100 миллионов продуктов в любом ассортименте."}],"section12_item4_title":[{"type":0,"value":"Доставка по всему миру"}],"section12_title":[{"type":0,"value":"Реакция потребителей на наши услуги"}],"select_files":[{"type":0,"value":"Выбрать файлы"}],"select_gender":[{"type":0,"value":"Ваш пол"}],"select_image":[{"type":0,"value":"Пожалуйста, выберите изображение"}],"select_location":[{"type":0,"value":"Выберите место"}],"send":[{"type":0,"value":"Отправить"}],"send(jonatish)":[{"type":0,"value":"Отправить"}],"send_again":[{"type":0,"value":"Отправить код еще раз"}],"send_ask_again":[{"type":0,"value":"Вы можете попросить повторно отправить код через 1 минуту"}],"send_message":[{"type":0,"value":"Отправить код"}],"share_on":[{"type":0,"value":"Поделиться"}],"shop":[{"type":0,"value":"Мой магазин"}],"shop_links":[{"type":0,"value":"Ссылки (Kанал, Facebook, YouTube ....), связанные с вашим магазином"}],"shop_name":[{"type":0,"value":"Название магазина"}],"shop_review":[{"type":0,"value":"Оставьте комментарий об этом магазине"}],"site":[{"type":0,"value":"Сайт"}],"social_links":[{"type":0,"value":"Социальные сети"}],"sold_by":[{"type":0,"value":"Продавец"}],"special_instruction":[{"type":0,"value":"Специальные инструкции"}],"stock_available":[{"type":0,"value":"Имеется в наличии"}],"submit":[{"type":0,"value":"Послать"}],"successfully_booked":[{"type":0,"value":"Вы успешно забронировали"}],"sum":[{"type":0,"value":"cум"}],"support_tickets":[{"type":0,"value":"Билеты на помощь"}],"surname":[{"type":0,"value":"Фамилия"}],"text_below_postcard":[{"type":0,"value":"Мы не говорим получателю от кого цветы. Если вы хотите, чтобы ваш адресат узнал, кто прислал подарок подпишитесь"}],"text_required":[{"type":0,"value":"Обратная связь обязательна"}],"top_category":[{"type":0,"value":"Популярные категории"}],"total_calculation":[{"type":0,"value":"Oбщее количество"}],"total_summary":[{"type":0,"value":"Итого"}],"unit":[{"type":0,"value":"Ед. изм"}],"unit_select":[{"type":0,"value":"Выберите единицы, относящиеся к вашему продукту"}],"units_error":[{"type":0,"value":"Выберите одну из единиц длины"}],"update_vendor_info_button":[{"type":0,"value":"Обновить информацию о продавце"}],"upload_images":[{"type":0,"value":"Загрузить фото"}],"upload_images2":[{"type":0,"value":"Загрузить файлы"}],"user":[{"type":0,"value":"Войти"}],"view_all":[{"type":0,"value":"Bсе"}],"visit_Shop":[{"type":0,"value":"Посетить магазин"}],"wallet":[{"type":0,"value":"Бумажник"}],"wallet_instructions":[{"type":0,"value":"Используйте учетную запись в кошельке для рекламы рекламы, покупки коллекции и отдельных объявлений"}],"welcome_login":[{"type":0,"value":"Добро пожаловать в цветочный магазин"}],"width":[{"type":0,"value":"Ширина"}],"width_unit":[{"type":0,"value":"Ширина eдиница "}],"wishlist":[{"type":0,"value":"Избранное"}],"work_end_time":[{"type":0,"value":"Конец времени работы"}],"work_start_time":[{"type":0,"value":"Время начала работы"}],"write_review":[{"type":0,"value":"Напишите отзыв об этом продукте"}],"your_address":[{"type":0,"value":"Ваш адресс"}],"your_address_map":[{"type":0,"value":"Ваш адрес (щелкните место на адресе на карте)"}],"youtube":[{"type":0,"value":"YouTube"}]}');
;// CONCATENATED MODULE: ../content/compiled-locales/uz.json
const uz_namespaceObject = JSON.parse('{"Add PostCard":[{"type":0,"value":"Otkridka qo\'shish"}],"All":[{"type":0,"value":"Hammasi"}],"Amount":[{"type":0,"value":"Miqdori"}],"Any Document":[{"type":0,"value":"Har qanday hujjat"}],"Apply Voucher":[{"type":0,"value":"Voucherni ishlatish"}],"Back To Order List":[{"type":0,"value":"Buyurtma ro\'yxatiga qaytish"}],"Back to Product List":[{"type":0,"value":"Mahsulotlar ro\'yxatiga qaytish"}],"Back to cart":[{"type":0,"value":"Savatga qaytish"}],"Booked":[{"type":0,"value":"Oldindan buyurtma qilingan mahsulotlar soni"}],"Buy Now":[{"type":0,"value":"Buyurtma berish"}],"Cards":[{"type":0,"value":"Kartalar"}],"Carriers":[{"type":0,"value":"Bo\'sh ish o\'rinlari"}],"Cash":[{"type":0,"value":"Naqd pul"}],"Checkout Now":[{"type":0,"value":"Hoziroq sotib Olish"}],"Choose Your Gender":[{"type":0,"value":"Sizning jinsingiz"}],"Consumed_products":[{"type":0,"value":"Ishlatilgan mahsulotlar"}],"Corparative Sales":[{"type":0,"value":"Korporativ xizmat"}],"Date Purchased":[{"type":0,"value":"Xarid qilingan sana"}],"Delivery Time":[{"type":0,"value":"Yetkazib berish vaqti"}],"Details":[{"type":0,"value":"Tafsilotlar"}],"Discount":[{"type":0,"value":"Chegirma"}],"Discount(without percentage)":[{"type":0,"value":"Chegirma  % da(majburiy emas)"}],"Female":[{"type":0,"value":"Ayol"}],"Free":[{"type":0,"value":"bepul"}],"Full Description":[{"type":0,"value":"To\'liq tavsif"}],"Male":[{"type":0,"value":"Erkak"}],"My Orders":[{"type":0,"value":"Mening buyurtmalarim"}],"Name":[{"type":0,"value":"Ism"}],"Order":[{"type":0,"value":"Buyurtma"}],"Order Details":[{"type":0,"value":"Buyurtma tafsilotlari"}],"Order ID":[{"type":0,"value":"Buyurtma ID"}],"Order_#":[{"type":0,"value":"Id"}],"Placed On":[{"type":0,"value":"Joylashtirildi"}],"Price Range":[{"type":0,"value":"Narxlar oralig\'i"}],"Product Name":[{"type":0,"value":"Mahsulot nomi"}],"Products":[{"type":0,"value":"Mahsulotlar"}],"Regular Price":[{"type":0,"value":"Narx"}],"Review":[{"type":0,"value":"Sharh"}],"Select List":[{"type":0,"value":"Ro\'yxatni tanlang"}],"Shipping":[{"type":0,"value":"Yetkazib berish"}],"Shops":[{"type":0,"value":"Magazinlar"}],"Sitemap":[{"type":0,"value":"Sayt xaritasi"}],"Sort By":[{"type":0,"value":"Narx bo\'yicha saralash"}],"Status":[{"type":0,"value":"Status"}],"Subscribe":[{"type":0,"value":"Obuna bo\'lish"}],"Subscribed":[{"type":0,"value":"Obunani bekor qilish"}],"Subtotal":[{"type":0,"value":"Jami"}],"Total":[{"type":0,"value":"Jami"}],"Type":[{"type":0,"value":"Turi"}],"Used Products":[{"type":0,"value":"Materiallar"}],"Validity Period":[{"type":0,"value":"Amal qilish muddati"}],"Voucher":[{"type":0,"value":"Voucher"}],"Your Message":[{"type":0,"value":"Sizning xabaringiz"}],"Your Rating":[{"type":0,"value":"Sizning reytingingiz"}],"Your Review":[{"type":0,"value":"Sizning sharhingiz"}],"Your_order":[{"type":0,"value":"Sizning buyurtmangiz"}],"acceptable_link":[{"type":0,"value":"Iltimos, havolani to\'g\'ri shaklini kiriting"}],"accepted_convenient_payment":[{"type":0,"value":"qabul qilindi. Iltimos to\'lov turini tanlang"}],"account_settings":[{"type":0,"value":"Profil sozlamalari"}],"actions":[{"type":0,"value":"Xarakat"}],"active":[{"type":0,"value":"Faol"}],"add_all_cart":[{"type":0,"value":"Hammasini savatchaga qo\'shish"}],"add_comment_success":[{"type":0,"value":"Muvaffaqiyatli izoh qoldirdingiz"}],"add_links":[{"type":0,"value":"Link + qo\'shing"}],"add_new_product":[{"type":0,"value":"Yangi mahsulot qo\'shish"}],"add_product":[{"type":0,"value":"Mahsulot qo\'shish"}],"add_to_cart":[{"type":0,"value":"Savatga qo\'shish"}],"add_used_products":[{"type":0,"value":"Ishlatilgan mahsulotni qo\'shing +"}],"address":[{"type":0,"value":"Manzil"}],"all_rights":[{"type":0,"value":"Dana.uz - Barcha huquqlar himoyalangan"}],"apply_now":[{"type":0,"value":"Hoziroq murojaat qiling"}],"back_to_product_list":[{"type":0,"value":"Mahsulotlar ro\'yxatiga qaytish"}],"balance":[{"type":0,"value":"Balans:"}],"banner_button":[{"type":0,"value":"Tashrif buyuring"}],"birthdate":[{"type":0,"value":"Tug\'ilgan kun"}],"blocked":[{"type":0,"value":"Bloklangan"}],"blogs":[{"type":0,"value":"Bloglar"}],"bolim":[{"type":0,"value":"Kategoriya bo\'limi"}],"bonus":[{"type":0,"value":"Bonus"}],"book":[{"type":0,"value":"Xarid qilish"}],"budget":[{"type":0,"value":"Xisob"}],"budget_fulfill":[{"type":0,"value":"Iltimos, pul miqdorini kiriting"}],"business_account":[{"type":0,"value":"Bizness akkount"}],"button_check_status":[{"type":0,"value":"buyurtma holatini tekshirish -ni bosing."}],"cancel":[{"type":0,"value":"Bekor qilish"}],"card_type":[{"type":0,"value":"Karta turi"}],"cart":[{"type":0,"value":"Savatcha"}],"cash_or_card":[{"type":0,"value":"Naxt/Plastik"}],"cashback":[{"type":0,"value":"Cashback"}],"categories":[{"type":0,"value":"Kategoriyalar"}],"category_name":[{"type":0,"value":"Kategoriya nomi"}],"changed_info_success":[{"type":0,"value":"Profil ma\'lumotlari muvaffaqiyatli o\'zgartirildi"}],"check_button_status":[{"type":0,"value":"Buyurtma holatini tekshirish"}],"check_order_status":[{"type":0,"value":"Buyurtma va to\'lov holatini bilish uchun uning raqamini kiriting va  "}],"choose_category":[{"type":0,"value":"Iltimos, kategoriyani tanlang"}],"choose_delivery_place":[{"type":0,"value":"Iltimos, yetkazib berish joyini tanlang"}],"choose_delivery_time":[{"type":0,"value":"Yetkazib berish vaqtini tanlang"}],"choose_length_scales":[{"type":0,"value":"Iltimos, uzunlik o\'lchovlaridan biriga tanlang"}],"choose_payment":[{"type":0,"value":"Iltimos to\'lov turini tanlang"}],"choose_place_map":[{"type":0,"value":"Xaritadan joy tanlang"}],"company_name":[{"type":0,"value":"Kompaniya nomi"}],"connectUs":[{"type":0,"value":"Biz bilan bog\'lanish"}],"connectUsText":[{"type":0,"value":"Tez orada"}],"connect_A":[{"type":0,"value":"Admin bilan bog\'lanish"}],"contact":[{"type":0,"value":"Aloqa"}],"corporate_send_sucess":[{"type":0,"value":"Xabar muvaffaqiyatli yuborildi"}],"corporative_page_text1":[{"type":0,"value":"Bizda korporativ mijozlar uchun har qanday did va byudjetga mos guldastalar, sovg\'alar, shirinliklar va boshqa turdagi tovarlar mavjud. Sovg\'alarimiz doimiy taassurot qoldirish uchun maxsus tayyorlanadi. Korporativ mijozlar uchun maxsus individual yondoshgan holda biz sizning mijozingiz yoki hamkasbingiz uchun tashkilot va korxonalarning ramzlari tasvirlangan brend guldastalar va bundan tashqari mijozga minnatdorchilik bildirish, xamkasblarni tabriklash, ofisni bezash, to\'y, majlis zallarini bezatish va boshqa zarur holatlarda mijozlarga juda qulay shartlarda xizmat ko\'rsatamiz."}],"corporative_page_text2":[{"type":0,"value":"To\'lovlarni naqd va pul o\'tkazish yoli bilan amalga oshirish imkoniyati mavjud."}],"correct_password":[{"type":0,"value":"To\'gri parolni kiriting"}],"correct_phone":[{"type":0,"value":"To\'gri formatdagi telefon raqamini kiriting"}],"create_shop":[{"type":0,"value":"Magazin yaratish"}],"create_shop_success":[{"type":0,"value":"Do\'kon muvaffaqiyatli yaratildi"}],"currency":[{"type":0,"value":"Valyuta"}],"currency_required":[{"type":0,"value":"Iltimos, pul birligini tanlang"}],"currency_unit":[{"type":0,"value":"Valyutalardan birini tanlang"}],"dashboard":[{"type":0,"value":"Boshqa"}],"date":[{"type":0,"value":"Sana"}],"delete":[{"type":0,"value":"O\'chirish"}],"deliver":[{"type":0,"value":"Yetkazib berish shartlari"}],"delivered":[{"type":0,"value":"Yetkazib berildi"}],"delivering":[{"type":0,"value":"Yetkazib berish jarayonida"}],"delivery_price":[{"type":0,"value":"Yetkazib berish narxi"}],"delivery_time":[{"type":0,"value":"Yetkazib berish vaqti"}],"description":[{"type":0,"value":"Tavsif"}],"desktop_navigation_cabinet":[{"type":0,"value":"Kabinet"}],"document":[{"type":0,"value":"Yuridik hujjatlar"}],"drag_drop":[{"type":0,"value":"Mahsulot tasvirini bu yerga  tashlang"}],"drag_drop2":[{"type":0,"value":"Fayllarni bu yerga  tashlang"}],"edit_p":[{"type":0,"value":"Profilni taxrirlash"}],"edit_product":[{"type":0,"value":"Mahsulotni tahrirlash"}],"email":[{"type":0,"value":"Elektron pochta"}],"empty":[{"type":0,"value":"Bo\'sh"}],"empty_carrier":[{"type":0,"value":"Bo\'sh ish o\'rinlari yoq"}],"empty_cart":[{"type":0,"value":"Sizning savatchangiz bo\'sh. Xarid qilishni boshlang"}],"error_booking":[{"type":0,"value":"Nimadir noto\'g\'ri bajarildi"}],"estimated_delivery":[{"type":0,"value":"Yetkazib berish vaqti"}],"footer_header_link1":[{"type":0,"value":"Kompaniya haqida"}],"footer_header_link1_sublink1":[{"type":0,"value":"Biz haqimizda"}],"footer_header_link1_sublink2":[{"type":0,"value":"Karyera"}],"footer_header_link1_sublink3":[{"type":0,"value":"Bo\'sh ish o\'rinlari"}],"footer_header_link1_sublink4":[{"type":0,"value":"Hamkorlik"}],"footer_header_link2":[{"type":0,"value":"Mijozlarga xizmat"}],"footer_header_link2_sublink1":[{"type":0,"value":"Yordam markazi"}],"footer_header_link2_sublink2":[{"type":0,"value":"Qanday sotib olish mumkin"}],"footer_header_link2_sublink3":[{"type":0,"value":"Qaytarish"}],"footer_header_link2_sublink4":[{"type":0,"value":"Xizmatlar"}],"footer_header_link2_sublink5":[{"type":0,"value":"Operatsion xizmatlar"}],"footer_header_link3":[{"type":0,"value":"Do\'konlar"}],"full":[{"type":0,"value":"To\'liq o\'qish"}],"full_info_order":[{"type":0,"value":"Harid haqida batafsil malumot"}],"full_name":[{"type":0,"value":"To\'liq ism"}],"go_back":[{"type":0,"value":"Ortga qaytish"}],"go_home":[{"type":0,"value":"Asosiy sahifaga qaytish"}],"height":[{"type":0,"value":"Bo\'yiga"}],"height_unit":[{"type":0,"value":"Bo\'yiga o\'lchovi "}],"help":[{"type":0,"value":"Yordam"}],"help_text":[{"type":0,"value":"Agar sizda veb -sayt bilan bog\'liq savollaringiz bo\'lsa, biz bilan bog\'laning"}],"hi":[{"type":0,"value":"Salom "}],"high_low":[{"type":0,"value":"Narxi yuqoridan pastga"}],"hisob":[{"type":0,"value":"Mening hisobim"}],"hour":[{"type":0,"value":"soat"}],"in_moderation":[{"type":0,"value":"O\'zgartirishda"}],"inactive":[{"type":0,"value":"Faol emas"}],"info_about_delivery_price":[{"type":0,"value":"Manzil haqida ma\'lumot"}],"input_phone":[{"type":0,"value":"Iltimos telefon raqamingizni kiriting!"}],"input_sms":[{"type":0,"value":"Iltimos, kiritilgan SMS dagi kodni kiriting"}],"kod":[{"type":0,"value":"Kod"}],"less":[{"type":0,"value":"Kamroq oqish"}],"login_phone":[{"type":0,"value":"Telefon raqami bilan ro\'yhatdan o\'ting"}],"logout":[{"type":0,"value":"Chiqish"}],"low_high":[{"type":0,"value":"Narx pastdan yuqori"}],"material_select":[{"type":0,"value":"Iltimos, mahsulotingizga tegishli materialni tanlang"}],"materials":[{"type":0,"value":"Material"}],"me":[{"type":0,"value":"Men"}],"min":[{"type":0,"value":"minut"}],"mobile_navigation_account":[{"type":0,"value":"Akkount"}],"mobile_navigation_category":[{"type":0,"value":"Kategoriya"}],"mobile_navigation_category2":[{"type":0,"value":"Bo\'limlar"}],"mobile_navigation_home":[{"type":0,"value":"Asosiy"}],"more_than_1000":[{"type":0,"value":"Narxi 1000 so\'mdan yuqori bo\'lishi kerak"}],"my_profile":[{"type":0,"value":"Mening profilim"}],"need_help":[{"type":0,"value":"Yordam kerakmi?"}],"need_today":[{"type":0,"value":"Bugun kerakmi?"}],"none_orders":[{"type":0,"value":"Sizning do\'koningizdan hech narsa buyurtma qilinmagan"}],"not_found":[{"type":0,"value":" 404 Topilmadi"}],"off(chegirma)":[{"type":0,"value":"Chegirma"}],"order_again":[{"type":0,"value":"Qayta buyurtma"}],"order_not_found":[{"type":0,"value":" sonli buyurtma topilmadi"}],"order_status":[{"type":0,"value":"Buyurtma holati"}],"orders":[{"type":0,"value":"Mening buyurtmalarim"}],"other":[{"type":0,"value":"Boshqa"}],"packaging":[{"type":0,"value":"Qadoqlash jarayonida"}],"pay":[{"type":0,"value":"To\'lov"}],"payment":[{"type":0,"value":"To\'lov"}],"payment_methods":[{"type":0,"value":"To\'lov usullari"}],"phone_number":[{"type":0,"value":"Telefon raqam"}],"postcard text":[{"type":0,"value":"Otkridka matni"}],"price_with_delivery":[{"type":0,"value":"Yetkazib berish bilan narxi"}],"products_total_price":[{"type":0,"value":"Mahsulotlarning umumiy narxi"}],"profile_info":[{"type":0,"value":"Profil haqida ma\'lumot"}],"rating":[{"type":0,"value":"Baholangan"}],"rating_required":[{"type":0,"value":"Baho qo\'yish majburiy"}],"recaptcha_verify":[{"type":0,"value":"Iltimos, oʻzingizni verifikatsiya orqali bot emasligingizni tasdiqlang"}],"recipent":[{"type":0,"value":"Qabul qiluvchi"}],"recipent_name":[{"type":0,"value":"Qabul qiluvchi ismi"}],"recipent_phone":[{"type":0,"value":"Qabul qilivchi telefon raqami"}],"register":[{"type":0,"value":"Registratsiya"}],"related_product":[{"type":0,"value":"Aloqador mahsulotlar"}],"replenish":[{"type":0,"value":"Hamyonni to\'ldirish"}],"replenish_account":[{"type":0,"value":"Hisobni to\'ldirish"}],"required_description":[{"type":0,"value":"Tavsif talab qilinadi"}],"results_found":[{"type":0,"value":"natijalar topildi"}],"review_placeholder":[{"type":0,"value":"Bu yerda sharh yozing ..."}],"rows_per_page":[{"type":0,"value":"Satrlar"}],"sale_price":[{"type":0,"value":"Sotish narxi"}],"search":[{"type":0,"value":"Qidiring va Enter tugmasini bosing..."}],"search_for":[{"type":0,"value":"Qidiruv so\'zi: "}],"section12_item1_text":[{"type":0,"value":"Toshkent bo\'ylab tezkor yetkazib berish"}],"section12_item1_title":[{"type":0,"value":"Tezkor yetkazib berish"}],"section12_item2_text":[{"type":0,"value":" Eng yangi gullarning katta assortimenti"}],"section12_item2_title":[{"type":0,"value":"Yangi gullar"}],"section12_item3_text":[{"type":0,"value":" Doimiy mijozlar uchun bonusli chegirmalar tizimi"}],"section12_item3_title":[{"type":0,"value":"Chegirmalar"}],"section12_item4_text":[{"type":0,"value":" Xavfsiz to\'lov usullari yordamida to\'lash"}],"section12_item4_title":[{"type":0,"value":"Xavfsiz to\'lov"}],"section12_title":[{"type":0,"value":"Iste\'molchilarning bizning xizmatimizga munosabati"}],"select_files":[{"type":0,"value":"Fayllarni tanlang"}],"select_gender":[{"type":0,"value":"Jinsingiz"}],"select_image":[{"type":0,"value":"Iltimos, rasmni tanlang"}],"select_location":[{"type":0,"value":"Joyni tanlang"}],"send":[{"type":0,"value":"Saqlash"}],"send(jonatish)":[{"type":0,"value":"Yuborish"}],"send_again":[{"type":0,"value":"Kodni qayta yuboring"}],"send_ask_again":[{"type":0,"value":"Siz 1 daqiqadan so\'ng kodni qayta yuborishni so\'rashingiz mumkin"}],"send_message":[{"type":0,"value":"Kodni yuboring"}],"share_on":[{"type":0,"value":"Baham ko\'ring"}],"shop":[{"type":0,"value":"Mening do\'konim"}],"shop_links":[{"type":0,"value":"Sizning do\'koningiz bilan bog\'liq havolalar (Kanal, Facebook, Youtube ....)"}],"shop_name":[{"type":0,"value":"Do\'kon nomi"}],"shop_review":[{"type":0,"value":"Ushbu do\'kon haqida fikr qoldiring"}],"site":[{"type":0,"value":"Sayt"}],"social_links":[{"type":0,"value":"Ijtimoiy tarmoqlar"}],"sold_by":[{"type":0,"value":"Sotuvchi"}],"special_instruction":[{"type":0,"value":"Maxsus ko\'rsatmalar"}],"stock_available":[{"type":0,"value":"Omborda bor"}],"submit":[{"type":0,"value":"Yuborish"}],"successfully_booked":[{"type":0,"value":"Siz buyurtmani muvaffaqqiyatli amalga oshirdingiz"}],"sum":[{"type":0,"value":"so\'m"}],"support_tickets":[{"type":0,"value":"Yordam chiptalari"}],"surname":[{"type":0,"value":"Familiya"}],"text_below_postcard":[{"type":0,"value":"Biz oluvchiga gullarning kimligini aytmaymiz. Agar qabul qiluvchingiz sovg\'ani kim yuborganini bilishini istasangiz obuna bo\'ling"}],"text_required":[{"type":0,"value":"Fikr bildirish majburiy"}],"top_category":[{"type":0,"value":"Top kategoriyalar"}],"total_calculation":[{"type":0,"value":"Umumiy hisob"}],"total_summary":[{"type":0,"value":"Umumiy hisob"}],"unit":[{"type":0,"value":"Birlik"}],"unit_select":[{"type":0,"value":"Iltimos, mahsulotingizga mos keladigan bitta birlikni tanlang"}],"units_error":[{"type":0,"value":"Iltimos, uzunlik birliklaridan birini tanlang"}],"update_vendor_info_button":[{"type":0,"value":"Sotuvchi haqidagi ma\'lumotlarni yangilang"}],"upload_images":[{"type":0,"value":"Rasmlarni yuklash"}],"upload_images2":[{"type":0,"value":"Fayllarni yuklash"}],"user":[{"type":0,"value":"Kirish"}],"view_all":[{"type":0,"value":"Hammasi"}],"visit_Shop":[{"type":0,"value":"Do\'konga kirish"}],"wallet":[{"type":0,"value":"Hamyon"}],"wallet_instructions":[{"type":0,"value":"Hamyoningizdagi hisobdan e\'lonlar reklama qilish,to\'plam va alohida e\'lonlar sotib olishda foydalaning"}],"welcome_login":[{"type":0,"value":"Gullar do\'koniga xush kelibsiz"}],"width":[{"type":0,"value":"Eniga"}],"width_unit":[{"type":0,"value":"Eniga  o\'lchovi "}],"wishlist":[{"type":0,"value":"Sevimlilar"}],"work_end_time":[{"type":0,"value":"Ishning tugash vaqti"}],"work_start_time":[{"type":0,"value":"Ishning boshlanish vaqti"}],"write_review":[{"type":0,"value":"Ushbu mahsulot haqida sharh yozing"}],"your_address":[{"type":0,"value":"Sizning manzilingiz"}],"your_address_map":[{"type":0,"value":"Sizning manzilingiz (xaritadagi manzilni belgilang)"}],"youtube":[{"type":0,"value":"YouTube"}]}');
;// CONCATENATED MODULE: ../content/compiled-locales/k.json
const k_namespaceObject = JSON.parse('{"Add PostCard":[{"type":0,"value":"Oткрыткa қўшиш"}],"All":[{"type":0,"value":"Ҳаммаси"}],"Amount":[{"type":0,"value":"Миқдори"}],"Any Document":[{"type":0,"value":"Ҳар қандай ҳужжат"}],"Apply Voucher":[{"type":0,"value":"Воучерни ишлатиш"}],"Back To Order List":[{"type":0,"value":"Буюртма рўйхатига қайтиш"}],"Back to Product List":[{"type":0,"value":"Маҳсулотлар рўйхатига қайтиш"}],"Back to cart":[{"type":0,"value":"Саватга қайтиш"}],"Booked":[{"type":0,"value":"Олдиндан буюртма қилинган маҳсулотлар сони"}],"Buy Now":[{"type":0,"value":"Ҳозироқ сотиб олиш"}],"Cards":[{"type":0,"value":"Карталар"}],"Carriers":[{"type":0,"value":"Бўш иш ўринлари"}],"Cash":[{"type":0,"value":"Нақд пул"}],"Checkout Now":[{"type":0,"value":"Xозироқ сотиб олиш"}],"Choose Your Gender":[{"type":0,"value":"Сизнинг жинсингиз"}],"Consumed_products":[{"type":0,"value":"Ишлатилган маҳсулотлар"}],"Corparative Sales":[{"type":0,"value":"Корпоратив хизмат"}],"Date Purchased":[{"type":0,"value":"Харид қилинган сана"}],"Delivery Time":[{"type":0,"value":"Йетказиб бериш вақти"}],"Details":[{"type":0,"value":"Тафсилотлар"}],"Discount":[{"type":0,"value":"Чегирма"}],"Discount(without percentage)":[{"type":0,"value":"Чегирма % дa (мажбурий эмас)"}],"Female":[{"type":0,"value":"Aёл"}],"Free":[{"type":0,"value":"бепул"}],"Full Description":[{"type":0,"value":"Тўлиқ тавсиф"}],"Male":[{"type":0,"value":"Еркак"}],"My Orders":[{"type":0,"value":"Менинг буюртмаларим"}],"Name":[{"type":0,"value":"Исм"}],"Order":[{"type":0,"value":"Буюртма"}],"Order Details":[{"type":0,"value":"Буюртма тафсилотлари"}],"Order ID":[{"type":0,"value":"Буюртма ИД"}],"Order_#":[{"type":0,"value":"Тартиб #"}],"Placed On":[{"type":0,"value":"Жойлаштирилди"}],"Price Range":[{"type":0,"value":"Нархлар оралиғи"}],"Product Name":[{"type":0,"value":"Маҳсулот номи"}],"Products":[{"type":0,"value":"Маҳсулотлар"}],"Regular Price":[{"type":0,"value":"Hарх"}],"Review":[{"type":0,"value":"Шарҳ"}],"Select List":[{"type":0,"value":"Рўйхатни танланг"}],"Shipping":[{"type":0,"value":"Йетказиб бериш"}],"Shops":[{"type":0,"value":"Магазинлар"}],"Sitemap":[{"type":0,"value":"Сайт харитаси"}],"Sort By":[{"type":0,"value":"Нарх бўйича саралаш"}],"Status":[{"type":0,"value":"Статус"}],"Subscribe":[{"type":0,"value":"Обуна бўлиш"}],"Subscribed":[{"type":0,"value":"Обунани бекор қилиш"}],"Subtotal":[{"type":0,"value":"Жами"}],"Total":[{"type":0,"value":"Жами"}],"Type":[{"type":0,"value":"Тури"}],"Used Products":[{"type":0,"value":"Материаллар"}],"Validity Period":[{"type":0,"value":"Aмал қилиш муддати"}],"Voucher":[{"type":0,"value":"Воучер"}],"Your Message":[{"type":0,"value":"Сизнинг хатингиз"}],"Your Rating":[{"type":0,"value":"Сизнинг рейтингингиз"}],"Your Review":[{"type":0,"value":"Сизнинг шарҳингиз"}],"Your_order":[{"type":0,"value":"Сизнинг буюртмангиз"}],"acceptable_link":[{"type":0,"value":"Илтимос, линкни тўғри шаклини киритинг"}],"accepted_convenient_payment":[{"type":0,"value":"қабул қилинди. Илтимос толов турини танланг"}],"account_settings":[{"type":0,"value":"Профил созламалари"}],"actions":[],"active":[{"type":0,"value":"Фаол"}],"add_all_cart":[{"type":0,"value":"Хаммаcини саватчага кошиш"}],"add_comment_success":[{"type":0,"value":"Муваффақиятли изоҳ қолдирдингиз"}],"add_links":[{"type":0,"value":"Линк + қўшинг"}],"add_new_product":[{"type":0,"value":"Янги маҳсулот қўшиш"}],"add_product":[{"type":0,"value":"Маҳсулот қўшиш"}],"add_to_cart":[{"type":0,"value":"Саватчага қўшиш"}],"add_used_products":[{"type":0,"value":"Ишлатилган маҳсулотни қўшинг + "}],"address":[{"type":0,"value":"Манзиллар"}],"all_rights":[{"type":0,"value":"Dana.uz - Барча ҳуқуқлар ҳимояланган"}],"apply_now":[{"type":0,"value":"Ҳозироқ мурожаат қилинг"}],"back_to_product_list":[{"type":0,"value":"Маҳсулотлар рўйхатига қайтиш"}],"balance":[{"type":0,"value":"Баланс:"}],"banner_button":[{"type":0,"value":"Ташриф буюринг"}],"birthdate":[{"type":0,"value":"Туғилган кун"}],"blocked":[{"type":0,"value":"Блокланган"}],"blogs":[{"type":0,"value":"Блоглар"}],"bolim":[{"type":0,"value":"Категория бўлими"}],"bonus":[{"type":0,"value":"Бонус"}],"book":[{"type":0,"value":"Харид қилиш"}],"budget":[{"type":0,"value":"Хисоб"}],"budget_fulfill":[{"type":0,"value":"Илтимос, пул миқдорини киритинг"}],"business_account":[{"type":0,"value":"Бизнесс аккоунт"}],"button_check_status":[{"type":0,"value":"буюртма ҳолатини текшириш -ни босинг."}],"cancel":[{"type":0,"value":"Бекор қилиш"}],"card_type":[{"type":0,"value":"Карта тури"}],"cart":[{"type":0,"value":"Саватча"}],"cash_or_card":[{"type":0,"value":"Нахт/Пластик"}],"cashback":[{"type":0,"value":"Kашбаcк"}],"categories":[{"type":0,"value":"Категориялар"}],"category_name":[{"type":0,"value":"Категория номи"}],"changed_info_success":[{"type":0,"value":"Профил маълумотлари муваффақиятли ўзгартирилди"}],"check_button_status":[{"type":0,"value":"Буюртма ҳолатини текшириш"}],"check_order_status":[{"type":0,"value":"Буюртма ва тўлов ҳолатини билиш учун унинг рақамини киритинг ва "}],"choose_category":[{"type":0,"value":"Илтимос, категорияни танланг"}],"choose_delivery_place":[{"type":0,"value":"Илтимос, етказиб бериш жойини танланг"}],"choose_delivery_time":[{"type":0,"value":"Етказиб бериш вақтини танланг"}],"choose_length_scales":[{"type":0,"value":"Илтимос, узунлик ўлчовларидан бирига танланг"}],"choose_payment":[{"type":0,"value":"Илтимос толов турини танланг"}],"choose_place_map":[{"type":0,"value":"Харитадан жой танланг"}],"company_name":[{"type":0,"value":"Компания номи"}],"connectUs":[{"type":0,"value":"Биз билан боғланиш"}],"connectUsText":[{"type":0,"value":"Тез орада"}],"connect_A":[{"type":0,"value":"Aдмин билан боғланиш"}],"contact":[{"type":0,"value":"Aлоқа"}],"corporate_send_sucess":[{"type":0,"value":"Хабар муваффақиятли юборилди"}],"corporative_page_text1":[{"type":0,"value":"Бизда корпоратив мижозлар учун ҳар қандай дид ва бюджетга мос гулдасталар, совғалар, ширинликлар ва бошқа турдаги товарлар мавжуд. Совғаларимиз доимий таассурот қолдириш учун махсус тайёрланади. Корпоратив мижозлар учун махсус индивидуал ёндошган ҳолда биз сизнинг мижозингиз ёки ҳамкасбингиз учун ташкилот ва корхоналарнинг рамзлари тасвирланган бренд гулдасталар ва бундан ташқари мижозга миннатдорчилик билдириш, хамкасбларни табриклаш, офисни безаш, тўй, мажлис залларини безатиш ва бошқа зарур ҳолатларда мижозларга жуда қулай шартларда хизмат кўрсатамиз."}],"corporative_page_text2":[{"type":0,"value":"Тўловларни нақд ва пул ўтказиш ёли билан амалга ошириш имконияти мавжуд."}],"correct_password":[{"type":0,"value":"Тўгри паролни киритинг"}],"correct_phone":[{"type":0,"value":"Тўгри форматдаги телефон рақамини киритинг"}],"create_shop":[{"type":0,"value":"Mагазин яратиш"}],"create_shop_success":[{"type":0,"value":"Дўкон муваффақиятли яратилди"}],"currency":[{"type":0,"value":"Валюта"}],"currency_required":[{"type":0,"value":"Илтимос, пул бирлигини танланг"}],"currency_unit":[{"type":0,"value":"Валюталардан бирини танланг"}],"dashboard":[{"type":0,"value":"Бошқа"}],"date":[{"type":0,"value":"Cана"}],"delete":[{"type":0,"value":"Ўчириш"}],"deliver":[{"type":0,"value":"Етказиб бериш шартлари"}],"delivered":[{"type":0,"value":"Етказиб берилди"}],"delivering":[{"type":0,"value":"Етказиб бериш жараёнида"}],"delivery_price":[{"type":0,"value":"Етказиб бериш нархи"}],"delivery_time":[{"type":0,"value":"Еткзаиб бериш вақти"}],"description":[{"type":0,"value":"Тавсиф"}],"desktop_navigation_cabinet":[{"type":0,"value":"Kабинет"}],"document":[{"type":0,"value":"Юридик ҳужжатлар"}],"drag_drop":[{"type":0,"value":"Маҳсулот тасвирини бу ерга  ташланг"}],"drag_drop2":[{"type":0,"value":"Файлларни бу ерга ташланг"}],"edit_p":[{"type":0,"value":"Профилни tахриpлаш"}],"edit_product":[{"type":0,"value":"Маҳсулотни таҳрирлаш"}],"email":[{"type":0,"value":"Электрон почта"}],"empty":[{"type":0,"value":"Бўш"}],"empty_carrier":[{"type":0,"value":"Бўш иш ўринлари йўқ"}],"empty_cart":[{"type":0,"value":"Сизнинг саватчангиз бўш. Харид қилишни бошланг"}],"error_booking":[{"type":0,"value":"Нимадир нотўғри бажарилди"}],"estimated_delivery":[{"type":0,"value":"Етказиб бериш вақти"}],"footer_header_link1":[{"type":0,"value":"Компания ҳақида"}],"footer_header_link1_sublink1":[{"type":0,"value":"Биз ҳақимизда"}],"footer_header_link1_sublink2":[{"type":0,"value":"Карйера"}],"footer_header_link1_sublink3":[{"type":0,"value":"Бўш иш ўринлари"}],"footer_header_link1_sublink4":[{"type":0,"value":"Ҳамкорлик"}],"footer_header_link2":[{"type":0,"value":"Мижозларга хизмат"}],"footer_header_link2_sublink1":[{"type":0,"value":"Ёрдам маркази"}],"footer_header_link2_sublink2":[{"type":0,"value":"Қандай сотиб олиш мумкин"}],"footer_header_link2_sublink3":[{"type":0,"value":"Қайтариш"}],"footer_header_link2_sublink4":[{"type":0,"value":"Хизматлар"}],"footer_header_link2_sublink5":[{"type":0,"value":"Операцион хизматлар"}],"footer_header_link3":[{"type":0,"value":"Дўконлар"}],"full":[{"type":0,"value":"Тўлиқ ўқиш"}],"full_info_order":[{"type":0,"value":"Ҳарид ҳақида батафсил малумот"}],"full_name":[{"type":0,"value":"Тўлиқ исм"}],"go_back":[{"type":0,"value":"Ортга қайтиш"}],"go_home":[{"type":0,"value":"Aсосий саҳифага қайтиш"}],"height":[{"type":0,"value":"Баландлик"}],"height_unit":[{"type":0,"value":"Баландлик бирлиги"}],"help":[{"type":0,"value":"Ёрдам"}],"help_text":[{"type":0,"value":" Aгар сизда веб -сайт билан боғлиқ саволларингиз бўлса, биз билан боғланинг"}],"hi":[{"type":0,"value":"Салом "}],"high_low":[{"type":0,"value":"Нархи юқоридан пастга"}],"hisob":[{"type":0,"value":"Менинг xисобим"}],"hour":[{"type":0,"value":"соат"}],"in_moderation":[{"type":0,"value":"Ўзгартиришда"}],"inactive":[{"type":0,"value":"Фаол емас"}],"info_about_delivery_price":[{"type":0,"value":"Манзил ҳақида маълумот"}],"input_phone":[{"type":0,"value":"Илтимос телефон рақамингизни киритинг!"}],"input_sms":[{"type":0,"value":"Илтимос, киритилган СМС-хабарни киритинг"}],"item":[{"type":0,"value":"та"}],"kod":[{"type":0,"value":"Код"}],"less":[{"type":0,"value":"Камроқ оқиш"}],"login_phone":[{"type":0,"value":"Телефон рақами билан рўйҳатдан ўтинг"}],"logout":[{"type":0,"value":"Чиkиш"}],"low_high":[{"type":0,"value":"Нарх пастдан юқорига"}],"material_select":[{"type":0,"value":"Илтимос, маҳсулотингизга тегишли материални танланг"}],"materials":[{"type":0,"value":"Материал"}],"me":[{"type":0,"value":"Мен"}],"min":[{"type":0,"value":"минут"}],"mobile_navigation_account":[{"type":0,"value":"Aккоунт"}],"mobile_navigation_category":[{"type":0,"value":"Категория"}],"mobile_navigation_category2":[{"type":0,"value":"Бўлимлар"}],"mobile_navigation_home":[{"type":0,"value":"Aсосий"}],"more_than_1000":[{"type":0,"value":"Нархи 1000 сўмдан юқори бўлиши керак"}],"my_profile":[{"type":0,"value":"Менинг профилим"}],"need_help":[{"type":0,"value":"Ёрдам керакми?"}],"need_today":[{"type":0,"value":"Бугун керакми?"}],"none_orders":[{"type":0,"value":"Сизнинг дўконингиздан ҳеч нарса буюртма қилинмаган"}],"not_found":[{"type":0,"value":" 404 Топилмади"}],"off(chegirma)":[{"type":0,"value":"Чегирма"}],"order_again":[{"type":0,"value":"Қайта буюртма"}],"order_not_found":[{"type":0,"value":" сонли буюртма топилмади"}],"order_status":[{"type":0,"value":"Буюртма ҳолати"}],"orders":[{"type":0,"value":"Менинг буюртмаларим"}],"other":[{"type":0,"value":"Бошқа"}],"packaging":[{"type":0,"value":"Қадоқлаш жараёнида"}],"pay":[{"type":0,"value":"Тўлаш"}],"payment":[{"type":0,"value":"Тўлов"}],"payment_methods":[{"type":0,"value":"Тўлов усуллари"}],"phone_number":[{"type":0,"value":"Телефон pаkам"}],"postcard text":[{"type":0,"value":"Откридка матни"}],"price_with_delivery":[{"type":0,"value":"Йетказиб бериш билан нархи"}],"products_total_price":[{"type":0,"value":"Маҳсулотларнинг умумий нархи"}],"profile_info":[{"type":0,"value":"Профил ҳақида маълумот"}],"rating":[{"type":0,"value":"Баҳоланган"}],"rating_required":[{"type":0,"value":"Баҳо қўйиш мажбурий"}],"recaptcha_verify":[{"type":0,"value":"Илтимос, ўзингизни верификация орқали бот эмаслигингизни тасдиқланг"}],"recipent":[{"type":0,"value":"Қабул қилувчи"}],"recipent_name":[{"type":0,"value":"Kабул қилувчи исми"}],"recipent_phone":[{"type":0,"value":"Kабул қиливчи телефон рақами"}],"register":[{"type":0,"value":"Регистрация"}],"related_product":[{"type":0,"value":"Aлоқадор маҳсулотлар"}],"replenish":[{"type":0,"value":"Ҳамённи тўлдириш"}],"replenish_account":[{"type":0,"value":"Ҳисобни тўлдириш"}],"required_description":[{"type":0,"value":"Тавсиф талаб қилинади"}],"results_found":[{"type":0,"value":"натижалар топилди"}],"review_placeholder":[{"type":0,"value":"Бу йерда шарҳ ёзинг ..."}],"rows_per_page":[{"type":0,"value":"Xар саҳифадаги сатрлар"}],"sale_price":[{"type":0,"value":"Сотиш нархи"}],"search":[{"type":0,"value":"Қидиринг ва eнтер тугмасини босинг ..."}],"search_for":[{"type":0,"value":"Қидирув сози:"}],"section12_item1_text":[{"type":0,"value":"  Биз ҳар қандай ассортиментдаги 100 миллион плюс маҳсулотимизга рақобатбардош нархларни таклиф етамиз."}],"section12_item1_title":[{"type":0,"value":"Дунё бўйлаб йетказиб бериш"}],"section12_item2_text":[{"type":0,"value":"  Биз ҳар қандай ассортиментдаги 100 миллион плюс маҳсулотимизга рақобатбардош нархларни таклиф етамиз."}],"section12_item2_title":[{"type":0,"value":"Дунё бўйлаб йетказиб бериш"}],"section12_item3_text":[{"type":0,"value":"  Биз ҳар қандай ассортиментдаги 100 миллион плюс маҳсулотимизга рақобатбардош нархларни таклиф етамиз."}],"section12_item3_title":[{"type":0,"value":"Дунё бўйлаб йетказиб бериш"}],"section12_item4_text":[{"type":0,"value":"  Биз ҳар қандай ассортиментдаги 100 миллион плюс маҳсулотимизга рақобатбардош нархларни таклиф етамиз."}],"section12_item4_title":[{"type":0,"value":"Дунё бўйлаб йетказиб бериш"}],"section12_title":[{"type":0,"value":"Истеъмолчиларнинг бизнинг хизматимизга муносабати"}],"select_files":[{"type":0,"value":"Файлларни танланг"}],"select_gender":[{"type":0,"value":"Жинсингиз"}],"select_image":[{"type":0,"value":"Илтимос, расмни танланг"}],"select_location":[{"type":0,"value":"Жойни танланг"}],"send":[{"type":0,"value":"Сақлаш"}],"send(jonatish)":[{"type":0,"value":"Юбориш"}],"send_again":[{"type":0,"value":"Кодни кайта юборинг"}],"send_ask_again":[{"type":0,"value":"Сиз 1 дакикадан сўнг кодни қайта юборишни сўрашингиз мумкин"}],"send_message":[{"type":0,"value":"Кодни юборинг"}],"share_on":[{"type":0,"value":"Баҳам кўринг"}],"shop":[{"type":0,"value":"Менинг доконим"}],"shop_links":[{"type":0,"value":"Сизнинг дўконингиз билан боғлиқ ҳаволалар (Kанал, Фаcебоок, Ютуб ....)\\""}],"shop_name":[{"type":0,"value":"Дўкон номи"}],"shop_review":[{"type":0,"value":"Ушбу дўкон ҳақида фикр қолдиринг"}],"site":[{"type":0,"value":"Сайт"}],"social_links":[{"type":0,"value":"Ижтимоий тармоқлар"}],"sold_by":[{"type":0,"value":"Сотувчи"}],"special_instruction":[{"type":0,"value":"Махсус кўрсатмалар"}],"stock_available":[{"type":0,"value":"Омборда бор"}],"submit":[{"type":0,"value":"Юбориш"}],"successfully_booked":[{"type":0,"value":"Сиз муваффақиятли брон қилдингиз"}],"sum":[{"type":0,"value":"cўм"}],"support_tickets":[{"type":0,"value":"Ёрдам чипталари"}],"surname":[{"type":0,"value":"Фамилия"}],"text_below_postcard":[{"type":0,"value":"Биз олувчига гулларнинг кимлигини айтмаймиз. Агар қабул қилувчингиз совғани ким юборганини билишини истасангиз обуна бўлинг"}],"text_required":[{"type":0,"value":"Фикр билдириш мажбурий"}],"top_category":[{"type":0,"value":"Топ категориялар"}],"total_calculation":[{"type":0,"value":"Умумий ҳисоб"}],"total_summary":[{"type":0,"value":"Умумий ҳисоб"}],"unit":[{"type":0,"value":"Бирлик"}],"unit_select":[{"type":0,"value":"Илтимос, маҳсулотингизга мос келадиган битта бирликни танланг"}],"units_error":[{"type":0,"value":"Илтимос, узунлик бирликларидан бирини танланг"}],"update_vendor_info_button":[{"type":0,"value":"Сотувчи ҳақидаги маълумотларни янгиланг"}],"upload_images":[{"type":0,"value":"Расмларни юклаш"}],"upload_images2":[{"type":0,"value":"Файлларни юклаш"}],"user":[{"type":0,"value":"Кириш"}],"view_all":[{"type":0,"value":"Ҳаммаси"}],"visit_Shop":[{"type":0,"value":"Дўконга кириш"}],"wallet":[{"type":0,"value":"Xамён"}],"wallet_instructions":[{"type":0,"value":"Xамёнингиздаги ҳисобдан еълонлар реклама қилиш,тўплам ва алоҳида еълонлар сотиб олишда фойдаланинг"}],"welcome_login":[{"type":0,"value":"Гуллар до\'конига xуш келибсиз"}],"width":[{"type":0,"value":"Ени"}],"width_unit":[{"type":0,"value":"Ени бирлиги"}],"wishlist":[{"type":0,"value":"Севимлилар"}],"work_end_time":[{"type":0,"value":"Ишнинг тугаш вақти"}],"work_start_time":[{"type":0,"value":"Ишнинг бошланиш вақти"}],"write_review":[{"type":0,"value":"Ушбу маҳсулот ҳақида шарҳ ёзинг"}],"your_address":[{"type":0,"value":"Сизнинг манзилингиз"}],"your_address_map":[{"type":0,"value":"Сизнинг манзилингиз (харитадаги манзилни белгиланг)"}],"youtube":[{"type":0,"value":"Ютуб"}]}');
;// CONCATENATED MODULE: ../content/compiled-locales/en.json
const en_namespaceObject = JSON.parse('{"Add PostCard":[{"type":0,"value":"Add postCard"}],"All":[{"type":0,"value":"All"}],"Amount":[{"type":0,"value":"Amount"}],"Any Document":[{"type":0,"value":"Any document"}],"Apply Voucher":[{"type":0,"value":"Apply voucher"}],"Back To Order List":[{"type":0,"value":"Back to order list"}],"Back to Product List":[{"type":0,"value":"Back to product list"}],"Back to cart":[{"type":0,"value":"Back to cart"}],"Booked":[{"type":0,"value":"Number of booked products in advance"}],"Buy Now":[{"type":0,"value":"Buy now"}],"Cards":[{"type":0,"value":"Cards"}],"Carriers":[{"type":0,"value":"Vacancies"}],"Cash":[{"type":0,"value":"Cash"}],"Checkout Now":[{"type":0,"value":"Checkout now"}],"Choose Your Gender":[{"type":0,"value":"Your gender"}],"Consumed_products":[{"type":0,"value":"Used products"}],"Corparative Sales":[{"type":0,"value":"Corparative service"}],"Date Purchased":[{"type":0,"value":"Date purchased"}],"Delivery Time":[{"type":0,"value":"Delivery time"}],"Details":[{"type":0,"value":"Details"}],"Discount":[{"type":0,"value":"Discount"}],"Discount(without percentage)":[{"type":0,"value":"Discount % (optional)"}],"Female":[{"type":0,"value":"Female"}],"Free":[{"type":0,"value":"free"}],"Full Description":[{"type":0,"value":"Full description"}],"Male":[{"type":0,"value":"Male"}],"My Orders":[{"type":0,"value":"My orders"}],"Name":[{"type":0,"value":"Name"}],"Order":[{"type":0,"value":"Order"}],"Order Details":[{"type":0,"value":"Order details"}],"Order ID":[{"type":0,"value":"Order ID"}],"Order_#":[{"type":0,"value":"Id"}],"Placed On":[{"type":0,"value":"Placed on"}],"Price Range":[{"type":0,"value":"Price range"}],"Product Name":[{"type":0,"value":"Product name"}],"Products":[{"type":0,"value":"Products"}],"Regular Price":[{"type":0,"value":"Price"}],"Review":[{"type":0,"value":"Review"}],"Select List":[{"type":0,"value":"Select List"}],"Shipping":[{"type":0,"value":"Shipping"}],"Shops":[{"type":0,"value":"Shops"}],"Sitemap":[{"type":0,"value":"Sitemap"}],"Sort By":[{"type":0,"value":"Sort by price"}],"Status":[{"type":0,"value":"Status"}],"Subscribe":[{"type":0,"value":"Subscribe"}],"Subscribed":[{"type":0,"value":"Unsubscribe"}],"Subtotal":[{"type":0,"value":"Subtotal"}],"Total":[{"type":0,"value":"Total"}],"Type":[{"type":0,"value":"Type"}],"Used Products":[{"type":0,"value":"Materials"}],"Validity Period":[{"type":0,"value":"Validity period"}],"Voucher":[{"type":0,"value":"Voucher"}],"Your Message":[{"type":0,"value":"Your message"}],"Your Rating":[{"type":0,"value":"Your rating"}],"Your Review":[{"type":0,"value":"Your review"}],"Your_order":[{"type":0,"value":"Your order"}],"acceptable_link":[{"type":0,"value":"Please, input correct form of link"}],"accepted_convenient_payment":[{"type":0,"value":". Please choose convenient payment method"}],"account_settings":[{"type":0,"value":"Account settings"}],"actions":[{"type":0,"value":"Actions"}],"active":[{"type":0,"value":"Active"}],"add_all_cart":[{"type":0,"value":"Add all to the cart"}],"add_comment_success":[{"type":0,"value":"You commented successfully"}],"add_links":[{"type":0,"value":"Add link + "}],"add_new_product":[{"type":0,"value":"Add new product"}],"add_product":[{"type":0,"value":"Add product"}],"add_to_cart":[{"type":0,"value":"Add to cart"}],"add_used_products":[{"type":0,"value":"Add used product + "}],"address":[{"type":0,"value":"Address"}],"all_rights":[{"type":0,"value":"Dana.uz - All rights reserved"}],"apply_now":[{"type":0,"value":"Apply now"}],"back_to_product_list":[{"type":0,"value":"Back to product list"}],"background_image_error":[{"type":0,"value":"Please choose background image"}],"balance":[{"type":0,"value":"Balance:"}],"banner_button":[{"type":0,"value":"Visit"}],"birthdate":[{"type":0,"value":"Birthday"}],"blocked":[{"type":0,"value":"Blocked"}],"blogs":[{"type":0,"value":"Blogs"}],"bolim":[{"type":0,"value":"Category section"}],"bonus":[{"type":0,"value":"Bonus"}],"book":[{"type":0,"value":"Shopping"}],"budget":[{"type":0,"value":"Balance"}],"budget_fulfill":[{"type":0,"value":"Input amount of money"}],"business_account":[{"type":0,"value":"Business account"}],"button_check_status":[{"type":0,"value":"click check order status."}],"cancel":[{"type":0,"value":"Cancel"}],"card_type":[{"type":0,"value":"Card type"}],"cart":[{"type":0,"value":"Cart"}],"cash_or_card":[{"type":0,"value":"Cash/Card"}],"cashback":[{"type":0,"value":"Cashback"}],"categories":[{"type":0,"value":"Categories"}],"category_name":[{"type":0,"value":"Category name"}],"changed_info_success":[{"type":0,"value":"Your profile infos changed sucessfully"}],"check_button_status":[{"type":0,"value":"Check order status"}],"check_order_status":[{"type":0,"value":"To find out the status of the order and pay, enter its number and "}],"choose_category":[{"type":0,"value":"Please choose category"}],"choose_delivery_place":[{"type":0,"value":"Please select a delivery location"}],"choose_delivery_time":[{"type":0,"value":"Choose delivery time from options"}],"choose_length_scales":[{"type":0,"value":"Please refer to one of the length scales"}],"choose_payment":[{"type":0,"value":"Please choose convenient payment method"}],"choose_place_map":[{"type":0,"value":"Please select place from map"}],"company_name":[{"type":0,"value":"Company Name"}],"connectUs":[{"type":0,"value":"Connect us"}],"connectUsText":[{"type":0,"value":"it will come soon"}],"connect_A":[{"type":0,"value":"Contact the admin"}],"contact":[{"type":0,"value":"Contact"}],"corporate_send_sucess":[{"type":0,"value":"Message is sent successfully"}],"corporative_page_text1":[{"type":0,"value":"We have bouquets, gifts, sweets and other types of goods for corporate customers of all tastes and budgets. Our gifts are specially made to make a lasting impression. In case of special individual approach for corporate clients, we provide branded bouquets with symbols of organizations and enterprises for your client or colleague, as well as thanking the client, congratulating colleagues, decorating the office, decorating weddings, conference halls and other necessary conditions."}],"corporative_page_text2":[{"type":0,"value":"Payments can be made in cash or by bank transfer."}],"correct_password":[{"type":0,"value":"Please input correct code"}],"correct_phone":[{"type":0,"value":"Please input telephone number in correct way "}],"create_shop":[{"type":0,"value":"Create shop"}],"create_shop_success":[{"type":0,"value":"Shop is created successfully"}],"currency":[{"type":0,"value":"Currency"}],"currency_required":[{"type":0,"value":"Please, choose currency"}],"currency_unit":[{"type":0,"value":"Please select one of currency"}],"dashboard":[{"type":0,"value":"Others"}],"date":[{"type":0,"value":"Date"}],"delete":[{"type":0,"value":"Delete"}],"deliver":[{"type":0,"value":"Delivery terms"}],"delivered":[{"type":0,"value":"Delivered"}],"delivering":[{"type":0,"value":"Delivering"}],"delivery_price":[{"type":0,"value":"Delivery price"}],"delivery_time":[{"type":0,"value":"Delivery time"}],"description":[{"type":0,"value":"Description"}],"desktop_navigation_cabinet":[{"type":0,"value":"Cabinet"}],"document":[{"type":0,"value":"Documents"}],"drag_drop":[{"type":0,"value":"Drag & drop product image here"}],"drag_drop2":[{"type":0,"value":"Drag & drop product files here"}],"edit_p":[{"type":0,"value":"Edit profile"}],"edit_product":[{"type":0,"value":"Edit product"}],"email":[{"type":0,"value":"Email"}],"empty":[{"type":0,"value":"Empty"}],"empty_carrier":[{"type":0,"value":"No vacancy"}],"empty_cart":[{"type":0,"value":"Your shopping bag is empty. Start shopping"}],"error_booking":[{"type":0,"value":"Something went wrong"}],"estimated_delivery":[{"type":0,"value":"Estimated delivery date"}],"footer_header_link1":[{"type":0,"value":"About company"}],"footer_header_link1_sublink1":[{"type":0,"value":"About us"}],"footer_header_link1_sublink2":[{"type":0,"value":"Careers"}],"footer_header_link1_sublink3":[{"type":0,"value":"Vacancies"}],"footer_header_link1_sublink4":[{"type":0,"value":"Cooperation"}],"footer_header_link2":[{"type":0,"value":"Customer care"}],"footer_header_link2_sublink1":[{"type":0,"value":"Help center"}],"footer_header_link2_sublink2":[{"type":0,"value":"How to buy"}],"footer_header_link2_sublink3":[{"type":0,"value":"Returns & refunds"}],"footer_header_link2_sublink4":[{"type":0,"value":"Services"}],"footer_header_link2_sublink5":[{"type":0,"value":"Operating services"}],"footer_header_link3":[{"type":0,"value":"Shops"}],"full":[{"type":0,"value":"Read more"}],"full_info_order":[{"type":0,"value":"Full info about order"}],"full_name":[{"type":0,"value":"Full name"}],"go_back":[{"type":0,"value":"Go back"}],"go_home":[{"type":0,"value":"Go to home"}],"height":[{"type":0,"value":"Height"}],"height_unit":[{"type":0,"value":"Height unit"}],"help":[{"type":0,"value":"Help"}],"help_text":[{"type":0,"value":"If you have any question related to website ,connect with us"}],"hi":[{"type":0,"value":"Hello "}],"high_low":[{"type":0,"value":"Price high to low"}],"hisob":[{"type":0,"value":"My balance"}],"hour":[{"type":0,"value":"hour"}],"in_moderation":[{"type":0,"value":"In moderation"}],"inactive":[{"type":0,"value":"Inactive"}],"info_about_delivery_price":[{"type":0,"value":"Info about delivery place"}],"input_phone":[{"type":0,"value":"Please input your telephone number!"}],"input_sms":[{"type":0,"value":"Please input the code that is sent"}],"item":[{"type":0,"value":"item"}],"kod":[{"type":0,"value":"Code"}],"less":[{"type":0,"value":"Read less"}],"login_phone":[{"type":0,"value":"Register with your telephone number"}],"logout":[{"type":0,"value":"Log out"}],"low_high":[{"type":0,"value":"Price low to high"}],"material_select":[{"type":0,"value":"Please select one material which related to your product"}],"materials":[{"type":0,"value":"Material"}],"me":[{"type":0,"value":"I am"}],"min":[{"type":0,"value":"minute"}],"mobile_navigation_account":[{"type":0,"value":"Account"}],"mobile_navigation_category":[{"type":0,"value":"Category"}],"mobile_navigation_category2":[{"type":0,"value":"Sections"}],"mobile_navigation_home":[{"type":0,"value":"Home"}],"more_than_1000":[{"type":0,"value":"Price must be more than 1000"}],"my_profile":[{"type":0,"value":"My profile"}],"need_help":[{"type":0,"value":"Need help?"}],"need_today":[{"type":0,"value":"Need today?"}],"none_orders":[{"type":0,"value":"Nothing is booked from your shop"}],"not_found":[{"type":0,"value":" 404 not found"}],"off(chegirma)":[{"type":0,"value":"Off"}],"order_again":[{"type":0,"value":"Order again"}],"order_not_found":[{"type":0,"value":" order was not found"}],"order_status":[{"type":0,"value":"Order status"}],"orders":[{"type":0,"value":"My orders"}],"other":[{"type":0,"value":"Other"}],"packaging":[{"type":0,"value":"Packaging"}],"pay":[{"type":0,"value":"Payment"}],"payment":[{"type":0,"value":"Payment"}],"payment_methods":[{"type":0,"value":"Payment methods"}],"phone_number":[{"type":0,"value":"Phone number"}],"postcard text":[{"type":0,"value":"PostCard text"}],"price_with_delivery":[{"type":0,"value":"Price(included delivery)"}],"products_total_price":[{"type":0,"value":"Total price of products"}],"profile_info":[{"type":0,"value":"Info about profile"}],"rating":[{"type":0,"value":"Rated"}],"rating_required":[{"type":0,"value":"Rating is required"}],"recaptcha_verify":[{"type":0,"value":"Please verify yourself"}],"recipent":[{"type":0,"value":"Recipent"}],"recipent_name":[{"type":0,"value":"Recipent Name"}],"recipent_phone":[{"type":0,"value":"Recipent phone number"}],"register":[{"type":0,"value":"Register"}],"related_product":[{"type":0,"value":"Related products"}],"replenish":[{"type":0,"value":"Fill your wallet"}],"replenish_account":[{"type":0,"value":"Replenish the account"}],"required_description":[{"type":0,"value":"Description is required"}],"results_found":[{"type":0,"value":"results found"}],"review_placeholder":[{"type":0,"value":"Write a review here..."}],"rows_per_page":[{"type":0,"value":"Rows per page"}],"sale_price":[{"type":0,"value":"Sale price"}],"search":[{"type":0,"value":"Search and press enter..."}],"search_for":[{"type":0,"value":"Searching for:"}],"section12_item1_text":[{"type":0,"value":" We offer competitive prices on our 100 million plus product any range."}],"section12_item1_title":[{"type":0,"value":"Worldwide delivery"}],"section12_item2_text":[{"type":0,"value":" We offer competitive prices on our 100 million plus product any range."}],"section12_item2_title":[{"type":0,"value":"Worldwide delivery"}],"section12_item3_text":[{"type":0,"value":" We offer competitive prices on our 100 million plus product any range."}],"section12_item3_title":[{"type":0,"value":"Worldwide delivery"}],"section12_item4_text":[{"type":0,"value":" We offer competitive prices on our 100 million plus product any range."}],"section12_item4_title":[{"type":0,"value":"Worldwide delivery"}],"section12_title":[{"type":0,"value":"Consumers\' reactions to our service"}],"select_files":[{"type":0,"value":"Select Files"}],"select_gender":[{"type":0,"value":"Your gender"}],"select_image":[{"type":0,"value":"Please select image"}],"select_location":[{"type":0,"value":"Select location"}],"send":[{"type":0,"value":"Save"}],"send(jonatish)":[{"type":0,"value":"Send"}],"send_again":[{"type":0,"value":"Resend the code"}],"send_ask_again":[{"type":0,"value":"You could ask to resend code after 1 minute"}],"send_message":[{"type":0,"value":"Send code"}],"share_on":[{"type":0,"value":"Share"}],"shop":[{"type":0,"value":"My shop"}],"shop_links":[{"type":0,"value":"Links(Channel,Facebook, Youtube....) related to your shop"}],"shop_name":[{"type":0,"value":"Shop name"}],"shop_review":[{"type":0,"value":"Write review about this shop"}],"site":[{"type":0,"value":"Site"}],"social_links":[{"type":0,"value":"Social networks"}],"sold_by":[{"type":0,"value":"Salesman"}],"special_instruction":[{"type":0,"value":"Special Instructions"}],"stock_available":[{"type":0,"value":"Stock available"}],"submit":[{"type":0,"value":"Submit"}],"successfully_booked":[{"type":0,"value":"You booked successfully"}],"sum":[{"type":0,"value":"sum"}],"support_tickets":[{"type":0,"value":"Support tickets"}],"surname":[{"type":0,"value":"Surname"}],"text_below_postcard":[{"type":0,"value":"We do not tell the recipient who the flowers are from. If you want your addressee to know who sent the gift, subscribe"}],"text_required":[{"type":0,"value":"Text required"}],"top_category":[{"type":0,"value":"Top categories"}],"total_calculation":[{"type":0,"value":"Total amount"}],"total_summary":[{"type":0,"value":"Total summary"}],"unit":[{"type":0,"value":"Unit"}],"unit_select":[{"type":0,"value":"Please select one units which related to your product"}],"units_error":[{"type":0,"value":"Please select one of units of length"}],"update_vendor_info_button":[{"type":0,"value":"Update info of  a vendor"}],"upload_images":[{"type":0,"value":"Upload images"}],"upload_images2":[{"type":0,"value":"Upload files"}],"user":[{"type":0,"value":"Log in"}],"view_all":[{"type":0,"value":"View all"}],"visit_Shop":[{"type":0,"value":"Visit shop"}],"wallet":[{"type":0,"value":"Wallet"}],"wallet_instructions":[{"type":0,"value":"Use the money in your wallet to advertise ads, purchase a collection and individual ads"}],"welcome_login":[{"type":0,"value":"Welcome Flower Store"}],"width":[{"type":0,"value":"Width"}],"width_unit":[{"type":0,"value":"Width unit"}],"wishlist":[{"type":0,"value":"Wishlist"}],"work_end_time":[{"type":0,"value":"End time of work"}],"work_start_time":[{"type":0,"value":"Beginning time of work"}],"write_review":[{"type":0,"value":"Write a review for this product"}],"your_address":[{"type":0,"value":"Your address"}],"your_address_map":[{"type":0,"value":"Your address(click place on address from map)"}],"youtube":[{"type":0,"value":"YouTube"}]}');
// EXTERNAL MODULE: external "next-seo"
var external_next_seo_ = __webpack_require__(6641);
// EXTERNAL MODULE: external "react-intl"
var external_react_intl_ = __webpack_require__(3126);
;// CONCATENATED MODULE: external "react-yandex-metrika"
const external_react_yandex_metrika_namespaceObject = require("react-yandex-metrika");
;// CONCATENATED MODULE: ./pages/_app.tsx






































Promise.resolve(/* import() */).then(__webpack_require__.t.bind(__webpack_require__, 7972, 23));
//Binding events.
router_default().events.on('routeChangeStart', ()=>external_nprogress_default().start()
);
router_default().events.on('routeChangeComplete', ()=>external_nprogress_default().done()
);
router_default().events.on('routeChangeError', ()=>external_nprogress_default().done()
);
external_nprogress_default().configure({
    showSpinner: false
});
const App = ({ Component , pageProps  })=>{
    let Layout = Component.layout || external_react_.Fragment;
    let router = (0,router_.useRouter)();
    //Axios defaults
    (external_axios_default()).defaults.headers.common.Authorization = `Bearer ${external_js_cookie_default().get('token')}` || 'asdfsdfasdfasdfasdf';
    (external_axios_default()).defaults.baseURL = Variables/* BASE_URL */._n;
    //Alternative Urls for SEO
    let alternatives_locales = router.locales.filter((one)=>one !== router.locale
    );
    let array = [];
    alternatives_locales.map((one)=>{
        array.push({
            hrefLang: one,
            href: `${Variables/* SITE_NAME */.px}${one === "uz" ? "" : "/" + one}${router.asPath}`
        });
    });
    (0,external_react_.useEffect)(()=>{
        external_aos_default().init({
            easing: "ease-out-cubic",
            once: false,
            offset: 50
        });
    }, []);
    (0,external_react_.useEffect)(()=>{
        Promise.resolve(/* import() */).then(__webpack_require__.t.bind(__webpack_require__, 9578, 23)).then((x)=>x.default
        ).then((ReactPixel)=>{
            ReactPixel.init('320315289983748') // facebookPixelId
            ;
            ReactPixel.pageView();
            router.events.on('routeChangeComplete', ()=>{
                ReactPixel.pageView();
            });
        });
    }, [
        router.events
    ]);
    //Alternative Urls for SEO
    const { 0: loading , 1: setloading  } = (0,external_react_.useState)(true);
    const { 0: banner , 1: setbanner  } = (0,external_react_.useState)([]);
    const { 0: category_products , 1: setcategory_products  } = (0,external_react_.useState)([]);
    const { 0: shop_list , 1: setshop_list  } = (0,external_react_.useState)([]);
    //Language configuration
    const { locale  } = (0,router_.useRouter)();
    const [shortLocale] = locale ? locale.split("-") : [
        "en"
    ];
    const messages = (0,external_react_.useMemo)(()=>{
        switch(shortLocale){
            case "ru":
                return ru_namespaceObject;
            case "en":
                return en_namespaceObject;
            case "k":
                return k_namespaceObject;
            default:
                return uz_namespaceObject;
        }
    }, [
        shortLocale
    ]);
    return(/*#__PURE__*/ jsx_runtime_.jsx(external_react_redux_.Provider, {
        store: store,
        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)(external_styled_components_.ThemeProvider, {
            theme: theme,
            children: [
                /*#__PURE__*/ (0,jsx_runtime_.jsxs)((head_default()), {
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                            name: "viewport",
                            content: "width=device-width, initial-scale=1"
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                            httpEquiv: "Content-Type",
                            content: "text/html; charset=utf-8"
                        })
                    ]
                }),
                /*#__PURE__*/ (0,jsx_runtime_.jsxs)(external_react_intl_.IntlProvider, {
                    locale: shortLocale,
                    messages: messages,
                    onError: ()=>null
                    ,
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx(GlobalStyles, {
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx(external_react_yandex_metrika_namespaceObject.YMInitializer, {
                            accounts: [
                                86371981,
                                86412148
                            ]
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx(AppContext/* AppProvider */.wI, {
                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)(Layout, {
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx(external_next_seo_.DefaultSeo, {
                                        canonical: `${Variables/* SITE_NAME */.px}${router.locale === "uz" ? "" : "/" + router.locale}${router.asPath}`,
                                        languageAlternates: array,
                                        openGraph: {
                                            locale: router.locale,
                                            site_name: "Dana.uz",
                                            url: `${Variables/* SITE_NAME */.px}${router.locale === "uz" ? "" : "/" + router.locale}${router.asPath}`,
                                            images: [
                                                {
                                                    url: "https://api.dana.uz/storage/about_company/1servicesK_d0kfBVUQzESAf8IuQv.png",
                                                    alt: "Dana.uz",
                                                    width: 600,
                                                    height: 200
                                                }
                                            ]
                                        }
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx(Component, {
                                        ...pageProps,
                                        loading: loading,
                                        setloading: (e)=>setloading(e)
                                        ,
                                        setcategory: (r)=>setcategory_products(r)
                                        ,
                                        category_products2: category_products,
                                        shop_list2: shop_list,
                                        banner2: banner,
                                        setbanner: (e)=>setbanner(e)
                                        ,
                                        setshop: (e)=>setshop_list(e)
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx((external_react_scroll_to_top_default()), {
                                        id: "scroll_to_top",
                                        smooth: true,
                                        color: "white"
                                    })
                                ]
                            })
                        })
                    ]
                })
            ]
        })
    }));
};
/* harmony default export */ const _app = (App);


/***/ }),

/***/ 9758:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "J": () => (/* binding */ deviceSize),
/* harmony export */   "P": () => (/* binding */ layoutConstant)
/* harmony export */ });
const deviceSize = {
    xs: 425,
    sm: 768,
    md: 1024,
    lg: 1440
};
const layoutConstant = {
    grocerySidenavWidth: "280px",
    containerWidth: "1200px",
    mobileNavHeight: "64px",
    headerHeight: "80px",
    mobileHeaderHeight: "64px"
};


/***/ }),

/***/ 191:
/***/ (() => {



/***/ }),

/***/ 5664:
/***/ (() => {



/***/ }),

/***/ 7972:
/***/ ((module) => {

"use strict";
module.exports = require("aos/dist/aos.js");

/***/ }),

/***/ 2167:
/***/ ((module) => {

"use strict";
module.exports = require("axios");

/***/ }),

/***/ 6734:
/***/ ((module) => {

"use strict";
module.exports = require("js-cookie");

/***/ }),

/***/ 6641:
/***/ ((module) => {

"use strict";
module.exports = require("next-seo");

/***/ }),

/***/ 968:
/***/ ((module) => {

"use strict";
module.exports = require("next/head");

/***/ }),

/***/ 1853:
/***/ ((module) => {

"use strict";
module.exports = require("next/router");

/***/ }),

/***/ 6689:
/***/ ((module) => {

"use strict";
module.exports = require("react");

/***/ }),

/***/ 9578:
/***/ ((module) => {

"use strict";
module.exports = require("react-facebook-pixel");

/***/ }),

/***/ 3126:
/***/ ((module) => {

"use strict";
module.exports = require("react-intl");

/***/ }),

/***/ 6022:
/***/ ((module) => {

"use strict";
module.exports = require("react-redux");

/***/ }),

/***/ 997:
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-runtime");

/***/ }),

/***/ 7518:
/***/ ((module) => {

"use strict";
module.exports = require("styled-components");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [8936,8970], () => (__webpack_exec__(3000)));
module.exports = __webpack_exports__;

})();