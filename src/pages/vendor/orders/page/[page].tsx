import DashboardPageHeader2 from "../../../../components/layout/DashboardPageHeader2";
import VendorDashboardLayout from "../../../../components/layout/VendorDashboardLayout";
import VendorOrderList from "../../../../components/orders/VendorOrderList";
import { NextSeo } from "next-seo";
import React, { useEffect } from "react";
import {useIntl} from "react-intl";
import Cookies from 'js-cookie'
import { useSelector } from "react-redux";
import { useRouter } from "next/router";
const Orders_vendor_page = () => {
 let intl = useIntl()
 let router = useRouter()
 let user = useSelector((state:any)=>state.token.user)
 let isLoggedIn = Cookies.get('isLoggedIn')
 useEffect(()=>{
    if(isLoggedIn === 'false' || user?.data?.is_shops === null){
        router.push('/404')
    }
 },[user,isLoggedIn])
  return (
    <VendorDashboardLayout >
        <NextSeo
            title={intl.formatMessage({id:"orders",defaultMessage:"Buyurtmalar"})}
        />
        <div>
        <DashboardPageHeader2
            title={intl.formatMessage({id:"orders",defaultMessage:"Buyurtmalar"})}
            iconName="bag_filled"
        />
        <VendorOrderList />
        </div>
    </VendorDashboardLayout>
  );
};


export default Orders_vendor_page;
