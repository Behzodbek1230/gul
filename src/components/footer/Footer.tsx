import AppStore from "../../components/AppStore";
import Image from "../../components/Image";
import Link from "next/link";
import React, {useEffect} from "react";
import styled2 from "styled-components";
import { getTheme } from "../../utils/utils";
import Box from "../Box";
import Container from "../Container";
import Grid from "../grid/Grid";
import Typography  from "../Typography";
import {useDispatch, useSelector} from "react-redux";
import "bootstrap/dist/css/bootstrap.min.css"
import {FormattedMessage, useIntl} from "react-intl";
import {ArrowForwardIosSharp, Facebook, Instagram, MusicNoteSharp, Telegram, YouTube} from "@material-ui/icons";
import axios from "axios";
import get_company_info from "../../../Redux/Actions/get_company_info";
import change_modal_status from "../../../Redux/Actions/change_modal_status";
import { useRouter } from "next/router";
import Currencies from "../../components/Currencies";
import { styled } from '@mui/material/styles';
import MuiAccordion, { AccordionProps } from '@mui/material/Accordion';
import MuiAccordionSummary, {
    AccordionSummaryProps,
} from '@mui/material/AccordionSummary';
import MuiAccordionDetails from '@mui/material/AccordionDetails';
import useWindowSize from "../../hooks/useWindowSize";


const StyledLink = styled2.a`
  position: relative;
  display: block;
  padding: 0.3rem 0rem;
  color: ${getTheme("colors.gray.500")};
  cursor: pointer;
  border-radius: 4px;
  :hover {
    color: ${getTheme("colors.gray.100")};
  }
`;



const Accordion = styled((props: AccordionProps) => (
    <MuiAccordion disableGutters elevation={0} square {...props} />
))(({  }) => ({
    '&:not(:last-child)': {
        borderBottom: 0,
    },
    '&:before': {
        display: 'none',
    },
}));

const AccordionSummary = styled((props: AccordionSummaryProps) => (
    <MuiAccordionSummary
        expandIcon={<ArrowForwardIosSharp fontSize={"small"}  />}
        {...props}
    />
))(({ theme }) => ({
    backgroundColor:"#F8CBDF !important",
    width:"100% !important",
    flexDirection: 'row-reverse',
    '& .MuiAccordionSummary-expandIconWrapper.Mui-expanded': {
        transform: 'rotate(90deg)',
    },
    '& .MuiAccordionSummary-content': {
        marginLeft: theme.spacing(1),
    },
    '& .MuiAccordionDetails-root':{
        backgroundColor:"#F8CBDF !important",
        borderColor:"0 !important"

    }
}));

const AccordionDetails = styled(MuiAccordionDetails)(({  }) => ({
    // padding: theme.spacing(2),
    // borderTop: '1px solid rgba(0, 0, 0, .125)',
}));



const Footer: React.FC = () => {
  const [expanded, setExpanded] = React.useState<string | false>('');
  let width = useWindowSize()
  const info = useSelector((state:any)=>state.new.company_info);
  let dispatch = useDispatch()
  let router = useRouter()
  let modal = useSelector((state:any)=>state.new.modal)
  let lang = router.locale
  useEffect(()=>{
      axios.get(`/references/about-company/${lang}`)
          .then(res=>{
              dispatch(get_company_info(res.data[0]))
          })
          .catch(()=>null)
  },[lang])
  let intl = useIntl();
  let footer = useSelector((state:any)=>state.new.footer)
  let aboutLinks = footer.about_company
  let shops = footer.shops
  let customerCareLinks = footer.services
  const handleChange =
        (panel: string) => (event: React.SyntheticEvent, newExpanded: boolean) => {
            setExpanded(newExpanded ? panel : false);
            console.log(event)
        };
  return (
    <footer data-aos="zoom-in" style={{bottom:"0px"}}>
      <Box
          bg="#f8cbdf"
          id="footer_container"
      >
        <Container
            p="1rem"
            color="white"

        >
          <Box
              py="3rem"
              px="0rem"
              style={{
                paddingTop:"40px",
                paddingBottom:"50px"
              }}
              overflow="hidden"
          >
            <Grid
                container
                spacing={0}
                id="footer_main_container"
                justifyContent="between"
            >
              <Grid
                  item
                  lg={2}
                  md={2}
                  sm={12}
                  xs={12}
                  className="mb-4 mb-sm-4 mb-md-0 mb-lg-0 mb-xl-0"
              >
                <Link href="/">
                  <a>
                    <Image
                      mb="0.5rem"
                      src={info.logo}
                      alt="logo"
                      className="logo_image"
                      width={200}
                      height={40}
                    />
                  </a>
                </Link>

                <div className="mt-3">
                    <AppStore  />
                </div>
              </Grid>


                  <Grid
                  item
                  lg={2}
                  md={2}
                  sm={12}
                  xs={12}
                  id="footer_navigatgion_header"

              >
                  <Accordion className="d-block d-sm-block d-md-block d-lg-none d-xl-none bg-f8cbdf" expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
                      <AccordionSummary className="mui-footer-accordion-summary" aria-controls="panel1d-content" id="panel1d-header">
                          <Typography
                              fontSize="20px"
                              fontWeight="600"
                              mb="0rem"
                              lineHeight="0"
                              className="text-secondary"
                              id="footer_header_text"
                          >
                              <FormattedMessage
                                id="social_links"
                              />
                          </Typography>
                      </AccordionSummary>
                      <AccordionDetails className="footer_mui_accordion-details">
                          <div>
                              <StyledLink href={info?.telegram} className=" text-secondary"><Telegram /> Telegram</StyledLink>
                              <StyledLink href={info?.facebook} className=" text-secondary"><Facebook /> Facebook</StyledLink>
                              <StyledLink href={info?.youtube} className="text-secondary"><YouTube /> YouTube</StyledLink>
                              <StyledLink href={info?.instagram}  className="text-secondary"><Instagram /> Instagram</StyledLink>
                              <StyledLink href={info?.tiktok}  className="text-secondary"><MusicNoteSharp /> TikTok</StyledLink>
                          </div>
                      </AccordionDetails>
                  </Accordion>
                <div className="d-none d-sm-none d-md-none d-lg-block d-xl-block">
                    <Typography
                        fontSize="20px"
                        fontWeight="600"
                        mb="0.5rem"
                        lineHeight="1"
                        className="text-secondary"
                        id="footer_header_text"
                    >
                        <FormattedMessage
                            id="social_links"
                        />

                    </Typography>

                    <div>
                        <StyledLink href={info?.telegram} className=" text-secondary"><Telegram /> Telegram</StyledLink>
                        <StyledLink href={info?.facebook} className=" text-secondary"><Facebook /> Facebook</StyledLink>
                        <StyledLink href={info?.youtube} className="text-secondary"><YouTube /> YouTube</StyledLink>
                        <StyledLink href={info?.instagram}  className="text-secondary"><Instagram /> Instagram</StyledLink>
                        <StyledLink href={info?.tiktok}  className="text-secondary"><MusicNoteSharp /> TikTok</StyledLink>
                    </div>
                </div>
              </Grid>


              <Grid
                  item
                  lg={2}
                  md={2}
                  sm={12}
                  xs={12}
                  id="footer_navigatgion_header"
                  className={`float-right ${width < 750 ? "" :"mb-4 mb-sm-4 mb-md-0 mb-lg-0 mb-xl-0 mr-0"}`}
              >
                  <Accordion className="d-block d-sm-block d-md-block d-lg-none d-xl-none bg-f8cbdf" expanded={expanded === 'panel2'} onChange={handleChange('panel2')}>
                      <AccordionSummary className="mui-footer-accordion-summary" aria-controls="panel1d-content" id="panel1d-header">
                          <Typography
                              fontSize="20px"
                              fontWeight="600"
                              mb="0rem"
                              lineHeight="1"
                              className="text-secondary"
                              id="footer_header_text"
                          >
                              <FormattedMessage
                                  id="footer_header_link1"
                                  defaultMessage="Biz haqimizda"
                              />
                          </Typography>
                      </AccordionSummary>
                      <AccordionDetails className="footer_mui_accordion-details">
                          <div>
                              {aboutLinks?.map((item, ind) => (
                                  <Link href={parseInt(item.type) === 3 ? "/faq" : `/company/${item.keyword}`} key={ind} >
                                      <StyledLink
                                          className="text-secondary"
                                      >
                                          {item.name}
                                      </StyledLink>
                                  </Link>
                              ))}
                              <Link
                                  href="/carriers"

                              >
                                  <StyledLink className="text-secondary" >
                                      {intl.formatMessage({id:"Carriers"})}
                                  </StyledLink>
                              </Link>
                              <Link
                                  href="/sitemap"

                              >
                                  <StyledLink className="text-secondary" >
                                      {intl.formatMessage({id:"Sitemap"})}
                                  </StyledLink>
                              </Link>
                              <Link
                                  href="/blogs"

                              >
                                  <StyledLink className="text-secondary" >
                                      {intl.formatMessage({id:"blogs"})}
                                  </StyledLink>
                              </Link>

                          </div>
                      </AccordionDetails>
                  </Accordion>




                <div className="d-none d-sm-none d-md-none d-lg-block d-xl-block">
                    <Typography
                        fontSize="20px"
                        fontWeight="600"
                        mb="0.5rem"
                        lineHeight="1"
                        className="text-secondary"
                        id="footer_header_text"
                    >
                        <FormattedMessage
                            id="footer_header_link1"
                            defaultMessage="Biz haqimizda"
                        />
                    </Typography>

                    <div>
                        {aboutLinks?.map((item, ind) => (
                            <Link href={parseInt(item.type) === 3 ? "/faq" : `/company/${item.keyword}`} key={ind} >
                                <StyledLink
                                    className="text-secondary"
                                >
                                    {item.name}
                                </StyledLink>
                            </Link>
                        ))}

                        <Link
                          href="/carriers"

                      >
                          <StyledLink className="text-secondary" >
                              {intl.formatMessage({id:"Carriers"})}
                          </StyledLink>
                        </Link>
                        <Link
                            href="/sitemap"

                        >
                            <StyledLink className="text-secondary" >
                                {intl.formatMessage({id:"Sitemap"})}
                            </StyledLink>
                        </Link>
                        <Link
                            href="/blogs"

                        >
                            <StyledLink className="text-secondary" >
                                {intl.formatMessage({id:"blogs"})}
                            </StyledLink>
                        </Link>
                    </div>
                </div>
              </Grid>

              <Grid
                  item
                  lg={2}
                  md={2}
                  sm={12}
                  xs={12}
                  id="footer_navigatgion_header"
                  className={`float-right${width < 750 ? "" :"mb-4 mb-sm-4 mb-md-0 mb-lg-0 mb-xl-0 mr-0"}`}
              >
                  <Accordion className="d-block d-sm-block d-md-block d-lg-none d-xl-none bg-f8cbdf" expanded={expanded === 'panel3'} onChange={handleChange('panel3')}>
                      <AccordionSummary className="mui-footer-accordion-summary" aria-controls="panel1d-content" id="panel1d-header">
                          <Typography
                              fontSize="20px"
                              fontWeight="600"
                              mb="0rem"
                              lineHeight="1"
                              className="text-secondary"
                              id="footer_header_text"
                          >
                              <FormattedMessage
                                  id="footer_header_link3"
                                  defaultMessage="Magazinlar"
                              />
                          </Typography>
                      </AccordionSummary>
                      <AccordionDetails className="footer_mui_accordion-details">
                          {shops?.map(one=>{
                              return (<Link href={`/shop/${one.keyword}/page/1`} key={one.id}>
                                  <StyledLink
                                      className="text-secondary"

                                  >
                                      {one.name}
                                  </StyledLink>
                              </Link>)
                          })}
                      </AccordionDetails>
                  </Accordion>




                <div className="d-none d-sm-none d-md-none d-lg-block d-xl-block">
                    <Typography
                        fontSize="20px"
                        fontWeight="600"
                        mb="0.5rem"
                        lineHeight="1"
                        className="text-secondary"
                        id="footer_header_text"
                    >
                        <FormattedMessage
                            id="footer_header_link3"
                            defaultMessage="Magazinlar"
                        />
                    </Typography>
                    {shops?.map(one=>{
                        return (<Link href={`/shop/${one.keyword}/page/1`} key={one.id}>
                            <StyledLink
                                className="text-secondary"

                            >
                                {one.name}
                            </StyledLink>
                        </Link>)
                    })}
                </div>



              </Grid>

              <Grid
                  item
                  lg={2}
                  md={2}
                  sm={12}
                  xs={12}
                  id="footer_navigatgion_header"
                  className={`float-right${width < 750 ? "" :"mb-4 mb-sm-4 mb-md-0 mb-lg-0 mb-xl-0 mr-0"}`}
              >



                  <Accordion className="d-block d-sm-block d-md-block d-lg-none d-xl-none bg-f8cbdf"  expanded={expanded === 'panel4'} onChange={handleChange('panel4')}>
                      <AccordionSummary className="mui-footer-accordion-summary" aria-controls="panel1d-content" id="panel1d-header">
                          <Typography
                              fontSize="20px"
                              fontWeight="600"
                              mb="0rem"
                              id="footer_header_text"
                              lineHeight="1"
                              className="text-secondary"

                          >
                              <FormattedMessage
                                  id="footer_header_link2"
                              />

                          </Typography>
                      </AccordionSummary>
                      <AccordionDetails className="footer_mui_accordion-details">
                          <div>

                              <Link
                                  href="/help"

                              >
                                  <StyledLink className="text-secondary" >
                                      {intl.formatMessage({id:"need_help"})}
                                  </StyledLink>
                              </Link>
                              <Link
                                  href="/faq"

                              >
                                  <StyledLink className="text-secondary" >
                                      {intl.formatMessage({id:"FAQ"})}
                                  </StyledLink>
                              </Link>

                              {customerCareLinks?.map((item, ind) => (
                                  <Link href={`/company/${item.keyword}`} key={ind}>
                                      <StyledLink
                                          className="text-secondary"
                                      >
                                          {item.name}
                                      </StyledLink>
                                  </Link>
                              ))}
                              <Link
                                  href="/corporate"

                              >
                                  <StyledLink className="text-secondary" >
                                      {intl.formatMessage({id:"Corparative Sales"})}
                                  </StyledLink>
                              </Link>
                              <StyledLink
                                  onClick={()=>dispatch(change_modal_status(!modal))}
                                  className="text-secondary"
                              >
                                  {intl.formatMessage({id: "order_status"})}
                              </StyledLink>

                          </div>
                      </AccordionDetails>
                  </Accordion>



                <div className="d-none d-sm-none d-md-none d-lg-block d-xl-block">
                    <Typography
                        fontSize="20px"
                        fontWeight="600"
                        mb="0.5rem"
                        id="footer_header_text"
                        lineHeight="1"
                        className="text-secondary"

                    >
                        <FormattedMessage
                            id="footer_header_link2"
                        />

                    </Typography>

                    <div>

                        <Link
                            href="/help"

                        >
                            <StyledLink className="text-secondary" >
                                {intl.formatMessage({id:"need_help"})}
                            </StyledLink>
                        </Link>
                        <Link
                            href="/faq"

                        >
                            <StyledLink className="text-secondary" >
                                {intl.formatMessage({id:"FAQ"})}
                            </StyledLink>
                        </Link>

                        {customerCareLinks?.map((item, ind) => (
                            <Link href={`/company/${item.keyword}`} key={ind}>
                                <StyledLink
                                    className="text-secondary"
                                >
                                    {item.name}
                                </StyledLink>
                            </Link>
                        ))}
                        <Link
                            href="/corporate"

                        >
                            <StyledLink className="text-secondary" >
                                {intl.formatMessage({id:"Corparative Sales"})}
                            </StyledLink>
                        </Link>
                        <StyledLink
                            onClick={()=>dispatch(change_modal_status(!modal))}
                            className="text-secondary"
                        >
                            {intl.formatMessage({id: "order_status"})}
                        </StyledLink>

                    </div>
                </div>
              </Grid>
            </Grid>
          </Box>

        </Container>

        <Container
            style={{paddingBottom:"0px",paddingTop:"20px"}}
            className="mb-sm-4 mb-lg-0 mb-md-0 mb-xl-0 mb-4"
        >

          <div
              className="col-12"
              style={{
                paddingBottom:"-20px !important",
                marginTop:"-65px"
              }}
          >

            <div
                className="d-flex flex-wrap justify-content-between "
            >
              <div
                  className="text-secondary "
              >

                  © 2021-{new Date().getUTCFullYear()} <FormattedMessage
                    id="all_rights"
                    defaultMessage=" © 2018-2021 Bisyor - Барча ҳуқуқлар ҳимояланган"
                />

              </div>
              <div className="text-truncate text-secondary text-left text-sm-left text-md-center text-lg-center text-xl-center" id="footer_Address">{info.address}</div>
                <Currencies />
            </div>
          </div>
        </Container>
      </Box>
    </footer>
  );
};



;

export default Footer;
