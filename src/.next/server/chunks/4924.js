"use strict";
exports.id = 4924;
exports.ids = [4924];
exports.modules = {

/***/ 4924:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _styled_system_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2038);
/* harmony import */ var _styled_system_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_styled_system_css__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _styled_system_theme_get__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(9099);
/* harmony import */ var _styled_system_theme_get__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_styled_system_theme_get__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(5834);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(styled_system__WEBPACK_IMPORTED_MODULE_5__);






const StyledMenu = styled_components__WEBPACK_IMPORTED_MODULE_4___default().div(_styled_system_css__WEBPACK_IMPORTED_MODULE_1___default()({
    position: "relative",
    ".menu-item-holder": {
        paddingTop: "0.5rem",
        paddingBottom: "0.5rem",
        minWidth: "200px",
        position: "absolute",
        borderRadius: "6px",
        top: "calc(100% + 0.5rem)",
        backgroundColor: (0,_styled_system_theme_get__WEBPACK_IMPORTED_MODULE_2__.themeGet)("body.paper", "#ffffff"),
        boxShadow: (0,_styled_system_theme_get__WEBPACK_IMPORTED_MODULE_2__.themeGet)("shadows.3", "0 6px 12px rgba(0, 0, 0, 0.16)"),
        zIndex: 100
    }
}), (0,styled_system__WEBPACK_IMPORTED_MODULE_5__.variant)({
    prop: "direction",
    variants: {
        left: {
            ".menu-item-holder": {
                left: 0,
                right: "auto"
            }
        },
        right: {
            ".menu-item-holder": {
                left: "auto",
                right: 0
            }
        }
    }
}));
const Menu = ({ handler , children , direction , ...props })=>{
    const { 0: show , 1: setShow  } = (0,react__WEBPACK_IMPORTED_MODULE_3__.useState)(false);
    const popoverRef = (0,react__WEBPACK_IMPORTED_MODULE_3__.useRef)(show);
    popoverRef.current = show;
    const handleDocumentClick = ()=>{
        if (popoverRef.current) setShow(false);
    };
    const togglePopover = (e)=>{
        e.stopPropagation();
        setShow(!show);
    };
    (0,react__WEBPACK_IMPORTED_MODULE_3__.useEffect)(()=>{
        window.addEventListener("click", handleDocumentClick);
        return ()=>{
            window.removeEventListener("click", handleDocumentClick);
        };
    }, []);
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(StyledMenu, {
        direction: direction,
        ...props,
        children: [
            /*#__PURE__*/ (0,react__WEBPACK_IMPORTED_MODULE_3__.cloneElement)(handler, {
                onClick: togglePopover,
                onMouseLeave: ()=>setShow(false)
            }),
            show && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                onMouseEnter: ()=>setShow(true)
                ,
                onMouseLeave: ()=>setShow(false)
                ,
                style: {
                    marginTop: "-10px"
                },
                className: "menu-item-holder",
                children: children
            })
        ]
    }));
};
Menu.defaultProps = {
    direction: "left"
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Menu);


/***/ })

};
;