import {FC} from "react";
import Icon from "../components/icon/Icon";
import Link from "next/link"
import {FormattedMessage} from "react-intl";

interface BreadcrumbProps{
    place:any
}

let Breadcrumb:FC<BreadcrumbProps> = ({place})=>{
    return(
        <nav aria-label='breadcrumb' >
            <ol className='breadcrumb-main'>
                <li className='breadcrumb-main-item text-capitalize cursor-pointer2 '>
                    <Link href="/" passHref>
                        <a  className='cursor-pointer2 text-secondary fw-bold' ><FormattedMessage id='mobile_navigation_home' /></a>
                    </Link>
                </li>
                <div className="mx-1"> <Icon size='12px'>next2</Icon></div>
                {place.map((one,ind)=>{
                    if(ind===place.length-1){
                        return  <li className='breadcrumb-main-item text-wrap fs-bold text-dark  active2' aria-current='page'>{one.title}</li>
                    }else{
                       return <>
                           <li className='breadcrumb-main-item text-secondary text-capitalize text-dark'>
                               <Link href={`/${one.keyword}`} passHref>
                                   <a className='cursor-pointer2 fw-bold text-secondary' >{one.title}</a>
                               </Link>
                           </li>
                            <div className="mx-1"> <Icon size='12px'>next2</Icon></div>
                       </>
                    }
                })}
            </ol>
        </nav>
    )
}

export default Breadcrumb
