import FlexBox from "../../../../components/FlexBox";
import Grid from "../../../../components/grid/Grid";
import Hidden from "../../../../components/hidden/Hidden";
import Icon from "../../../../components/icon/Icon";
import ShopIntroCard from "../../../../components/shop/ShopIntroCard";
import Sidenav from "../../../../components/sidenav/Sidenav";
import React, { useEffect, useState} from "react";
import cookies from "next-cookies";
import axios from "axios";
import {useDispatch, useSelector} from "react-redux";
import {H5} from "../../../../components/Typography";
import Box from "../../../../components/Box";
import Shop_Review from "../../../../components/Shop_Review";
import ProductCard3List from "../../../../components/products/ProductCard3List";
import {useRouter} from "next/router";
import Cookies from "js-cookie"
import ProductFilterCard3 from "../../../../components/products/ProductFilterCard3";
import {FormattedMessage} from "react-intl";
import useWindowSize from "../../../../hooks/useWindowSize";
import get_search_results from "../../../../../Redux/Actions/get_search_results";
import get_one_shop_products from "../../../../../Redux/Actions/get_one_shop_products";
import get_shop_info from "../../../../../Redux/Actions/get_shop_info";
import { BASE_URL } from "../../../../components/Variables";
import AppLayout from "../../../../components/layout/AppLayout";
import Container from "../../../../components/Container";
import { NextSeo } from "next-seo";

const Shop_page = ({shop_data,shop_products}) => {
  const router = useRouter()
  const data4 = useSelector((state:any)=>state.new.shop)
  const dispatch = useDispatch() 
  const [from_price,setfrom_price] = useState("")
  const [to_price,setto_price] = useState("")
  const [cat_id,setcat_id] = useState("")
  let [reviews,setreviews] = useState({data:{ratingList:[]},lastPage:"0"})
  let [ReviewPage,setReviewPage] = useState(0)
  let [addedStatus,setaddedStatus] = useState("")
  const sort = ""
  const width = useWindowSize();
  const isTablet = width < 1025;
  const [selectedOption, setSelectedOption] = useState("description");
  let lang = router.locale
  let current_currency = useSelector((state:any)=>state.token.current_currency)
  const { id,page } = router.query

  useEffect(()=>{
        axios(`/shops/show/${id}/${lang}`)
            .then((res)=>{
                dispatch(get_shop_info(res.data))
            })
            .catch(()=>null)
    },[lang])

  useEffect(()=> {
      setreviews(data4)
  },[data4])

  useEffect(()=>{
    dispatch(get_one_shop_products(shop_products))
    dispatch(get_shop_info(shop_data))
    dispatch(get_search_results(shop_products))
  },[])
  useEffect(()=>{
          axios.get(`/shops/rating-list/${id}/${lang}?page=${ReviewPage}`)
              .then(res=>{
                  setreviews(res.data)
              })
              .catch(()=>null)
  },[ReviewPage,addedStatus])
  useEffect(()=>{
    let currency_id = Cookies.get("currency_id");
    let currency_text = typeof currency_id !== "undefined" ?  `&currency=${currency_id}` : ""
    let url = `/flowers/search/${lang}?page=${page}&from_price=${from_price}&to_price=${to_price}&keyword=${cat_id}&shop_keyword=${id}${currency_text}`
    axios(url)
        .then(res=>{
            dispatch(get_search_results(res.data))
        })
        .catch(()=>{
            return null;
        })
  },[from_price,to_price,cat_id,sort,id,current_currency,page])
  const handleOptionClick = (opt) => () => {
      setSelectedOption(opt);
  }
  return (
    <AppLayout >
        <NextSeo
            title={data4?.seo?.view_share_title}
            description={shop_data?.seo?.view_mdescription}
            additionalMetaTags={[{
            name: 'keyword',
            content: shop_data?.seo?.view_mkeywords
            }, ]}
            openGraph={{
                title:shop_data?.data4?.seo?.view_share_title,
                description:shop_data?.seo?.view_share_description,
                site_name:shop_data?.seo?.view_share_sitename,
                profile: {
                    firstName:shop_data?.data?.name,
                    username: shop_data?.data?.keyword,
                    // gender:"male"

                },
                images:[
                    {
                        url:shop_data?.data?.logo,
                        alt:shop_data?.data?.name
                    }
                ],
                
              }}
        />
       <Container className="mt-50">
        <div>
            <ShopIntroCard  />

                <FlexBox
                    borderBottom="1px solid"
                    borderColor="gray.400"
                    mt="80px"
                    mb="26px"
                    className="whitespace-break word-break"
                >
                    <H5
                        className="cursor-pointer"
                        mr="10px"
                        p="4px 10px"
                        color={
                            selectedOption === "description" ? "primary.main" : "text.muted"
                        }
                        borderBottom={selectedOption === "description" && "2px solid"}
                        borderColor="primary.main"
                        onClick={handleOptionClick("description")}
                    >
                        <FormattedMessage
                            id="Products"
                            defaultMessage="Mahsulotlar"
                        />
                    </H5>
                    <H5
                        className="cursor-pointer"
                        p="4px 10px"
                        mr="10px"
                        color={selectedOption === "review" ? "primary.main" : "text.muted"}
                        onClick={handleOptionClick("review")}
                        borderBottom={selectedOption === "review" && "2px solid"}
                        borderColor="primary.main"
                    >
                        <FormattedMessage
                            id="Review"
                            defaultMessage="Tavsif"
                        />
                    </H5>
                    <H5
                        className="cursor-pointer"
                        p="4px 10px"

                        color={selectedOption === "contact" ? "primary.main" : "text.muted"}
                        onClick={handleOptionClick("contact")}
                        borderBottom={selectedOption === "contact" && "2px solid"}
                        borderColor="primary.main"
                    >
                        <FormattedMessage
                            id="description"
                            defaultMessage="Tavsif"
                        />
                    </H5>
                </FlexBox>
                <Box mb="50px">
                    {
                        selectedOption === "description"
                        &&
                            <Grid container spacing={6}>
                                <Hidden as={Grid} item md={3} xs={12} down={1024}>
                                    <ProductFilterCard3
                                        setfrom_price={(e)=>setfrom_price(e)}
                                        setto_price={(e)=>setto_price(e)}
                                        setcat_id={(g)=>setcat_id(g)}
                                    />
                                </Hidden>

                                <Grid item md={9} xs={12}>
                            {isTablet && (
                                <Sidenav
                                position="left"
                                scroll={true}
                                handle={
                                <FlexBox justifyContent="flex-end" mb="12px">
                                <Icon>options</Icon>
                                </FlexBox>
                            }
                                >
                                    <ProductFilterCard3
                                        setfrom_price={(e)=>setfrom_price(e)}
                                        setto_price={(e)=>setto_price(e)}
                                        setcat_id={(g)=>setcat_id(g)}
                                    />
                                </Sidenav>
                                )}
                                    <ProductCard3List />
                                </Grid>
                                </Grid>
                    }
                    {selectedOption === "review" &&
                        <div
                            style=
                                {{
                                    backgroundColor:"white",
                                    borderRadius:"15px",
                                    padding:"20px",
                                }}
                        >
                            <Shop_Review setaddedstatus={(e)=>setaddedStatus(e)}   reviews={reviews} setreviews={(r)=>setreviews(r)} setReviewPage={(e)=>setReviewPage(e)} ReviewPage={ReviewPage} />
                        </div>
                    }
                    {
                        selectedOption === "contact" &&
                            <div style={{backgroundColor:"white",borderRadius:"15px",padding:"20px"}}>
                                <div className="whitespace-break" dangerouslySetInnerHTML={{__html:shop_data?.data?.description}} ></div> 
                            </div>
                    }
                </Box>

            </div>
       </Container>
    </AppLayout>
  );
};

export async function getStaticPaths() {
    let paths=[{params:{id:"safia",page:"1"}}]
    // Get the paths we want to pre-render based on posts
    // const paths = posts.map((post) => ({
    //     params: { id: post.id },
    // }))

    // We'll pre-render only these paths at build time.
    // { fallback: blocking } will server-render pages
    // on-demand if the path doesn't exist.
    return { paths, fallback: 'blocking' }
}


export async function getStaticProps(ctx) {
    let {token} = await cookies(ctx)
    let  lang = ctx.locale
    const request2 = await axios({
        method: "GET",
        url: `${BASE_URL}/profile/max-value/${lang}`,
        headers: {
            "Authorization": `Bearer ${token} `
        },
    })
    const answer =  request2.data;
    return {
        props:{
            shop_data:[],
            data:answer,
            shop_products:[]
        },
        revalidate:2
    }

}
export default Shop_page;
