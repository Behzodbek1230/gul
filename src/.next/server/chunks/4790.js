"use strict";
exports.id = 4790;
exports.ids = [4790,9221,9758];
exports.modules = {

/***/ 8516:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
const logout = ()=>{
    return {
        type: "LOG_OUT"
    };
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (logout);


/***/ }),

/***/ 4726:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
let change_modal_status = (data)=>{
    return {
        type: "CHANGE_MODAL_STATUS",
        payload: data
    };
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (change_modal_status);


/***/ }),

/***/ 9221:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5834);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_system__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _utils_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(9758);



const Container = (styled_components__WEBPACK_IMPORTED_MODULE_0___default().div)`
  max-width: ${_utils_constants__WEBPACK_IMPORTED_MODULE_2__/* .layoutConstant.containerWidth */ .P.containerWidth};
  margin-left: auto;
  margin-right: auto;

  @media only screen and (max-width: 1199px) {
    margin-left: 1rem;
    margin-right: 1rem;
  }

  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.color}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.position}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.flexbox}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.layout}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.space}
`;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Container);


/***/ }),

/***/ 1016:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
function CurrencyFormatter(number) {
    return new Intl.NumberFormat("ru-Ru").format(number);
}
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CurrencyFormatter);


/***/ }),

/***/ 7650:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5834);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_system__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _utils_utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1972);



const Divider = (styled_components__WEBPACK_IMPORTED_MODULE_0___default().div)`
  height: 1px;
  background-color: ${(0,_utils_utils__WEBPACK_IMPORTED_MODULE_2__/* .getTheme */ .gh)("colors.gray.200")};
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.color}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.space}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.layout}
`;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Divider);


/***/ }),

/***/ 4585:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var next_image__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8579);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5834);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(styled_system__WEBPACK_IMPORTED_MODULE_2__);



const LazyImage = styled_components__WEBPACK_IMPORTED_MODULE_1___default()(next_image__WEBPACK_IMPORTED_MODULE_0__["default"])`
  display: block;
  ${styled_system__WEBPACK_IMPORTED_MODULE_2__.color}
  ${styled_system__WEBPACK_IMPORTED_MODULE_2__.space}
  ${styled_system__WEBPACK_IMPORTED_MODULE_2__.border}
`;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (LazyImage);


/***/ }),

/***/ 8943:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _styled_system_theme_get__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(9099);
/* harmony import */ var _styled_system_theme_get__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_styled_system_theme_get__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5834);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(styled_system__WEBPACK_IMPORTED_MODULE_2__);



const MenuItem = (styled_components__WEBPACK_IMPORTED_MODULE_1___default().div)`
  padding: 0.5rem 1rem;
  cursor: pointer;
  word-break: break-all;
  color: ${(0,_styled_system_theme_get__WEBPACK_IMPORTED_MODULE_0__.themeGet)("colors.text.secondary")};
  display: flex;
  align-items: center;

  &:hover {
    color: ${(0,_styled_system_theme_get__WEBPACK_IMPORTED_MODULE_0__.themeGet)("colors.primary.main")};
    background-color: ${(0,_styled_system_theme_get__WEBPACK_IMPORTED_MODULE_0__.themeGet)("colors.gray.100")};
  }
  ${styled_system__WEBPACK_IMPORTED_MODULE_2__.color}
  ${styled_system__WEBPACK_IMPORTED_MODULE_2__.space}
`;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (MenuItem);


/***/ }),

/***/ 4790:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ header_Header)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: ./components/buttons/IconButton.tsx
var IconButton = __webpack_require__(8661);
// EXTERNAL MODULE: ./components/Image.tsx
var Image = __webpack_require__(1891);
// EXTERNAL MODULE: ./components/Container.tsx
var Container = __webpack_require__(9221);
// EXTERNAL MODULE: ./components/avatar/Avatar.tsx + 1 modules
var Avatar = __webpack_require__(3392);
// EXTERNAL MODULE: ./components/FlexBox.tsx
var FlexBox = __webpack_require__(5359);
// EXTERNAL MODULE: ./components/LazyImage.tsx
var LazyImage = __webpack_require__(4585);
// EXTERNAL MODULE: ./contexts/app/AppContext.tsx + 4 modules
var AppContext = __webpack_require__(8936);
// EXTERNAL MODULE: ../node_modules/next/link.js
var next_link = __webpack_require__(9894);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);
// EXTERNAL MODULE: ./components/buttons/Button.tsx
var Button = __webpack_require__(2389);
// EXTERNAL MODULE: ./components/Divider.tsx
var Divider = __webpack_require__(7650);
// EXTERNAL MODULE: ./components/icon/Icon.tsx + 1 modules
var Icon = __webpack_require__(4154);
// EXTERNAL MODULE: ./components/Typography.tsx
var Typography = __webpack_require__(9374);
// EXTERNAL MODULE: ./components/mini-cart/MiniCartStyle.tsx
var MiniCartStyle = __webpack_require__(5907);
// EXTERNAL MODULE: external "js-cookie"
var external_js_cookie_ = __webpack_require__(6734);
var external_js_cookie_default = /*#__PURE__*/__webpack_require__.n(external_js_cookie_);
// EXTERNAL MODULE: external "axios"
var external_axios_ = __webpack_require__(2167);
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_);
// EXTERNAL MODULE: external "react-redux"
var external_react_redux_ = __webpack_require__(6022);
// EXTERNAL MODULE: external "react-intl"
var external_react_intl_ = __webpack_require__(3126);
// EXTERNAL MODULE: ../Redux/Actions/change_modal_status.js
var change_modal_status = __webpack_require__(4726);
// EXTERNAL MODULE: ./components/CurrencyFormatter.js
var CurrencyFormatter = __webpack_require__(1016);
// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(1853);
;// CONCATENATED MODULE: ./components/mini-cart/MiniCart.tsx



















const MiniCart = ({ toggleSidenav , mobile  })=>{
    const { state: state1 , dispatch  } = (0,AppContext/* useAppContext */.bp)();
    let { 0: currency_name , 1: setcurrency  } = (0,external_react_.useState)([
        {
            name: ''
        }
    ]);
    let lang = (0,router_.useRouter)().locale;
    const { cartList  } = state1.cart;
    let dispatch2 = (0,external_react_redux_.useDispatch)();
    let modal = (0,external_react_redux_.useSelector)((state)=>state.new.modal
    );
    let currencies = (0,external_react_redux_.useSelector)((state)=>state.token.currencies
    );
    let c_id = external_js_cookie_default().get('currency_id');
    (0,external_react_.useEffect)(()=>{
        setcurrency(currencies.filter((currency)=>currency.id === parseInt(c_id)
        ));
    }, [
        c_id
    ]);
    const getTotalPrice = ()=>{
        return cartList.reduce((accumulator, item)=>accumulator + Number.parseFloat(item.price.replace(/ /g, "")) * item.qty
        , 0) || 0;
    };
    const handleAddCart = (0,external_react_.useCallback)((amount, product)=>{
        return ()=>{
            const login = external_js_cookie_default().get("isLoggedIn");
            if (login === "true") {
                const data = new FormData();
                data.append("keyword", product.id.toString());
                external_axios_default()({
                    method: "POST",
                    url: `/orders/add-basket/${lang}`,
                    data: data
                }).then((res)=>{
                    var ref;
                    if ((res === null || res === void 0 ? void 0 : (ref = res.data) === null || ref === void 0 ? void 0 : ref.errors) === false) {
                        dispatch({
                            type: "CHANGE_CART_AMOUNT",
                            payload: {
                                ...product,
                                id: product.id,
                                imgUrl: product.image,
                                qty: amount
                            }
                        });
                    }
                }).catch(()=>null
                );
            } else {
                dispatch({
                    type: "CHANGE_CART_AMOUNT",
                    payload: {
                        ...product,
                        id: product.id,
                        imgUrl: product.image,
                        qty: amount
                    }
                });
            }
        };
    }, []);
    const handleremove_1 = (0,external_react_.useCallback)((amount, product)=>{
        return ()=>{
            const login = external_js_cookie_default().get("isLoggedIn");
            if (login === "true") {
                const data = new FormData();
                data.append("keyword", product.id.toString());
                external_axios_default()({
                    method: "POST",
                    url: `/orders/remove-basket/${lang}`,
                    data: data
                }).then((res)=>{
                    var ref;
                    if ((res === null || res === void 0 ? void 0 : (ref = res.data) === null || ref === void 0 ? void 0 : ref.errors) === false) {
                        dispatch({
                            type: "CHANGE_CART_AMOUNT",
                            payload: {
                                id: product.id,
                                name: product.name,
                                price: product.price,
                                imgUrl: product.image,
                                qty: amount,
                                categoryKeyword: product.categoryKeyword
                            }
                        });
                    }
                });
            } else {
                dispatch({
                    type: "CHANGE_CART_AMOUNT",
                    payload: {
                        id: product.id,
                        name: product.name,
                        price: product.price,
                        imgUrl: product.image,
                        qty: amount,
                        categoryKeyword: product.categoryKeyword
                    }
                });
            }
        };
    }, []);
    const handleremovefull = (0,external_react_.useCallback)((amount, product)=>{
        return ()=>{
            const login = external_js_cookie_default().get("isLoggedIn");
            if (login === "true") {
                const data = new FormData();
                data.append("keyword", product.id.toString());
                external_axios_default()({
                    method: "POST",
                    url: `/orders/delete-basket/${lang}`,
                    data: data
                }).then((res)=>{
                    var ref;
                    if ((res === null || res === void 0 ? void 0 : (ref = res.data) === null || ref === void 0 ? void 0 : ref.errors) === false) {
                        dispatch({
                            type: "CHANGE_CART_AMOUNT",
                            payload: {
                                ...product,
                                qty: amount
                            }
                        });
                    }
                }).catch(()=>null
                );
            } else {
                dispatch({
                    type: "CHANGE_CART_AMOUNT",
                    payload: {
                        ...product,
                        qty: amount
                    }
                });
            }
        };
    }, []);
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(MiniCartStyle/* StyledMiniCart */.T, {
        style: mobile ? {
            height: "fit-content"
        } : {
            height: "100vh"
        },
        children: [
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                className: `cart-list ${mobile && (cartList === null || cartList === void 0 ? void 0 : cartList.length) ? `h-50vh-scroll` : `w-100`}`,
                children: [
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)(FlexBox["default"], {
                        alignItems: "center",
                        m: "0px 20px",
                        height: "74px",
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx(Icon["default"], {
                                size: "1.75rem",
                                children: "cart"
                            }),
                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(Typography["default"], {
                                fontWeight: 600,
                                fontSize: "16px",
                                ml: "0.5rem",
                                children: [
                                    cartList === null || cartList === void 0 ? void 0 : cartList.length,
                                    " ",
                                    /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                                        id: "item",
                                        defaultMessage: "ta"
                                    })
                                ]
                            })
                        ]
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx(Divider/* default */.Z, {
                    }),
                    !!!(cartList === null || cartList === void 0 ? void 0 : cartList.length) && /*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
                        children: [
                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(FlexBox["default"], {
                                flexDirection: "column",
                                alignItems: "center",
                                justifyContent: "center",
                                height: mobile ? "50vh" : "calc(100% - 180px)",
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx(LazyImage/* default */.Z, {
                                        quality: 100,
                                        src: "/assets/images/logos/shopping-bag.svg",
                                        width: "90px",
                                        height: "100%"
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx(Typography.Paragraph, {
                                        mt: "1rem",
                                        color: "text.muted",
                                        textAlign: "center",
                                        maxWidth: "200px",
                                        children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                                            id: "empty_cart"
                                        })
                                    })
                                ]
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx(external_react_.Fragment, {
                                children: /*#__PURE__*/ jsx_runtime_.jsx(Button/* default */.Z, {
                                    variant: "contained",
                                    className: "mb-5 w-95",
                                    color: "primary",
                                    m: "1rem 0.75rem",
                                    onClick: ()=>dispatch2((0,change_modal_status/* default */.Z)(!modal))
                                    ,
                                    children: /*#__PURE__*/ jsx_runtime_.jsx(Typography["default"], {
                                        fontWeight: 600,
                                        children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                                            id: "order_status",
                                            defaultMessage: "Buyurtma xolati"
                                        })
                                    })
                                })
                            })
                        ]
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        children: cartList.map((item)=>{
                            var ref;
                            return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(external_react_.Fragment, {
                                children: [
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: "cart-item",
                                        children: [
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(FlexBox["default"], {
                                                alignItems: "center",
                                                flexDirection: "column",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx(Button/* default */.Z, {
                                                        variant: "outlined",
                                                        color: "primary",
                                                        padding: "5px",
                                                        size: "none",
                                                        borderColor: "primary.light",
                                                        borderRadius: "300px",
                                                        onClick: handleAddCart(item.qty + 1, item),
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx(Icon["default"], {
                                                            variant: "small",
                                                            children: "plus"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx(Typography["default"], {
                                                        fontWeight: 600,
                                                        fontSize: "15px",
                                                        my: "3px",
                                                        children: item === null || item === void 0 ? void 0 : item.qty
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx(Button/* default */.Z, {
                                                        variant: "outlined",
                                                        color: "primary",
                                                        padding: "5px",
                                                        size: "none",
                                                        borderColor: "primary.light",
                                                        borderRadius: "300px",
                                                        onClick: handleremove_1(item.qty - 1, item),
                                                        disabled: item.qty === 1,
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx(Icon["default"], {
                                                            variant: "small",
                                                            children: "minus"
                                                        })
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                                                href: `${item.categoryKeyword}/${item.id}`,
                                                children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                    children: /*#__PURE__*/ jsx_runtime_.jsx(Avatar/* default */.Z, {
                                                        src: item.imgUrl || "/assets/images/products/iphone-x.png",
                                                        mx: "1rem",
                                                        alt: item === null || item === void 0 ? void 0 : item.name,
                                                        size: 76
                                                    })
                                                })
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "product-details",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                                                        href: `${item.categoryKeyword}/${item.id}`,
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                            children: /*#__PURE__*/ jsx_runtime_.jsx(Typography.H5, {
                                                                className: "title py-1",
                                                                fontSize: "14px",
                                                                children: item === null || item === void 0 ? void 0 : item.name
                                                            })
                                                        })
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)(Typography.Tiny, {
                                                        color: "text.muted",
                                                        children: [
                                                            item === null || item === void 0 ? void 0 : item.price,
                                                            " x ",
                                                            item === null || item === void 0 ? void 0 : item.qty,
                                                            " = ",
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("label", {
                                                                className: "fw-bolder",
                                                                children: [
                                                                    (0,CurrencyFormatter/* default */.Z)(item.qty * parseFloat(item.price.replace(/ /g, ""))) + " ",
                                                                    " ",
                                                                    (currency_name === null || currency_name === void 0 ? void 0 : currency_name.length) >= 1 && currency_name ? (ref = currency_name[0]) === null || ref === void 0 ? void 0 : ref.name : currencies.filter((currency)=>{
                                                                        return (currency === null || currency === void 0 ? void 0 : currency.code) === "UZS";
                                                                    })[0].name
                                                                ]
                                                            })
                                                        ]
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx(Icon["default"], {
                                                className: "clear-icon",
                                                size: "1rem",
                                                ml: "1.25rem",
                                                onClick: handleremovefull(0, item),
                                                children: "close"
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx(Divider/* default */.Z, {
                                    })
                                ]
                            }, item.id));
                        })
                    })
                ]
            }),
            !!cartList.length && /*#__PURE__*/ (0,jsx_runtime_.jsxs)(external_react_.Fragment, {
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                        href: "/cart",
                        children: /*#__PURE__*/ jsx_runtime_.jsx(Button/* default */.Z, {
                            variant: "contained",
                            className: "mb-0",
                            color: "primary",
                            m: "1rem 1rem 0.75rem",
                            onClick: toggleSidenav,
                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)(Typography["default"], {
                                fontWeight: 600,
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                                        id: "Checkout Now",
                                        defaultMessage: "Hoziroq sotib olish"
                                    }),
                                    " (",
                                    (0,CurrencyFormatter/* default */.Z)(getTotalPrice()),
                                    " ",
                                    currency_name.length >= 1 && currency_name ? currency_name[0].name : currencies.filter((currency)=>currency.code == "UZS"
                                    )[0].name,
                                    ")"
                                ]
                            })
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx(Button/* default */.Z, {
                        variant: "contained",
                        className: "mb-5",
                        color: "primary",
                        m: "1rem 1rem 0.75rem",
                        onClick: ()=>dispatch2((0,change_modal_status/* default */.Z)(!modal))
                        ,
                        children: /*#__PURE__*/ jsx_runtime_.jsx(Typography["default"], {
                            fontWeight: 600,
                            children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                                id: "order_status",
                                defaultMessage: "Buyurtma xolati"
                            })
                        })
                    })
                ]
            })
        ]
    }));
};
MiniCart.defaultProps = {
    toggleSidenav: ()=>{
    }
};
/* harmony default export */ const mini_cart_MiniCart = (MiniCart);

// EXTERNAL MODULE: external "formik"
var external_formik_ = __webpack_require__(2296);
// EXTERNAL MODULE: external "yup"
var external_yup_ = __webpack_require__(5609);
// EXTERNAL MODULE: ./components/sessions/SessionStyle.tsx
var SessionStyle = __webpack_require__(8476);
// EXTERNAL MODULE: ../Redux/Actions/Action.js
var Action = __webpack_require__(7079);
// EXTERNAL MODULE: ../Redux/Actions/LoginModel.js
var LoginModel = __webpack_require__(2448);
// EXTERNAL MODULE: ../Redux/Actions/get_cart_products.js
var get_cart_products = __webpack_require__(903);
// EXTERNAL MODULE: ./components/login/Section1.tsx
var Section1 = __webpack_require__(5001);
// EXTERNAL MODULE: ./components/login/Section2.tsx
var Section2 = __webpack_require__(4900);
;// CONCATENATED MODULE: ./components/sessions/Login.tsx
















const Login2 = ()=>{
    const router = (0,router_.useRouter)();
    const { 0: telephonenumber , 1: Settelephonenumber  } = (0,external_react_.useState)(" ");
    const { 0: errors2 , 1: seterrors2  } = (0,external_react_.useState)(" ");
    const { 0: errors3 , 1: seterrors3  } = (0,external_react_.useState)(" ");
    const { 0: change , 1: setchange  } = (0,external_react_.useState)(false);
    const { 0: button , 1: setbutton  } = (0,external_react_.useState)(true);
    const { 0: code , 1: setcode  } = (0,external_react_.useState)(undefined);
    const { 0: error , 1: seterror  } = (0,external_react_.useState)(" ");
    const { 0: send_button , 1: setsend_button  } = (0,external_react_.useState)(false);
    let intl = (0,external_react_intl_.useIntl)();
    let lang = router.locale;
    const responseFacebook = (response1)=>{
        let data = new FormData();
        data.append("phone", response1 === null || response1 === void 0 ? void 0 : response1.phone);
        data.append("id", response1 === null || response1 === void 0 ? void 0 : response1.id);
        data.append("fio", response1 === null || response1 === void 0 ? void 0 : response1.name);
        data.append("email", response1 === null || response1 === void 0 ? void 0 : response1.email);
        data.append("type", "2");
        external_axios_default()({
            method: "POST",
            url: `/login/social-set`,
            data: data
        }).then((response)=>{
            let today = new Date();
            let day = today.getUTCDate();
            today.setDate(day + 30);
            external_js_cookie_default().remove("token");
            external_js_cookie_default().remove("isLoggedIn");
            document.cookie = `token=${response.data.access_token}; path=/; expires=${today}`;
            document.cookie = `isLoggedIn=true; path=/; expires=${today}`;
            dispatch2((0,Action/* get_token */.p)(response.data.access_token));
            dispatch2((0,LoginModel/* close_login */.I)());
            if (response.data.errors) {
                return null;
            } else {
                //--------------------- Clearing data from carts---------------------------
                handleremovecart();
                router.push(router.asPath);
            }
        }).catch(()=>{
            return null;
        });
    };
    const responseGoogle = (response2)=>{
        var ref, ref1, ref2;
        let data = new FormData();
        data.append("phone", response2 === null || response2 === void 0 ? void 0 : (ref = response2.profileObj) === null || ref === void 0 ? void 0 : ref.phone);
        data.append("id", response2 === null || response2 === void 0 ? void 0 : response2.googleId);
        data.append("fio", response2 === null || response2 === void 0 ? void 0 : (ref1 = response2.profileObj) === null || ref1 === void 0 ? void 0 : ref1.name);
        data.append("email", response2 === null || response2 === void 0 ? void 0 : (ref2 = response2.profileObj) === null || ref2 === void 0 ? void 0 : ref2.email);
        data.append("type", "1");
        external_axios_default()({
            method: "POST",
            url: `/login/social-set`,
            data: data
        }).then((response)=>{
            let today = new Date();
            let day = today.getUTCDate();
            today.setDate(day + 30);
            external_js_cookie_default().remove("token");
            external_js_cookie_default().remove("isLoggedIn");
            document.cookie = `token=${response.data.access_token}; path=/; expires=${today}`;
            document.cookie = `isLoggedIn=true; path=/; expires=${today}`;
            dispatch2((0,Action/* get_token */.p)(response.data.access_token));
            dispatch2((0,LoginModel/* close_login */.I)());
            if (response.data.errors) {
                return null;
            } else {
                //--------------------- Clearing data from carts---------------------------
                handleremovecart();
                router.push(router.asPath);
            }
        }).catch(()=>{
            return null;
        });
    };
    const dispatch2 = (0,external_react_redux_.useDispatch)();
    const handle222222 = (event)=>{
        event.preventDefault();
        const phone = event.target.value.slice(1, 17);
        const x = phone.replace(/-/g, "");
        const x2 = x.replace(/_/g, "");
        const isValid = x2.length === 12 ? true : false;
        if (isValid) {
            const phone_number = event.target.value.toString().replace(/-/g, "");
            Settelephonenumber(phone_number);
            seterrors2("");
        } else {
            if (event.target.value === "") {
                seterrors2(intl.formatMessage({
                    id: "input_phone"
                }));
            } else {
                seterrors2(intl.formatMessage({
                    id: "correct_phone"
                }));
            }
        }
    };
    const handlecode = (event)=>{
        event.preventDefault();
        if (event.target.value.length === 4) {
            seterrors3("");
            setcode(event.target.value);
        } else {
            seterrors3(intl.formatMessage({
                id: "correct_password"
            }));
        }
    };
    const { state , dispatch  } = (0,AppContext/* useAppContext */.bp)();
    const { cartList  } = state.cart;
    const handleCartAmountChange2 = (amount, product)=>{
        dispatch({
            type: "CHANGE_CART_AMOUNT",
            payload: {
                ...product,
                qty: amount
            }
        });
    };
    const handleCartAmountChange = (amount, product)=>{
        dispatch({
            type: "CHANGE_CART_AMOUNT",
            payload: {
                ...product,
                imgUrl: product.image,
                id: product.keyword,
                qty: amount
            }
        });
    };
    const handleremovecart = ()=>{
        try {
            if (external_js_cookie_default().get("isLoggedIn") === true) {
                cartList.forEach((value)=>{
                    handleCartAmountChange2(0, value);
                });
            }
        } catch  {
            return null;
        }
        let currency_text = typeof external_js_cookie_default().get("currency_id") === "undefined" ? "" : `?currency=${external_js_cookie_default().get("currency_id")}`;
        if (external_js_cookie_default().get("isLoggedIn") === "true") {
            external_axios_default()(`/orders/basket-list/${lang}${currency_text}`).then((response)=>{
                dispatch2((0,get_cart_products/* default */.Z)(response.data));
                try {
                    cartList.forEach((product)=>handleCartAmountChange2(0, product)
                    );
                    response.data.products.map((product)=>handleCartAmountChange(product.count, product)
                    );
                } catch  {
                    return null;
                }
            }).catch(()=>null
            );
        }
    };
    const handleSubmit4 = (e)=>{
        e.preventDefault();
        external_axios_default()({
            url: `/login/auth`,
            method: "POST",
            data: {
                "phone": telephonenumber,
                "code": code
            }
        }).then((response)=>{
            if (response.data.errors) {
                seterror(intl.formatMessage({
                    id: "correct_password"
                }));
                return null;
            } else {
                let today = new Date();
                let day = today.getUTCDate();
                today.setDate(day + 30);
                external_js_cookie_default().remove("token");
                external_js_cookie_default().remove("isLoggedIn");
                document.cookie = `token=${response.data.access_token}; path=/; expires=${today}`;
                document.cookie = `isLoggedIn=true; path=/; expires=${today}`;
                dispatch2((0,Action/* get_token */.p)(response.data.access_token));
                dispatch2((0,LoginModel/* close_login */.I)());
                //--------------------- Clearing data from carts---------------------------
                handleremovecart();
                router.push(router.asPath);
            }
        }).catch(()=>{
            seterror(intl.formatMessage({
                id: "correct_password"
            }));
        });
    };
    const handleSubmit5 = (e)=>{
        setsend_button(true);
        e.preventDefault();
        external_axios_default()({
            url: `/login/verify`,
            method: "POST",
            data: {
                "phone": telephonenumber
            }
        }).then(()=>{
            setchange(true);
            setbutton(true);
            setsend_button(false);
            setTimeout(()=>{
                setbutton(false);
            }, 60 * 1000);
        });
    };
    const handleFormSubmit = async (values)=>{
        console.log(values);
        router.push("/profile");
    };
    const {} = (0,external_formik_.useFormik)({
        onSubmit: handleFormSubmit,
        initialValues,
        validationSchema: formSchema
    });
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(SessionStyle/* StyledSessionCard */.b, {
        mx: "auto",
        my: "2rem",
        boxShadow: "large",
        children: [
            change ? /*#__PURE__*/ jsx_runtime_.jsx(Section1/* default */.Z, {
                button: button,
                code: code,
                error: error,
                errors3: errors3,
                handleSubmit4: (e)=>handleSubmit4(e)
                ,
                handleSubmit5: (e)=>handleSubmit5(e)
                ,
                handlecode: (r)=>handlecode(r)
                ,
                setchange: (e)=>setchange(e)
            }) : /*#__PURE__*/ jsx_runtime_.jsx(Section2/* default */.Z, {
                errors2: errors2,
                handle222222: (r)=>handle222222(r)
                ,
                handleSubmit5: (e)=>handleSubmit5(e)
                ,
                responseFacebook: (e)=>responseFacebook(e)
                ,
                responseGoogle: (e)=>responseGoogle(e)
                ,
                send_button: send_button
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("br", {
            })
        ]
    }));
};
const initialValues = {
    email: "",
    password: ""
};
const formSchema = external_yup_.object().shape({
    email: external_yup_.string().email("invalid email").required("${path} is required"),
    password: external_yup_.string().required("${path} is required")
});
/* harmony default export */ const Login = (Login2);

// EXTERNAL MODULE: ./components/modal/Modal.tsx + 1 modules
var Modal = __webpack_require__(7748);
;// CONCATENATED MODULE: ./components/header/UserLoginDialog.tsx





const UserLoginDialog = ({ handle , children ,  })=>{
    const dispatch = (0,external_react_redux_.useDispatch)();
    const open3 = (0,external_react_redux_.useSelector)((state)=>state.token.open
    );
    const isLogged = (0,external_react_redux_.useSelector)((state)=>state.token.isLoggedIn
    );
    const toggleDialog = ()=>{
        if (open3) {
            dispatch((0,LoginModel/* close_login */.I)());
        } else {
            if (!isLogged) {
                dispatch((0,LoginModel/* open_login */.K)());
            }
        }
    };
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(external_react_.Fragment, {
        children: [
            /*#__PURE__*/ (0,external_react_.cloneElement)(handle, {
                onClick: toggleDialog
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(Modal/* default */.Z, {
                open: open3,
                onClose: toggleDialog,
                children: children
            })
        ]
    }));
};
/* harmony default export */ const header_UserLoginDialog = (UserLoginDialog);

;// CONCATENATED MODULE: ./components/OrderStatus/Part1.tsx


const Part1 = ({ handleSubmit , setorder_id , orderError  })=>{
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
        className: " check_status_container",
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx("h6", {
                children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                    id: "order_status",
                    defaultMessage: "Buyurtma xolati"
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                id: "check_order_status"
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                className: "fw-bold d-inline",
                children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                    id: "button_check_status"
                })
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("form", {
                onSubmit: handleSubmit,
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("br", {
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("input", {
                        type: "number",
                        min: 1,
                        required: true,
                        className: "form-control",
                        onChange: (r)=>setorder_id(r.target.value)
                    }),
                    orderError === "" ? "" : /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: "text-danger",
                        children: orderError
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("br", {
                    }),
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("button", {
                        type: "submit",
                        className: "btn btn-info text-white",
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                                id: "check_button_status"
                            }),
                            " "
                        ]
                    })
                ]
            })
        ]
    }));
};
/* harmony default export */ const OrderStatus_Part1 = (Part1);

;// CONCATENATED MODULE: ./components/OrderStatus/Part2.tsx


const Part2 = ({ info , setinfo  })=>{
    return(/*#__PURE__*/ jsx_runtime_.jsx("div", {
        className: "p-3 check_status_container",
        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
            children: [
                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                    className: "fw-bold d-inline my-5",
                    children: "Id"
                }),
                ": ",
                info.orderId,
                /*#__PURE__*/ jsx_runtime_.jsx("br", {
                }),
                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                    className: "fw-bold d-inline my-5",
                    children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                        id: "products_total_price"
                    })
                }),
                ": ",
                info.product_price,
                " ",
                info.currencyName,
                /*#__PURE__*/ jsx_runtime_.jsx("br", {
                }),
                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                    className: "fw-bold d-inline my-5",
                    children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                        id: "delivery_price"
                    })
                }),
                ": ",
                info.delivery_price,
                " ",
                info.currencyName,
                /*#__PURE__*/ jsx_runtime_.jsx("br", {
                }),
                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                    className: "fw-bold d-inline my-5",
                    children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                        id: "Discount"
                    })
                }),
                ": ",
                info.discount,
                " ",
                info.currencyName,
                /*#__PURE__*/ jsx_runtime_.jsx("br", {
                }),
                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                    className: "fw-bold d-inline my-5",
                    children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                        id: "Total"
                    })
                }),
                ": ",
                info.total_price,
                " ",
                info.currencyName,
                /*#__PURE__*/ jsx_runtime_.jsx("br", {
                }),
                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                    className: "fw-bold d-inline my-5",
                    children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                        id: "order_status"
                    })
                }),
                ": ",
                info.statusName,
                /*#__PURE__*/ jsx_runtime_.jsx("br", {
                }),
                /*#__PURE__*/ jsx_runtime_.jsx("br", {
                }),
                /*#__PURE__*/ jsx_runtime_.jsx("button", {
                    onClick: ()=>setinfo([])
                    ,
                    className: "w-100 btn btn-danger",
                    children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                        id: "go_back"
                    })
                })
            ]
        })
    }));
};
/* harmony default export */ const OrderStatus_Part2 = (Part2);

;// CONCATENATED MODULE: ./components/OrderStatus/CheckStatus.tsx







const CheckStatus = ()=>{
    let { 0: order_id , 1: setorder_id  } = (0,external_react_.useState)("");
    let { 0: order_info , 1: setorder_info  } = (0,external_react_.useState)([]);
    let { 0: orderError , 1: setorderError  } = (0,external_react_.useState)("");
    let intl = (0,external_react_intl_.useIntl)();
    let handleSubmit = (e)=>{
        event.preventDefault();
        let lang = external_js_cookie_default().get("lang");
        let currency_id = external_js_cookie_default().get("currency_id");
        let currency_text = typeof currency_id !== "undefined" ? `&currency=${currency_id}` : "";
        e.preventDefault();
        external_axios_default().get(`/orders/check-status-order/${lang}?order_id=${order_id}${currency_text}`).then((res)=>{
            setorder_info(res.data);
        }).catch(()=>setorderError("№ " + order_id + intl.formatMessage({
                id: "order_not_found"
            }))
        );
    };
    return(/*#__PURE__*/ jsx_runtime_.jsx("div", {
        className: "pb-0 mb-0",
        children: order_info.length !== 0 ? /*#__PURE__*/ jsx_runtime_.jsx(OrderStatus_Part2, {
            info: order_info,
            setinfo: (t)=>setorder_info(t)
        }) : /*#__PURE__*/ jsx_runtime_.jsx(OrderStatus_Part1, {
            orderError: orderError,
            handleSubmit: (r)=>handleSubmit(r)
            ,
            setorder_id: (t)=>setorder_id(t)
        })
    }));
};
/* harmony default export */ const OrderStatus_CheckStatus = (CheckStatus);

// EXTERNAL MODULE: external "reactstrap"
var external_reactstrap_ = __webpack_require__(6981);
// EXTERNAL MODULE: ./components/Card.tsx
var Card = __webpack_require__(4660);
// EXTERNAL MODULE: ./components/Box.tsx
var Box = __webpack_require__(4875);
// EXTERNAL MODULE: ./components/MenuItem.tsx
var MenuItem = __webpack_require__(8943);
// EXTERNAL MODULE: ./components/text-field/TextField.tsx + 1 modules
var TextField = __webpack_require__(3594);
// EXTERNAL MODULE: external "styled-components"
var external_styled_components_ = __webpack_require__(7518);
var external_styled_components_default = /*#__PURE__*/__webpack_require__.n(external_styled_components_);
// EXTERNAL MODULE: ./utils/utils.ts
var utils = __webpack_require__(1972);
;// CONCATENATED MODULE: ./components/search-box/SearchBoxStyle.tsx


const StyledSearchBox = (external_styled_components_default()).div`
  position: relative;
  display: flex;
  align-items: center;

  .search-icon {
    position: absolute;
    color: white;
    right: 0rem;
    z-index: 1;
  }

  .search-field {
    flex: 1 1 0;
    padding-left: 3rem;
    padding-right: 11.5rem;
    height: 44px;
    border-radius: 300px;
  }
  .search-button {
    position: absolute;
    height: 100%;
    right: 0;
    border-radius: 0 300px 300px 0;
    padding-left: 55px;
    padding-right: 55px;
  }
  .category-dropdown {
    position: absolute;
    right: 0px;
    color: ${(0,utils/* getTheme */.gh)("colors.text.hint")};
  }
  .dropdown-handler {
    height: 40px;
    cursor: pointer;
    min-width: 90px;
    padding-left: 1.25rem;
    padding-right: 1rem;
    border-left: 1px solid ${(0,utils/* getTheme */.gh)("colors.text.disabled")};
    span {
      margin-right: 0.75rem;
    }
  }
  .menu-button {
    display: none;
  }
  @media only screen and (max-width: 900px) {
    .category-dropdown {
      display: none;
    }
    .search-icon {
      left: 1rem;
    }
    .search-field {
      height: 40px;
      border-radius: 300px;
      padding-left: 2.75rem;
      padding-right: 3.5rem;
    }
    .search-button {
      padding-left: 1.25rem;
      padding-right: 1.25rem;
    }
    .menu-button {
      display: unset;
    }
  }
`;
/* harmony default export */ const SearchBoxStyle = (StyledSearchBox);

// EXTERNAL MODULE: ./hooks/useWindowSize.js
var useWindowSize = __webpack_require__(12);
;// CONCATENATED MODULE: ./components/search-box/SearchBox.tsx














const SearchBox = ()=>{
    const size = (0,useWindowSize/* default */.Z)();
    const router = (0,router_.useRouter)();
    const { 0: search2 , 1: setsearch2  } = (0,external_react_.useState)("");
    const { 0: resultList , 1: setResultList  } = (0,external_react_.useState)([]);
    let intl = (0,external_react_intl_.useIntl)();
    const handleSubmit = (e)=>{
        e.preventDefault();
        if (search2 === "") {
            return null;
        } else {
            router.push(`/product/search/${encodeURIComponent(search2)}`);
        }
    };
    const handleSubmit2 = ()=>{
        if (search2 === "") {
            return null;
        } else {
            router.push(`/product/search/${encodeURIComponent(search2)}`);
        }
    };
    const hanldeSearch = (0,external_react_.useCallback)((event)=>{
        event.preventDefault();
        setsearch2(event.target.value);
    }, []);
    const handleDocumentClick = ()=>{
        setResultList([]);
    };
    (0,external_react_.useEffect)(()=>{
        window.addEventListener("click", handleDocumentClick);
        return ()=>{
            window.removeEventListener("click", handleDocumentClick);
        };
    }, []);
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(Box["default"], {
        position: "relative",
        flex: "1 1 0",
        minWidth: "150px",
        mx: "auto",
        style: {
            width: '100%'
        },
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx(SearchBoxStyle, {
                children: size < 901 ? /*#__PURE__*/ jsx_runtime_.jsx(jsx_runtime_.Fragment, {
                    children: /*#__PURE__*/ jsx_runtime_.jsx("form", {
                        style: {
                            width: "100%"
                        },
                        method: "GET",
                        onSubmit: handleSubmit,
                        children: /*#__PURE__*/ jsx_runtime_.jsx("input", {
                            required: true,
                            className: "form-control border-none form-control-shadow-danger",
                            placeholder: intl.formatMessage({
                                id: "search"
                            }),
                            style: {
                                paddingLeft: '40px',
                                paddingRight: '10px',
                                width: "100%",
                                border: "1px solid lavender",
                                color: "black"
                            },
                            onChange: hanldeSearch
                        })
                    })
                }) : /*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx("form", {
                            className: "search_header_field",
                            onSubmit: handleSubmit,
                            children: /*#__PURE__*/ jsx_runtime_.jsx(TextField/* default */.Z, {
                                className: "form-control",
                                placeholder: intl.formatMessage({
                                    id: "search"
                                }),
                                fullwidth: true,
                                required: true,
                                style: {
                                    paddingLeft: '10px',
                                    borderRadius: '0px',
                                    margin: "0px",
                                    height: "40px",
                                    paddingRight: "5px",
                                    borderTopLeftRadius: "5px",
                                    borderBottomLeftRadius: "5px"
                                },
                                onChange: hanldeSearch
                            })
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            onClick: handleSubmit2,
                            className: "search-icon",
                            style: {
                                height: "40px",
                                width: "40px",
                                borderTopRightRadius: "5px",
                                borderBottomRightRadius: "5px",
                                backgroundColor: '#ae99d2',
                                position: "relative",
                                cursor: "pointer"
                            },
                            children: /*#__PURE__*/ jsx_runtime_.jsx(Icon["default"], {
                                size: "18px",
                                style: {
                                    position: "absolute",
                                    top: "25%",
                                    left: "25%"
                                },
                                children: "search"
                            })
                        })
                    ]
                })
            }),
            !!resultList.length && /*#__PURE__*/ jsx_runtime_.jsx(Card["default"], {
                position: "absolute",
                top: "100%",
                py: "0.5rem",
                width: "100%",
                boxShadow: "large",
                zIndex: 99,
                children: resultList.map((item)=>/*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                        href: `/product/search/${item}`,
                        children: /*#__PURE__*/ jsx_runtime_.jsx(MenuItem["default"], {
                            children: /*#__PURE__*/ jsx_runtime_.jsx(Typography.Span, {
                                fontSize: "14px",
                                children: item
                            })
                        }, item)
                    }, item)
                )
            })
        ]
    }));
};
/* harmony default export */ const search_box_SearchBox = (SearchBox);

// EXTERNAL MODULE: ./components/sidenav/Sidenav.tsx + 1 modules
var Sidenav = __webpack_require__(1211);
;// CONCATENATED MODULE: ./components/header/wishlistHandle.tsx






let WishlistHandle = ({ router , isloggedin , wishlist , user2  })=>{
    var ref, ref1;
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(FlexBox["default"], {
        ml: "20px",
        alignItems: "flex-start",
        children: [
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(IconButton/* default */.Z, {
                style: {
                    zIndex: 0,
                    background: "rgba(255, 255, 255, 0)",
                    marginLeft: "-12px"
                },
                color: "text.muted",
                bg: "white",
                onClick: ()=>router.push("/wish-list")
                ,
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx(Icon["default"], {
                        className: "d-flex justify-content-center",
                        style: {
                            fontSize: "25px",
                            color: "#ae99d2"
                        },
                        children: "heart-filled"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        style: {
                            fontSize: "small",
                            fontWeight: "lighter"
                        },
                        children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                            id: "wishlist",
                            defaultMessage: "Sevimlilar"
                        })
                    })
                ]
            }),
            isloggedin !== "true" && /*#__PURE__*/ jsx_runtime_.jsx(FlexBox["default"], {
                borderRadius: "300px",
                bg: "error.main",
                px: "5px",
                py: "2px",
                alignItems: "center",
                justifyContent: "center",
                ml: "-1rem",
                mt: "3px",
                children: /*#__PURE__*/ jsx_runtime_.jsx(Typography.Tiny, {
                    color: "white",
                    "test-id": "desktop_navigation_unauthenticated_wishlist_count",
                    fontWeight: "600",
                    children: wishlist.length
                })
            }),
            !!(user2 === null || user2 === void 0 ? void 0 : (ref = user2.data) === null || ref === void 0 ? void 0 : ref.favoritesCount) && /*#__PURE__*/ jsx_runtime_.jsx(FlexBox["default"], {
                borderRadius: "300px",
                bg: "error.main",
                px: "5px",
                py: "2px",
                alignItems: "center",
                justifyContent: "center",
                ml: "-1rem",
                mt: "3px",
                children: /*#__PURE__*/ jsx_runtime_.jsx(Typography.Tiny, {
                    color: "white",
                    "data-test": "desktop_navigation_authenticated_wishlist_count",
                    fontWeight: "600",
                    children: user2 === null || user2 === void 0 ? void 0 : (ref1 = user2.data) === null || ref1 === void 0 ? void 0 : ref1.favoritesCount
                })
            })
        ]
    }));
};
/* harmony default export */ const wishlistHandle = (WishlistHandle);

// EXTERNAL MODULE: ./utils/constants.ts
var constants = __webpack_require__(9758);
;// CONCATENATED MODULE: ./components/header/HeaderStyle.tsx



const StyledHeader = (external_styled_components_default()).header`
  position: relative;
  z-index: 1;
  height: ${constants/* layoutConstant.headerHeight */.P.headerHeight};
  background: ${(0,utils/* getTheme */.gh)("colors.body.paper")};

  .logo {
    img {
      display: block;
    }
  }
  .icon-holder {
    span {
      font-size: 12px;
      line-height: 1;
      margin-bottom: 4px;
    }
    h4 {
      margin: 0px;
      font-size: 14px;
      line-height: 1;
      font-weight: 600;
    }
    div {
      margin-left: 6px;
    }
  }

  .user {
    cursor: pointer;
  }

  @media only screen and (max-width: 900px) {
    height: ${constants/* layoutConstant.mobileHeaderHeight */.P.mobileHeaderHeight};

    .logo,
    .icon-holder,
    .category-holder {
      display: none;
    }
    .header-right {
      display: none !important;
    }
  }
`;
/* harmony default export */ const HeaderStyle = (StyledHeader);

// EXTERNAL MODULE: ../Redux/Actions/LogoutAction.js
var LogoutAction = __webpack_require__(8516);
// EXTERNAL MODULE: external "@material-ui/core"
var core_ = __webpack_require__(8130);
;// CONCATENATED MODULE: ./components/header/loggedInUserMenuItems.tsx













let LoggedInUserMenuList = ({ shop2 , setOpen2 , setloggedin  })=>{
    var ref, ref1, ref2, ref3;
    let user = (0,external_react_redux_.useSelector)((state)=>state.token.user
    );
    let router = (0,router_.useRouter)();
    let lang = router.locale;
    let dispatch2 = (0,external_react_redux_.useDispatch)();
    const { state: state1 , dispatch  } = (0,AppContext/* useAppContext */.bp)();
    const { cartList  } = state1.cart;
    function handleListKeyDown(event) {
        if (event.key === 'Tab') {
            event.preventDefault();
            setOpen2(false);
        }
    }
    const handleclick = ()=>{
        external_axios_default()({
            method: "POST",
            url: `/login/logout`
        }).then(()=>{
            external_js_cookie_default().remove("isLoggedIn");
            external_js_cookie_default().remove("token");
            dispatch2((0,LogoutAction/* default */.Z)());
            dispatch2((0,Action/* fetch_user_info */.W)({
            }));
            //--------------------- Clearing data from carts---------------------------
            handleremovecart();
            handleremovecart();
            setloggedin("false");
            if (router.pathname.startsWith("/profile") || router.pathname.startsWith("/vendor") || router.pathname.startsWith("/wish-list")) {
                router.push('/');
            }
        }).catch(()=>{
            return null;
        });
    };
    const handleCartAmountChange2 = (amount, product)=>{
        dispatch({
            type: "CHANGE_CART_AMOUNT",
            payload: {
                ...product,
                qty: amount
            }
        });
    };
    const handleremovecart = ()=>{
        try {
            cartList.forEach((value)=>{
                handleCartAmountChange2(0, value);
            });
        } catch  {
            return;
        }
        let currency_text = typeof external_js_cookie_default().get("currency_id") === "undefined" ? "" : `?currency=${external_js_cookie_default().get("currency_id")}`;
        if (external_js_cookie_default().get("isLoggedIn") === "true") {
            external_axios_default()(`/orders/basket-list/${lang}${currency_text}`).then((response)=>{
                dispatch2((0,get_cart_products/* default */.Z)(response.data));
            }).catch(()=>null
            );
        }
    };
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(core_.MenuList, {
        id: "menu-list-grow",
        onKeyDown: handleListKeyDown,
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx(core_.MenuItem, {
                style: {
                    padding: "0px",
                    margin: "0px"
                },
                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: "fs-small w-100 padding-20",
                    children: [
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("label", {
                            className: "fs-large",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                                    id: "hi",
                                    defaultMessage: "Salom"
                                }),
                                "  ",
                                (user === null || user === void 0 ? void 0 : (ref = user.data) === null || ref === void 0 ? void 0 : ref.fullname) ? user.data.fullname : user === null || user === void 0 ? void 0 : (ref1 = user.data) === null || ref1 === void 0 ? void 0 : ref1.phone
                            ]
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("br", {
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                            id: "balance",
                            defaultMessage: "Balans"
                        }),
                        " ",
                        (user === null || user === void 0 ? void 0 : (ref2 = user.data) === null || ref2 === void 0 ? void 0 : ref2.balance) ? user === null || user === void 0 ? void 0 : (ref3 = user.data) === null || ref3 === void 0 ? void 0 : ref3.balance : 0,
                        " ",
                        /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                            id: "sum"
                        })
                    ]
                })
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(core_.MenuItem, {
                onClick: ()=>router.push("/profile/edit")
                ,
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx(Icon["default"], {
                        variant: "small",
                        defaultcolor: "currentColor",
                        mr: "10px",
                        children: "user-profile"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                        id: "my_profile",
                        defaultMessage: "Mening profilim"
                    })
                ]
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(core_.MenuItem, {
                onClick: ()=>router.push(shop2 !== null ? "/vendor/products/page/1" : "/vendor/create")
                ,
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx(Icon["default"], {
                        variant: "small",
                        defaultcolor: "currentColor",
                        mr: "10px",
                        children: "crown"
                    }),
                    shop2 ? /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                        id: "shop",
                        defaultMessage: "Mening dokonlarim"
                    }) : /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                        id: "create_shop",
                        defaultMessage: "Magazin Yaratish"
                    })
                ]
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(core_.MenuItem, {
                onClick: handleclick,
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx(Icon["default"], {
                        variant: "small",
                        defaultcolor: "currentColor",
                        mr: "10px",
                        children: "logout"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                        id: "logout",
                        defaultMessage: "Chiqish"
                    })
                ]
            })
        ]
    }));
};
/* harmony default export */ const loggedInUserMenuItems = (LoggedInUserMenuList);

// EXTERNAL MODULE: external "@material-ui/core/Paper"
var Paper_ = __webpack_require__(640);
var Paper_default = /*#__PURE__*/__webpack_require__.n(Paper_);
// EXTERNAL MODULE: external "@material-ui/core/Grow"
var Grow_ = __webpack_require__(6491);
var Grow_default = /*#__PURE__*/__webpack_require__.n(Grow_);
// EXTERNAL MODULE: external "@material-ui/core/Popper"
var Popper_ = __webpack_require__(2767);
var Popper_default = /*#__PURE__*/__webpack_require__.n(Popper_);
;// CONCATENATED MODULE: ./components/header/cartHandle.tsx






let cartHandle = (cartList)=>/*#__PURE__*/ (0,jsx_runtime_.jsxs)(FlexBox["default"], {
        ml: "20px",
        alignItems: "flex-start",
        children: [
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(IconButton/* default */.Z, {
                style: {
                    zIndex: 0,
                    background: "rgba(255, 255, 255, 0)",
                    marginLeft: "-12px"
                },
                color: "text.muted",
                bg: "white",
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx(Icon["default"], {
                        className: "d-flex justify-content-center header_cart_style2",
                        children: "grocery-store (2)"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        style: {
                            fontSize: "small",
                            fontWeight: "lighter"
                        },
                        children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                            id: "cart",
                            defaultMessage: "Savatcha"
                        })
                    })
                ]
            }),
            !!cartList.length && /*#__PURE__*/ jsx_runtime_.jsx(FlexBox["default"], {
                borderRadius: "300px",
                bg: "error.main",
                px: "5px",
                py: "2px",
                alignItems: "center",
                justifyContent: "center",
                ml: "-1rem",
                mt: "3px",
                children: /*#__PURE__*/ jsx_runtime_.jsx(Typography.Tiny, {
                    color: "white",
                    fontWeight: "600",
                    children: cartList.length
                })
            })
        ]
    })
;
/* harmony default export */ const header_cartHandle = (cartHandle);

// EXTERNAL MODULE: external "@material-ui/core/ClickAwayListener"
var ClickAwayListener_ = __webpack_require__(7730);
var ClickAwayListener_default = /*#__PURE__*/__webpack_require__.n(ClickAwayListener_);
// EXTERNAL MODULE: external "@material-ui/core/styles"
var styles_ = __webpack_require__(8308);
;// CONCATENATED MODULE: ./components/header/Header.tsx




































const useStyles = (0,styles_.makeStyles)((theme)=>({
        root: {
            display: 'flex'
        },
        paper: {
            marginRight: theme.spacing(2)
        }
    })
);
const Header = ({ className  })=>{
    const router = (0,router_.useRouter)();
    const { 0: open2 , 1: setOpen2  } = (0,external_react_.useState)(false);
    const anchorRef = external_react_default().useRef(null);
    const info = (0,external_react_redux_.useSelector)((state)=>state.new.company_info
    );
    const dispatch2 = (0,external_react_redux_.useDispatch)();
    const { 0: open , 1: setOpen  } = (0,external_react_.useState)(false);
    const toggleSidenav = ()=>setOpen(!open)
    ;
    const { state: state1  } = (0,AppContext/* useAppContext */.bp)();
    const { cartList  } = state1.cart;
    const user2 = (0,external_react_redux_.useSelector)((state)=>state.token.user
    );
    const wishlist = (0,external_react_redux_.useSelector)((state)=>state.new.wishlist
    );
    const { 0: user , 1: setuser  } = (0,external_react_.useState)(user2);
    let { 0: isloggedin , 1: setloggedin  } = (0,external_react_.useState)("false");
    let modal = (0,external_react_redux_.useSelector)((state)=>state.new.modal
    );
    let loggedIn = external_js_cookie_default().get("isLoggedIn");
    let lang = router.locale;
    (0,external_react_.useEffect)(()=>{
        setloggedin(external_js_cookie_default().get("isLoggedIn"));
    }, [
        loggedIn
    ]);
    let userInfoFetch = ()=>{
        external_axios_default()(`/profile/max-value/${lang}`).then((response2)=>{
            var ref, ref1, ref2;
            if ((ref = response2.data) === null || ref === void 0 ? void 0 : (ref1 = ref.data) === null || ref1 === void 0 ? void 0 : ref1.id) {
                dispatch2((0,Action/* fetch_user_info */.W)(response2.data));
                setuser(response2.data);
                external_js_cookie_default().remove("isLoggedIn");
                external_js_cookie_default().set("isLoggedIn", "true", {
                    expires: 1
                });
            } else if ((ref2 = response2.data) === null || ref2 === void 0 ? void 0 : ref2.errors) {
                external_js_cookie_default().remove("isLoggedIn");
                external_js_cookie_default().remove("token");
                external_js_cookie_default().set("isLoggedIn", "false", {
                    expires: 1
                });
                setloggedin("false");
            }
        });
    };
    (0,external_react_.useEffect)(()=>{
        userInfoFetch();
    }, []);
    (0,external_react_.useEffect)(()=>{
        userInfoFetch();
    }, [
        loggedIn
    ]);
    //-----------------------------------------------------
    let classes = useStyles();
    let shop2;
    try {
        var ref3;
        shop2 = (ref3 = user2.data) === null || ref3 === void 0 ? void 0 : ref3.is_shops;
    } catch (e1) {
        try {
            let loggedin = external_js_cookie_default().get("isLoggedIn") || "false";
            if (loggedin === "true" && user.errors) {
                dispatch2((0,LogoutAction/* default */.Z)());
                external_js_cookie_default().remove("isLoggedIn");
                external_js_cookie_default().set("isLoggedIn", "false", {
                    expires: 1
                });
                shop2 = null;
            }
        } catch  {
            return null;
        }
    }
    const handleToggle = ()=>{
        setOpen2((prevOpen)=>!prevOpen
        );
    };
    const handleClose = (event)=>{
        if (anchorRef.current && anchorRef.current.contains(event.target)) {
            return null;
        }
        setOpen2(false);
    };
    return(/*#__PURE__*/ jsx_runtime_.jsx(HeaderStyle, {
        className: className,
        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)(Container["default"], {
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            height: "100%",
            children: [
                /*#__PURE__*/ jsx_runtime_.jsx(FlexBox["default"], {
                    className: "logo",
                    alignItems: "center",
                    mr: "1rem",
                    children: /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                        href: "/",
                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                            children: /*#__PURE__*/ jsx_runtime_.jsx(Image["default"], {
                                src: info.logo,
                                className: "logo_image",
                                alt: "logo"
                            })
                        })
                    })
                }),
                /*#__PURE__*/ jsx_runtime_.jsx(FlexBox["default"], {
                    flex: "1 1 0",
                    children: /*#__PURE__*/ jsx_runtime_.jsx(search_box_SearchBox, {
                    })
                }),
                /*#__PURE__*/ (0,jsx_runtime_.jsxs)(FlexBox["default"], {
                    className: "header-right",
                    alignItems: "right",
                    children: [
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)(IconButton/* default */.Z, {
                            onClick: ()=>dispatch2((0,change_modal_status/* default */.Z)(!modal))
                            ,
                            style: {
                                zIndex: 0,
                                background: "rgba(255, 255, 255, 0)",
                                marginLeft: "-5px",
                                width: "112px",
                                overflowX: "hidden",
                                borderRadius: "0px",
                                wordBreak: "keep-all",
                                wordWrap: "revert"
                            },
                            color: "text.muted",
                            bg: "white",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx(Icon["default"], {
                                    style: {
                                        fontSize: "25px",
                                        marginLeft: "2px",
                                        left: "40%",
                                        position: "relative",
                                        marginBottom: "1px"
                                    },
                                    children: "order_s"
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                    style: {
                                        fontSize: "small",
                                        fontWeight: "lighter",
                                        marginLeft: "2px",
                                        whiteSpace: "nowrap"
                                    },
                                    children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                                        id: "order_status",
                                        defaultMessage: "Buyurtma xolati"
                                    })
                                })
                            ]
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)(external_reactstrap_.Modal, {
                            isOpen: modal,
                            toggle: ()=>dispatch2((0,change_modal_status/* default */.Z)(!modal))
                            ,
                            className: "modal-dialog-centered",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx(external_reactstrap_.ModalHeader, {
                                    toggle: ()=>dispatch2((0,change_modal_status/* default */.Z)(!modal))
                                    ,
                                    children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                                        id: "order_status",
                                        defaultMessage: "Buyurtma holati"
                                    })
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx(external_reactstrap_.ModalBody, {
                                    children: /*#__PURE__*/ jsx_runtime_.jsx(OrderStatus_CheckStatus, {
                                    })
                                })
                            ]
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx(jsx_runtime_.Fragment, {
                            children: /*#__PURE__*/ jsx_runtime_.jsx(wishlistHandle, {
                                router: router,
                                isloggedin: isloggedin,
                                wishlist: wishlist,
                                user2: user2
                            })
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx(Sidenav/* default */.Z, {
                            handle: header_cartHandle(cartList),
                            position: "right",
                            open: open,
                            width: 380,
                            toggleSidenav: toggleSidenav,
                            children: /*#__PURE__*/ jsx_runtime_.jsx(mini_cart_MiniCart, {
                                toggleSidenav: toggleSidenav
                            })
                        }),
                        isloggedin === "true" ? /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: classes.root,
                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx(Button/* default */.Z, {
                                        ref: anchorRef,
                                        "aria-controls": open2 ? 'menu-list-grow' : undefined,
                                        "aria-haspopup": "true",
                                        className: "hover_button_akkount",
                                        onClick: ()=>handleToggle()
                                        ,
                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)(IconButton/* default */.Z, {
                                            className: "hover_button_user",
                                            bg: "white",
                                            p: "0px",
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx(Icon["default"], {
                                                    size: "24px",
                                                    className: "header_icon_style d-flex m-auto justify-content-center",
                                                    children: "round-account-button-with-user-inside"
                                                }),
                                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                    className: "header_icon_text_Style",
                                                    color: "text.muted",
                                                    children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        className: "text-secondary akkount_margin",
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                                                            id: "desktop_navigation_cabinet",
                                                            defaultMessage: "Kabinet"
                                                        })
                                                    })
                                                })
                                            ]
                                        })
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx((Popper_default()), {
                                        open: open2,
                                        anchorEl: anchorRef.current,
                                        role: undefined,
                                        transition: true,
                                        disablePortal: true,
                                        children: ({ TransitionProps , placement  })=>/*#__PURE__*/ jsx_runtime_.jsx((Grow_default()), {
                                                ...TransitionProps,
                                                style: {
                                                    transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom'
                                                },
                                                children: /*#__PURE__*/ jsx_runtime_.jsx((Paper_default()), {
                                                    children: /*#__PURE__*/ jsx_runtime_.jsx((ClickAwayListener_default()), {
                                                        onClickAway: handleClose,
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx(loggedInUserMenuItems, {
                                                            shop2: shop2,
                                                            setloggedin: (e)=>setloggedin(e)
                                                            ,
                                                            setOpen2: (e)=>setOpen2(e)
                                                        })
                                                    })
                                                })
                                            })
                                    })
                                ]
                            })
                        }) : /*#__PURE__*/ jsx_runtime_.jsx(header_UserLoginDialog, {
                            handle: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)(IconButton/* default */.Z, {
                                    bg: "white",
                                    color: "text.muted",
                                    className: "background-rgb",
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx(Icon["default"], {
                                            className: "header_icon_style d-flex justify-content-center",
                                            children: "round-account-button-with-user-inside"
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                            className: "header_icon_text_Style2",
                                            children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                className: "text-secondary",
                                                children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                                                    id: "user",
                                                    defaultMessage: "Kirish"
                                                })
                                            })
                                        })
                                    ]
                                })
                            }),
                            children: /*#__PURE__*/ jsx_runtime_.jsx(Box["default"], {
                                children: /*#__PURE__*/ jsx_runtime_.jsx(Login, {
                                })
                            })
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                            href: "/help",
                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)(IconButton/* default */.Z, {
                                className: "header_help_icon_container background-rgb",
                                color: "text.muted",
                                bg: "white",
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx(Icon["default"], {
                                        className: "header_help_icon_style d-flex justify-content-center",
                                        children: "help-web-button"
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                        className: "fs-small fw-lighter ml-2px",
                                        children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                                            id: "help",
                                            defaultMessage: "Yordam"
                                        })
                                    })
                                ]
                            })
                        })
                    ]
                })
            ]
        })
    }));
};
/* harmony default export */ const header_Header = (Header);


/***/ }),

/***/ 5907:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "T": () => (/* binding */ StyledMiniCart)
/* harmony export */ });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _utils_utils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1972);


const StyledMiniCart = (styled_components__WEBPACK_IMPORTED_MODULE_0___default().div)`
  display: flex;
  flex-direction: column;
  height: 100vh;

  .cart-list {
    flex: 1 1 0;
    overflow: auto;
  }

  .cart-item {
    display: flex;
    align-items: center;
    padding: 1rem;

    .add-cart {
      text-align: center;
    }

    .product-image {
      border-radius: 5px;
      margin-left: 1rem;
      margin-right: 1rem;
      width: 64px;
      height: 64px;
    }

    .product-details {
      flex: 1 1 0;
      min-width: 0px;

      .title {
        margin: 0px;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        line-height: 1;
      }
    }
    .clear-icon {
      color: ${(0,_utils_utils__WEBPACK_IMPORTED_MODULE_1__/* .getTheme */ .gh)("colors.gray.600")};
      cursor: pointer;
    }
  }
`;


/***/ }),

/***/ 1211:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ sidenav_Sidenav)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "react-dom"
var external_react_dom_ = __webpack_require__(6405);
// EXTERNAL MODULE: external "styled-components"
var external_styled_components_ = __webpack_require__(7518);
var external_styled_components_default = /*#__PURE__*/__webpack_require__.n(external_styled_components_);
// EXTERNAL MODULE: ./utils/utils.ts
var utils = __webpack_require__(1972);
;// CONCATENATED MODULE: ./components/sidenav/SidenavStyle.tsx


const StyledSidenav = (external_styled_components_default()).div`
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: 990;
  background: rgba(0, 0, 0, 0.53);

  @keyframes slide {
    from {
      ${({ position  })=>position
}: -${(props)=>props.width
}px;
    }
    to {
      ${({ position  })=>position
}: 0;
    }
  }

  .sidenav-content {
    position: absolute;
    background-color: ${(0,utils/* getTheme */.gh)("colors.body.paper")};
    ${({ position  })=>position
}: 0;
    width: ${(props)=>props.width
}px;
    height: 100%;
    overflow: ${({ scroll  })=>scroll ? "auto" : "hidden"
};
    animation: slide 250ms linear;
  }

  & ~ .cursor-pointer {
    cursor: pointer;
  }
`;

;// CONCATENATED MODULE: ./components/sidenav/Sidenav.tsx




const Sidenav = ({ position , open , width , scroll , handle , children , toggleSidenav ,  })=>{
    var ref;
    const { 0: sidenavOpen , 1: setSidenavOpen  } = (0,external_react_.useState)(open);
    const handleModalContentClick = (e)=>{
        e.stopPropagation();
    };
    const handleToggleSidenav = ()=>{
        setSidenavOpen(!sidenavOpen);
    };
    (0,external_react_.useEffect)(()=>{
        setSidenavOpen(open);
    }, [
        open
    ]);
    if (globalThis.document && sidenavOpen) {
        var ref1;
        let sidenav = document.querySelector("#sidenav-root");
        if (!sidenav) {
            sidenav = document.createElement("div");
            sidenav.setAttribute("id", "sidenav-root");
            document.body.appendChild(sidenav);
        }
        return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(external_react_.Fragment, {
            children: [
                /*#__PURE__*/ (0,external_react_dom_.createPortal)(/*#__PURE__*/ jsx_runtime_.jsx(StyledSidenav, {
                    open: sidenavOpen,
                    width: width,
                    position: position,
                    scroll: scroll,
                    onClick: toggleSidenav || handleToggleSidenav,
                    children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: "sidenav-content",
                        onClick: handleModalContentClick,
                        children: children
                    })
                }), sidenav),
                handle && /*#__PURE__*/ (0,external_react_.cloneElement)(handle, {
                    className: ((ref1 = handle.props) === null || ref1 === void 0 ? void 0 : ref1.className) + " cursor-pointer",
                    onClick: toggleSidenav || handleToggleSidenav
                })
            ]
        }));
    } else return handle && /*#__PURE__*/ (0,external_react_.cloneElement)(handle, {
        className: ((ref = handle.props) === null || ref === void 0 ? void 0 : ref.className) + " cursor-pointer",
        onClick: toggleSidenav || handleToggleSidenav
    });
};
Sidenav.defaultProps = {
    width: 280,
    position: "right",
    open: false,
    scroll: false
};
/* harmony default export */ const sidenav_Sidenav = (Sidenav);


/***/ }),

/***/ 3594:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ text_field_TextField)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: ./components/Box.tsx
var Box = __webpack_require__(4875);
// EXTERNAL MODULE: external "@styled-system/css"
var css_ = __webpack_require__(2038);
var css_default = /*#__PURE__*/__webpack_require__.n(css_);
// EXTERNAL MODULE: external "styled-components"
var external_styled_components_ = __webpack_require__(7518);
var external_styled_components_default = /*#__PURE__*/__webpack_require__.n(external_styled_components_);
// EXTERNAL MODULE: external "styled-system"
var external_styled_system_ = __webpack_require__(5834);
// EXTERNAL MODULE: ./utils/utils.ts
var utils = __webpack_require__(1972);
;// CONCATENATED MODULE: ./components/text-field/TextFieldStyle.tsx




const SyledTextField = external_styled_components_default().input((props)=>css_default()({
        padding: "8px 12px",
        height: "40px",
        fontSize: "inherit",
        color: "body.text",
        borderRadius: 5,
        border: "1px solid",
        borderColor: "text.disabled",
        width: props.fullwidth ? "100%" : "inherit",
        outline: "none",
        fontFamily: "inherit",
        "&:hover": {
            borderColor: "gray.500"
        },
        "&:focus": {
            outlineColor: "primary.main",
            borderColor: "primary.main",
            boxShadow: `1px 1px 8px 4px rgba(${(0,utils/* convertHexToRGB */.Q5)(props.theme.colors.primary.light)}, 0.1)`
        }
    })
, (0,external_styled_system_.compose)(external_styled_system_.color));
const TextFieldWrapper = external_styled_components_default().div((props)=>css_default()({
        position: "relative",
        width: props.fullwidth ? "100%" : "inherit",
        label: {
            display: "block",
            marginBottom: "6px",
            fontSize: "0.875rem"
        },
        small: {
            display: "block",
            color: "error.main",
            marginTop: "0.25rem",
            marginLeft: "0.25rem"
        },
        ".end-adornment": {
            position: "absolute",
            top: "50%",
            transform: "translateY(-50%)",
            right: "0.25rem"
        }
    })
, (0,external_styled_system_.compose)(external_styled_system_.color, external_styled_system_.space));

;// CONCATENATED MODULE: ./components/text-field/TextField.tsx




const TextField = ({ id , label , errorText , labelColor , endAdornment , ...props })=>{
    const { 0: textId , 1: setTextId  } = (0,external_react_.useState)(id);
    // extract spacing props
    let spacingProps = {
    };
    for(const key in props){
        if (key.startsWith("m") || key.startsWith("p")) spacingProps[key] = props[key];
    }
    (0,external_react_.useEffect)(()=>{
        if (!id) setTextId(Math.random());
    }, []);
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(TextFieldWrapper, {
        color: labelColor && `${labelColor}.main`,
        fullwidth: props.fullwidth,
        ...spacingProps,
        children: [
            label && /*#__PURE__*/ jsx_runtime_.jsx("label", {
                htmlFor: textId,
                children: label
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(Box["default"], {
                position: "relative",
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx(SyledTextField, {
                        id: textId,
                        ...props
                    }),
                    endAdornment && /*#__PURE__*/ (0,external_react_.cloneElement)(endAdornment, {
                        className: `end-adornment ${endAdornment.className}`
                    })
                ]
            }),
            errorText && /*#__PURE__*/ jsx_runtime_.jsx("small", {
                children: errorText
            })
        ]
    }));
};
TextField.defaultProps = {
    color: "default"
};
/* harmony default export */ const text_field_TextField = (TextField);


/***/ }),

/***/ 9758:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "J": () => (/* binding */ deviceSize),
/* harmony export */   "P": () => (/* binding */ layoutConstant)
/* harmony export */ });
const deviceSize = {
    xs: 425,
    sm: 768,
    md: 1024,
    lg: 1440
};
const layoutConstant = {
    grocerySidenavWidth: "280px",
    containerWidth: "1200px",
    mobileNavHeight: "64px",
    headerHeight: "80px",
    mobileHeaderHeight: "64px"
};


/***/ })

};
;