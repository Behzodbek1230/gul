"use strict";
exports.id = 448;
exports.ids = [448];
exports.modules = {

/***/ 448:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _hooks_useWindowSize__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(12);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(1853);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(2167);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var next_dynamic__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(5218);






const ProductCard6 = (0,next_dynamic__WEBPACK_IMPORTED_MODULE_5__["default"])(()=>Promise.all(/* import() */[__webpack_require__.e(1972), __webpack_require__.e(1601)]).then(__webpack_require__.bind(__webpack_require__, 1601))
, {
    ssr: false
});
const Carousel = (0,next_dynamic__WEBPACK_IMPORTED_MODULE_5__["default"])(()=>Promise.all(/* import() */[__webpack_require__.e(1972), __webpack_require__.e(4154), __webpack_require__.e(8661), __webpack_require__.e(9868), __webpack_require__.e(9758)]).then(__webpack_require__.bind(__webpack_require__, 9868))
, {
    ssr: false
});
const Container = (0,next_dynamic__WEBPACK_IMPORTED_MODULE_5__["default"])(()=>__webpack_require__.e(/* import() */ 9221).then(__webpack_require__.bind(__webpack_require__, 9221))
, {
    ssr: false
});
const Box = (0,next_dynamic__WEBPACK_IMPORTED_MODULE_5__["default"])(()=>__webpack_require__.e(/* import() */ 4875).then(__webpack_require__.bind(__webpack_require__, 4875))
, {
    ssr: false
});
const Section3 = ()=>{
    const width = (0,_hooks_useWindowSize__WEBPACK_IMPORTED_MODULE_1__/* ["default"] */ .Z)();
    let router = (0,next_router__WEBPACK_IMPORTED_MODULE_3__.useRouter)();
    const { 0: visibleSlides , 1: setVisibleSlides  } = (0,react__WEBPACK_IMPORTED_MODULE_2__.useState)(0);
    let lang = router.locale;
    (0,react__WEBPACK_IMPORTED_MODULE_2__.useEffect)(()=>{
        if (width < 1050 && width > 950) {
            setVisibleSlides(7);
        } else if (width < 835 && width > 650) {
            setVisibleSlides(5);
        } else if (width < 650) {
            setVisibleSlides(4);
        } else {
            setVisibleSlides(8);
        }
    }, [
        width
    ]);
    let { 0: categoryList2 , 1: setcategoryList2  } = (0,react__WEBPACK_IMPORTED_MODULE_2__.useState)([]);
    (0,react__WEBPACK_IMPORTED_MODULE_2__.useEffect)(()=>{
        axios__WEBPACK_IMPORTED_MODULE_4___default().get(`/flowers/general-category/list/${lang}`).then((res)=>{
            setcategoryList2(res.data);
        }).catch(()=>null
        );
    }, [
        lang
    ]);
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        className: "margin-top-25 margin-bottom-5 container section3_height",
        children: width > 650 ? /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
            // data-aos="zoom-in"  
            // data-aos-easing="ease-in-sine"
            // data-aos-delay='1400'
            className: "carousel_margin_top margin-bottom-5",
            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(Box, {
                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(Container, {
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(Carousel, {
                            leftButtonClass: "ml-lg-2",
                            leftButtonStyle: {
                                position: "absolute"
                            },
                            rightButtonStyle: {
                                position: "absolute"
                            },
                            showArrow: width < 1150 || categoryList2.length === 0 ? false : true,
                            totalSlides: categoryList2.length,
                            visibleSlides: visibleSlides,
                            children: categoryList2.length === 0 && !categoryList2 ? "" : categoryList2 === null || categoryList2 === void 0 ? void 0 : categoryList2.map((item)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    onClick: ()=>router.push(`/${item.keyword}`)
                                    ,
                                    className: "index_category_image_height",
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(ProductCard6, {
                                        title: item.title,
                                        subtitle: item.keyword,
                                        imgUrl: item.icon_b,
                                        s_imgUrl: item.icon_s
                                    })
                                }, item)
                            )
                        })
                    })
                })
            })
        }) : /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
            className: "carouselStyle ",
            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(Carousel, {
                showArrow: false,
                totalSlides: categoryList2.length,
                visibleSlides: visibleSlides,
                children: categoryList2.length === 0 && !categoryList2 ? "" : categoryList2 === null || categoryList2 === void 0 ? void 0 : categoryList2.map((item)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "cursor-pointer2",
                        onClick: ()=>router.push(`/${item.keyword}`)
                        ,
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(ProductCard6, {
                            title: item.title,
                            subtitle: item.keyword,
                            imgUrl: item.icon_b,
                            s_imgUrl: item.icon_s
                        })
                    }, item)
                )
            })
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Section3);


/***/ })

};
;