import {FormattedMessage, useIntl} from "react-intl";
import React, {useState} from "react";
import Box from "../components/Box";
import InputMask from "react-input-mask";
import DefaultDropzone from "../components/DefaultDropzone";
import axios from "axios";

let VacancyForm = ({id,close})=>{
    const [file,setfile] = useState([])
    const [name,setname] = useState("")
    const [phone,setphone] = useState("")
    const [msg,setmsg] = useState("")
    const [err,seterr] = useState("")
    let intl = useIntl()
    const handleSubmit = (event)=>{
        event.preventDefault()
        const data = new FormData()
        data.append("name",name)
        data.append("phone",phone)
        data.append("description",msg)
        data.append("file",file[0])
        data.append("vacancy_id",id)
        if(file?.length === 0 || typeof file === "undefined"){
            seterr(intl.formatMessage({id:"select_files"}))
        }
        else{
            axios({
                method:"POST",
                url:`/vacancy/store`,
                data:data
            })
                .then(()=>{
                    seterr(intl.formatMessage({id:"corporate_send_sucess"}))
                    setTimeout(()=>{
                        close(false)
                    },2000)
                })
                .catch(()=>seterr(intl.formatMessage({id:"error_booking"})))
        }

    }
    // @ts-ignore
    return (
        <Box>
            <div>
                <form onSubmit={handleSubmit} encType="multipart/form-data" className="row ml-0 pl-0">
                    <div className="col-md-12">
                        <div>
                            <FormattedMessage
                                id="full_name"
                                defaultMessage="Sizning smsingiz"
                            />
                        </div>
                        <input
                            className="form-control"
                            required={true}
                            value={name}
                            onChange={(r)=>setname(r.target.value)}
                        />

                    </div>
                    <div className="col-12">
                        <div className="mt-1">
                            <FormattedMessage
                                id="phone_number"
                                defaultMessage=""
                            />
                        </div>
                        <InputMask
                            mask="+\9\9\8-99-999-99-99"
                            onChange={(e)=>setphone(e.target.value)}

                        >
                            {()=>(
                                <input
                                    required={true}
                                    type="text"
                                    className="form-control"
                                    placeholder={"+998-__-___-__-__"}
                                    name="number"
                                />
                            )}
                        </InputMask>
                    </div>
                    <div className="col-md-12">
                        <div className="mt-1 mb-0">
                            <FormattedMessage
                                id="Your Message"
                                defaultMessage="Sizning xabaringiz"
                            />
                        </div>
                       <textarea
                           className="form-control"
                           onChange={(r)=>setmsg(r.target.value)}
                            required={true}
                       >

                       </textarea>

                    </div>
                    <div className="col-md-12 h-100" >
                        <div className="mt-1">
                            <FormattedMessage
                                id="Any Document"
                                defaultMessage="Biror Dokument"
                            />
                        </div>
                        <DefaultDropzone
                            onChange={(files:any) => setfile(files)}
                        >
                        </DefaultDropzone>
                        {file.length ===0 || typeof file === "undefined" ? "" : <div className="text-primary" >{file[0].name}</div>}
                        <br/>
                        <button className="btn btn-primary col-md-12 mt-3">
                            <FormattedMessage
                                id="submit"
                            />
                        </button>
                        {err === "" ? "" : <div className="text-danger" >{err}</div>}
                    </div>
                </form>
            </div>

        </Box>
    )
}
export default VacancyForm;
