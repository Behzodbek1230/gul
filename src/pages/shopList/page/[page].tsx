import FlexBox from "../../../components/FlexBox";
import Grid from "../../../components/grid/Grid";
import NavbarLayout from "../../../components/layout/NavbarLayout";
import ShopCard1 from "../../../components/shop/ShopCard1";
import { H1 } from "../../../components/Typography";
import React, {useEffect} from "react";
import axios from "axios";
import {useDispatch, useSelector} from "react-redux";
import {StyledPagination} from "../../../components/pagination/PaginationStyle";
import Button from "../../../components/buttons/Button";
import Icon from "../../../components/icon/Icon";
import ReactPaginate from "react-paginate";
import {useRouter} from "next/router";
import {FormattedMessage, useIntl} from "react-intl";
import { NextSeo } from "next-seo";
import get_shop_list from "../../../../Redux/Actions/get_shop_list";
const ShopList = () => {
    let router = useRouter()
    let intl = useIntl()
    let {page} = router.query;
    let dispatch = useDispatch()
    let wishlist = useSelector((state:any)=>state.new.shops)
    let lang = router.locale
    useEffect(() => {
        axios.get(`/shops/list/${lang}?page=${page}`)
            .then(response=>{
                dispatch(get_shop_list(response.data))
            })
            .catch(()=>{
               return null;
            })
    },[page]);
  return (
    <NavbarLayout>
    <div>
        <NextSeo
            title={intl.formatMessage({id:"footer_header_link3"})}
            description={intl.formatMessage({id:"footer_header_link3"})}
            additionalMetaTags={[{
            name: 'keyword',
            content: intl.formatMessage({id:"footer_header_link3"})
            }, 
        ]}
        />
      <H1 mb="24px">
          <FormattedMessage
            id="footer_header_link3"
            defaultMessage="Magazinlar"
          />
      </H1>

      <Grid container spacing={6}>
        {wishlist?.datas?.data?.map((item, ind) => (
          <Grid item lg={4} sm={6} xs={12} key={ind}>
            <ShopCard1 {...item} />
          </Grid>
        ))}
      </Grid>

      {wishlist?.datas?.last_page !==1 
        ?
        <FlexBox
        flexWrap="wrap"
        justifyContent="space-between"
        alignItems="center"
        mt="32px"
      >
          <StyledPagination>
              <ReactPaginate
                  previousLabel={
                      <Button
                          style={{cursor: "pointer"}}
                          className="control-button"
                          color="primary"
                          overflow="hidden"
                          height="auto"
                          padding="6px"
                          borderRadius="50%"
                      >
                          <Icon defaultcolor="currentColor" variant="small">
                              chevron-left
                          </Icon>
                      </Button>

                  }
                  nextLabel={
                      <Button
                          style={{cursor: "pointer"}}
                          className="control-button"
                          color="primary"
                          overflow="hidden"
                          height="auto"
                          padding="6px"
                          borderRadius="50%"
                      >
                          <Icon defaultcolor="currentColor" variant="small">
                              chevron-right
                          </Icon>
                      </Button>
                  }
                  breakLabel={
                      <Icon defaultcolor="currentColor" variant="small">
                          triple-dot
                      </Icon>
                  }
                  pageCount={wishlist?.lastPage || 0}
                  marginPagesDisplayed={true}
                  pageRangeDisplayed={false}
                  onPageChange={(r)=>{
                    let query2 = {...router.query}
                    query2.page = r.selected+1
                    router.push( {pathname:router.pathname,query:query2})
                   }
                  }
                  containerClassName="pagination"
                  subContainerClassName="pages pagination"
                  activeClassName="active"
                  disabledClassName="disabled"
              />
          </StyledPagination>
      </FlexBox>
      :
      ""  
    }
    </div>
    </NavbarLayout>
  );
};

export default ShopList;
