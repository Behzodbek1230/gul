import React from "react";
import Container from "../Container";
import AppLayout from "./AppLayout";


type Props = {
  title?: string;
  description?:string;
  keyword?:string;
  seoRelated?:any;
};

const NavbarLayout: React.FC<Props> = ({
   title,
   description,
   keyword,
   children,
   seoRelated
}) => {
  return (
    <AppLayout title={title} description={description} keyword={keyword} seoRelated={seoRelated} >
      <Container my="2rem">{children}</Container>
    </AppLayout>
  );
};

export default NavbarLayout;
