"use strict";
exports.id = 8167;
exports.ids = [8167];
exports.modules = {

/***/ 8167:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ grid_Grid)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "styled-components"
var external_styled_components_ = __webpack_require__(7518);
var external_styled_components_default = /*#__PURE__*/__webpack_require__.n(external_styled_components_);
// EXTERNAL MODULE: external "styled-system"
var external_styled_system_ = __webpack_require__(5834);
;// CONCATENATED MODULE: ./components/grid/GridStyle.tsx


const StyledGrid = external_styled_components_default().div(({ container , item , spacing , horizontal_spacing , vertical_spacing , xl , lg , md , sm , xs , containerHeight ,  })=>{
    let mediaProps = {
        xl,
        lg,
        md,
        sm,
        xs
    };
    let style = {
    };
    if (container) {
        style = {
            display: "flex",
            flexWrap: "wrap",
            height: containerHeight,
            margin: spacing ? `-${spacing / 2 * 0.25}rem` : "unset"
        };
        if (horizontal_spacing) {
            style.marginLeft = `-${horizontal_spacing / 2 * 0.25}rem`;
            style.marginRight = `-${horizontal_spacing / 2 * 0.25}rem`;
        }
        if (vertical_spacing) {
            style.marginTop = `-${vertical_spacing / 2 * 0.25}rem`;
            style.marginBottom = `-${vertical_spacing / 2 * 0.25}rem`;
        }
    } else if (item) {
        if (spacing) style.padding = `${spacing / 2 * 0.25}rem`;
        if (horizontal_spacing) {
            style.paddingLeft = `${horizontal_spacing / 2 * 0.25}rem`;
            style.paddingRight = `${horizontal_spacing / 2 * 0.25}rem`;
        }
        if (vertical_spacing) {
            style.paddingTop = `${vertical_spacing / 2 * 0.25}rem`;
            style.paddingBottom = `${vertical_spacing / 2 * 0.25}rem`;
        }
        for(const key in mediaSize){
            if (mediaProps[key]) {
                style = {
                    ...style,
                    [`@media only screen and (min-width: ${mediaSize[key]}px)`]: {
                        width: `${mediaProps[key] / 12 * 100}%`
                    }
                };
            }
        }
    }
    return style;
}, (0,external_styled_system_.compose)(external_styled_system_.flexbox));
const mediaSize = {
    xs: 0,
    sm: 426,
    md: 769,
    lg: 1025,
    xl: 1441
};
/* harmony default export */ const GridStyle = (StyledGrid);

;// CONCATENATED MODULE: ./components/grid/Grid.tsx



const Grid = ({ children , ...props })=>{
    let childList = children;
    if (props.container) {
        childList = external_react_.Children.map(children, (child)=>{
            return(/*#__PURE__*/ (0,external_react_.cloneElement)(child, {
                spacing: props.spacing,
                horizontal_spacing: props.horizontal_spacing,
                vertical_spacing: props.vertical_spacing
            }));
        });
    }
    return(/*#__PURE__*/ jsx_runtime_.jsx(GridStyle, {
        ...props,
        children: childList
    }));
};
Grid.defaultProps = {
    spacing: 0,
    containerHeight: "unset"
};
/* harmony default export */ const grid_Grid = (Grid);


/***/ })

};
;