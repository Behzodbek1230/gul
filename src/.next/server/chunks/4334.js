"use strict";
exports.id = 4334;
exports.ids = [4334];
exports.modules = {

/***/ 4334:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ layout_CheckoutNavLayout)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(1853);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: ./components/Container.tsx
var Container = __webpack_require__(9221);
// EXTERNAL MODULE: ./components/Box.tsx
var Box = __webpack_require__(4875);
// EXTERNAL MODULE: ./components/grid/Grid.tsx + 1 modules
var Grid = __webpack_require__(8167);
// EXTERNAL MODULE: ./components/Chip.tsx
var Chip = __webpack_require__(9778);
// EXTERNAL MODULE: ./components/FlexBox.tsx
var FlexBox = __webpack_require__(5359);
;// CONCATENATED MODULE: ./components/stepper/Stepper.tsx





const Stepper = ({ selectedStep , stepperList , onChange ,  })=>{
    const { 0: selected , 1: setSelected  } = (0,external_react_.useState)(selectedStep - 1);
    const handleStepClick = (step, ind)=>()=>{
            if (!step.disabled) {
                setSelected(ind);
                if (onChange) onChange(step, ind);
            }
        }
    ;
    (0,external_react_.useEffect)(()=>{
        setSelected(selectedStep - 1);
    }, [
        selectedStep
    ]);
    return(/*#__PURE__*/ jsx_runtime_.jsx(FlexBox["default"], {
        alignItems: "center",
        flexWrap: "nowrap",
        justifyContent: "center",
        my: "-4px",
        children: stepperList.map((step, ind)=>/*#__PURE__*/ (0,jsx_runtime_.jsxs)(external_react_.Fragment, {
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx(Chip/* Chip */.A, {
                        bg: ind <= selected ? "primary.main" : "primary.light",
                        color: ind <= selected ? "white" : "primary.main",
                        p: "0.5rem 1.5rem",
                        fontSize: "14px",
                        fontWeight: "600",
                        my: "4px",
                        cursor: step.disabled ? "grab" : "pointer",
                        onClick: handleStepClick(step, ind),
                        children: step.title
                    }),
                    ind < stepperList.length - 1 && /*#__PURE__*/ jsx_runtime_.jsx(Box["default"], {
                        width: "50px",
                        height: "4px",
                        bg: ind < selected ? "primary.main" : "primary.light"
                    })
                ]
            }, step.title)
        )
    }));
};
Stepper.defaultProps = {
    selectedStep: 1
};
/* harmony default export */ const stepper_Stepper = (Stepper);

// EXTERNAL MODULE: ./components/layout/AppLayout.tsx + 12 modules
var AppLayout = __webpack_require__(7198);
// EXTERNAL MODULE: external "react-intl"
var external_react_intl_ = __webpack_require__(3126);
;// CONCATENATED MODULE: ./components/layout/CheckoutNavLayout.tsx









const CheckoutNavLayout = ({ children , title , description , keyword  })=>{
    const { 0: selectedStep , 1: setSelectedStep  } = (0,external_react_.useState)(0);
    const router = (0,router_.useRouter)();
    const { pathname  } = router;
    let intl = (0,external_react_intl_.useIntl)();
    const stepperList = [
        {
            title: intl.formatMessage({
                id: "cart",
                defaultMessage: "Savatcha"
            }),
            disabled: true
        },
        {
            title: intl.formatMessage({
                id: "Details",
                defaultMessage: "Details"
            }),
            disabled: true
        },
        {
            title: intl.formatMessage({
                id: "payment"
            }),
            disabled: true
        }, 
    ];
    const handleStepChange = (_step, ind)=>{
        switch(ind){
            case 0:
                router.push("/cart");
                break;
            case 1:
                router.push("/cart");
                break;
            case 2:
                router.push("/review");
                break;
            case 3:
                router.push("/orders");
                break;
            default:
                break;
        }
    };
    (0,external_react_.useEffect)(()=>{
        if (pathname.startsWith("/cart")) {
            setSelectedStep(1);
            setSelectedStep(2);
        } else if (pathname.startsWith("/review")) {
            setSelectedStep(3);
        }
    }, [
        pathname
    ]);
    return(/*#__PURE__*/ jsx_runtime_.jsx(AppLayout["default"], {
        title: title,
        description: description,
        keyword: keyword,
        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)(Container["default"], {
            my: "2rem",
            children: [
                /*#__PURE__*/ jsx_runtime_.jsx(Box["default"], {
                    mb: "14px",
                    children: /*#__PURE__*/ jsx_runtime_.jsx(Grid["default"], {
                        container: true,
                        spacing: 6,
                        children: /*#__PURE__*/ jsx_runtime_.jsx(Grid["default"], {
                            item: true,
                            lg: 12,
                            md: 12,
                            xs: 12,
                            children: /*#__PURE__*/ jsx_runtime_.jsx(stepper_Stepper, {
                                stepperList: stepperList,
                                selectedStep: selectedStep,
                                onChange: handleStepChange
                            })
                        })
                    })
                }),
                children
            ]
        })
    }));
};
/* harmony default export */ const layout_CheckoutNavLayout = (CheckoutNavLayout);


/***/ })

};
;