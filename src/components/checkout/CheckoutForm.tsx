import { useRouter } from "next/router";
import React, { useState} from "react";
import Button from "../buttons/Button";
import Grid from "../grid/Grid";
import InputMask from "react-input-mask";
import Typography from "../Typography";
import {useDispatch, useSelector} from "react-redux";
import "bootstrap/dist/css/bootstrap.min.css"
import Map3 from "../map3";
import Cookies from "js-cookie"
import axios from "axios";
import {useAppContext} from "../../contexts/app/AppContext";
import get_cart_products from "../../../Redux/Actions/get_cart_products";
import get_order_id from "../../../Redux/Actions/get_order_id";
import {FormattedMessage, useIntl} from "react-intl";
import get_current_product from "../../../Redux/Actions/get_current_product";
import Postcard from "../../components/postcard";
import { Card1 } from "../../components/Card1";
import TextField from '@mui/material/TextField';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import { StaticDateTimePicker } from "@mui/lab";
import { Dialog, DialogActions, DialogContent } from "@material-ui/core";
import { addMinutes } from "date-fns";

export interface CheckoutFormProps {
    postcard_visible?:boolean,
    deliveryTime?:any,
    beginDeliveryTime?: any;
    endDeliveryTime?:any;
  }


const CheckoutForm:React.FC<CheckoutFormProps> = ({
    postcard_visible,
    deliveryTime,
    endDeliveryTime,
    beginDeliveryTime
})=>{
  const router = useRouter();
  let intl = useIntl()
  let current_product = useSelector((state:any)=>state.token.current_product)
  let postcard = useSelector((state:any)=>state.token.postcard_text)
  const user = useSelector((state:any)=>state.token.user)
  const [name,setname] = useState(user?.data?.fullname)
  const [phone,setphone] = useState(user?.data?.phone)
  const [coordinates,setcoordinates] = useState(null)
  const [message,setmessage] = useState("")
  const [cash_or_card,setpayment] = useState("Naqd")
  const [date,setdata] = useState(undefined)
  const [recipent_me,setrecipent_me] = useState(1)
  const [recipent_name,setrecipent_name] = useState('')
  const [recipent_phone,setrecipent_phone] = useState('')
  let [address,setaddress] = useState("")
  const dispatch2 = useDispatch()
  let [open2,setopen2] = useState(false)
  let [instructions,setinstructions] = useState("")
  // Changing cart state
    const { state, dispatch } = useAppContext();
    const { cartList } = state.cart;
    const handleCartAmountChange2 = (amount,product) =>  {
        dispatch({
            type: "CHANGE_CART_AMOUNT",
            payload: {
                ...product,
                qty: amount,
            },
        });
    }
    const handleCartAmountChange = (amount,product) =>  {
        dispatch({
            type: "CHANGE_CART_AMOUNT",
            payload: {
                ...product,
                imgUrl:product.image,
                id:product.keyword,
                qty: amount,
            },
        });
    }
  const handleFormSubmit = async (values) => {
      values.preventDefault()
      let lang = Cookies.get("lang")
      const data = new FormData()

      data.append('is_order', '1');
      data.append("cash_or_card",cash_or_card)
      data.append("delivery_time",date?.toLocaleDateString("ru-Ru",{day:"numeric",month:"numeric",year:"numeric",hour:"numeric",minute:"numeric"}))
      data.append("address",address)
      data.append("card",postcard)
      data.append("keyword",current_product)
      data.append('name', name);
      data.append('phone', phone);
      data.append("instructions",instructions)
      data.append('coordinate_x', coordinates && coordinates.length >=1  ? coordinates[0] : "");
      data.append('coordinate_y', coordinates && coordinates.length >=1 ? coordinates[1] : "");
      data.append('recipent',recipent_me.toString())
      if(recipent_me==2){
          data.append('recipent_name',recipent_name)
          data.append('recipent_phone',recipent_phone)
      }
      if( typeof coordinates === "undefined" || !coordinates || coordinates === null || coordinates?.length  === 0 ){
         setmessage(intl.formatMessage({id:"choose_delivery_place"}))
      }
      else if(typeof date === "undefined"){
        setmessage(intl.formatMessage({id:"choose_delivery_time"}))
      }
      else if(Cookies.get("isLoggedIn") === "true"){
        let url = current_product === "" ? `/orders/set-orders/${lang}` : `/orders/buy-now/ru`
            axios( {
                method: 'POST',
                url: url,
                data : data
            })
                .then((res)=>{
                    if(res?.data?.errors === "true" || res?.data?.errors === true) {
                        return null;
                    }
                    else{
                        setmessage(intl.formatMessage({id:"successfully_booked"}))
                        dispatch2(get_order_id(res.data))
                        //Checking cart
                        let lang4
                        const lang2 = router.locale|| "uz";
                        if(typeof lang2 !== "undefined"){
                            lang4 = lang2
                        }
                        else{
                            lang4 = "uz"
                        }
                        if(Cookies.get("isLoggedIn") === "true" && current_product === ""){
                            axios(`/orders/basket-list/${lang4}`)
                                .then(response =>{
                                    try{
                                        if(current_product === ""){
                                            cartList.forEach(product=>handleCartAmountChange2(0,product))
                                            dispatch2(get_cart_products(response.data));
                                            response.data.products.map(product=>handleCartAmountChange(product.count,product))
                                        }
                                    }
                                    catch{
                                        return null;
                                    }
                                })
                                .catch(()=>null)
                        }
                        dispatch2(get_current_product(""))
                        router.push("/review")
                    }
                })
                .catch(()=>{
                    return null;
            })

    }
    else{
        if(current_product === "") {
            cartList.forEach((value, index) => {
                data.append(`products[${index}][keyword]`, value.id);
                data.append(`products[${index}][count]`, value.qty);
            })
        }
        else{
            data.append("keyword",current_product)
        }
        let url = current_product === "" ? `/orders/set-orders/${lang}` : `/orders/buy-now/${lang}`
        axios( {
            method: 'POST',
            url: url,
            headers: {
                'Accept': 'application/json',
            },
            data : data
        })
            .then((res)=>{
                if(res?.data?.errors){
                    setmessage(intl.formatMessage({id:"error_booking"}))

                }else{
                    dispatch2(get_order_id(res?.data))
                    setmessage(intl.formatMessage({id:"successfully_booked"}))
                    if(current_product === ""){
                        cartList.forEach(product=>handleCartAmountChange2(0,product))
                    }
                    dispatch2(get_current_product(""))
                    router.push("/review")
                }
            })
            .catch(()=>null)
    }
  };

  return (

        <form onSubmit={handleFormSubmit}>
          <Card1 mb="2rem" className="shadow-none">
            <div className="col-12 mx-0 px-0">
                <Typography className="fw-bold h6" fontWeight="600" mb="1rem">
                    <FormattedMessage id="Order Details" defaultMessage="Buyurtma tafsilotlari " />
                </Typography>
            </div>
            <div className="col-md-12 mx-0 px-0">
                <div className="row">
                    <div className="col-md-6 col-12 col-sm-12 col-lg-6 col-xl-6">
                      <label>
                          <FormattedMessage
                              id="Name"
                              defaultMessage="Ismingiz"
                          />
                      </label>
                      <input
                          required={true}
                          type="text"
                          value={name}
                          className="form-control"
                          onChange={(e)=>setname(e.target.value)}
                      />
                    </div>
                  <div className="col-md-6 col-12 col-sm-12 col-lg-6 col-xl-6 mt-3 mt-sm-3 mt-md-0 mt-lg-0 mt-xl-0">
                    <label htmlFor="number">
                        <FormattedMessage id="phone_number" defaultMessage="Telefon Nomer" />
                    </label>
                  
                    <InputMask
                        mask="+\9\9\8-99-999-99-99"
                        onChange={(e)=>setphone(e.target.value)}
                        value={phone}
                        required={true}
                    >
                      {()=>(
                          <input
                              type="text"
                              required={true}
                              className="form-control"
                              placeholder={"+998-xx-xxx-xx-xx"}
                              name="number"
                          />
                      )}
                    </InputMask><br/>
                  </div>
                    <div className="col-md-12">
                        <label>
                            <FormattedMessage
                                id="Delivery Time"
                                defaultMessage="Yetkazib berish vaqti"
                            />
                        </label>
                
                        <input 
                            type="datetime" 
                            placeholder={intl.formatMessage({id:"choose_delivery_time"})}
                            value={typeof date === "undefined" ? "" : new Date(date)?.toLocaleDateString("ru-Ru",{day:"numeric",month:"numeric",year:"numeric",hour:"numeric",minute:"numeric"})}
                            className="form-control" 
                            readOnly={true} 
                            onClick={()=>setopen2(!open2)}
                        />
                        <Dialog
                            open={open2}
                            onClose={()=>setopen2(false)}
                            aria-labelledby="alert-dialog-title"
                            aria-describedby="alert-dialog-description"
                        >
                            <DialogContent>
                                <LocalizationProvider dateAdapter={AdapterDateFns}>
                                    <StaticDateTimePicker
                                        ampm={false}
                                        minDate={addMinutes(Date.now(),parseInt(deliveryTime))}
                                        minTime={new Date(0,0,0,parseInt(beginDeliveryTime?.split(":")[0]),parseInt(beginDeliveryTime?.split(":")[1]))}
                                        maxTime={new Date(0,0,0,parseInt(endDeliveryTime?.split(":")[0]),parseInt(endDeliveryTime?.split(":")[1]))}
                                        mask="__/__/____ __:__"
                                        displayStaticWrapperAs="desktop"
                                        disableCloseOnSelect={true}
                                        openTo="day"
                                        value={date}
                                        inputFormat="dd/mm/yyyy hh:ss"
                                        onChange={(newValue)=>setdata(newValue)}
                                        renderInput={(params) => <TextField {...params} />}
                                        disableIgnoringDatePartForTimeValidation={false}
                                    />
                                </LocalizationProvider>
                            </DialogContent>
                            <DialogActions>
                            <Button onClick={()=>setopen2(false)} autoFocus>
                                Ok
                            </Button>
                            </DialogActions>
                        </Dialog>
                    </div>
                    <div className="col-md-12 col-12 col-sm-12  col-lg-12  col-xl-12 py-2 mt-2">
                        <div className="row">
                            <div className="col-12 fw-bold h6">
                                <FormattedMessage
                                    id="payment_methods"
                                    defaultMessage="Tolov Turi"
                                />
                            </div>
                            <div className="col-6">
                                <div className="form-check">
                                    <label className="form-check-label" htmlFor="check1">
                                        <input
                                            checked={cash_or_card !== "Plastik" ? true : false}
                                            type="radio"
                                            required={true}
                                            onClick={()=>setpayment("Naqd")}
                                            className="form-check-input"
                                            id="check1"
                                            name="option2"
                                            value="Naqd"
                                        />
                                        <FormattedMessage
                                            id="Cash"
                                            defaultMessage="Naqd Pul"
                                        />
                                    </label>
                                </div>

                            </div>
                            <div className="col-6">
                                <div className="form-check">
                                    <label className="form-check-label" htmlFor="check2">
                                        <input
                                            type="radio"
                                            className="form-check-input"
                                            onClick={()=>setpayment("Plastik")}
                                            id="check2"
                                            name="option2"
                                        />
                                        <FormattedMessage
                                            id="Cards"
                                            defaultMessage="Plastik Karta"
                                        />
                                    </label>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div className="col-12">
                        <label><FormattedMessage id="special_instruction" /></label>
                        <textarea value={instructions} onChange={(e)=>setinstructions(e.target.value)} name="instructions" id="" className="form-control" cols={10} rows={3}></textarea>
                    </div>
                   {
                       postcard_visible 
                       ? 
                            <div className="col-12 mb-3 mt-3">
                                <Postcard />
                            </div>
                        :
                        ""
                   }
                   <div className={`col-12 ${!postcard_visible && 'pt-2'}`}>
                       <div className="row">
                           <div className="col-12 mb-1 h6">
                               <FormattedMessage
                                id="recipent"
                               />
                           </div>
                           <div className="col-6">
                            <div className='form-check'>
                                <label htmlFor="receiver1">
                                    <input
                                        checked={recipent_me==1 && true}
                                        onClick={()=>setrecipent_me(1)}
                                        id='receiver1'
                                        type="radio"
                                        className="form-check-input"
                                        value='1'
                                    />
                                    <FormattedMessage
                                        id="me"
                                    />
                                </label>
                            </div>
                           </div>
                           <div className="col-6">
                               <div className='form-check'>
                                   <label htmlFor='receiver2'>
                                       <input
                                           checked={recipent_me==2 && true}
                                           id="receiver2"
                                           onChange={()=>setrecipent_me(2)}
                                           type="radio"
                                           className="form-check-input"
                                           value='1'
                                       />
                                       <FormattedMessage
                                           id="other"
                                       />
                                   </label>
                               </div>
                           </div>
                       </div>
                   </div>
                    {recipent_me ==2 &&
                        <div className="col-12 pb-2">
                            <div className="row">
                                <div className="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <label>
                                        <FormattedMessage
                                            id="recipent_name"
                                            defaultMessage="Qabul qiluvchi ismi"
                                        />
                                    </label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        placeholder={intl.formatMessage({id:"recipent_name"})}
                                        value={recipent_name}
                                        required={recipent_me == 2 && true}
                                        onChange={(e)=>setrecipent_name(e.target.value)}
                                    />
                                </div>
                                <div className="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <label>
                                        <FormattedMessage
                                            id="recipent_phone"
                                            defaultMessage="Qabul qiluvchi telefon raqami"
                                        />
                                    </label>
                                    <InputMask
                                        mask="+\9\9\8-99-999-99-99"
                                        onChange={(e)=>setrecipent_phone(e.target.value)}
                                        value={recipent_phone}
                                        required={recipent_me == 2 && true}
                                    >
                                        {()=>(
                                            <input
                                                type="text"
                                                required={recipent_me == 2 && true}
                                                className="form-control"
                                                placeholder={"+998-xx-xxx-xx-xx"}
                                                name="number"
                                            />
                                        )}
                                    </InputMask><br/>
                                </div>
                            </div>
                        </div>
                    }

                    <div className="col-md-12 col-12 col-sm-12  col-lg-12  col-xl-12 ">
                        <label>
                            <FormattedMessage
                                id="your_address"
                                defaultMessage="Sizning Addresingiz"
                            />
                        </label>
                        <input
                            type="text"
                            className="form-control"
                            placeholder={intl.formatMessage({id:"address"})}
                            value={address}
                            required={true}
                            onChange={(e)=>setaddress(e.target.value)}
                        />
                    </div>
                    <div className="col-md-12 col-12 col-sm-12  col-lg-12  col-xl-12 pt-2 ">
                        <label>
                            <FormattedMessage
                                id="your_address_map"
                                defaultMessage="Sizning manzilingiz(kartadan belgilang)"
                            />
                        </label>
                        <Map3 set={(e)=>setcoordinates(e)} coordinate={coordinates}/>
                    </div>

                  </div>
                </div>
          </Card1>

            <Grid container spacing={7}>
                <Grid item sm={12} xs={12}>
                    <Button
                        variant="contained"
                        color="primary"
                        type="submit"
                        fullwidth
                    >
                        <FormattedMessage
                            id="book"
                            defaultMessage="Xarid qilish"
                        />
                    </Button>
                    {message === "" ? "" : <div className="text-danger"> {message}</div>}
                </Grid>
            </Grid>
        </form>
  );
};

export default CheckoutForm;
