
const ProductCard6 = dynamic(()=>import('../product-cards/ProductCard6'),{ssr:false})
const Carousel = dynamic(()=>import('../../components/carousel/Carousel'),{ssr:false})
const Container = dynamic(()=>import('../../components/Container'),{ssr:false})
const Box = dynamic(()=>import('../../components/Box'),{ssr:false})
import useWindowSize from "../../hooks/useWindowSize";
import React, { useEffect, useState } from "react";
import {useRouter} from "next/router";
import axios from "axios";
import dynamic from "next/dynamic";

const Section3: React.FC = () => {
  const width = useWindowSize();
  let router= useRouter()
  const [visibleSlides, setVisibleSlides] = useState(0);
  let lang = router.locale
  useEffect(()=>{
    if(width<1050&& width>950){
      setVisibleSlides(7)
    }
    else if(width< 835 && width  > 650){
      setVisibleSlides(5)
    }

    else if(width < 650 )
    {
      setVisibleSlides(4)
    }
    else{
      setVisibleSlides(8)
    }
  },[width])
  let [categoryList2,setcategoryList2] = useState([])
  useEffect(()=>{
      axios.get(`/flowers/general-category/list/${lang}`)
        .then(res=>{
          setcategoryList2(res.data)
        })
        .catch(()=>null)
  },[lang])

  return (
        <div
            className="margin-top-25 margin-bottom-5 container section3_height"
            
        >
            {width > 650 
            ?
            (<div
              // data-aos="zoom-in"  
              // data-aos-easing="ease-in-sine"
              // data-aos-delay='1400'
              className="carousel_margin_top margin-bottom-5"
           >
                <div>
                  <Box>
                  <Container>
                   <Carousel
                       leftButtonClass="ml-lg-2"
                       leftButtonStyle={{position:"absolute"}}
                       rightButtonStyle={{position:"absolute"}}
                       showArrow={width<1150 || categoryList2.length === 0  ? false : true}
                       totalSlides={categoryList2.length}
                       visibleSlides={visibleSlides}
                   >
                     {categoryList2.length === 0 && !categoryList2  ?
                           ""
                         :
                           categoryList2?.map((item) => (
                                <div
                                  onClick={()=>router.push(`/${item.keyword}`)}
                                  key={item}
                                  className='index_category_image_height'
                                >
                                    <ProductCard6
                                      title={item.title}
                                      subtitle={item.keyword}
                                      imgUrl={item.icon_b}
                                      s_imgUrl={item.icon_s}
                                    />
                                  
                                </div>
                           ))
                     }
                   </Carousel>
                  </Container>
                  </Box>
                </div>
               </div>)
            :
            <div
              className="carouselStyle "
            >
              <Carousel
                  showArrow={false}
                  totalSlides={categoryList2.length}
                  visibleSlides={visibleSlides}
              >
                {categoryList2.length === 0 && !categoryList2 ?
                    ""
                    :
                      categoryList2?.map(item => (
                      <div
                            className="cursor-pointer2"
                            onClick={()=>router.push(`/${item.keyword}`)}
                            key={item}
                      >
                          <ProductCard6
                              title={item.title}
                              subtitle={item.keyword}
                              imgUrl={item.icon_b}
                              s_imgUrl={item.icon_s}
                          />
                      </div>
                      ))
                }
              </Carousel>
            </div>
            }
        </div>
  );
};



export default Section3;
