"use strict";
exports.id = 2597;
exports.ids = [2597];
exports.modules = {

/***/ 9778:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "A": () => (/* binding */ Chip),
/* harmony export */   "b": () => (/* binding */ ChipwithoutRounded)
/* harmony export */ });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5834);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_system__WEBPACK_IMPORTED_MODULE_1__);


const Chip = (styled_components__WEBPACK_IMPORTED_MODULE_0___default().div)`
  display: inline-flex;
  cursor: ${(props)=>props.cursor || "unset"
};
  box-shadow: ${(props)=>props.boxShadow || "unset"
};
  transition: all 150ms ease-in-out;
  border-radius:15px;
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.space}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.color}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.position}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.typography}
`;
const ChipwithoutRounded = (styled_components__WEBPACK_IMPORTED_MODULE_0___default().div)`
  display: inline-flex;
  cursor: ${(props)=>props.cursor || "unset"
};
  box-shadow: ${(props)=>props.boxShadow || "unset"
};
  transition: all 150ms ease-in-out;
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.space}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.color}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.position}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.typography}
`;


/***/ }),

/***/ 2597:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ mobile_navigation_MobileNavigationBar)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: ./components/Chip.tsx
var Chip = __webpack_require__(9778);
// EXTERNAL MODULE: ./contexts/app/AppContext.tsx + 4 modules
var AppContext = __webpack_require__(8936);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: ./hooks/useWindowSize.js
var useWindowSize = __webpack_require__(12);
// EXTERNAL MODULE: ./components/icon/Icon.tsx + 1 modules
var Icon = __webpack_require__(4154);
// EXTERNAL MODULE: ./components/nav-link/NavLink.tsx + 1 modules
var NavLink = __webpack_require__(612);
// EXTERNAL MODULE: external "styled-components"
var external_styled_components_ = __webpack_require__(7518);
var external_styled_components_default = /*#__PURE__*/__webpack_require__.n(external_styled_components_);
// EXTERNAL MODULE: ./utils/constants.ts
var constants = __webpack_require__(9758);
// EXTERNAL MODULE: ./utils/utils.ts
var utils = __webpack_require__(1972);
;// CONCATENATED MODULE: ./components/mobile-navigation/MobileNavigationBar.style.tsx



const StyledMobileNavigationBar = (external_styled_components_default()).div`
  display: none;
  position: fixed;
  bottom: 0;
  left: 0;
  right: 0;
  height: ${constants/* layoutConstant.mobileNavHeight */.P.mobileNavHeight};
  justify-content: space-around;
  background: ${(0,utils/* getTheme */.gh)("colors.body.paper")};
  box-shadow: 0px 1px 4px 3px rgba(0, 0, 0, 0.1);
  z-index: 999;

  .link {
    flex: 1 1 0;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    font-size: 13px;

    .icon {
      margin-bottom: 4px;
      display: flex;
      justify-content: center;
      align-items: center;
    }
  }

  @media only screen and (max-width: 900px) {
    display: flex;
    width: 100vw;
  }
`;
/* harmony default export */ const MobileNavigationBar_style = (StyledMobileNavigationBar);

// EXTERNAL MODULE: external "js-cookie"
var external_js_cookie_ = __webpack_require__(6734);
var external_js_cookie_default = /*#__PURE__*/__webpack_require__.n(external_js_cookie_);
// EXTERNAL MODULE: external "react-intl"
var external_react_intl_ = __webpack_require__(3126);
// EXTERNAL MODULE: external "react-redux"
var external_react_redux_ = __webpack_require__(6022);
;// CONCATENATED MODULE: ./components/mobile-navigation/MobileNavigationBar.tsx












const MobileNavigationBar = ()=>{
    var ref;
    const width = (0,useWindowSize/* default */.Z)();
    const { state: state1  } = (0,AppContext/* useAppContext */.bp)();
    const { cartList  } = state1.cart;
    const href2 = external_js_cookie_default().get("isLoggedIn") === "true" ? "/profile/edit" : "/register";
    let intl = (0,external_react_intl_.useIntl)();
    let wishlist = (0,external_react_redux_.useSelector)((state)=>state.new.wishlist
    );
    let isloggedin = external_js_cookie_default().get("isLoggedIn");
    let user2 = (0,external_react_redux_.useSelector)((state)=>state.token.user
    );
    const list = [
        {
            title: intl.formatMessage({
                id: "mobile_navigation_home"
            }),
            icon: "home",
            href: "/"
        },
        {
            title: intl.formatMessage({
                id: "mobile_navigation_category2"
            }),
            icon: "category",
            href: "/mobile-category-nav"
        },
        {
            title: intl.formatMessage({
                id: "cart"
            }),
            icon: "cart",
            href: "/mobile-cart"
        }, 
    ];
    const wishlisthandle = /*#__PURE__*/ (0,jsx_runtime_.jsxs)(NavLink["default"], {
        className: "link",
        href: "/wish-list",
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx(Icon["default"], {
                className: "icon",
                variant: "small",
                children: "heart"
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                id: "wishlist",
                defaultMessage: "Sevimlilar"
            }),
            isloggedin === "true" && /*#__PURE__*/ jsx_runtime_.jsx(Chip/* Chip */.A, {
                bg: "primary.main",
                position: "absolute",
                color: "primary.text",
                fontWeight: "600",
                px: "0.25rem",
                top: "4px",
                "data-test": "mobile_navigation_authenticated_wishlist_count",
                left: "calc(50% + 8px)",
                className: "rounded-circle",
                children: user2 === null || user2 === void 0 ? void 0 : (ref = user2.data) === null || ref === void 0 ? void 0 : ref.favoritesCount
            }),
            isloggedin !== "true" && /*#__PURE__*/ jsx_runtime_.jsx(Chip/* Chip */.A, {
                bg: "primary.main",
                position: "absolute",
                color: "primary.text",
                fontWeight: "600",
                px: "0.25rem",
                top: "4px",
                "data-test": "mobile_navigation_unauthenticated_wishlist_count",
                left: "calc(50% + 8px)",
                className: "rounded-circle",
                children: wishlist.length
            })
        ]
    }, "Wishlist");
    return width <= 900 && /*#__PURE__*/ (0,jsx_runtime_.jsxs)(MobileNavigationBar_style, {
        children: [
            list.map((item)=>/*#__PURE__*/ (0,jsx_runtime_.jsxs)(NavLink["default"], {
                    className: "link",
                    href: item.href,
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx(Icon["default"], {
                            className: "icon",
                            variant: "small",
                            children: item.icon
                        }),
                        item.title,
                        item.icon === "cart" && !!cartList.length && /*#__PURE__*/ jsx_runtime_.jsx(Chip/* Chip */.A, {
                            bg: "primary.main",
                            position: "absolute",
                            color: "primary.text",
                            fontWeight: "600",
                            px: "0.25rem",
                            top: "4px",
                            left: "calc(50% + 8px)",
                            className: "rounded-circle",
                            children: cartList.length
                        })
                    ]
                }, item.title)
            ),
            wishlisthandle,
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(NavLink["default"], {
                className: "link",
                href: href2,
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx(Icon["default"], {
                        className: "icon",
                        variant: "small",
                        children: "user-2"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                        id: "mobile_navigation_account",
                        defaultMessage: "Akkount"
                    })
                ]
            }, "Account")
        ]
    });
};
/* harmony default export */ const mobile_navigation_MobileNavigationBar = (MobileNavigationBar);


/***/ })

};
;