import Avatar from "../../components/avatar/Avatar";
import Box from "../../components/Box";
import Card from "../../components/Card";
import Divider from "../../components/Divider";
import FlexBox from "../../components/FlexBox";
import Grid from "../../components/grid/Grid";
import Icon from "../../components/icon/Icon";
import DashboardLayout from "../../components/layout/CustomerDashboardLayout";
import DashboardPageHeader from "../../components/layout/DashboardPageHeader";
import TableRow from "../../components/TableRow";
import Typography, { H5, H6, Paragraph } from "../../components/Typography";
import useWindowSize from "../../hooks/useWindowSize";
import React, { useEffect, useState} from "react";
import axios from "axios";
import {FormattedMessage, useIntl} from "react-intl";
import Cookies from "js-cookie";
import {useRouter} from "next/router";
import {Step,ProgressBar} from "react-step-progress-bar"
import { NextSeo } from "next-seo";

const OrderDetails = () => {
  let [o_info,setinfo] = useState(null)
  let intl = useIntl()
  let width = useWindowSize()
  let router = useRouter()
  const orderStatus = o_info?.order?.status-1
  let {id} = router.query
  let lang = router.locale
  const breakpoint = 350;
  useEffect(()=>{
    let currency_id = Cookies.get("currency_id")
    let currency_text = typeof currency_id === "undefined" ? "" : `&currency=${currency_id}`
      axios(`/orders/order-details/${lang}?order_id=${id}${currency_text}`)
          .then(res=>{
            setinfo(res.data)
          })
          .catch(()=>router.push('/404'))
  },[Cookies.get("currency_id"),lang])
  return (
    <DashboardLayout>
      <NextSeo
       title={intl.formatMessage({id:"Order Details"})} 
      />
    <div>
      <DashboardPageHeader
        title={intl.formatMessage({id:"Order Details"})}
        iconName="bag_filled"
      />

      <Card p="2rem 1.5rem" mb="30px" >
        <div className="padding-y-30 margin_bottom_50">
          <ProgressBar percent={orderStatus*50} filledBackground="linear-gradient(to right, pink, red)">
          <Step>
            {({ accomplished }) => (
              <div className={`indexedStep `}>
                <div className={`indexedStep1 step1_order ${accomplished ? "accomplished" : ""}  `}>
                  <Icon size="32px" defaultcolor="currentColor">
                        package-box
                  </Icon>
                </div>
                <div className="order_status_text">
                {intl.formatMessage({id:"packaging"})}
                </div>
              </div>
            )}
          </Step>
          <Step>
            {({ accomplished }) => (
              <div className={`indexedStep`}>
              <div className={`indexedStep1  ${accomplished ? "accomplished" : ""}  `}>
                <Icon size="32px" defaultcolor="currentColor">
                      truck-1
                </Icon>
              </div>
              <div className="order_status_text">
              {intl.formatMessage({id:"delivering"})}
              </div>
            </div>
            )}
          </Step>
          <Step>
            {({ accomplished }) => (
              <div className={`indexedStep `}>
              <div className={`indexedStep1  ${accomplished ? "accomplished" : ""}  `}>
                <Icon size="32px" defaultcolor="currentColor">
                      delivery
                </Icon>
              </div>
              <div className="order_status_text" >
              {intl.formatMessage({id:"delivered"})}
              </div>
            </div>
            )}
          </Step>
        </ProgressBar>
        </div>
        <FlexBox justifyContent={width < breakpoint ? "center" : "flex-end"}>
          <Typography
            p="0.5rem 1rem"
            borderRadius="300px"
            bg="primary.light"
            color="primary.main"
            textAlign="center"
            style={{width:"350px"}}
          >
            <FormattedMessage id="estimated_delivery" />: <b>{o_info?.order?.delivery_time}</b>
          </Typography>
        </FlexBox>
      </Card>

      <Card p="0px" mb="30px" overflow="hidden">
        <TableRow bg="gray.200" p="12px" boxShadow="none" borderRadius={0}>
          <FlexBox className="pre" m="6px" alignItems="center">
            <Typography fontSize="14px" color="text.muted" mr="4px">
              {intl.formatMessage({id:"Order ID"})}:
            </Typography>
            <Typography fontSize="14px">{o_info?.order?.orderId}</Typography>
          </FlexBox>
        </TableRow>

        <Box py="0.5rem">
          {o_info?.datas?.map((item,ind) => (
            <FlexBox
              px="1rem"
              py="0.5rem"
              flexWrap="wrap"
              alignItems="center"
              key={ind}
            >
              <FlexBox flex="2 2 260px" m="6px" alignItems="center">
                <Avatar src={item.image} size={64} />
                <Box ml="20px">
                  <H6 my="0px">{item.productName}</H6>
                  <Typography fontSize="14px" color="text.muted">
                    {item.price}  x {item.count} = {item.totalPrice}
                  </Typography>
                </Box>
              </FlexBox>
              <FlexBox flex="1 1 260px" m="6px" alignItems="center">
                <Typography fontSize="14px" color="text.muted">

                </Typography>
              </FlexBox>
              <FlexBox flex="160px" m="6px" alignItems="center">
              </FlexBox>
            </FlexBox>
          ))}
        </Box>
      </Card>

      <Grid container spacing={6}>
        <Grid item lg={6} md={6} xs={12}>
          <Card p="20px 30px">
            <H5 mt="0px" mb="14px">
              <FormattedMessage id="address" />
            </H5>
            <Paragraph fontSize="14px" my="0px">
              {o_info?.order?.address}
            </Paragraph>
          </Card>
        </Grid>
        <Grid item lg={6} md={6} xs={12}>
          <Card p="20px 30px">
            <H5 mt="0px" mb="14px">
              <FormattedMessage id="total_summary" />
            </H5>
            <FlexBox
              justifyContent="space-between"
              alignItems="center"
              mb="0.5rem"
            >
              <Typography fontSize="14px" color="text.hint">
                <FormattedMessage id="Subtotal"/>:
              </Typography>
              <H6 my="0px">{o_info?.order?.product_price} {o_info?.order?.currencyName}</H6>
            </FlexBox>
            <FlexBox
              justifyContent="space-between"
              alignItems="center"
              mb="0.5rem"
            >
              <Typography fontSize="14px" color="text.hint">
                <FormattedMessage id="delivery_price" />:
              </Typography>
              <H6 my="0px">{o_info?.order?.delivery_price} {o_info?.order?.currencyName}</H6>
            </FlexBox>
            <FlexBox
              justifyContent="space-between"
              alignItems="center"
              mb="0.5rem"
            >
              <Typography fontSize="14px" color="text.hint">
                <FormattedMessage id="Discount" />:
              </Typography>
              <H6 my="0px">{o_info?.order?.discount} {o_info?.order?.currencyName}</H6>
            </FlexBox>

            <Divider mb="0.5rem" />

            <FlexBox
              justifyContent="space-between"
              alignItems="center"
              mb="1rem"
            >
              <H6 my="0px"><FormattedMessage id="Total" /></H6>
              <H6 my="0px">{o_info?.order?.total_price} {o_info?.order?.currencyName}</H6>
            </FlexBox>
            <Typography fontSize="14px">{o_info?.order?.statusName}</Typography>
          </Card>
        </Grid>
      </Grid>
    </div>
    </DashboardLayout>
  );
};

export default OrderDetails;
