const ProductCard1 = dynamic(()=>import('../../components/product-cards/ProductCard1'),{ssr:false})
const Box = dynamic(()=>import('../../components/Box'),{ssr:false})
import CategorySectionCreator from "../CategorySectionCreator";
import Carousel from "../../components/carousel/Carousel";
import React, { useEffect, useState } from "react";
import useWindowSize from "../../hooks/useWindowSize";
import { InView } from 'react-intersection-observer'
import dynamic from "next/dynamic";
export interface Section2Props{
  data:any
}


const Section2:React.FC<Section2Props>= ({data}) => {
  const [visibleSlides, setVisibleSlides] = useState(4);
  const width = useWindowSize();
  useEffect(() => {
    if (width < 500) setVisibleSlides(2 );
    else if (width < 650) setVisibleSlides(2);
    else if (width < 1050) setVisibleSlides(3);
    else setVisibleSlides(4);
  }, [width]);
  let left_button_style;
  let right_button_style;
  if(width < 650) {
    left_button_style = {
      marginLeft:'-20px',
    }
    right_button_style = {
      marginRight:'-20px',
    }
  }
  return (
      <div>
        {!data && data?.length <1 && typeof data !== 'object' ? "" : data?.map((category:any)=>(
          category?.flowers?.length !==0 ?(
                <InView triggerOnce={true} key={category?.id}>
                  {(props)=>(
                    <div
                    ref={props.ref}
                    className="mt-0 mt-sm-0 mt-md-0 mt-lg-0 mt-xl-0 "
                    id={category?.keyword}
                >
                  {props.inView  && (
                    <CategorySectionCreator
                    iconName="light"
                    title={category.title}
                    seeMoreLink={`/${category?.keyword}`}
                >
                  <Box
                      mb="0rem"
                  >
                    <Carousel
                      leftButtonStyle={left_button_style}
                      rightButtonStyle={right_button_style}
                      totalSlides={category.flowers.length }
                      visibleSlides={visibleSlides}
                    >
                      {category?.flowers?.map((item) => (
                          <InView triggerOnce={true}>
                            {(props2)=>(
                              <div
                              ref={props2.ref}
                              key={item?.keyword}
                              className='index_product_container'
                              data-aos-delay={width < 650 ? 1000 : 0}
                          >
                            {props2.inView && (
                            <Box
                              py="0rem"
                            >
                              <ProductCard1
                                  id={item?.keyword}
                                  category_keyword = {item?.categoryKeyword}
                                  imgUrl={item?.image}
                                  title={item?.name}
                                  rating={item?.rating}
                                  price={item?.price}
                                  key={item?.keyword}
                                  is_favourite={item?.is_favorites}
                                  shopName={item?.shopName}
                                  shopKeyword={item?.shopKeyword}
                              />
                            </Box>
                            )}
                          </div>
                            )}
                          </InView>
                      ))}
                    </Carousel>
                  </Box>
                </CategorySectionCreator>
                  )}
                </div>
                  )}
                </InView>
              )  
              :
              ""
        ))}

    </div>
  );
};

export default Section2;
