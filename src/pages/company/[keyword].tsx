import Loading from "../../components/Loading";
import axios from "axios";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { fetch_user_info } from "../../../Redux/Actions/Action";
import { Card1 } from "../../components/Card1";
import Grid from "../../components/grid/Grid";
import VendorDashboardLayout from "../../components/layout/VendorDashboardLayout";
import DashboardPageHeader2 from "../../components/layout/DashboardPageHeader2";
import { NextSeo } from "next-seo";
const CompanyServices = ()=>{
    let [loading,setloading] = useState(true)
    let router = useRouter()
    let [data,setdata] = useState({"name":"","text":""})
    let dispatch = useDispatch()
    let {keyword} = router.query
    let lang = router.locale
    useEffect(()=>{
        axios(`/references/helps/show/${lang}`)
            .then(res=>{
                dispatch(fetch_user_info(res.data))
            })
            .catch(()=>null)
        axios(`/references/helps/show/${keyword}/${lang}`)
            .then(res=>{
                setdata(res.data)
                setloading(false)
            })
            .catch(()=>null)
    },[keyword,router.locale])
    if(loading){
        return <>
            <NextSeo
            title={data?.name}
            description={data?.text}
            additionalMetaTags={[{
            name: 'keyword',
            content: data?.name
            }, ]}
            />
            <Loading />
        </>
    }
    else{
    return(<VendorDashboardLayout >
            <NextSeo
                title={data?.name}
                description={data?.text}
                additionalMetaTags={[{
                name: 'keyword',
                content: data?.name
                }, ]}
            />

            <div style={{marginBottom:"50px"}}>
                <div  style={{color:"#f7961"}}>
                    <DashboardPageHeader2
                    title={data?.name}
                    />
                    <Card1 style={{marginLeft:"13px",marginRight:"13px"}}>
                            <Grid  style={{padding:"5px",paddingBottom:"5px",paddingTop:"8px"}}>
                            <div className="whitespace-break" dangerouslySetInnerHTML={{__html: data.text}} ></div> 
                            </Grid>
                    </Card1>
                
                </div>
            </div>
        </VendorDashboardLayout>
    )
    }
}
export default CompanyServices;

