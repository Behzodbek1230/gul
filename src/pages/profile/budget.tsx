import DashboardLayout from "../../components/layout/CustomerDashboardLayout";
import DashboardPageHeader from "../../components/layout/DashboardPageHeader";
import React, {useEffect, useState} from "react";
import {useSelector} from "react-redux";
import axios from "axios";
import Card from "../../components/Card";
import FlexBox from "../../components/FlexBox";
import Typography, {Small} from "../../components/Typography";
import Box from "../../components/Box";
import Grid from "../../components/grid/Grid";
import Button from "../../components/buttons/Button";
import {useIntl} from "react-intl";
import {FormattedMessage} from "react-intl";
import { NextSeo } from "next-seo";
import { DataGrid } from '@mui/x-data-grid';
import {Dialog,DialogTitle} from "@mui/material";
import Balance from "../../components/Balance";
import { useRouter } from "next/router";
import Cookies from 'js-cookie'


const Budget = () => {
  let intl = useIntl()
  let router = useRouter()
  const user = useSelector((state:any)=>state.token.user)
  let [open2,setopen2] = useState(false)
  let [paymentHistory,setpaymentHistory] = useState([])
  /* DataGrid*/
  let lang = router.locale
  let isLoggedIn = Cookies.get('isLoggedIn')
  useEffect(()=>{
    if(isLoggedIn === 'false'){
      router.push('/404')
    }
  },[])
  useEffect(()=>{
    axios(`/profile/payment-history/${lang}`)
    .then(res=>{
      setpaymentHistory(res.data)
    })
    .catch(()=>null)
  },[])
const columns = [
  {
    field:"id",
    headerName:"№",
    headerClassName:"budget_class",
    width: 50,
    editable: false
  },
  {
    field:"datePay",
    headerName: intl.formatMessage({id:"date"}),
    width: 200,
    headerClassName:"budget_class",
    editable: false,
  },
  {
        field: "amount",
        headerName: intl.formatMessage({id:"Amount"}),
        width: 190,
        editable: false,
  },
  {
        field: "status",
        headerName: intl.formatMessage({id:"Status"}),
        headerClassName:"budget_class",
        width:200,
        editable: false,
  },
  {
      field: "psystem",
      headerName:intl.formatMessage({id:"Type"}),
      headerClassName:"budget_class",
      width: 200,
      editable: false
  },

];

  return (
    <DashboardLayout title={intl.formatMessage({id:"hisob",defaultMessage:"Mening Hisobim"})}>
      <NextSeo
        title={intl.formatMessage({id:"Mening Hisobim"})}
      />
    <div>
      <div >
          <DashboardPageHeader
              iconName="money-transfer"
              title={intl.formatMessage({id:"hisob",defaultMessage:"Mening Hisobim"})}

          />
      </div>

             <Box mb="30px" >
                <Grid container spacing={6}>
                  <Grid item lg={5} md={5} sm={12} xs={12} style={{position:"relative"}}>
                    <FlexBox as={Card} p="14px 14px" height="100%" alignItems="center">
                      <Box ml="0px" flex="1 1 0">
                        <FlexBox
                          flexWrap="wrap"
                          justifyContent="space-between"
                          alignItems="center"
                        >
                          <div >
                            <FlexBox alignItems="center">
                              <Typography fontSize="25px">
                                  <FormattedMessage
                                    id="budget"
                                    defaultMessage="Balans"
                                  />:
                              </Typography>
                              <Typography ml="4px" fontSize="25px" color="primary.main">
                                  {user?.data?.balance ? user?.data?.balance : 0} <FormattedMessage id="sum" />
                              </Typography>
                            </FlexBox>
                          </div>

                        </FlexBox><br/>
                           <Grid container>
                               <Grid item lg={4} md={4} sm={4} xs={4}>
                                <Small color="text.muted" textAlign="center">
                                   <FormattedMessage
                                    id="wallet"
                                    defaultMessage="Hamyon"
                                   /> :<br/>0 <FormattedMessage id="sum" defaultMessage="So'm" />
                                </Small>
                            </Grid>
                           <Grid item lg={4} md={4} sm={4} xs={4}>
                                <Small color="text.muted" textAlign="center">
                                    <FormattedMessage
                                        id="bonus"
                                        defaultMessage="Bonus"
                                    /> :<br/>0 <FormattedMessage
                                        id="sum"
                                        defaultMessage="So'm"
                                    />
                                </Small>
                            </Grid>
                           <Grid item lg={4} md={4} sm={4} xs={4}>
                                <Small color="text.muted" textAlign="center">
                                    <FormattedMessage
                                        id="cashback"
                                        defaultMessage="Kashbak"
                                    /> :<br/>0 <FormattedMessage
                                    id="sum"
                                    defaultMessage="So'm"
                                    />
                                </Small>
                            </Grid>
                           </Grid><br/>

                            <Button
                                color="primary"
                                bg="primary.light"
                                onClick={()=>setopen2(true)}
                            >
                              <FormattedMessage
                                  id="replenish"
                                  defaultMessage="Hisobni to'ldirish"
                              />
                            </Button>
                              <Dialog
                                  open={open2}
                                  onClose={()=>setopen2(false)}
                                  aria-labelledby="alert-dialog-title"
                                  aria-describedby="alert-dialog-description"
                              >
                                  <DialogTitle>
                                      <FormattedMessage
                                          id="payment"
                                      />
                                  </DialogTitle>
                                  <>
                                     <Balance setopen2={(e)=>setopen2(e)} />
                                  </>

                              </Dialog>

                      </Box>
                    </FlexBox>

                  </Grid>
                     <Grid item lg={12} md={12} sm={12} xs={12}>
                       <Card style={{height:"200px",width:"100%",overflowX:"hidden"}}>
                       <DataGrid
                          checkboxSelection={false}
                          rows={paymentHistory}
                          columns={columns}
                          pageSize={10}
                          disableColumnFilter={true}
                          disableColumnMenu={true}
                          disableColumnSelector={true}

                        />
                       </Card>
                  </Grid>
                </Grid>

              </Box>
    </div>
    </DashboardLayout>
  );
};


export default Budget;
