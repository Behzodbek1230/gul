import Avatar from "../../../components/avatar/Avatar";
import Box from "../../../components/Box";
import Button from "../../../components/buttons/Button";
import Card from "../../../components/Card";
import FlexBox from "../../../components/FlexBox";
import DashboardPageHeader2 from "../../../components/layout/DashboardPageHeader2";
import VendorDashboardLayout from "../../../components/layout/VendorDashboardLayout";
import TableRow from "../../../components/TableRow";
import Typography, { H6} from "../../../components/Typography";
import Link from "next/link";
import { useRouter } from "next/router";
import React, {useEffect, useState} from "react";
import axios from "axios";
import {useSelector} from "react-redux";
import Cookies from "js-cookie"
import {FormattedMessage, useIntl} from "react-intl";
import Loading from "../../../components/Loading";
import { NextSeo } from "next-seo";
import Grid from "../../../components/grid/Grid";
import Map_Shop from "../../../components/map_shop";
import Divider from "../../../components/Divider";
const OrderDetails = () => {
  const router = useRouter();
  let [loading,setloading] = useState(true)
  let lang = router.locale
  let intl = useIntl()
  const { id } = router.query;
  let [statuses,setstatuses] = useState([])
  let [postcard_full,setpostcard_full] = useState(false)
  let [instructions_full,setinstructions_full] = useState(false)
  const [info,setinfo] = useState(null)
  let user = useSelector((state:any)=>state.token.user)
  let isLoggedIn = Cookies.get('isLoggedIn')
  useEffect(()=>{
    if(isLoggedIn === 'false' || user?.data?.is_shops === null){
        router.push('/404')
    }
  },[user,isLoggedIn])
  useEffect(() => {
        let currency_id = Cookies.get("currency_id");
        let currency_text = typeof currency_id !== "undefined" ? `?currency=${currency_id}` : ""
        axios(`/shops/orders-products/${user?.data?.is_shops ? user.data?.is_shops : ""}/${id}/${lang}${currency_text}`)
            .then(res=>{
              setinfo(res.data)
              setloading(false)
            })
            .catch(()=>null)
        axios(`/orders/status-list/${lang}`)
            .then(res=>{
                setstatuses(res.data)
            })
            .catch(()=>null)
  }, [Cookies.get("currency_id"),lang]);
  
  let handleStatusChange = (e) =>{
    setloading(true)
    const data = new FormData()
    data.append("keyword",user.data.is_shops);
    data.append("order_id",info?.order?.orderId)
    data.append("status",e)
    axios({
      method:"POST",
      url:`/shops/order-change-status/${lang}`,
      data:data
    })
        .then(response=>{
          if (response.data.errors){
            return null;
          }
          else{
            axios(`/shops/orders-products/${user.data?.is_shops ? user.data?.is_shops : ""}/${id}/${lang}`)
                .then((res)=>{
                  setloading(false)
                  setinfo(res.data)
                })
                .catch(()=>null)
          }
        })
  }




  if(loading){
    return <Loading/>
  }
  else{
    return (
      <VendorDashboardLayout title={intl.formatMessage({id:"Order Details"})}>
         <NextSeo
            title={intl.formatMessage({id:"Order Details",})}
        />
      <div>
        <DashboardPageHeader2
          title={intl.formatMessage({id:"Order Details"})}
          iconName="bag_filled"
          button={
            <Link href="/vendor/orders/page/1">
              <Button color="primary" bg="primary.light" px="2rem">
                <FormattedMessage
                  id="Back To Order List"
                  defaultMessage="Buyurtmalar listiga qaytish"
                />
              </Button>
            </Link>
          }
        />

        <Card p="0px" mb="30px" overflow="hidden">

          <TableRow bg="gray.200" p="12px" boxShadow="none" borderRadius={0}>
            <FlexBox
              className="pre"
              flex="0 0 0 !important"
              m="6px"
              alignItems="center"
            >
              <Typography fontSize="14px" color="text.muted" mr="4px">
                <FormattedMessage
                  id="Order ID"
                  defaultMessage="Buyurtma raqami"
                />
                :
              </Typography>
              <Typography fontSize="14px">{id}</Typography>
            </FlexBox>
            <FlexBox m="6px" alignItems="center" >
              <Typography style={{width:"-webkit-fit-content",whiteSpace:"nowrap"}} mr="4px" fontSize="14px" color="text.muted">
                <FormattedMessage
                  id="Placed On"
                  defaultMessage="Joylashgan"
                />: 
              </Typography> 
              <Typography style={{width:"130px"}}  fontSize="14px"  >
                  {info.datas[0]?.delivery_time}
              </Typography>


            </FlexBox>
            <FlexBox
              className="pre "
              m="6px"
              ml="0px"
              alignItems="center"
            >
                  <Typography  fontSize="14px" color="text.muted" mr="4px" ml="6px" >
                  <FormattedMessage
                    id="Total"
                    defaultMessage="Umumiy"
                  />:
                </Typography>
                <Typography style={{width:"220px"}} fontSize="14px">{info?.order.total_price} </Typography>
            </FlexBox>

            <Box  ml="6px">
              <Typography  fontSize="14px" color="text.muted" className="whitespace-nowrap  ">
                <FormattedMessage
                  id="Select List"
                  defaultMessage="Listni tanlang"
                />:
              </Typography>
              <select
                  value={parseInt(info.order.status) }
                  disabled={info.order.statusName === statuses[statuses.length-1]}
                  onChange={(r)=>handleStatusChange(r.target.value)}
                  className="form-control mt-1"
                  id="sel1"
              >
                {statuses?.map((val,ind) => (<option key={ind} value={ind+1}>{val}</option>))}
              </select>
            </Box>
          </TableRow>

          <Box py="0.5rem">
            {info?.datas?.map((item) => (
              <FlexBox
                px="1rem"
                py="0.5rem"
                flexWrap="wrap"
                alignItems="center"
                key={item}
              >
                <FlexBox flex="2 2 260px" m="6px" alignItems="center">
                  <Avatar src={item.image} size={64} />
                  <Box ml="20px">
                    <H6 my="0px">{item.productName}</H6>
                    <FlexBox alignItems="center">
                      <Typography fontSize="14px" color="text.muted">
                        {item.price}  x
                      </Typography>
                      <Box maxWidth="60px" ml="0.3rem" mr="0.3rem" mt="0rem">
                          <Typography fontSize="14px" color="text.muted">
                              {item.count}
                          </Typography>

                      </Box>  <Typography fontSize="14px" color="text.muted">
                              = {item.totalPrice}
                          </Typography>
                    </FlexBox>
                  </Box>
                </FlexBox>
              </FlexBox>
            ))}
          </Box>
        </Card>
          <Grid container spacing={5} >
              <Grid  item md={6} xs={12} >
                  <Card p="0px" mb="30px" overflow="hidden">
                      <TableRow bg="gray.200" p="12px"   boxShadow="none" borderRadius={0}>
                          <FormattedMessage id="info_about_delivery_price" />
                      </TableRow>
                      <Box  padding="15px">
                          <div className="pb-3" >{info?.order?.address}</div>
                          <div className="order_map_container"><Map_Shop height={140}  coordinate={[info?.order?.coordinate_x, info?.order?.coordinate_y]}/></div>
                      </Box>
                  </Card>
              </Grid>
              <Grid  item md={6} xs={12} >
                  <Card p="0px" mb="30px" overflow="hidden">
                      <TableRow bg="gray.200" p="12px"   boxShadow="none" borderRadius={0}>
                          <FormattedMessage id="full_info_order" />
                      </TableRow>
                      <Box  padding="15px">
                          <div className="mb-1"><FormattedMessage id="full_name" />: {info?.order?.name} <br/></div>
                          <div className="mb-1"><FormattedMessage id="phone_number" />: {info?.order?.phone} </div>
                          <div className="mb-1"><FormattedMessage id="delivery_price" />: {info.order.delivery_price}</div>
                          <div className="mb-1"><FormattedMessage id="Discount" />: {info.order.discount}</div>
                          <div className="mb-1"><FormattedMessage id="cash_or_card" />: {info?.order?.cash_or_card}</div>
                          {/*<div className="mb-1"><FormattedMessage id="Voucher" />: Ishlatilgan</div>*/}
                          <div className="mb-1"><FormattedMessage id="total_calculation" />: {info.order.total_price}</div>
                      </Box>
                      <TableRow bg="gray.200" p="12px"   boxShadow="none" borderRadius={0}>
                          <FormattedMessage id="delivery_time" />: {info.order.delivery_time}
                      </TableRow>
                  </Card>
              </Grid>
          </Grid>
          <Grid container spacing={5} >
              <Grid  item md={6} xs={12} >
                  <Card p="0px" mb="30px" overflow="hidden">
                      <TableRow bg="gray.200" p="12px"   boxShadow="none" borderRadius={0}>
                          <FormattedMessage id="postcard text" />
                      </TableRow>
                      <Box  padding="15px" minHeight="105px">
                          {postcard_full  ? info?.order?.card : info?.order?.card?.slice(0,187)} {info?.order?.card?.length > 187 ? "..." :""} <br/>
                          {info?.order?.card?.length > 187 ? <button className="border-0 mt-1 bg-white text-danger" onClick={()=>setpostcard_full(!postcard_full)}>{postcard_full ? <FormattedMessage id="less" /> : <FormattedMessage id="full" />}</button> :  ""}
                      </Box>
                  </Card>
              </Grid>
              <Grid  item md={6} xs={12} >
                  <Card p="0px" mb="30px" overflow="hidden">
                      <TableRow bg="gray.200" p="12px"   boxShadow="none" borderRadius={0}>
                          <FormattedMessage id="special_instruction" />
                      </TableRow>
                      <Box  padding="15px" minHeight="105px">
                          {instructions_full  ? info?.order?.instructions : info?.order?.instructions?.slice(0,187)} {info?.order?.instructions?.length > 187 ? "..." :""} <br/>
                          {info?.order?.instructions?.length > 187 ? <button className="border-0 mt-1 bg-white text-danger" onClick={()=>setinstructions_full(!instructions_full)}>{instructions_full ? <FormattedMessage id="less" /> : <FormattedMessage id="full" />}</button> :  ""}
                      </Box>
                  </Card>
              </Grid>
          </Grid>


          <Grid container spacing={5} >
              <Grid  item md={12} xs={12} >
                  <Card p="0px" mb="30px" overflow="hidden">
                      <TableRow bg="gray.200" p="12px"   boxShadow="none" borderRadius={0}>
                          <FormattedMessage id="total_calculation" />
                      </TableRow>

                  <Box padding="15px">

                          <FlexBox
                              justifyContent="space-between"
                              alignItems="center"
                              mb="0.5rem"
                          >
                              <Typography fontSize="14px" color="text.hint">
                                  <FormattedMessage id="delivery_price" />:
                              </Typography>
                              <H6 my="0px">{info.order.delivery_price} </H6>
                          </FlexBox>
                          <FlexBox
                              justifyContent="space-between"
                              alignItems="center"
                              mb="0.5rem"
                          >
                              <Typography fontSize="14px" color="text.hint">
                                  <FormattedMessage id="Discount" />:
                              </Typography>
                              <H6 my="0px">{info.order.discount} </H6>
                          </FlexBox>
                          <FlexBox
                              justifyContent="space-between"
                              alignItems="center"
                              mb="0.5rem"
                          >
                              <Typography fontSize="14px" color="text.hint">
                                  <FormattedMessage id="cash_or_card" />:
                              </Typography>
                              <H6 my="0px">{info?.order?.cash_or_card} </H6>
                          </FlexBox>


                          <Divider mb="0.5rem" />

                          <FlexBox
                              justifyContent="space-between"
                              alignItems="center"
                              mb="1rem"
                          >
                              <H6 my="0px"><FormattedMessage id="Total" /></H6>
                              <H6 my="0px">{info?.order?.total_price} </H6>
                          </FlexBox>
                          <FlexBox
                              justifyContent="space-between"
                              alignItems="center"
                              mb="0rem"
                          >
                              <H6 my="0px"><FormattedMessage id="order_status" /></H6>
                              <H6 my="0px">{info?.order?.statusName}</H6>
                          </FlexBox>
                    </Box>
                  </Card>
              </Grid>
          </Grid>
      </div>
    </VendorDashboardLayout>
    )  
  }
};
export default OrderDetails;
