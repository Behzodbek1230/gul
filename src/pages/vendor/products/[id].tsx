import Button from "../../../components/buttons/Button";
import Card from "../../../components/Card";
import DropZone from "../../../components/DropZone";
import Grid from "../../../components/grid/Grid";
import DashboardPageHeader2 from "../../../components/layout/DashboardPageHeader2";
import VendorDashboardLayout from "../../../components/layout/VendorDashboardLayout";
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import Link from "next/link";
import React, {useEffect, useState} from "react";
import axios from "axios";
import {useSelector} from "react-redux";
import {useRouter} from "next/router";
import {ButtonGroup, Dropdown, DropdownButton} from "react-bootstrap";
import useWindowSize from "../../../hooks/useWindowSize";
import {FormattedMessage, useIntl} from "react-intl";
import { BASE_URL } from "../../../components/Variables";
import { NextSeo } from "next-seo";
import CurrencyFormatter from "../../../components/CurrencyFormatter";
import {Delete} from "@material-ui/icons";
const EditProduct = ({currency2,material2,units2,one}) => {
  let intl = useIntl()
  let router = useRouter()
  let lang = router.locale
  // -------States----------
  const [name,setname] = useState(one.data.name)
  const [desc,setdesc] = useState(one.data.description)
  const [cat_id,setcat_id] = useState(one.data.categoryId)
  const [cat_name,setcat] = useState(one.data.categoryName)
  const [cat_er,setcat_er] = useState("")
  const [price,setprice] = useState(one?.data?.priceFlowers)
  const [price_c,setprice_c] = useState(one?.data?.currency_id)
  const [price_er,setprice_er] = useState("")
  const [width,setwidth] = useState(one.data.width)
  const [width_unit,setwidth_unit] = useState(one.data.width_unit_id)
  const [width_er,setwidth_er] = useState("")
  const [height,setheight] = useState(one.data.height)
  const [height_unit,setheight_unit] = useState(one.data.height_unit_id)
  const [height_er,setheight_er] = useState("")
  const [count,setcount] = useState(one.data.count)
  const [booked,setbooked] = useState(one.data.booked)
  const [discount,setdiscount] = useState(one.data.discount||0)
  const [photo,setphoto] = useState(one?.data?.image ? one?.data?.image : [])
  let [photo_err,setphoto_err] = useState("")
  const [structure,setstructure] = useState(one.data.structure)
  let width2 = useWindowSize()
  let [currency,setcurrency] = useState(currency2);
  let [material,setmaterial] = useState(material2)
  let [units,setunits] = useState(units2)
  const [delivery_time,setdelivery] = useState(0)

  const handleCategoryClick = (id,title) => {
    setcat_id(id),
    setcat(title)
  }

  useEffect(() => {
    axios(`/references/currency-flowers/${lang}`)
        .then(res=>{
          setcurrency(res.data)
        })
        .catch(()=>null)
    axios(`/flowers/material/list/${lang}`)
        .then(res=>{
          setmaterial(res.data)
        })
        .catch(()=>null)
    axios(`/flowers/units/list/${lang}`)
        .then(res=>{
          setunits(res.data)
        })
        .catch(()=>null)
  }, [lang]);
  // -------States----------
  const category_dropdown = items =>{
    return items.map((item,ind)=>
        item.children.length !==0
            ?
            <div className="mb-2" key={ind}>
              <DropdownButton
                  as={ButtonGroup}
                  key="end"
                  id={`dropdown-button-drop-bottom`}
                  drop="down"
                  variant="light"
                  title={item.title}
              >
                {category_dropdown(item.children)}
              </DropdownButton>
            </div>
            :
            <Dropdown.Item onClick={()=>handleCategoryClick(item.id,item.title)} eventKey={item.id}>{item.title}</Dropdown.Item>

    )}
  const updatematerial2 = (e,id) =>{
    e.preventDefault()
    let array = [...structure]
    array[id].material_id = e.target.value;
    setstructure(array)
  }
  const updatecount2 = (t,id) =>{
    t.preventDefault()
    let array = [...structure]
    array[id].count = t.target.value;
    setstructure(array)
  }
  const updateunit2 = (x,id )=>{
    x.preventDefault()
    let array = [...structure]
    array[id].unit_id =x.target.value;
    setstructure(array)
  }
  const addstructure = ()=>{
    let array = [...structure]
    array.push({
      material_id:undefined,
      count:0,
      unit_id:undefined
    })
    setstructure(array)
  }
  const handleremove = (id) => {
    let array = [...structure];
    array.splice(id - 1, 1)
    setstructure(array);
  }
  const category = useSelector((state:any)=>state.new.category)
  const handlePhotoAdd = (photos) =>{
    setphoto(prevState=>[...prevState,...photos])
  } 
  const handleRemoveImage = (ind) =>{
    let array = [...photo];
    array.splice(ind, 1)
    setphoto(array);
  }
  const handlePriceChange = (t) => {
    if(t.target.value !=="")
      setprice(parseInt(t.target.value.replace(/\s+/g,"")))
    else
      setprice(0)
  }
  const handleFormSubmit = async (values) => {
        values.preventDefault();
        let data = new FormData();
        data.append('name', name);
        data.append('description', desc);
        data.append('cat_id', cat_id);
        data.append('shop_id',one?.data?.shop_id);
        data.append('price', price.toString());
        data.append('currency_id', price_c);
        data.append('width', width);
        data.append('width_unit_id', width_unit);
        data.append('height', height);
        data.append('height_unit_id', height_unit);
        data.append('count', count || 0);
        data.append('booked', booked || 0);
        data.append('discount', discount || 0);
        data.append('delivery_time',delivery_time.toString())
        photo.forEach(function (value, index){
          if(typeof value !== "string"){
            data.append(`photo[${index}]`, value);
          }
        });
        structure.forEach(function (value, index){
          data.append(`structure[${index}][material_id]`, value.material_id);
          data.append(`structure[${index}][count]`, value.count.toString());
          data.append(`structure[${index}][unit_id]`, value.unit_id);
        });
        setcat_er("")
        setprice_er("")
        setwidth_er("")
        setheight_er("")
    let r = router.asPath.split("/")[3]
        if(cat_id === undefined){
          setcat_er(intl.formatMessage({id:"choose_category"}))
          window.scrollTo({top:100});
        }
        else if(price_c === undefined){

          setprice_er(intl.formatMessage({id:"currency_unit"}))
        }
        else if(width_unit === undefined){

          setwidth_er(intl.formatMessage({id:"units_error"}))
        }
        else if(height_unit === undefined){
          setheight_er(intl.formatMessage({id:"units_error"}))
        }
        else if(photo === [] || photo.length ===0 || typeof photo === "undefined" ){
          window.scrollTo({top:90})
          setphoto_err(intl.formatMessage({id:"select_image"}))
        }
        else{
          axios({
            method:"POST",
            url:`/flowers/update/${r}/${lang}`,
            data:data
          })
              .then(()=>{
                router.push("/vendor/products/page/1")
              })
              .catch(()=>{
                return null;
              })
        }
      }
  ;

  return (
    <VendorDashboardLayout >
      <NextSeo
            title={intl.formatMessage({id:"edit_product"})}
      />
      <div>
        <DashboardPageHeader2
            title={intl.formatMessage({id:"edit_product",defaultMessage:"Mahsulotni tahrirlash"})}
            iconName="delivery-box"
            button={
              <Link href="/vendor/products/page/1">
                <Button color="primary" bg="primary.light" px="2rem">
                    <FormattedMessage
                      id="back_to_product_list"
                      defaultMessage="Mahsulot listiga qaytish"
                    />
                </Button>
              </Link>
            }
        />

        <Card p="30px">
          <form onSubmit={handleFormSubmit}>
            <Grid container spacing={6}>
              <Grid item sm={12} xs={12} md={6}>
                <label>
                  <FormattedMessage
                       id="Product Name"
                       defaultMessage="Nomi"
                   />
                </label>
                <input
                    type="text"
                    required={true}
                    value={name}
                    onChange={(r)=>setname(r.target.value)}
                    className="form-control"
                />
              </Grid>
              <Grid item sm={12} xs={12} md={6}>
                <label htmlFor="sel6">
                  <FormattedMessage
                    id="mobile_navigation_category"
                    defaultMessage="Kategoriyalar"
                  />:
                </label><br/>
                <DropdownButton
                    style={{border:"1px solid #c7cbd1",borderRadius:"5px",width:"100%"}}
                    border="secondary"
                    as={ButtonGroup}
                    key="end"
                    id={`dropdown-button-drop-down`}
                    drop="down"
                    variant="light"
                    title={cat_name === "" ? "PLease choose one of category for your product" : cat_name}
                >
                  {category_dropdown(category)}
                </DropdownButton>
                {cat_er !== "" ? <div className="text-danger">{cat_er}</div>: ""}
              </Grid>
              <Grid item xs={12}>
                <DropZone
                    onChange={(files) => {
                      handlePhotoAdd(files)
                    }}
                />
              </Grid>
              <div>{ photo_err !=="" ? <div className="col-12 text-danger">{photo_err}</div>:""}</div>
              {photo.map((img,ind)=>(
                <Grid item sm={3} xs={6} key={ind} >
                      <div style={{position:"relative"}} >
                      <img src={typeof img === "string" ?  img  : URL.createObjectURL(img)} alt={name} width="50px" height='50px' />
                        <div  style={{position:"absolute",zIndex:10,top:"-10px",left:"38px",}}>
                            <HighlightOffIcon className="text-danger" onClick={()=>handleRemoveImage(ind)} />
                        </div>
                      </div>
                </Grid>
                ))}
              <Grid item xs={12} md={12} sm={12}>
                <label className="mt-1 mb-1" >
                  <FormattedMessage
                    id="description"
                    defaultMessage="Tavsif"
                  />
                </label>
                <textarea
                    onChange={(e)=>setdesc(e.target.value)}
                    cols={20}
                    rows={7}
                    required = {true}
                    value={desc}
                    className="form-control"
                />
              </Grid>
              <Grid item sm={12} xs={12} md={6}>
                <label className="mt-1 mb-1" >
                  <FormattedMessage
                    id="Amount"
                    defaultMessage="Soni"
                  />
                </label>
                <input
                    type="number"
                    className="form-control form-control-color-danger"
                    value={count}
                    placeholder={intl.formatMessage({id:"Amount"})}
                    onChange={(e)=>setcount(e.target.value)}
                />
              </Grid>

              <Grid item sm={8} xs={9} md={3}>
                <label className="mt-1 mb-1">

                  <FormattedMessage
                    id="Regular Price"
                    defaultMessage="Narxi"
                  />
                </label>
                <input
                    value={typeof price !== "undefined" ?  CurrencyFormatter(price) : undefined}
                    type="text"
                    required={true}
                    className="form-control"
                    placeholder={intl.formatMessage({id:"Regular Price",defaultMessage:"Narx"})}
                    onChange={(t)=>handlePriceChange(t)}
                />
              </Grid>
              <Grid item sm={4} xs={3} md={3}>
                <label>
                    {intl.formatMessage({id:'currency',defaultMessage:"Pul birligi"})}
                </label>
                <select
                    required={true}
                    onChange={(e)=>setprice_c(e.target.value)}
                    className="form-control"
                    value={price_c}
                    id="sel2"
                >
                  <option value="" hidden={true}>
                    {intl.formatMessage({id:'currency',defaultMessage:"Pul birligi"})}
                    </option>
                  {currency.map(curr=>(
                      <option key={curr.id} value={curr.id}>{curr.code}</option>
                  ))}
                </select>
                {price_er !== "" ? <div className="text-danger">{price_er}</div>: ""}

              </Grid>
              <Grid item sm={12} xs={12} md={12}>
                <label className="mt-1 mb-1" >
                  <FormattedMessage
                    id="Discount(without percentage)"
                    defaultMessage="Chegirma(foiz yozish shart emas)"
                  />
                </label>
                <input
                    type="number"
                    onChange={(r)=>setdiscount(r.target.value)}
                    placeholder={intl.formatMessage({id:"Discount(without percentage)"})}
                    className="form-control form-control-color-danger"
                    value={discount}
                    
                />
              </Grid>
              <Grid item sm={6} xs={6} md={3}>
                <label className="mt-1 mb-1" >
                  <FormattedMessage
                    id="width"
                    defaultMessage="Eni"
                  />
                </label>
                <input
                    type="number"
                    placeholder={intl.formatMessage({id:"width"})}
                    className="form-control form-control-color-danger"
                    required={true}
                    value={width}
                    onChange={(y)=>setwidth(y.target.value)}
                />
              </Grid>
              <Grid item sm={6} xs={6} md={3}>
                <label>
                  {intl.formatMessage({id:"width_unit"})}
                </label>
                <select
                    required={true}
                    onChange={(r)=>setwidth_unit(r.target.value)}
                    className="form-control"

                    id="sel3"
                    value={width_unit}
                >
                  <option value="" hidden={true}>
                    {intl.formatMessage({id:"width_unit"})}
                  </option>
                  {units.map(categor=>(
                      <option key={categor.id}  value={categor.id}>{categor.name}</option>
                  ))}
                </select>
                {width_er !== "" ? <div className="text-danger">{width_er}</div>: ""}

              </Grid>
              <Grid item sm={6} xs={6} md={3}>
                <label className="mt-1 mb-1" >
                  <FormattedMessage
                    id="height"
                    defaultMessage="Bo'yi"
                  />
                </label>
                <input
                    type="number"
                    placeholder={intl.formatMessage({id:"height"})}
                    className="form-control form-control-color-danger"
                    required={true}
                    value={height}
                    onChange={(t)=>setheight(t.target.value)}
                />
              </Grid>
              <Grid item sm={6} xs={6} md={3}>
                <label>
                  {intl.formatMessage({id:"height_unit",defaultMessage:"Bo'yi O'lchovi"})}
                </label>
                <select
                    required={true}
                    onChange={(y)=>setheight_unit(y.target.value)}
                    className="form-control"

                    id="sel1"
                    value={height_unit}
                >
                  <option value="" hidden={true}>
                    {intl.formatMessage({id:"height_unit",defaultMessage:"Bo'yi O'lchovi"})}
                  </option>
                  {units.map(categor=>(
                      <option key={categor.id}  value={categor.id}>{categor.name}</option>
                  ))}
                </select>
                {height_er !== "" ? <div className="text-danger">{height_er}</div>: ""}

              </Grid>
              <Grid item sm={12} md={6} lg={6} xl={6} xs={12}>
                  <label className="mt-1 mb-1" >
                    <FormattedMessage
                      id="delivery_time"
                    />(m)
                  </label>
                  <input
                      type="number"
                      required
                      placeholder={intl.formatMessage({id:"delivery_time"})}
                      className="form-control form-control-color-danger"
                      value={delivery_time}
                      onChange={(e)=>setdelivery(parseInt(e.target.value))}
                  />
              </Grid>
              <Grid item sm={12} md={6} lg={6} xl={6} xs={12}>
                <label className="mt-1 mb-1" >
                  <FormattedMessage
                    id="Booked"
                    defaultMessage="Oldindan band qilib qoyilganlar soni"
                  />
                </label>
                <input
                    type="number"
                    placeholder={intl.formatMessage({id:"Booked",defaultMessage:"Oldindan band qilib qoyilganlar soni"})}
                    className="form-control form-control-color-danger"
                    value={booked}
                    onChange={(e)=>setbooked(e.target.value)}
                />
              </Grid>
              <Grid item sm={12} md={12} style={{textAlign:"center",fontSize:"large"}}>
                <FormattedMessage
                  id="Consumed_products"
                  defaultMessage="Ishlatilgan maxsulotlar"
                />
              </Grid>
              {structure.map((struct,index)=>(
                    <>
                    <Grid item sm={12} xs={12}  md={12} key={index}  alignContent="center" className="justify-content-center mb-2"  >
                      <div
                          className=" border-muted justify-content-center pt-3"
                          style={{borderBottom:"3px solid lavender"}}
                      />
                    </Grid>
                    <Grid item sm={7} xs={7} md={9} className="ml-3" style={{marginRight:"5%"}}>
                      <label htmlFor="sel4">
                        Material
                        <FormattedMessage
                          id="Used Products"
                          defaultMessage="Mahsulot"
                        />
                      </label>
                      <select 
                        value={struct.material_id} 
                        className='vendor_add_product_select_Style form-control' 
                        required={true} 
                        onChange={(t)=>updatematerial2(t,index)} 
                        id="sel4"
                      >
                        <option>
                          {intl.formatMessage({id:'material_select'})}
                        </option>
                        {material.map(categor=>{
                          let r = 0
                          structure?.map((structu)=>structu?.material_id=== parseInt(categor?.id) ? r++ : "")
                          if(r<1) {
                            return (
                                <option key={categor?.id} value={categor?.id}>{categor?.name}</option>)
                          }
                          else{
                            return (
                                <option key={categor?.id} hidden={true} value={categor?.id}>{categor?.name}</option>)
                          }
                        })}
                      </select>
                    </Grid>
                  <Grid item sm={3} xs={3} md={2} className="float-right justify-content-right align-items-right ml-2 ml-sm-2 ml-md-0 ml-lg-0 ml-xl-0" >
                    <label className="mt-1 mb-1" >
                      <FormattedMessage
                        id="Amount"
                        defaultMessage="Soni"
                      />
                    </label>
                   <input
                      type="number"
                      className="form-control form-control-color-danger"
                      value={struct.count || ""}
                      placeholder={intl.formatMessage({id:"Amount"})}
                      onChange={(e)=>updatecount2(e,index)}
                    />
                  </Grid>
                  <Grid item sm={8} xs={8} md={9}  className="ml-3" style={{marginRight:"5%"}} >
                    <label htmlFor="sel5">
                      <FormattedMessage
                        id="unit"
                        defaultMessage="Birligi"
                      />
                    </label>
                    <select 
                      value={struct.unit_id}  
                      onChange={(t)=>updateunit2(t,index)} 
                      className="form-control vendor_add_product_select_Style"  
                      id="sel5"
                    >
                        <option style={{textOverflow:"ellipsis",overflow:"hidden",width:"90%",whiteSpace:"nowrap"}}>
                          {intl.formatMessage({id:'unit_select',defaultMessage:"iltimos birligini tanlang"})}
                        </option>
                        {units.map(categor=>(
                        <option key={categor.id}  value={categor.id}>{categor.name}</option>
                        ))}
                    </select>
                  </Grid>
                    <Grid item sm={2} xs={2} md={2} className="text-right float-right justify-content-right" >
                          <button onClick={()=>handleremove(index)} className="btn btn-danger w-100 ml-2 ml-sm-2 ml-md-0 ml-lg-0 ml-xl-0 pl-1 pr-1 vendor_product_card_delete_style1" >
                            {width2 < 650 ? <Delete/> : intl.formatMessage({id:"delete"})}
                          </button>
                    </Grid>
                    
                    </>
                ))}

              
              <Grid item md={12} xs={12}  >
                <div
                    className="btn btn-light"
                    onClick={addstructure}
                    style={{textAlign:"center",width:"100%",cursor:"pointer"}}
                >
                  <FormattedMessage
                    id="add_used_products"
                    defaultMessage="Ishlatilgan material qo'shish +"
                  />

                </div>
              </Grid>
              <Button
                  mt="25px"
                  variant="contained"
                  color="primary"
                  type="submit"
              >
                <FormattedMessage
                  id="send"
                  defaultMessage="Saqlash"
                />
              </Button><br/>
              <div className="col-12 text-danger">{cat_er === "" ? "" : cat_er}</div>
              <div className="col-12 text-danger">{price_er === "" ? "" : price_er}</div>
              <div className="col-12 text-danger">{height_er === "" ? "" : height_er}</div>
              <div className="col-12 text-danger">{width_er === "" ? "" : width_er}</div>
              <div className="col-12 text-danger">{photo_err === "" ? "" : photo_err}</div>
            </Grid>
          </form>
        </Card>
      </div>
   </VendorDashboardLayout>
  );
};

export default EditProduct;


export async function getStaticPaths(){
  return {paths:[],fallback:'blocking'}
}



export async function getStaticProps(ctx) {
  try {
    let { id } = ctx.params
    let lang= ctx.locale
    const request2_4 = await axios(`${BASE_URL}/flowers/show/${id}/${lang}`)
    const answer_4 =  request2_4.data;
    return {
      props:{
        currency2:[],
        material2:[],
        units2:[],
        one:answer_4
      }
    }
  }
  catch{
    return{
      notFound:true
    }
  }
}
