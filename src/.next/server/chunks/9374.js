"use strict";
exports.id = 9374;
exports.ids = [9374];
exports.modules = {

/***/ 9374:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "H1": () => (/* binding */ H1),
/* harmony export */   "H2": () => (/* binding */ H2),
/* harmony export */   "H3": () => (/* binding */ H3),
/* harmony export */   "H4": () => (/* binding */ H4),
/* harmony export */   "H5": () => (/* binding */ H5),
/* harmony export */   "H6": () => (/* binding */ H6),
/* harmony export */   "Paragraph": () => (/* binding */ Paragraph),
/* harmony export */   "Span": () => (/* binding */ Span),
/* harmony export */   "SemiSpan": () => (/* binding */ SemiSpan),
/* harmony export */   "Small": () => (/* binding */ Small),
/* harmony export */   "Tiny": () => (/* binding */ Tiny),
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(5834);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(styled_system__WEBPACK_IMPORTED_MODULE_3__);




const Typography = (styled_components__WEBPACK_IMPORTED_MODULE_2___default().div)`
  ${(props)=>props.ellipsis ? `
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  ` : ""
}

  ${styled_system__WEBPACK_IMPORTED_MODULE_3__.border}
  ${styled_system__WEBPACK_IMPORTED_MODULE_3__.typography}
  ${styled_system__WEBPACK_IMPORTED_MODULE_3__.space}
  ${styled_system__WEBPACK_IMPORTED_MODULE_3__.color}
  ${styled_system__WEBPACK_IMPORTED_MODULE_3__.flex}
  ${styled_system__WEBPACK_IMPORTED_MODULE_3__.layout}
`;
const H1 = (props)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(Typography, {
        as: "h1",
        mb: "0",
        mt: "0",
        fontSize: "30px",
        ...props
    })
;
const H2 = (props)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(Typography, {
        as: "h2",
        mb: "0",
        mt: "0",
        fontSize: "25px",
        ...props
    })
;
const H3 = (props)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(Typography, {
        as: "h3",
        mb: "0",
        mt: "0",
        fontSize: "20px",
        ...props
    })
;
const H4 = (props)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(Typography, {
        as: "h4",
        mb: "0",
        mt: "0",
        fontWeight: "600",
        fontSize: "17px",
        ...props
    })
;
const H5 = (props)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(Typography, {
        as: "h5",
        mb: "0",
        mt: "0",
        fontWeight: "600",
        fontSize: "16px",
        ...props
    })
;
const H6 = (props)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(Typography, {
        as: "h6",
        mb: "0",
        mt: "0",
        fontWeight: "600",
        fontSize: "14px",
        ...props
    })
;
const Paragraph = (props)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(Typography, {
        as: "p",
        mb: "0",
        mt: "0",
        ...props
    })
;
const Span = (props)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(Typography, {
        as: "span",
        fontSize: "16px",
        ...props
    })
;
const SemiSpan = (props)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(Typography, {
        as: "span",
        fontSize: "14px",
        color: "text.muted",
        ...props
    })
;
const Small = (props)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(Typography, {
        as: "span",
        fontSize: "12px",
        ...props
    })
;
const Tiny = (props)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(Typography, {
        as: "span",
        fontSize: "10px",
        ...props
    })
;
// const H1 =
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Typography);


/***/ })

};
;