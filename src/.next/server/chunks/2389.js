"use strict";
exports.id = 2389;
exports.ids = [2389];
exports.modules = {

/***/ 2389:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _styled_system_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2038);
/* harmony import */ var _styled_system_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_styled_system_css__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5834);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(styled_system__WEBPACK_IMPORTED_MODULE_2__);



const Button = styled_components__WEBPACK_IMPORTED_MODULE_1___default().button(({ color , fullwidth  })=>_styled_system_css__WEBPACK_IMPORTED_MODULE_0___default()({
        display: "flex",
        width: fullwidth ? "100%" : "unset",
        justifyContent: "center",
        alignItems: "center",
        outline: "none",
        border: "none",
        cursor: "pointer",
        padding: "11px 1.5rem",
        fontSize: "1rem",
        fontWeight: 600,
        color: color ? `${color}.main` : "body.text",
        background: "transparent",
        transition: "all 150ms ease-in-out",
        lineHeight: 1,
        "&:focus": {
            boxShadow: 3
        },
        "&:disabled": {
            bg: "text.disabled",
            color: "text.hint",
            borderColor: "text.disabled",
            cursor: "unset",
            "svg path": {
                fill: "text.hint"
            },
            "svg polyline, svg polygon": {
                color: "text.hint"
            }
        }
    })
, ({ theme , color  })=>{
    var ref, ref1, ref2, ref3, ref4, ref5, ref6, ref7;
    return (0,styled_system__WEBPACK_IMPORTED_MODULE_2__.variant)({
        prop: "variant",
        variants: {
            text: {
                border: "none",
                color: `${color}.main`,
                "&:hover": {
                    bg: color ? `${color}.light` : "gray.100"
                }
            },
            outlined: {
                padding: "10px 16px",
                color: `${color}.main`,
                border: "1px solid",
                borderColor: color ? `${color}.main` : "text.disabled",
                "&:enabled svg path": {
                    fill: color ? `${(ref = theme.colors[color]) === null || ref === void 0 ? void 0 : ref.main} !important` : "text.primary"
                },
                "&:enabled svg polyline, svg polygon": {
                    color: color ? `${(ref1 = theme.colors[color]) === null || ref1 === void 0 ? void 0 : ref1.main} !important` : "text.primary"
                },
                "&:focus": {
                    boxShadow: `0px 1px 4px 0px ${(ref2 = theme.colors[color]) === null || ref2 === void 0 ? void 0 : ref2.light}`
                },
                "&:hover:enabled": {
                    bg: color && `${color}.main`,
                    borderColor: color && `${color}.main`,
                    color: color && `${color}.text`,
                    "svg path": {
                        fill: color ? `${(ref3 = theme.colors[color]) === null || ref3 === void 0 ? void 0 : ref3.text} !important` : "text.primary"
                    },
                    "svg polyline, svg polygon": {
                        color: color ? `${(ref4 = theme.colors[color]) === null || ref4 === void 0 ? void 0 : ref4.text} !important` : "text.primary"
                    }
                }
            },
            contained: {
                border: "none",
                color: `${color}.text`,
                bg: `${color}.main`,
                "&:focus": {
                    boxShadow: `0px 1px 4px 0px ${(ref5 = theme.colors[color]) === null || ref5 === void 0 ? void 0 : ref5.light}`
                },
                "&:enabled svg path": {
                    fill: color ? `${(ref6 = theme.colors[color]) === null || ref6 === void 0 ? void 0 : ref6.text} !important` : "text.primary"
                },
                "&:enabled svg polyline, svg polygon": {
                    color: color ? `${(ref7 = theme.colors[color]) === null || ref7 === void 0 ? void 0 : ref7.text} !important` : "text.primary"
                }
            }
        }
    });
}, (0,styled_system__WEBPACK_IMPORTED_MODULE_2__.variant)({
    prop: "size",
    variants: {
        large: {
            height: "56px",
            px: 30
        },
        medium: {
            height: "48px",
            px: 30
        },
        small: {
            height: "40px",
            fontSize: 14
        }
    }
}), (0,styled_system__WEBPACK_IMPORTED_MODULE_2__.compose)(styled_system__WEBPACK_IMPORTED_MODULE_2__.color, styled_system__WEBPACK_IMPORTED_MODULE_2__.layout, styled_system__WEBPACK_IMPORTED_MODULE_2__.space, styled_system__WEBPACK_IMPORTED_MODULE_2__.border, styled_system__WEBPACK_IMPORTED_MODULE_2__.shadow));
Button.defaultProps = {
    size: "small",
    borderRadius: 5
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Button);


/***/ })

};
;