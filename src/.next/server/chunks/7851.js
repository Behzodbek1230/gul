"use strict";
exports.id = 7851;
exports.ids = [7851];
exports.modules = {

/***/ 9778:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "A": () => (/* binding */ Chip),
/* harmony export */   "b": () => (/* binding */ ChipwithoutRounded)
/* harmony export */ });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5834);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_system__WEBPACK_IMPORTED_MODULE_1__);


const Chip = (styled_components__WEBPACK_IMPORTED_MODULE_0___default().div)`
  display: inline-flex;
  cursor: ${(props)=>props.cursor || "unset"
};
  box-shadow: ${(props)=>props.boxShadow || "unset"
};
  transition: all 150ms ease-in-out;
  border-radius:15px;
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.space}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.color}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.position}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.typography}
`;
const ChipwithoutRounded = (styled_components__WEBPACK_IMPORTED_MODULE_0___default().div)`
  display: inline-flex;
  cursor: ${(props)=>props.cursor || "unset"
};
  box-shadow: ${(props)=>props.boxShadow || "unset"
};
  transition: all 150ms ease-in-out;
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.space}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.color}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.position}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.typography}
`;


/***/ }),

/***/ 9427:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5834);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_system__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _utils_utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1972);



const TableRow = (styled_components__WEBPACK_IMPORTED_MODULE_0___default().div)`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  border-radius: 10px;
  box-shadow: ${({ boxShadow  })=>(0,_utils_utils__WEBPACK_IMPORTED_MODULE_2__/* .getTheme */ .gh)(`shadows.${boxShadow || "small"}`)
};
  //@media only screen and  (max-width: 650px){
  //  //display: block !important;
  //  //flex-wrap: nowrap !important;
  //  //white-space: nowrap !important;
  //  //word-break: keep-all !important;
  //  //overflow: scroll !important;
  // 
  //}
  & > * {
    flex: 1 1 0;
  }

  .pre {
    white-space: pre;
  }

  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.space}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.color}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.border}
`;
TableRow.defaultProps = {
    bg: "body.paper"
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (TableRow);


/***/ }),

/***/ 7851:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _hooks_useWindowSize__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(12);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(9894);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _Box__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(4875);
/* harmony import */ var _buttons_IconButton__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(8661);
/* harmony import */ var _Chip__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(9778);
/* harmony import */ var _TableRow__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(9427);
/* harmony import */ var _Typography__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(9374);
/* harmony import */ var _material_ui_icons__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(2105);
/* harmony import */ var _material_ui_icons__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(6981);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(reactstrap__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(2167);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(1853);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(6734);
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(js_cookie__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(6022);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_14__);















const OrderRow = ({ item , detail , setdata  })=>{
    let width = (0,_hooks_useWindowSize__WEBPACK_IMPORTED_MODULE_1__/* ["default"] */ .Z)();
    const { 0: open , 1: setopen  } = (0,react__WEBPACK_IMPORTED_MODULE_3__.useState)(false);
    const { 0: statuses , 1: setstatuses  } = (0,react__WEBPACK_IMPORTED_MODULE_3__.useState)([]);
    const user = (0,react_redux__WEBPACK_IMPORTED_MODULE_14__.useSelector)((state)=>state.token.user
    );
    let router = (0,next_router__WEBPACK_IMPORTED_MODULE_12__.useRouter)();
    const lang = router.locale;
    const { page  } = router.query;
    (0,react__WEBPACK_IMPORTED_MODULE_3__.useEffect)(()=>{
        axios__WEBPACK_IMPORTED_MODULE_11___default()(`/orders/status-list/${lang}`).then((res)=>{
            setstatuses(res.data);
        }).catch(()=>null
        );
    }, [
        lang
    ]);
    const handleStatusChange = (id)=>{
        const data = new FormData();
        data.append("keyword", user.data.is_shops);
        data.append("order_id", item.orderId);
        data.append("status", id);
        axios__WEBPACK_IMPORTED_MODULE_11___default()({
            method: "POST",
            url: `/shops/order-change-status/${lang}`,
            data: data
        }).then(()=>{
            var ref, ref1;
            let currency_text = typeof js_cookie__WEBPACK_IMPORTED_MODULE_13___default().get("currency_id") === "undefined" ? "" : `currency=${js_cookie__WEBPACK_IMPORTED_MODULE_13___default().get("currency_id")}`;
            axios__WEBPACK_IMPORTED_MODULE_11___default()(`/shops/orders-list/${(user === null || user === void 0 ? void 0 : (ref = user.data) === null || ref === void 0 ? void 0 : ref.is_shops) ? user === null || user === void 0 ? void 0 : (ref1 = user.data) === null || ref1 === void 0 ? void 0 : ref1.is_shops : ""}/${lang}${typeof page === "undefined" ? "?" : `?page=${page}&`}${currency_text}`).then((response)=>{
                setdata(response.data);
            }).catch(()=>{
                return null;
            });
        }).catch(()=>null
        );
    };
    const getColor = (status)=>{
        switch(status){
            case 1:
                return "danger";
            case 2:
                return "warning";
            case 3:
                return "success";
            case 4:
                return "secondary";
            default:
                return "";
        }
    };
    if (width < 770) {
        return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_TableRow__WEBPACK_IMPORTED_MODULE_7__/* ["default"] */ .Z, {
            className: "text-transformation-none shop_orders_table",
            my: "1rem",
            padding: "6px 18px",
            children: [
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_Typography__WEBPACK_IMPORTED_MODULE_8__.H5, {
                    fontSize: "14px",
                    m: "6px",
                    ml: "0px",
                    textAlign: "left",
                    className: "order_h5",
                    children: item.orderId
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_Box__WEBPACK_IMPORTED_MODULE_4__["default"], {
                    ml: "-30px",
                    mr: "25px",
                    textAlign: "left",
                    className: "",
                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_Chip__WEBPACK_IMPORTED_MODULE_6__/* .Chip */ .A, {
                        p: "0.25rem 1rem",
                        style: {
                            width: "180px"
                        },
                        className: `bg2-${getColor(item.status)} ${!detail ? "order_class4 text-center" : " text-center d-flex justify-content-center"}`,
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_Typography__WEBPACK_IMPORTED_MODULE_8__.Small, {
                            children: item.statusName
                        })
                    })
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_Typography__WEBPACK_IMPORTED_MODULE_8__["default"], {
                    className: "flex-grow pre ",
                    m: "6px",
                    textAlign: "left",
                    children: item.delivery_time
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_Typography__WEBPACK_IMPORTED_MODULE_8__["default"], {
                    m: "6px",
                    ml: "30px",
                    textAlign: "left",
                    className: "whitespace-nowrap",
                    style: {
                        wordBreak: "break-word"
                    },
                    children: item.total_price
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "d-flex flex-nowrap align-items-center justify-content-end align-content-end",
                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_Typography__WEBPACK_IMPORTED_MODULE_8__["default"], {
                        textAlign: "center",
                        color: "",
                        children: [
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(reactstrap__WEBPACK_IMPORTED_MODULE_10__.Dropdown, {
                                className: "d-inline",
                                size: "small",
                                isOpen: open,
                                toggle: ()=>setopen(!open)
                                ,
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(reactstrap__WEBPACK_IMPORTED_MODULE_10__.DropdownToggle, {
                                        tag: "span",
                                        onClick: ()=>setopen(!open)
                                        ,
                                        "data-toggle": "dropdown",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_buttons_IconButton__WEBPACK_IMPORTED_MODULE_5__/* ["default"] */ .Z, {
                                            size: "too_small",
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_material_ui_icons__WEBPACK_IMPORTED_MODULE_9__.MoreHoriz, {
                                                style: {
                                                    fontSize: "15px"
                                                }
                                            })
                                        })
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(reactstrap__WEBPACK_IMPORTED_MODULE_10__.DropdownMenu, {
                                        right: true,
                                        children: statuses.map((val, ind)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(reactstrap__WEBPACK_IMPORTED_MODULE_10__.DropdownItem, {
                                                disabled: ind + 1 <= item.status,
                                                onClick: ()=>handleStatusChange(ind + 1)
                                                ,
                                                children: val
                                            })
                                        )
                                    })
                                ]
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_link__WEBPACK_IMPORTED_MODULE_2__["default"], {
                                href: detail ? `/vendor/orders/${item.orderId}` : `/orders/${item === null || item === void 0 ? void 0 : item.orderId}`,
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_buttons_IconButton__WEBPACK_IMPORTED_MODULE_5__/* ["default"] */ .Z, {
                                    size: "too_small",
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_material_ui_icons__WEBPACK_IMPORTED_MODULE_9__.ArrowForward, {
                                        style: {
                                            fontSize: "15px"
                                        }
                                    })
                                })
                            })
                        ]
                    })
                })
            ]
        }));
    } else {
        return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_TableRow__WEBPACK_IMPORTED_MODULE_7__/* ["default"] */ .Z, {
            className: "text-transformation-none shop_orders_table",
            my: "1rem",
            padding: "6px 18px",
            children: [
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_Typography__WEBPACK_IMPORTED_MODULE_8__.H5, {
                    fontSize: "14px",
                    m: "6px",
                    ml: "0px",
                    textAlign: "left",
                    className: "order_h5",
                    children: item.orderId
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_Box__WEBPACK_IMPORTED_MODULE_4__["default"], {
                    ml: "-30px",
                    mr: "37px",
                    textAlign: "left",
                    className: "",
                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_Chip__WEBPACK_IMPORTED_MODULE_6__/* .Chip */ .A, {
                        p: "0.25rem 1rem",
                        style: {
                            width: "180px"
                        },
                        className: `bg2-${getColor(item.status)} ${!detail ? "order_class4 text-center" : " text-center d-flex justify-content-center"}`,
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_Typography__WEBPACK_IMPORTED_MODULE_8__.Small, {
                            children: item.statusName
                        })
                    })
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_Typography__WEBPACK_IMPORTED_MODULE_8__["default"], {
                    className: "flex-grow pre ",
                    m: "6px",
                    textAlign: "left",
                    children: item.delivery_time
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_Typography__WEBPACK_IMPORTED_MODULE_8__["default"], {
                    m: "0px",
                    ml: "37px",
                    mr: "0px",
                    textAlign: "left",
                    className: "whitespace-nowrap",
                    style: {
                        wordBreak: "break-word"
                    },
                    children: item.total_price
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "d-flex flex-nowrap align-items-center justify-content-end align-content-end",
                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_Typography__WEBPACK_IMPORTED_MODULE_8__["default"], {
                        textAlign: "center",
                        color: "",
                        children: [
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(reactstrap__WEBPACK_IMPORTED_MODULE_10__.Dropdown, {
                                className: "d-inline",
                                size: "small",
                                isOpen: open,
                                toggle: ()=>setopen(!open)
                                ,
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(reactstrap__WEBPACK_IMPORTED_MODULE_10__.DropdownToggle, {
                                        tag: "span",
                                        onClick: ()=>setopen(!open)
                                        ,
                                        "data-toggle": "dropdown",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_buttons_IconButton__WEBPACK_IMPORTED_MODULE_5__/* ["default"] */ .Z, {
                                            size: "too_small",
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_material_ui_icons__WEBPACK_IMPORTED_MODULE_9__.MoreHoriz, {
                                                style: {
                                                    fontSize: "15px"
                                                }
                                            })
                                        })
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(reactstrap__WEBPACK_IMPORTED_MODULE_10__.DropdownMenu, {
                                        right: true,
                                        children: statuses.map((val, ind)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(reactstrap__WEBPACK_IMPORTED_MODULE_10__.DropdownItem, {
                                                disabled: ind + 1 <= item.status,
                                                onClick: ()=>handleStatusChange(ind + 1)
                                                ,
                                                children: val
                                            })
                                        )
                                    })
                                ]
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_link__WEBPACK_IMPORTED_MODULE_2__["default"], {
                                href: detail ? `/vendor/orders/${item.orderId}` : `/orders/${item === null || item === void 0 ? void 0 : item.orderId}`,
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_buttons_IconButton__WEBPACK_IMPORTED_MODULE_5__/* ["default"] */ .Z, {
                                    size: "too_small",
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_material_ui_icons__WEBPACK_IMPORTED_MODULE_9__.ArrowForward, {
                                        style: {
                                            fontSize: "15px"
                                        }
                                    })
                                })
                            })
                        ]
                    })
                })
            ]
        }));
    }
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (OrderRow);


/***/ })

};
;