"use strict";
exports.id = 5326;
exports.ids = [5326];
exports.modules = {

/***/ 4668:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Container__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(9221);
/* harmony import */ var _grid_Grid__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(8167);
/* harmony import */ var _hidden_Hidden__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3518);
/* harmony import */ var _AppLayout__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(7198);
/* harmony import */ var _CustomerDashboardNavigation__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(6106);







const CustomerDashboardLayout = ({ title , description , keyword , children  })=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_AppLayout__WEBPACK_IMPORTED_MODULE_5__["default"], {
        title: title,
        description: description,
        keyword: keyword,
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_Container__WEBPACK_IMPORTED_MODULE_2__["default"], {
            my: "2rem",
            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_grid_Grid__WEBPACK_IMPORTED_MODULE_3__["default"], {
                container: true,
                spacing: 6,
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_hidden_Hidden__WEBPACK_IMPORTED_MODULE_4__/* ["default"] */ .Z, {
                        as: _grid_Grid__WEBPACK_IMPORTED_MODULE_3__["default"],
                        item: true,
                        lg: 3,
                        xs: 12,
                        down: 1024,
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_CustomerDashboardNavigation__WEBPACK_IMPORTED_MODULE_6__/* ["default"] */ .Z, {
                        })
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_grid_Grid__WEBPACK_IMPORTED_MODULE_3__["default"], {
                        item: true,
                        lg: 9,
                        xs: 12,
                        children: children
                    })
                ]
            })
        })
    })
;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CustomerDashboardLayout);


/***/ }),

/***/ 6106:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_Box__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4875);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1853);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _FlexBox__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(5359);
/* harmony import */ var _icon_Icon__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(4154);
/* harmony import */ var _Typography__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(9374);
/* harmony import */ var _DashboardStyle__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(5129);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(6022);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _Redux_Actions_LogoutAction__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(8516);
/* harmony import */ var _components_avatar_Avatar__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(3392);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(2167);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(6734);
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(js_cookie__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _contexts_app_AppContext__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(8936);
/* harmony import */ var _Redux_Actions_get_cart_products__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(903);
/* harmony import */ var _hooks_useWindowSize__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(12);
/* harmony import */ var _Redux_Actions_Action__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(7079);
/* harmony import */ var react_intl__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(3126);
/* harmony import */ var react_intl__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(react_intl__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var _Redux_Actions_change_modal_status__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(4726);



















const CustomerDashboardNavigation = ()=>{
    var ref, ref1, ref2, ref3, ref4, ref5, ref6;
    const user2 = (0,react_redux__WEBPACK_IMPORTED_MODULE_8__.useSelector)((state)=>state.token.user
    );
    let modal = (0,react_redux__WEBPACK_IMPORTED_MODULE_8__.useSelector)((state)=>state.new.modal
    );
    const dispatch2 = (0,react_redux__WEBPACK_IMPORTED_MODULE_8__.useDispatch)();
    const width = (0,_hooks_useWindowSize__WEBPACK_IMPORTED_MODULE_13__/* ["default"] */ .Z)();
    const router = (0,next_router__WEBPACK_IMPORTED_MODULE_2__.useRouter)();
    let intl = (0,react_intl__WEBPACK_IMPORTED_MODULE_14__.useIntl)();
    let lang = router.locale;
    //Cart Things
    const { state: state1 , dispatch  } = (0,_contexts_app_AppContext__WEBPACK_IMPORTED_MODULE_12__/* .useAppContext */ .bp)();
    const { cartList  } = state1.cart;
    const handleCartAmountChange2 = (amount, product)=>{
        dispatch({
            type: "CHANGE_CART_AMOUNT",
            payload: {
                ...product,
                qty: amount
            }
        });
    };
    const handleremovecart = ()=>{
        try {
            cartList.forEach((value)=>{
                handleCartAmountChange2(0, value);
            });
        } catch  {
            return null;
        }
        if (js_cookie__WEBPACK_IMPORTED_MODULE_11___default().get("isLoggedIn") === "true") {
            axios__WEBPACK_IMPORTED_MODULE_10___default()(`/orders/basket-list/${lang}`).then((response)=>{
                dispatch2((0,_Redux_Actions_get_cart_products__WEBPACK_IMPORTED_MODULE_15__/* ["default"] */ .Z)(response.data));
            }).catch(()=>null
            );
        }
    };
    const logout2 = ()=>{
        axios__WEBPACK_IMPORTED_MODULE_10___default()({
            method: "POST",
            url: `/login/logout`
        }).then(()=>{
            js_cookie__WEBPACK_IMPORTED_MODULE_11___default().remove("isLoggedIn");
            js_cookie__WEBPACK_IMPORTED_MODULE_11___default().remove("token");
            dispatch2((0,_Redux_Actions_LogoutAction__WEBPACK_IMPORTED_MODULE_16__/* ["default"] */ .Z)());
            dispatch2((0,_Redux_Actions_Action__WEBPACK_IMPORTED_MODULE_17__/* .fetch_user_info */ .W)({
            }));
            handleremovecart();
            handleremovecart();
            if (router.pathname.startsWith("/profile") || router.pathname.startsWith("/vendor") || router.pathname.startsWith("/wish-list")) {
                //--------------------- Clearing data from carts---------------------------
                router.push('/');
            }
        }).catch(()=>{
            return null;
        });
    };
    if (user2 === null || user2 === void 0 ? void 0 : user2.errors) {
        let loggedin = ((js_cookie__WEBPACK_IMPORTED_MODULE_11___default()) === null || (js_cookie__WEBPACK_IMPORTED_MODULE_11___default()) === void 0 ? void 0 : js_cookie__WEBPACK_IMPORTED_MODULE_11___default().get("isLoggedIn")) || "false";
        if (loggedin === "true" && !js_cookie__WEBPACK_IMPORTED_MODULE_11___default().get("token")) {
            dispatch2((0,_Redux_Actions_LogoutAction__WEBPACK_IMPORTED_MODULE_16__/* ["default"] */ .Z)());
            router.push("/");
        }
    }
    const { pathname  } = (0,next_router__WEBPACK_IMPORTED_MODULE_2__.useRouter)();
    //LinkLists
    const linkList = [
        {
            title: "",
            list: [
                {
                    href: "/profile/edit",
                    title: intl.formatMessage({
                        id: "edit_p"
                    }),
                    iconName: "user-profile"
                },
                {
                    href: (user2 === null || user2 === void 0 ? void 0 : (ref = user2.data) === null || ref === void 0 ? void 0 : ref.is_shops) ? "/vendor/products/page/1" : "/vendor/create",
                    title: (user2 === null || user2 === void 0 ? void 0 : (ref1 = user2.data) === null || ref1 === void 0 ? void 0 : ref1.is_shops) !== null ? intl.formatMessage({
                        id: "business_account"
                    }) : intl.formatMessage({
                        id: "create_shop"
                    }),
                    iconName: "crown"
                },
                {
                    href: "/wish-list",
                    title: intl.formatMessage({
                        id: "wishlist"
                    }),
                    iconName: "heart"
                },
                {
                    href: "/orders/page/1",
                    title: intl.formatMessage({
                        id: "orders"
                    }),
                    iconName: "cart"
                }, 
            ]
        }, 
    ];
    const linkList2 = [
        {
            title: "",
            list: [
                {
                    href: "/profile/budget",
                    title: intl.formatMessage({
                        id: "hisob"
                    }),
                    iconName: "money-transfer"
                },
                {
                    href: "/company/agreement",
                    title: intl.formatMessage({
                        id: "document"
                    }),
                    iconName: "file"
                },
                {
                    href: "/company/dostavka",
                    title: intl.formatMessage({
                        id: "deliver"
                    }),
                    iconName: "truck"
                },
                {
                    href: "/help",
                    title: intl.formatMessage({
                        id: "need_help"
                    }),
                    iconName: "help"
                }, 
            ]
        }, 
    ];
    //linklists
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_DashboardStyle__WEBPACK_IMPORTED_MODULE_7__/* .DashboardNavigationWrapper */ .nX, {
        px: "0px",
        pb: "1.5rem",
        color: "gray.900",
        children: [
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_FlexBox__WEBPACK_IMPORTED_MODULE_4__["default"], {
                p: "14px 32px",
                alignItems: "center",
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_avatar_Avatar__WEBPACK_IMPORTED_MODULE_9__/* ["default"] */ .Z, {
                        src: (user2 === null || user2 === void 0 ? void 0 : (ref2 = user2.data) === null || ref2 === void 0 ? void 0 : ref2.avatar) ? user2 === null || user2 === void 0 ? void 0 : (ref3 = user2.data) === null || ref3 === void 0 ? void 0 : ref3.avatar : "https://api.dana.uz/storage/images/noimg.jpg",
                        size: 64
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_Box__WEBPACK_IMPORTED_MODULE_1__["default"], {
                        ml: "12px",
                        flex: "1 1 0",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_FlexBox__WEBPACK_IMPORTED_MODULE_4__["default"], {
                            flexWrap: "wrap",
                            justifyContent: "space-between",
                            alignItems: "center",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_Typography__WEBPACK_IMPORTED_MODULE_6__.H5, {
                                    my: "0px",
                                    children: (user2 === null || user2 === void 0 ? void 0 : (ref4 = user2.data) === null || ref4 === void 0 ? void 0 : ref4.fullname) ? user2 === null || user2 === void 0 ? void 0 : (ref5 = user2.data) === null || ref5 === void 0 ? void 0 : ref5.fullname : user2 === null || user2 === void 0 ? void 0 : (ref6 = user2.data) === null || ref6 === void 0 ? void 0 : ref6.phone
                                })
                            })
                        })
                    })
                ]
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {
            }),
            linkList.map((item1)=>/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react__WEBPACK_IMPORTED_MODULE_3__.Fragment, {
                    children: [
                        item1.list.map((item)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_DashboardStyle__WEBPACK_IMPORTED_MODULE_7__/* .StyledDashboardNav */ .Wt, {
                                isCurrentPath: pathname.includes(item.href),
                                href: item.href,
                                px: "1.5rem",
                                mb: "1.25rem",
                                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_FlexBox__WEBPACK_IMPORTED_MODULE_4__["default"], {
                                    alignItems: "center",
                                    children: [
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_Box__WEBPACK_IMPORTED_MODULE_1__["default"], {
                                            className: "dashboard-nav-icon-holder",
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_icon_Icon__WEBPACK_IMPORTED_MODULE_5__["default"], {
                                                variant: "small",
                                                defaultcolor: "currentColor",
                                                mr: "10px",
                                                children: item.iconName
                                            })
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                            children: item.title
                                        })
                                    ]
                                })
                            }, item.title)
                        ),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_DashboardStyle__WEBPACK_IMPORTED_MODULE_7__/* .StyledDashboardNav */ .Wt, {
                            onClick: ()=>dispatch2((0,_Redux_Actions_change_modal_status__WEBPACK_IMPORTED_MODULE_18__/* ["default"] */ .Z)(!modal))
                            ,
                            isCurrentPath: false,
                            href: "#",
                            px: "1.5rem",
                            mb: "1.25rem",
                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_FlexBox__WEBPACK_IMPORTED_MODULE_4__["default"], {
                                alignItems: "center",
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_Box__WEBPACK_IMPORTED_MODULE_1__["default"], {
                                        className: "dashboard-nav-icon-holder",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_icon_Icon__WEBPACK_IMPORTED_MODULE_5__["default"], {
                                            variant: "small",
                                            defaultcolor: "currentColor",
                                            mr: "10px",
                                            children: "order_s2"
                                        })
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                        children: intl.formatMessage({
                                            id: "order_status"
                                        })
                                    })
                                ]
                            })
                        }, intl.formatMessage({
                            id: "order_status"
                        }))
                    ]
                }, item1.title)
            ),
            linkList2.map((item2)=>/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react__WEBPACK_IMPORTED_MODULE_3__.Fragment, {
                    children: [
                        item2.list.map((item)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_DashboardStyle__WEBPACK_IMPORTED_MODULE_7__/* .StyledDashboardNav */ .Wt, {
                                isCurrentPath: pathname.includes(item.href),
                                href: item.href,
                                px: "1.5rem",
                                mb: "1.25rem",
                                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_FlexBox__WEBPACK_IMPORTED_MODULE_4__["default"], {
                                    alignItems: "center",
                                    children: [
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_Box__WEBPACK_IMPORTED_MODULE_1__["default"], {
                                            className: "dashboard-nav-icon-holder",
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_icon_Icon__WEBPACK_IMPORTED_MODULE_5__["default"], {
                                                variant: "small",
                                                defaultcolor: "currentColor",
                                                mr: "10px",
                                                children: item.iconName
                                            })
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                            children: item.title
                                        })
                                    ]
                                })
                            }, item.title)
                        ),
                        width < 650 ? /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_DashboardStyle__WEBPACK_IMPORTED_MODULE_7__/* .StyledDashboardNav */ .Wt, {
                            // isCurrentPath={pathname.includes("/profile")}
                            href: "#",
                            px: "1.5rem",
                            mb: "1.25rem",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                onClick: logout2,
                                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_FlexBox__WEBPACK_IMPORTED_MODULE_4__["default"], {
                                    alignItems: "center",
                                    children: [
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_Box__WEBPACK_IMPORTED_MODULE_1__["default"], {
                                            className: "dashboard-nav-icon-holder",
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_icon_Icon__WEBPACK_IMPORTED_MODULE_5__["default"], {
                                                variant: "small",
                                                defaultcolor: "currentColor",
                                                mr: "10px",
                                                children: "logout"
                                            })
                                        }),
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                                            children: [
                                                intl.formatMessage({
                                                    id: "logout"
                                                }),
                                                " "
                                            ]
                                        })
                                    ]
                                })
                            })
                        }, "Log Out") : ""
                    ]
                }, item2.title)
            )
        ]
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CustomerDashboardNavigation);


/***/ }),

/***/ 5376:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _hooks_useWindowSize__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(12);
/* harmony import */ var _Box__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(4875);
/* harmony import */ var _FlexBox__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(5359);
/* harmony import */ var _icon_Icon__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(4154);
/* harmony import */ var _sidenav_Sidenav__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(1211);
/* harmony import */ var _Typography__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(9374);
/* harmony import */ var _CustomerDashboardNavigation__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(6106);









const DashboardPageHeader = ({ iconName , title , button ,  })=>{
    const width = (0,_hooks_useWindowSize__WEBPACK_IMPORTED_MODULE_2__/* ["default"] */ .Z)();
    const isTablet = width < 1025;
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_Box__WEBPACK_IMPORTED_MODULE_3__["default"], {
        mb: "1.5rem",
        mt: "-1rem",
        children: [
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_FlexBox__WEBPACK_IMPORTED_MODULE_4__["default"], {
                justifyContent: "space-between",
                alignItems: "center",
                mt: "1rem",
                children: [
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_FlexBox__WEBPACK_IMPORTED_MODULE_4__["default"], {
                        alignItems: "center",
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_icon_Icon__WEBPACK_IMPORTED_MODULE_5__["default"], {
                                color: "primary",
                                children: iconName
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_Typography__WEBPACK_IMPORTED_MODULE_7__.H1, {
                                ml: "12px",
                                my: "0px",
                                lineHeight: "1",
                                whitespace: "pre",
                                children: title
                            })
                        ]
                    }),
                    isTablet && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_sidenav_Sidenav__WEBPACK_IMPORTED_MODULE_6__/* ["default"] */ .Z, {
                        position: "left",
                        handle: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_icon_Icon__WEBPACK_IMPORTED_MODULE_5__["default"], {
                            mx: "1rem",
                            children: "menu"
                        }),
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_CustomerDashboardNavigation__WEBPACK_IMPORTED_MODULE_8__/* ["default"] */ .Z, {
                        })
                    }),
                    !isTablet && button
                ]
            }),
            isTablet && !!button && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_Box__WEBPACK_IMPORTED_MODULE_3__["default"], {
                mt: "1rem",
                children: button
            })
        ]
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (DashboardPageHeader);


/***/ })

};
;