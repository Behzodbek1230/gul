import Box from "../../components/Box";
import { useRouter } from "next/router";
import React from "react";
import FlexBox from "../FlexBox";
import Icon from "../icon/Icon";
import {
  DashboardNavigationWrapper,
  StyledDashboardNav,
} from "./DashboardStyle";
import { useSelector } from "react-redux";
import {useIntl,FormattedMessage} from "react-intl";

const CompanyDashboardNavigation = () => {
  let router = useRouter()
  let {asPath} = router
  let footer_sections = useSelector((state:any)=>state.new.footer)
  const linkList = [];
  let intl = useIntl()
  
    if(footer_sections.length !==0){
        footer_sections.about_company?.map(one=>
            linkList.push(
                {
                    href: `/company/${one.keyword}`,
                    title: one.name,
                },
            )
        )
        footer_sections.services?.map(one=>
            linkList.push(
                {
                    href: `/company/${one.keyword}`,
                    title: one.name,
                },
            )
        )
    }
    
  return (
    <DashboardNavigationWrapper px="0px" py="1.5rem" color="gray.900">
      {linkList.map((item) => (
        <StyledDashboardNav
          isCurrentPath={asPath===item.href}
          href={item.href}
          key={item.title}
          px="1.5rem"
          mb="1.25rem"
        >
          <FlexBox alignItems="center">
            <Box className="dashboard-nav-icon-holder">
              <Icon variant="small" defaultcolor="currentColor" mr="10px">
                {item.iconName}
              </Icon>
            </Box>
            <span>{item.title}</span>
          </FlexBox>
        </StyledDashboardNav>
      ))}

    <StyledDashboardNav
        isCurrentPath={asPath==="/sitemap"}
        href="/sitemap"
        px="2rem"
        mb="1.25rem"
    >
        <FlexBox alignItems="center">
            <Box className="dashboard-nav-icon-holder">
            </Box>
            <span>{intl.formatMessage({id:"Sitemap"})}</span>
        </FlexBox>
    </StyledDashboardNav>
    <StyledDashboardNav
        isCurrentPath={asPath==="/corporate"}
        href="/corporate"
        px="2rem"
        mb="1.25rem"
    >
        <FlexBox alignItems="center">
            <Box className="dashboard-nav-icon-holder">
            </Box>
            <span>{intl.formatMessage({id:"Corparative Sales"})}</span>
        </FlexBox>
    </StyledDashboardNav>

      <StyledDashboardNav
        isCurrentPath={asPath==="/carriers"}
        href="/carriers"
        px="2rem"
        mb="1.25rem"
        >
            <FlexBox alignItems="center">
                <Box className="dashboard-nav-icon-holder">
                </Box>
                <span>{intl.formatMessage({id:"Carriers"})}</span>
            </FlexBox>
        </StyledDashboardNav>
        <StyledDashboardNav
            isCurrentPath={asPath==="/faq"}
            href="/faq"
            px="2rem"
            mb="1.25rem"
        >
            <FlexBox alignItems="center">
                <Box className="dashboard-nav-icon-holder">
                </Box>
                <span>FAQ</span>
            </FlexBox>
        </StyledDashboardNav>
        <StyledDashboardNav
            isCurrentPath={asPath==="/blogs"}
            href="/blogs"
            px="2rem"
            mb="1.25rem"
        >
            <FlexBox alignItems="center">
                <Box className="dashboard-nav-icon-holder">
                </Box>
                <span><FormattedMessage id="blogs" /></span>
            </FlexBox>
        </StyledDashboardNav>
        <StyledDashboardNav
                isCurrentPath={asPath==="/help"}
                href="/help"
                px="2rem"
                mb="1.25rem"
        >
                <FlexBox alignItems="center">
                    <Box className="dashboard-nav-icon-holder">
                    </Box>
                    <span>{intl.formatMessage({id:"help"})}</span>
                </FlexBox>
        </StyledDashboardNav>
    </DashboardNavigationWrapper>
  );
};


export default CompanyDashboardNavigation;
