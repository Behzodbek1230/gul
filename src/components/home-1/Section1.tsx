const Carousel = dynamic(()=>import('../../components/carousel/Carousel'),{ssr:false})
const Box = dynamic(()=>import('../../components/Box'),{ssr:false})
const Typography = dynamic(()=>import('../../components/Typography'),{ssr:false})
import React, { Fragment, useEffect, useState } from "react";
import {useDispatch} from "react-redux";
import useWindowSize from "../../hooks/useWindowSize";
import {FormattedMessage} from "react-intl";
import { useRouter } from "next/router";
import { LazyLoadImage } from "react-lazy-load-image-component";
import axios from "axios";
import get_banner from "../../../Redux/Actions/get_banner";
import { InView } from 'react-intersection-observer'
import dynamic from "next/dynamic";


const Section1= () => {
  let router = useRouter()
  const width = useWindowSize()
  let [banner,setbanner] = useState([])
  let dispatch = useDispatch()
  let lang = router.locale
  useEffect(()=>{
    axios.get(`/banner/list/${lang}`)
        .then(res=>{
            if(res.data.length >= 1){
                let banner_id = res?.data?.length !== 0  && res?.data? res?.data[0]?.id : ""
                axios({
                    method:"GET",
                    url:`/banner/item-list/${banner_id}/${lang}`,
                })
                    .then(res=>{
                        setbanner(res.data)
                        dispatch(get_banner(res.data))
                    })
                    .catch(()=>{
                        return null;

                    })
            }
        })
        .catch(()=>null)
  },[])
  if(banner?.length !==0 && banner) {
      return (
          <Fragment >
              <Box mb="30px" mt="9px" bg="gray.white">
                  <Carousel
                      totalSlides={banner?.length || 0}
                      visibleSlides={1}
                      infinite={true}
                      autoPlay={true}
                      showDots={true}
                      showArrow={false}
                      spacing="0px"
                      interval={4000}
                      dotGroupMarginTop="-30px"
                  >
                      {banner?.map(banners =>
                          (
                             <InView triggerOnce={true}>
                                 {(props)=>(
                                    <div
                                      ref={props.ref}
                                      key={banners?.id}
                                      style={{position: "relative"}}
                                    >
    
                                     {props.inView && (
                                         <>
                                            <div className='banner_index_carousel_style'>
                                                <LazyLoadImage
                                                    style={{
                                                        width: "100%",
                                                        height: "auto",
                                                        objectFit: "cover",
                                                        objectPosition: "center"
                                                    }}
                                                    src={width < 650 ? banners.img_s : banners.img}
                                                    alt={banners.title}
                                                    className="category_image"
                                                    visibleByDefault={false}
                                                    threshold={0}
                                                    effect="blur"
                                                    placeholderSrc=""
                                                />
                                            </div>
        
                                            <div
        
                                                className="mt-1"
                                                style={width < 750 ? {
                                                        position: "absolute",
                                                        top: "10%",
                                                        left: "10%",
                                                        color: banners.text_color || "red",
                                                        fontSize: "small"
                                                    } :
                                                    {
                                                        position: "absolute",
                                                        top: "10%",
                                                        left: "10%",
                                                        color: banners.text_color || "red",
                                                    }}
                                            >
                                                <h2
                                                    className="title fs-sm-6  banner_title banner_title_h2"
                                                >
                                                    {banners.title}
                                                </h2>
                                                <Typography
                                                    className='banner_description'
                                                    mb="0.35rem"
                                                >
                                                    {banners.description}
                                                </Typography>
                                                <button
                                                    onClick={() => router.push(banners.url)}
                                                    className="btn btn-banner px-0"
                                                    style={width < 750 ?
                                                        {
                                                            backgroundColor: banners.button_color || "red",
                                                            color: banners.text_color || "red",
                                                            position: "absolute",
                                                            width: "100px",
                                                            paddingTop: "2px",
                                                            paddingBottom: "2px",
                                                            paddingLeft: "0px",
                                                            paddingRight: "0px",
                                                            fontSize: "12px",
                                                            top: "45px"
                                                        }
                                                        :
                                                        {
                                                            backgroundColor: banners.button_color || "red",
                                                            color: banners.text_color || "red",
                                                            position: "absolute",
                                                            top: "100px",
                                                            width: "160px"
                                                        }
                                                    }
                                                >
                                                    <FormattedMessage
                                                        id="banner_button"
                                                        defaultMessage="Visit Collection"
                                                    />
                                                </button>
                                            </div>
                                         </>
                                     )}
                                  </div>
                                 )}
                             </InView>
                          )
                      )}
                  </Carousel>

              </Box>
          </Fragment>
      );
  }
  else{
      return <></>
  }
};

export default Section1;
