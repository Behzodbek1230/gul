"use strict";
exports.id = 3887;
exports.ids = [3887];
exports.modules = {

/***/ 2933:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "s": () => (/* binding */ Card1)
/* harmony export */ });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Card__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4660);


const Card1 = styled_components__WEBPACK_IMPORTED_MODULE_0___default()(_Card__WEBPACK_IMPORTED_MODULE_1__["default"])`
  position: relative;
  padding: 1.5rem 1.75rem;
  @media only screen and (max-width: 678px) {
    padding: 1rem;
  }
`;


/***/ }),

/***/ 8174:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_yandex_maps__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3742);
/* harmony import */ var react_yandex_maps__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_yandex_maps__WEBPACK_IMPORTED_MODULE_2__);


// import ReactDOM from "react-dom";

const Map3 = (props)=>{
    let coordinate = props.coordinate === null && !(props === null || props === void 0 ? void 0 : props.coordinate) ? [
        41.316441,
        69.294861
    ] : props.coordinate;
    const mapState = {
        center: coordinate,
        zoom: 12
    };
    const mapRef = (0,react__WEBPACK_IMPORTED_MODULE_1__.useRef)(null);
    const searchRef = (0,react__WEBPACK_IMPORTED_MODULE_1__.useRef)(null);
    const handleplacemark = ()=>{
        if (props.coordinate !== null) {
            return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_yandex_maps__WEBPACK_IMPORTED_MODULE_2__.Placemark, {
                onDrag: (e)=>props.set(e._sourceEvent.originalEvent.coords)
                ,
                options: {
                    draggable: true
                },
                geometry: coordinate
            }));
        }
    };
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        className: "App",
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_yandex_maps__WEBPACK_IMPORTED_MODULE_2__.YMaps, {
            version: "2.1.77",
            options: {
                width: '100%'
            },
            query: {
                lang: 'en_RU',
                apikey: '7acd9319-78e1-4abe-a6af-d64c62828777'
            },
            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_yandex_maps__WEBPACK_IMPORTED_MODULE_2__.Map, {
                onClick: (e)=>props.set(e._sourceEvent.originalEvent.coords)
                ,
                width: "100%",
                height: "400px",
                modules: [
                    "multiRouter.MultiRoute",
                    "coordSystem.geo",
                    "geocode",
                    "util.bounds"
                ],
                state: mapState,
                instanceRef: (ref)=>{
                    if (ref) {
                        mapRef.current = ref;
                    }
                },
                children: [
                    handleplacemark(),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_yandex_maps__WEBPACK_IMPORTED_MODULE_2__.SearchControl, {
                        instanceRef: (ref)=>{
                            if (ref) searchRef.current = ref;
                        },
                        options: {
                            float: "right",
                            provider: "yandex#search",
                            size: "large"
                        }
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_yandex_maps__WEBPACK_IMPORTED_MODULE_2__.ZoomControl, {
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_yandex_maps__WEBPACK_IMPORTED_MODULE_2__.GeolocationControl, {
                    })
                ]
            })
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Map3);


/***/ })

};
;