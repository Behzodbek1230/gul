"use strict";
(() => {
var exports = {};
exports.id = 5405;
exports.ids = [5405];
exports.modules = {

/***/ 9382:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
const get_banner = (data)=>{
    return {
        type: "GET_BANNER",
        payload: data
    };
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (get_banner);


/***/ }),

/***/ 8392:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
let get_seo_text = (data)=>{
    return {
        type: 'GET_SEO_TEXT',
        payload: data
    };
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (get_seo_text);


/***/ }),

/***/ 2293:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ pages),
  "getStaticProps": () => (/* binding */ getStaticProps)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: ../node_modules/next/dynamic.js
var dynamic = __webpack_require__(5218);
// EXTERNAL MODULE: external "axios"
var external_axios_ = __webpack_require__(2167);
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_);
// EXTERNAL MODULE: external "react-redux"
var external_react_redux_ = __webpack_require__(6022);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "js-cookie"
var external_js_cookie_ = __webpack_require__(6734);
var external_js_cookie_default = /*#__PURE__*/__webpack_require__.n(external_js_cookie_);
// EXTERNAL MODULE: ./hooks/useWindowSize.js
var useWindowSize = __webpack_require__(12);
;// CONCATENATED MODULE: ../Redux/Actions/get_category_products.js
const get_category_products = (data)=>{
    return {
        type: "GET_CATEGORY_PRODUCTS",
        payload: data
    };
};
/* harmony default export */ const Actions_get_category_products = (get_category_products);

// EXTERNAL MODULE: ../Redux/Actions/Action.js
var Action = __webpack_require__(7079);
// EXTERNAL MODULE: ../Redux/Actions/get_banner.js
var get_banner = __webpack_require__(9382);
// EXTERNAL MODULE: external "next-seo"
var external_next_seo_ = __webpack_require__(6641);
// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(1853);
// EXTERNAL MODULE: ../Redux/Actions/get_seo_text.js
var get_seo_text = __webpack_require__(8392);
// EXTERNAL MODULE: external "next-cookies"
var external_next_cookies_ = __webpack_require__(7486);
var external_next_cookies_default = /*#__PURE__*/__webpack_require__.n(external_next_cookies_);
// EXTERNAL MODULE: external "react-intersection-observer"
var external_react_intersection_observer_ = __webpack_require__(9785);
// EXTERNAL MODULE: ./components/Variables.tsx
var Variables = __webpack_require__(5153);
;// CONCATENATED MODULE: ./pages/index.tsx
















const Section1 = (0,dynamic["default"])(()=>__webpack_require__.e(/* import() */ 2519).then(__webpack_require__.bind(__webpack_require__, 2519))
, {
    loadableGenerated: {
        webpack: ()=>[
                /*require.resolve*/(2519)
            ]
        ,
        modules: [
            "index.tsx -> " + "../components/home-1/Section1"
        ]
    },
    ssr: false
});
const Section12 = (0,dynamic["default"])(()=>__webpack_require__.e(/* import() */ 2184).then(__webpack_require__.bind(__webpack_require__, 2184))
, {
    loadableGenerated: {
        webpack: ()=>[
                /*require.resolve*/(2184)
            ]
        ,
        modules: [
            "index.tsx -> " + "../components/home-1/Section12"
        ]
    },
    ssr: false
});
const Section15 = (0,dynamic["default"])(()=>__webpack_require__.e(/* import() */ 6681).then(__webpack_require__.bind(__webpack_require__, 6681))
, {
    loadableGenerated: {
        webpack: ()=>[
                /*require.resolve*/(6681)
            ]
        ,
        modules: [
            "index.tsx -> " + "../components/home-1/Section15"
        ]
    },
    ssr: false
});
const Section2 = (0,dynamic["default"])(()=>Promise.all(/* import() */[__webpack_require__.e(2652), __webpack_require__.e(9894), __webpack_require__.e(1972), __webpack_require__.e(9374), __webpack_require__.e(4154), __webpack_require__.e(8661), __webpack_require__.e(7272), __webpack_require__.e(9868), __webpack_require__.e(966)]).then(__webpack_require__.bind(__webpack_require__, 3211))
, {
    loadableGenerated: {
        webpack: ()=>[
                /*require.resolve*/(3211)
            ]
        ,
        modules: [
            "index.tsx -> " + "../components/home-1/Section2"
        ]
    },
    ssr: false
});
const Section3 = (0,dynamic["default"])(()=>__webpack_require__.e(/* import() */ 448).then(__webpack_require__.bind(__webpack_require__, 448))
, {
    loadableGenerated: {
        webpack: ()=>[
                /*require.resolve*/(448)
            ]
        ,
        modules: [
            "index.tsx -> " + "../components/home-1/Section3"
        ]
    },
    ssr: false
});
const AppLayout = (0,dynamic["default"])(()=>Promise.all(/* import() */[__webpack_require__.e(2652), __webpack_require__.e(9894), __webpack_require__.e(8579), __webpack_require__.e(1972), __webpack_require__.e(9374), __webpack_require__.e(4154), __webpack_require__.e(2389), __webpack_require__.e(8936), __webpack_require__.e(8661), __webpack_require__.e(695), __webpack_require__.e(4396), __webpack_require__.e(8414), __webpack_require__.e(4790), __webpack_require__.e(7198)]).then(__webpack_require__.bind(__webpack_require__, 7198))
, {
    loadableGenerated: {
        webpack: ()=>[
                /*require.resolve*/(7198)
            ]
        ,
        modules: [
            "index.tsx -> " + "../components/layout/AppLayout"
        ]
    },
    ssr: false
});
const SeoText = (0,dynamic["default"])(()=>Promise.all(/* import() */[__webpack_require__.e(1972), __webpack_require__.e(6253), __webpack_require__.e(2933)]).then(__webpack_require__.bind(__webpack_require__, 6253))
, {
    loadableGenerated: {
        webpack: ()=>[
                /*require.resolve*/(6253)
            ]
        ,
        modules: [
            "index.tsx -> " + "../components/SeoText"
        ]
    },
    ssr: false
});
const Loading = (0,dynamic["default"])(()=>__webpack_require__.e(/* import() */ 6648).then(__webpack_require__.bind(__webpack_require__, 6648))
, {
    loadableGenerated: {
        webpack: ()=>[
                /*require.resolve*/(6648)
            ]
        ,
        modules: [
            "index.tsx -> " + "../components/Loading"
        ]
    },
    ssr: false
});
const IndexPage = ({ banner , category_products , shop_list , setcategory , data2 , setbanner , category_products2 , home_seo  })=>{
    const dispatch = (0,external_react_redux_.useDispatch)();
    (0,external_react_.useEffect)(()=>{
        setcategory("");
        dispatch((0,get_banner/* default */.Z)(banner));
        dispatch((0,Action/* fetch_user_info */.W)(data2));
        dispatch(Actions_get_category_products(category_products2));
        setseo(home_seo);
        console.log(shop_list, setbanner, category_products);
    }, []);
    const size = (0,useWindowSize/* default */.Z)();
    let router = (0,router_.useRouter)();
    let lang = router.locale;
    let { 0: seo , 1: setseo  } = (0,external_react_.useState)(home_seo);
    let { 0: loading2 , 1: setloading2  } = (0,external_react_.useState)(false);
    let { 0: categoryProducts , 1: setcategoryProducts  } = (0,external_react_.useState)(category_products2);
    let currency_id = external_js_cookie_default().get("currency_id");
    let currency_text = typeof currency_id !== "undefined" ? `?currency=${currency_id}` : "";
    let CategoryFetch = (0,external_react_.useCallback)(()=>{
        external_axios_default().get(`/flowers/category-item/${router.locale}${currency_text}`).then((res)=>{
            dispatch(Actions_get_category_products(res.data));
            setcategoryProducts(res.data);
            setloading2(false);
        }).catch(()=>setloading2(false)
        );
    }, [
        currency_id,
        lang
    ]);
    (0,external_react_.useEffect)(()=>{
        CategoryFetch();
    }, [
        currency_id,
        lang
    ]);
    let PageDataChangeWhenChanged = (0,external_react_.useCallback)(()=>{
        external_axios_default().get(`/seo/home-page/${lang}`).then((res)=>{
            var ref;
            dispatch((0,get_seo_text/* default */.Z)(res === null || res === void 0 ? void 0 : (ref = res.data) === null || ref === void 0 ? void 0 : ref.site_settings_main_seotext));
            setseo(res.data);
        }).catch(()=>null
        );
    }, [
        lang
    ]);
    (0,external_react_.useEffect)(()=>{
        PageDataChangeWhenChanged();
    }, [
        lang
    ]);
    if (loading2) {
        return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
            children: [
                /*#__PURE__*/ jsx_runtime_.jsx(external_next_seo_.NextSeo, {
                    title: seo === null || seo === void 0 ? void 0 : seo.site_settings_main_title,
                    description: seo === null || seo === void 0 ? void 0 : seo.site_settings_main_description,
                    additionalMetaTags: [
                        {
                            name: 'keyword',
                            content: seo === null || seo === void 0 ? void 0 : seo.site_settings_main_keyword
                        }, 
                    ],
                    openGraph: {
                        images: [
                            {
                                url: seo === null || seo === void 0 ? void 0 : seo.logo,
                                alt: "Dana.uz",
                                width: 600,
                                height: 200
                            }
                        ]
                    }
                }),
                /*#__PURE__*/ jsx_runtime_.jsx(Loading, {
                })
            ]
        }));
    } else {
        return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(AppLayout, {
            children: [
                /*#__PURE__*/ jsx_runtime_.jsx(external_next_seo_.NextSeo, {
                    title: seo === null || seo === void 0 ? void 0 : seo.site_settings_main_title,
                    description: seo === null || seo === void 0 ? void 0 : seo.site_settings_main_description,
                    additionalMetaTags: [
                        {
                            name: 'keyword',
                            content: seo === null || seo === void 0 ? void 0 : seo.site_settings_main_keyword
                        }, 
                    ],
                    openGraph: {
                        images: [
                            {
                                url: seo === null || seo === void 0 ? void 0 : seo.logo,
                                alt: "Dana.uz",
                                width: 600,
                                height: 200
                            }
                        ]
                    }
                }),
                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    children: [
                        size < 650 ? /*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx(external_react_intersection_observer_.InView, {
                                    triggerOnce: true,
                                    children: ({ ref , inView  })=>/*#__PURE__*/ jsx_runtime_.jsx("div", {
                                            ref: ref,
                                            children: inView && /*#__PURE__*/ jsx_runtime_.jsx(Section3, {
                                            })
                                        })
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx(external_react_intersection_observer_.InView, {
                                    triggerOnce: true,
                                    children: ({ ref , inView  })=>/*#__PURE__*/ jsx_runtime_.jsx("div", {
                                            ref: ref,
                                            children: inView && /*#__PURE__*/ jsx_runtime_.jsx(Section1, {
                                            })
                                        })
                                })
                            ]
                        }) : /*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx(external_react_intersection_observer_.InView, {
                                    triggerOnce: true,
                                    children: ({ ref , inView  })=>/*#__PURE__*/ jsx_runtime_.jsx("div", {
                                            ref: ref,
                                            children: inView && /*#__PURE__*/ jsx_runtime_.jsx(Section1, {
                                            })
                                        })
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx(external_react_intersection_observer_.InView, {
                                    triggerOnce: true,
                                    children: ({ ref , inView  })=>/*#__PURE__*/ jsx_runtime_.jsx("div", {
                                            ref: ref,
                                            children: inView && /*#__PURE__*/ jsx_runtime_.jsx(Section3, {
                                            })
                                        })
                                })
                            ]
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("main", {
                            className: "container",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                    className: "category_flower_height",
                                    children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_intersection_observer_.InView, {
                                        triggerOnce: true,
                                        children: ({ ref , inView  })=>/*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                ref: ref,
                                                children: inView && /*#__PURE__*/ jsx_runtime_.jsx(Section2, {
                                                    data: categoryProducts
                                                })
                                            })
                                    })
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx(external_react_intersection_observer_.InView, {
                                    triggerOnce: true,
                                    children: ({ ref , inView  })=>/*#__PURE__*/ jsx_runtime_.jsx("div", {
                                            ref: ref,
                                            children: inView && /*#__PURE__*/ jsx_runtime_.jsx(Section15, {
                                            })
                                        })
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx(external_react_intersection_observer_.InView, {
                                    triggerOnce: true,
                                    children: ({ ref , inView  })=>/*#__PURE__*/ jsx_runtime_.jsx("div", {
                                            ref: ref,
                                            children: inView && /*#__PURE__*/ jsx_runtime_.jsx(Section12, {
                                            })
                                        })
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx(external_react_intersection_observer_.InView, {
                                    triggerOnce: true,
                                    children: ({ ref , inView  })=>{
                                        return(/*#__PURE__*/ jsx_runtime_.jsx("div", {
                                            ref: ref,
                                            children: inView && /*#__PURE__*/ jsx_runtime_.jsx(SeoText, {
                                                text: seo === null || seo === void 0 ? void 0 : seo.site_settings_main_seotext
                                            })
                                        }));
                                    }
                                })
                            ]
                        })
                    ]
                })
            ]
        }));
    }
};
/* harmony default export */ const pages = (IndexPage);
async function getStaticProps(ctx) {
    let lang = ctx.locale;
    let { currency_id  } = external_next_cookies_default()(ctx);
    let data = await external_axios_default().get(`${Variables/* BASE_URL */._n}/flowers/category-item/${lang}currency_id=${currency_id}`);
    let seo_request = await external_axios_default().get(`${Variables/* BASE_URL */._n}/seo/home-page/${lang}`);
    return {
        props: {
            category_products2: data.data,
            home_seo: seo_request.data
        },
        revalidate: 1
    };
}


/***/ }),

/***/ 8130:
/***/ ((module) => {

module.exports = require("@material-ui/core");

/***/ }),

/***/ 7730:
/***/ ((module) => {

module.exports = require("@material-ui/core/ClickAwayListener");

/***/ }),

/***/ 6491:
/***/ ((module) => {

module.exports = require("@material-ui/core/Grow");

/***/ }),

/***/ 640:
/***/ ((module) => {

module.exports = require("@material-ui/core/Paper");

/***/ }),

/***/ 2767:
/***/ ((module) => {

module.exports = require("@material-ui/core/Popper");

/***/ }),

/***/ 8308:
/***/ ((module) => {

module.exports = require("@material-ui/core/styles");

/***/ }),

/***/ 2105:
/***/ ((module) => {

module.exports = require("@material-ui/icons");

/***/ }),

/***/ 3935:
/***/ ((module) => {

module.exports = require("@material-ui/icons/Add");

/***/ }),

/***/ 66:
/***/ ((module) => {

module.exports = require("@material-ui/icons/AddShoppingCart");

/***/ }),

/***/ 7149:
/***/ ((module) => {

module.exports = require("@material-ui/icons/Remove");

/***/ }),

/***/ 6072:
/***/ ((module) => {

module.exports = require("@mui/lab");

/***/ }),

/***/ 6715:
/***/ ((module) => {

module.exports = require("@mui/lab/AdapterDateFns");

/***/ }),

/***/ 9904:
/***/ ((module) => {

module.exports = require("@mui/lab/LocalizationProvider");

/***/ }),

/***/ 5692:
/***/ ((module) => {

module.exports = require("@mui/material");

/***/ }),

/***/ 9409:
/***/ ((module) => {

module.exports = require("@mui/material/Accordion");

/***/ }),

/***/ 8279:
/***/ ((module) => {

module.exports = require("@mui/material/AccordionDetails");

/***/ }),

/***/ 4604:
/***/ ((module) => {

module.exports = require("@mui/material/AccordionSummary");

/***/ }),

/***/ 6042:
/***/ ((module) => {

module.exports = require("@mui/material/TextField");

/***/ }),

/***/ 8442:
/***/ ((module) => {

module.exports = require("@mui/material/styles");

/***/ }),

/***/ 2038:
/***/ ((module) => {

module.exports = require("@styled-system/css");

/***/ }),

/***/ 9099:
/***/ ((module) => {

module.exports = require("@styled-system/theme-get");

/***/ }),

/***/ 2167:
/***/ ((module) => {

module.exports = require("axios");

/***/ }),

/***/ 8103:
/***/ ((module) => {

module.exports = require("clsx");

/***/ }),

/***/ 4146:
/***/ ((module) => {

module.exports = require("date-fns");

/***/ }),

/***/ 2296:
/***/ ((module) => {

module.exports = require("formik");

/***/ }),

/***/ 6734:
/***/ ((module) => {

module.exports = require("js-cookie");

/***/ }),

/***/ 6517:
/***/ ((module) => {

module.exports = require("lodash");

/***/ }),

/***/ 7486:
/***/ ((module) => {

module.exports = require("next-cookies");

/***/ }),

/***/ 6641:
/***/ ((module) => {

module.exports = require("next-seo");

/***/ }),

/***/ 562:
/***/ ((module) => {

module.exports = require("next/dist/server/denormalize-page-path.js");

/***/ }),

/***/ 8028:
/***/ ((module) => {

module.exports = require("next/dist/server/image-config.js");

/***/ }),

/***/ 4957:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/head.js");

/***/ }),

/***/ 3539:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/i18n/detect-domain-locale.js");

/***/ }),

/***/ 4014:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/i18n/normalize-locale-path.js");

/***/ }),

/***/ 5832:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/loadable.js");

/***/ }),

/***/ 8020:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/mitt.js");

/***/ }),

/***/ 4964:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router-context.js");

/***/ }),

/***/ 9565:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/get-asset-path-from-route.js");

/***/ }),

/***/ 4365:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/get-middleware-regex.js");

/***/ }),

/***/ 1428:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/is-dynamic.js");

/***/ }),

/***/ 1292:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/parse-relative-url.js");

/***/ }),

/***/ 979:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/querystring.js");

/***/ }),

/***/ 6052:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/resolve-rewrites.js");

/***/ }),

/***/ 4226:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/route-matcher.js");

/***/ }),

/***/ 5052:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/route-regex.js");

/***/ }),

/***/ 3018:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/to-base-64.js");

/***/ }),

/***/ 9232:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ 968:
/***/ ((module) => {

module.exports = require("next/head");

/***/ }),

/***/ 1853:
/***/ ((module) => {

module.exports = require("next/router");

/***/ }),

/***/ 2947:
/***/ ((module) => {

module.exports = require("pure-react-carousel");

/***/ }),

/***/ 6689:
/***/ ((module) => {

module.exports = require("react");

/***/ }),

/***/ 6405:
/***/ ((module) => {

module.exports = require("react-dom");

/***/ }),

/***/ 6804:
/***/ ((module) => {

module.exports = require("react-facebook-login/dist/facebook-login-render-props");

/***/ }),

/***/ 67:
/***/ ((module) => {

module.exports = require("react-google-login");

/***/ }),

/***/ 2791:
/***/ ((module) => {

module.exports = require("react-helmet");

/***/ }),

/***/ 3967:
/***/ ((module) => {

module.exports = require("react-inner-image-zoom");

/***/ }),

/***/ 4648:
/***/ ((module) => {

module.exports = require("react-input-mask");

/***/ }),

/***/ 9785:
/***/ ((module) => {

module.exports = require("react-intersection-observer");

/***/ }),

/***/ 3126:
/***/ ((module) => {

module.exports = require("react-intl");

/***/ }),

/***/ 9252:
/***/ ((module) => {

module.exports = require("react-lazy-load-image-component");

/***/ }),

/***/ 6022:
/***/ ((module) => {

module.exports = require("react-redux");

/***/ }),

/***/ 6158:
/***/ ((module) => {

module.exports = require("react-share");

/***/ }),

/***/ 7128:
/***/ ((module) => {

module.exports = require("react-svg");

/***/ }),

/***/ 3742:
/***/ ((module) => {

module.exports = require("react-yandex-maps");

/***/ }),

/***/ 997:
/***/ ((module) => {

module.exports = require("react/jsx-runtime");

/***/ }),

/***/ 6981:
/***/ ((module) => {

module.exports = require("reactstrap");

/***/ }),

/***/ 7518:
/***/ ((module) => {

module.exports = require("styled-components");

/***/ }),

/***/ 5834:
/***/ ((module) => {

module.exports = require("styled-system");

/***/ }),

/***/ 5609:
/***/ ((module) => {

module.exports = require("yup");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [5218,3756,12], () => (__webpack_exec__(2293)));
module.exports = __webpack_exports__;

})();