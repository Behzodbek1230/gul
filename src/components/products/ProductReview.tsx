import React, {useEffect, useState} from "react";
import Box from "../Box";
import Button from "../buttons/Button";
import FlexBox from "../FlexBox";
import Rating from "../rating/Rating";

import { H3, H5} from "../Typography";
import ProductComment from "./ProductComment";
import { useFormik } from "formik";
import {useRouter} from "next/router";
import Cookies from "js-cookie"
import axios from "axios";
import {FormattedMessage, useIntl} from "react-intl";
import {StyledPagination} from "../../components/pagination/PaginationStyle";
import Icon from "../../components/icon/Icon";
import ReactPaginate from "react-paginate"
import dynamic from "next/dynamic";
export interface ProductReviewProps {}


if(typeof window !== "undefined"){
  const Quill = require("react-quill").Quill
  const Image = Quill.import('formats/image');
  Image.className = 'quill_image';
  Quill.register(Image, true);
}


const ReactQuill = dynamic(
    () => import('react-quill'),
    { ssr: false }
);


const modules = {
  toolbar: [
    [{ header: '1' }, { header: '2' }, { font: [] }],
    [{ size: [] }],
    ['bold', 'italic', 'underline', 'strike', 'blockquote'],
    [
      { list: 'ordered' },
      { list: 'bullet' },
      { indent: '-1' },
      { indent: '+1' },
    ],
    ['link', 'image', 'video'],
    ['clean'],
  ],
  clipboard: {
    // toggle to add extra line breaks when pasting HTML:
    matchVisual: false,
  },
}



const ProductReview: React.FC<ProductReviewProps> = () => {
  const router = useRouter()
  let intl = useIntl()
  const [msg,setmsg] = useState("")
  const [rating_error,setrating_error] = useState("")
  const [comment2,setcomment2] = useState("")
  let lang = router.locale
  const handleFormSubmit = async (values, { resetForm }) => {
    const {keyword} = router.query;
    //FormData
    const formData = new FormData()
    formData.append("keyword",keyword.toString())
    formData.append("ball",values.rating)
    formData.append("comment",comment2)
    let logged_in = Cookies.get("isLoggedIn")

    setrating_error("")
    setmsg("")
    if(!values?.rating){
      setrating_error(intl.formatMessage({id:"rating_required"}))
    }
    else if(comment2==="" && comment2){
      setmsg(intl.formatMessage({id:"text_required"}))
      }
    else if(logged_in === "true"){
      axios({
        method:"POST",
        url:`/flowers/rate/${lang}`,
        data:formData
      })
          .then(response=>{
            if(response.data.errors){
              setmsg(intl.formatMessage({id:"error_booking"}))
            }
            else{
              setmsg(intl.formatMessage({id:"add_comment_success"}))
              setTimeout(() => {
                setmsg("")
              }, 2000);
              resetForm();
              setcomment2("")
            }
          })
    }
    else{
      setmsg("Iltimos komment yozish saytdan ro'yhatdan o'ting")
    }

  };
  const {
    values,
    handleSubmit,
    setFieldValue,
  } = useFormik({
    initialValues: initialValues,
    onSubmit: handleFormSubmit,
  });
  let [ReviewPage,setReviewPage] = useState(1)
  let [reviews,setreviews] = useState({data:[],last_page:1})
  useEffect(()=>{
    axios({
      method:"GET",
      url:`/flowers/rating-list/${router.query.keyword}?page=${ReviewPage}`
    })
        .then(res=>{
          setreviews(res.data)
        })
        .catch(()=>null)
  },[msg,ReviewPage])
  
  return (
    <Box>
      {reviews?.data?.length !==0 ?
          <div className="p-1 mb-4">
            {reviews?.data?.map((rating,ind) => {
              return (<ProductComment
                  id={ind}
                  key={rating.id}
                  imgUrl={rating.userAvatar}
                  date={rating.date}
                  name={rating.userFio}
                  rating={rating.ball}
                  comment={rating.comment}
                  is_shop={true}
                  extended={rating?.extend}
                  reviews={reviews}
                  setreviews = {(e)=>setreviews(e)}
              />)
            })}
            {reviews.last_page !== 1 ?
                <FlexBox
                    flexWrap="wrap"
                    justifyContent="space-between"
                    alignItems="center"
                    mt="32px"
                >
                  <StyledPagination>
                    <ReactPaginate
                        initialPage={ReviewPage-1}
                        previousLabel={
                          <Button
                              style={{cursor: "pointer"}}
                              className="control-button"
                              color="primary"
                              overflow="hidden"
                              height="auto"
                              padding="6px"
                              borderRadius="50%"
                          >
                            <Icon defaultcolor="currentColor" variant="small">
                              chevron-left
                            </Icon>
                          </Button>

                        }
                        nextLabel={
                          <Button
                              style={{cursor: "pointer"}}
                              className="control-button"
                              color="primary"
                              overflow="hidden"
                              height="auto"
                              padding="6px"
                              borderRadius="50%"
                          >
                            <Icon defaultcolor="currentColor" variant="small">
                              chevron-right
                            </Icon>
                          </Button>
                        }
                        breakLabel={
                          <Icon defaultcolor="currentColor" variant="small">
                            triple-dot
                          </Icon>
                        }
                        pageCount={reviews.last_page}
                        marginPagesDisplayed={true}
                        pageRangeDisplayed={false}
                        onPageChange={(r) => {
                          setReviewPage(r.selected + 1)
                        }
                        }
                        containerClassName="pagination"
                        subContainerClassName="pages pagination"
                        activeClassName="active"
                        disabledClassName="disabled"
                    />
                  </StyledPagination>
                </FlexBox>
                :
                ""
            }


          </div>
          :
          ""
      }

      <H3 fontWeight="700" mt="5px" mb="20">
        <FormattedMessage
            id="write_review"
            defaultMessage="Izoh qoldiring"
        />

      </H3>

      <form onSubmit={handleSubmit}>
        <Box mb="20px">
          <FlexBox mb="12px">
            <div style={{fontWeight:"bolder"}}>
                 <FormattedMessage
                  id="Your Rating"
                  defaultMessage="Your Rating"
                 />
              </div>

            <H5 color="error.main">*</H5>
          </FlexBox>
          
          <Rating
            outof={5}
            color="warn"
            size="medium"
            readonly={false}
            value={values.rating || 0}
            onChange={(value) => setFieldValue("rating", value)}
          
          />
          {rating_error === "" ? "" : <div
              style=
                  {{
                    color:"red",
                    fontSize:"small"
                  }}
          >
            {rating_error}
          </div> }
        </Box>
        
        <Box mb="24px">
          <FlexBox mb="12px">
              <div style={{fontWeight:"bolder"}}>
                 <FormattedMessage
                   id="Your Review"
                  defaultMessage="Sizning izohingiz"
                 />
              </div>
            <H5 color="error.main">*</H5>
          </FlexBox>

          <div className="react-quil-container432">
            <ReactQuill
                modules={modules}
                className="reactquill-height2 my-editing-area"
                value={comment2}
                onChange={(e)=>setcomment2(e)}
            />
          </div>
          {msg === "" ? "" : <div
              style=
                  {{
                    color:"red",
                    fontSize:"small"
                  }}
          >
            {msg}
          </div> }
        </Box>

        <Button
          variant="contained"
          color="primary"
          size="small"
          type="submit"
        >
          <FormattedMessage
              id="submit"
              defaultMessage="Saqlash"
          />
        </Button>
      </form>
    </Box>
  );
};

const initialValues = {
  rating: "",
  comment: "",
  date: new Date().toISOString(),
};


export default ProductReview;
