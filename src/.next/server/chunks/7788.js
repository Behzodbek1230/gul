"use strict";
exports.id = 7788;
exports.ids = [7788];
exports.modules = {

/***/ 7788:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "I": () => (/* binding */ StyledPagination)
/* harmony export */ });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5834);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_system__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _utils_utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1972);



const StyledPagination = (styled_components__WEBPACK_IMPORTED_MODULE_0___default().div)`
  .pagination {
    margin: 0px;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    align-items: center;
    list-style-type: none;
    padding: 0px;

    li {
      cursor: pointer;

      a {
        display: flex;
        justify-content: center;
        align-items: center;
        height: 32px;
        width: 32px;
        margin: 0px 5px;
        border-radius: 5px;
        outline: none;
        border: 1px solid transparent;
        border-radius: 50%;
        @media only screen and (max-width: 450px) {
          margin: 4px;
        }
      }
      &:not(.active):hover {
        a {
          border: 1px solid ${(0,_utils_utils__WEBPACK_IMPORTED_MODULE_2__/* .getTheme */ .gh)("colors.primary.main")};
          color: ${(0,_utils_utils__WEBPACK_IMPORTED_MODULE_2__/* .getTheme */ .gh)("colors.primary.main")};
        }
      }
    }
    .active {
      cursor: none;
      a {
        border: 1px solid ${(0,_utils_utils__WEBPACK_IMPORTED_MODULE_2__/* .getTheme */ .gh)("colors.primary.main")};
        color: ${(0,_utils_utils__WEBPACK_IMPORTED_MODULE_2__/* .getTheme */ .gh)("colors.primary.main")};
      }
    }

    .disabled {
      .control-button {
        cursor: none;
        border: 1px solid ${(0,_utils_utils__WEBPACK_IMPORTED_MODULE_2__/* .getTheme */ .gh)("colors.primary.light")};
        color: ${(0,_utils_utils__WEBPACK_IMPORTED_MODULE_2__/* .getTheme */ .gh)("colors.primary.light")};
      }
    }
  }

  .control-button {
    height: 32px;
    width: 32px;
    min-width: 32px;
    border: 1px solid ${(0,_utils_utils__WEBPACK_IMPORTED_MODULE_2__/* .getTheme */ .gh)("colors.primary.main")};
  }

  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.space}
`;


/***/ })

};
;