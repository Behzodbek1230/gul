import Avatar from "../../../../components/avatar/Avatar";
import IconButton from "../../../../components/buttons/IconButton";
import FlexBox from "../../../../components/FlexBox";
import Icon from "../../../../components/icon/Icon";
import DashboardPageHeader2 from "../../../../components/layout/DashboardPageHeader2";
import VendorDashboardLayout from "../../../../components/layout/VendorDashboardLayout";
import TableRow from "../../../../components/TableRow";
import Typography, { H5 } from "../../../../components/Typography";
import React, {useEffect, useState} from "react";
import axios from "axios";
import {useDispatch, useSelector} from "react-redux";
import Button from "../../../../components/buttons/Button";
import {StyledPagination} from "../../../../components/pagination/PaginationStyle";
import ReactPaginate from "react-paginate";
import Cookies from "js-cookie"
import {useRouter} from "next/router";
import {FormattedMessage, useIntl} from "react-intl";
import get_shop_products from "../../../../../Redux/Actions/get_product_shop";
import { NextSeo } from "next-seo";
import {ArrowForward, Edit} from "@material-ui/icons";
import Link  from "next/link"
import useWindowSize from "../../../../hooks/useWindowSize";
const Products_vendor_page = () => {
    let intl = useIntl()
    const router = useRouter()
    const dispatch = useDispatch()
    const [data2,setdata2] = useState({datas:{current_page:1,data:[]},lastPage:1,length:0})
    let user = useSelector((state:any)=>state.token.user)
    let lang = router.locale
    let width = useWindowSize()
    let {status,page} = router.query
    let isLoggedIn = Cookies.get('isLoggedIn')

    const handleStatusChange = (r) => {
        let query2 = {...router.query}
        query2.status = r.target.value
        router.push( {pathname:router.pathname,query:query2})
    }

    useEffect(()=>{
        if(isLoggedIn === 'false' || user?.data?.is_shops === null){
            router.push('/404')
        }
    },[user,isLoggedIn])
    useEffect(() => {
        let currency_id = Cookies.get("currency_id");
        let currency_text = typeof currency_id !== "undefined" ?  `&currency=${currency_id}` : ""
        let status_text = typeof status ==="undefined" ? "" : "?status=" + status;
        if(typeof page !== "undefined" && typeof status !== "undefined"){
            axios(`/flowers/personal-flowers/${lang}${status_text}&page=${page}${currency_text}`)
                .then(response=>{
                    dispatch(get_shop_products(response.data))
                    setdata2(response.data)
                })
                .catch(()=>{
                    return null;
                })
        }
        else if(typeof page === "undefined" && typeof status !== "undefined"){
            axios(`/flowers/personal-flowers/${lang}${status_text}${currency_text}`)
            .then(res=>{
                dispatch(get_shop_products(res.data))
                setdata2(res.data)
            })
            .catch(()=>{
                return null;
            })
        }
        else if(typeof page !== "undefined" && typeof status === "undefined") {
            axios(`/flowers/personal-flowers/${lang}?page=${page}${currency_text}`)
                .then(response=>{
                    dispatch(get_shop_products(response.data))
                    setdata2(response.data)
                })
                .catch(()=>null)
        }
        else if(typeof page === "undefined" && typeof status === "undefined"){
            status_text = typeof status ==="undefined" ? "?status=" : "?status=" + status;
            axios(`/flowers/personal-flowers/${lang}${status_text}${currency_text}`)
            .then(res=>{
                dispatch(get_shop_products(res.data))
                setdata2(res.data)
            })
            .catch(()=>{
                return null;
            })
        }
    }, [status,page,lang,Cookies.get("currency_id")]);


  return (
    <VendorDashboardLayout title={intl.formatMessage({id:"Products"})}>
        <NextSeo
            title={intl.formatMessage({id:"Products"})}
        />
        <div>
        <div className="flex flex-wrap">
            <div className="d-none w-fit  float-right d-sm-none d-md-none d-lg-inline d-xl-inline">
                    <select onChange={(r)=>handleStatusChange(r)}
                    className="form-select" aria-label="Default select example"
                    >
                        <option value="">
                            {intl.formatMessage({id:"All"})}
                        </option>
                        <option value="2">
                            {intl.formatMessage({id:"inactive"})}
                        </option>
                        <option value="1">

                            {intl.formatMessage({id:"active"})}
                        </option>
                        <option value="4">
                            {intl.formatMessage({id:"blocked"})}
                        </option>
                        <option value="3">
                            {intl.formatMessage({id:"in_moderation"})}
                        </option>
                    </select>
                </div>
                <DashboardPageHeader2 title={intl.formatMessage({id:"Products"})} iconName="delivery-box" />
        </div>
                <div  className="col-12 col-sm-4 w-80 col-md-3 d-inline d-sm-inline d-md-inline d-lg-none d-xl-none">
                    <select id="vendor_product_filter" onChange={(r)=>handleStatusChange(r)}
                    className="form-select" aria-label="Default select example"
                    >
                        <option value="">
                            {intl.formatMessage({id:"All"})}
                        </option>
                        <option value="2">
                            {intl.formatMessage({id:"inactive"})}
                        </option>
                        <option value="1">

                            {intl.formatMessage({id:"active"})}
                        </option>
                        <option value="4">
                            {intl.formatMessage({id:"blocked"})}
                        </option>
                        <option value="3">
                            {intl.formatMessage({id:"in_moderation"})}
                        </option>
                    </select>
                </div>
            {data2?.datas?.data.length === 0 ? <div>Siz xaliham birorta mahsulot yaratmagansiz</div> :
                <>
                    {data2?.datas?.data.length !== 0 ?
                        width < 650
                        ?
                            <div className="overflow-scroll">
                                <div  style={{width:"640px"}}>
                                <div className="d-flex py-1 px-3" style={{width:"640px"}}>

                                    <H5 flex="0 0 30% !important" className="text-truncate" ml="0px" color="text.muted" textAlign="left">
                                        <FormattedMessage
                                            id="Product Name"
                                            defaultMessage="Mahsulot nomi"
                                        />
                                    </H5>

                                    <H5 color="text.muted" flex="0 0 24% !important"  className="text-truncate whitespace-nowrap width-fit-content" width="110px" ml="40px" >
                                        <FormattedMessage
                                            id="bolim"
                                            defaultMessage="Kategoriya Nomi"
                                        />
                                    </H5>
                                    <H5 color="text.muted" flex="0 0 7% !important" my="0px"  textAlign="left">
                                        <FormattedMessage
                                            id="Regular Price"
                                            defaultMessage="Narx"
                                        />
                                    </H5>
                                    <H5
                                        flex="0 0 0 !important"
                                        color="text.muted"
                                        px="22px"
                                        my="0px"
                                        ml="80px"
                                    >
                                        <FormattedMessage
                                            id="actions"
                                        />
                                    </H5>
                                </div>
                                {data2?.datas?.data?.map((item, ind) => (
                                    <>
                                        <TableRow
                                            style={{width:"640px"}}
                                            key={ind}
                                            my="1rem"
                                            padding="6px 18px"
                                        >
                                            <FlexBox   alignItems="center" m="6px" flex="2 2 2px !important">
                                                <Avatar src={item.image} size={36}/>
                                                <Typography textAlign="left" width="135px" className="whitespace-nowrap text-truncate"  ml="20px">
                                                    {item.name}
                                                </Typography>
                                            </FlexBox>
                                            <H5
                                                className="text-truncate "
                                                m="6px"
                                                textAlign="left"
                                                fontWeight="600"
                                                ml="-30px"
                                                mr="20px"
                                                color={item.stock < 6 ? "error.main" : "inherit"}
                                            >
                                                {item.categoryName}
                                            </H5>
                                            <H5 m="6px"  textAlign="left" fontWeight="400">
                                                {item.price}
                                            </H5>


                                            <div>
                                                <Typography textAlign="center" color="">

                                                    {/*<IconButton  size="small">*/}
                                                    {/*    <MoreHoriz style={{fontSize:"15px"}}/>*/}
                                                    {/*</IconButton>*/}

                                                    <Link href={`/vendor/products/${item.keyword}`}>
                                                        <IconButton  size="too_small">
                                                            <Edit style={{fontSize:"15px"}}/>
                                                        </IconButton>
                                                    </Link>
                                                    <Link href={`/${item.categoryKeyword}/${item.keyword}`}>
                                                        <IconButton
                                                            size="small"
                                                        >
                                                            <ArrowForward style={{fontSize:"15px"}}/>
                                                        </IconButton>
                                                    </Link>
                                                </Typography>
                                            </div>
                                        </TableRow>
                                    </>
                                ))}
                                {data2.lastPage !==1
                                    ?
                                    <StyledPagination>
                                        <ReactPaginate
                                            initialPage={typeof page !== "undefined" ? parseInt(page.toString())-1 : 0 }
                                            previousLabel={
                                                <Button
                                                    style={{cursor: "pointer"}}
                                                    className="control-button"
                                                    color="primary"
                                                    overflow="hidden"
                                                    height="auto"
                                                    padding="6px"
                                                    borderRadius="50%"
                                                >
                                                    <Icon defaultcolor="currentColor" variant="small">
                                                        chevron-left
                                                    </Icon>
                                                </Button>

                                            }
                                            nextLabel={
                                                <Button
                                                    style={{cursor: "pointer"}}
                                                    className="control-button"
                                                    color="primary"
                                                    overflow="hidden"
                                                    height="auto"
                                                    padding="6px"
                                                    borderRadius="50%"
                                                >
                                                    <Icon defaultcolor="currentColor" variant="small">
                                                        chevron-right
                                                    </Icon>
                                                </Button>
                                            }
                                            breakLabel={
                                                <Icon defaultcolor="currentColor" variant="small">
                                                    triple-dot
                                                </Icon>
                                            }
                                            pageCount={data2?.lastPage}
                                            marginPagesDisplayed={true}
                                            pageRangeDisplayed={false}
                                            onPageChange={r=>{
                                                let query2 = {...router.query}
                                                query2.page = r.selected + 1
                                                router.push( {pathname:router.pathname,query:query2})
                                            }
                                            }
                                            containerClassName="pagination"
                                            subContainerClassName="pages pagination"
                                            activeClassName="active"
                                            disabledClassName="disabled"
                                        />
                                    </StyledPagination>
                                    :
                                    ""
                                }

                            </div>
                            </div>
                            :
                        <>
                            <div className="d-flex py-1 px-3" >

                                <H5 flex="0 0 36% !important"  ml="0px" color="text.muted" textAlign="left">
                                    <FormattedMessage
                                        id="Product Name"
                                        defaultMessage="Mahsulot nomi"
                                    />
                                </H5>

                                <H5 color="text.muted" flex="0 0 24% !important"  className="text-truncate whitespace-nowrap width-fit-content"  >
                                    <FormattedMessage
                                        id="bolim"
                                        defaultMessage="Kategoriya Nomi"
                                    />
                                </H5>
                                <H5 color="text.muted" my="0px" flex="0 0 23% !important" textAlign="left">
                                    <FormattedMessage
                                        id="Regular Price"
                                        defaultMessage="Narx"
                                    />
                                </H5>
                                <H5
                                    // flex="0 0 0 !important"
                                    color="text.muted"
                                    px="22px"
                                    my="0px"
                                    // ml="80px"
                                >
                                    <FormattedMessage
                                        id="actions"
                                    />
                                </H5>
                            </div>
                            {data2?.datas?.data?.map((item, ind) => (
                            <>
                                    <TableRow

                                        key={ind}
                                        my="1rem"
                                        padding="6px 18px"
                                    >
                                        <FlexBox flex="0 0 36% !important"    alignItems="center"  >
                                            <Avatar src={item.image} size={36}/>
                                            <Typography textAlign="left" width="135px" className="whitespace-nowrap text-truncate"  ml="20px">
                                                {item.name}
                                            </Typography>
                                        </FlexBox>
                                        <H5
                                            flex="0 0 24% !important"
                                            className="text-truncate "
                                            textAlign="left"
                                            fontWeight="600"

                                            color={item.stock < 6 ? "error.main" : "inherit"}
                                        >
                                            {item.categoryName}
                                        </H5>
                                        <H5  flex="0 0 20% !important"  textAlign="left" fontWeight="400">
                                            {item.price}
                                        </H5>


                                        <div>
                                            <Typography textAlign="center" color="">

                                                {/*<IconButton  size="small">*/}
                                                {/*    <MoreHoriz style={{fontSize:"15px"}}/>*/}
                                                {/*</IconButton>*/}

                                                <Link href={`/vendor/products/${item.keyword}`}>
                                                    <IconButton  size="too_small">
                                                        <Edit style={{fontSize:"15px"}}/>
                                                    </IconButton>
                                                </Link>
                                                <Link href={`/${item.categoryKeyword}/${item.keyword}`}>
                                                    <IconButton
                                                        size="small"
                                                    >
                                                        <ArrowForward style={{fontSize:"15px"}}/>
                                                    </IconButton>
                                                </Link>
                                            </Typography>
                                        </div>
                                    </TableRow>
                        </>
                        ))}
                        {data2.lastPage !==1 
                            ?
                            <StyledPagination>
                                <ReactPaginate
                                    initialPage={typeof page !== "undefined" ? parseInt(page.toString())-1 : 0 }
                                    previousLabel={
                                        <Button
                                            style={{cursor: "pointer"}}
                                            className="control-button"
                                            color="primary"
                                            overflow="hidden"
                                            height="auto"
                                            padding="6px"
                                            borderRadius="50%"
                                        >
                                            <Icon defaultcolor="currentColor" variant="small">
                                                chevron-left
                                            </Icon>
                                        </Button>

                                    }
                                    nextLabel={
                                        <Button
                                            style={{cursor: "pointer"}}
                                            className="control-button"
                                            color="primary"
                                            overflow="hidden"
                                            height="auto"
                                            padding="6px"
                                            borderRadius="50%"
                                        >
                                            <Icon defaultcolor="currentColor" variant="small">
                                                chevron-right
                                            </Icon>
                                        </Button>
                                    }
                                    breakLabel={
                                        <Icon defaultcolor="currentColor" variant="small">
                                            triple-dot
                                        </Icon>
                                    }
                                    pageCount={data2?.lastPage}
                                    marginPagesDisplayed={true}
                                    pageRangeDisplayed={false}
                                    onPageChange={r=>{
                                        let query2 = {...router.query}
                                        query2.page = r.selected + 1
                                        router.push( {pathname:router.pathname,query:query2})
                                        }
                                    }
                                    containerClassName="pagination"
                                    subContainerClassName="pages pagination"
                                    activeClassName="active"
                                    disabledClassName="disabled"
                                />
                            </StyledPagination>
                        :
                        ""
                        }
                    </> :<div>Siz xaliham birorta mahsulot yaratmagansiz</div> }

                </>
            }
        </div>
    </VendorDashboardLayout>
  );
};

export default Products_vendor_page;
