"use strict";
exports.id = 8414;
exports.ids = [8414];
exports.modules = {

/***/ 1891:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5834);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_system__WEBPACK_IMPORTED_MODULE_1__);


const Image = (styled_components__WEBPACK_IMPORTED_MODULE_0___default().img)`
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.space}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.border}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.layout}
`;
Image.defaultProps = {
    display: "block"
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Image);


/***/ }),

/***/ 7748:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ modal_Modal)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "react-dom"
var external_react_dom_ = __webpack_require__(6405);
// EXTERNAL MODULE: ./components/FlexBox.tsx
var FlexBox = __webpack_require__(5359);
// EXTERNAL MODULE: external "styled-components"
var external_styled_components_ = __webpack_require__(7518);
var external_styled_components_default = /*#__PURE__*/__webpack_require__.n(external_styled_components_);
;// CONCATENATED MODULE: ./components/modal/ModalStyle.tsx


const StyledModal = external_styled_components_default()(FlexBox["default"])`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  height: 100%;
  background: rgba(0, 0, 0, 0.4);
  z-index: 999;

  .container {
    position: relative;
    width: 100%;
    top: 50%;
    transform: translateY(-50%);
    overflow: auto;
    background: transparent;
  }
`;
/* harmony default export */ const ModalStyle = (StyledModal);

;// CONCATENATED MODULE: ./components/modal/Modal.tsx





const Modal = ({ children , open , onClose  })=>{
    const handleModalContentClick = (e)=>{
        e.stopPropagation();
    };
    const handleBackdropClick = ()=>{
        if (onClose) onClose();
    };
    if (globalThis.document && open) {
        let modal = document.querySelector("#modal-root");
        if (!modal) {
            modal = document.createElement("div");
            modal.setAttribute("id", "modal-root");
            document.body.appendChild(modal);
        }
        return(/*#__PURE__*/ (0,external_react_dom_.createPortal)(/*#__PURE__*/ jsx_runtime_.jsx(ModalStyle, {
            flexDirection: "column",
            alignItems: "center",
            onClick: handleBackdropClick,
            children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                className: "container",
                children: /*#__PURE__*/ jsx_runtime_.jsx(FlexBox["default"], {
                    justifyContent: "center",
                    m: "0.5rem",
                    children: /*#__PURE__*/ (0,external_react_.cloneElement)(children, {
                        onClick: handleModalContentClick
                    })
                })
            })
        }), modal));
    } else return null;
};
Modal.defaultProps = {
    open: false
};
/* harmony default export */ const modal_Modal = (Modal);


/***/ })

};
;