import useWindowSize from "../../hooks/useWindowSize";
import Link from "next/link";
import React, {useEffect, useState} from "react";
import Box from "../Box";
import IconButton from "../buttons/IconButton";
import { Chip } from "../Chip";
import TableRow from "../TableRow";
import Typography, { H5, Small } from "../Typography";
import {ArrowForward, MoreHoriz} from "@material-ui/icons";
import {Dropdown, DropdownItem, DropdownMenu, DropdownToggle} from 'reactstrap';
import axios from "axios";
import { useRouter } from "next/router";
import Cookies from "js-cookie"
import {useSelector} from "react-redux";
export interface OrderRowProps {
  item?: {
    orderId?: any;
    status?: number;
    statusName?: string;
    delivery_time?: string | Date;
    total_price?: number;

  };
  detail?:boolean,
  setdata?:any
}

const OrderRow: React.FC<OrderRowProps> = ({ item ,detail,setdata}) => {
  let width = useWindowSize()
  const [open,setopen] = useState(false)
  const [statuses,setstatuses] = useState([])
  const user = useSelector((state:any)=>state.token.user)
  let router = useRouter()
  const lang = router.locale
  const {page} = router.query
  useEffect(()=>{
    axios(`/orders/status-list/${lang}`)
        .then(res=>{
            setstatuses(res.data)
        })
        .catch(()=>null)
  }, [lang]);

  const handleStatusChange = (id)=>{
      const data = new FormData()
      data.append("keyword",user.data.is_shops);
      data.append("order_id",item.orderId)
      data.append("status",id)
      axios({
          method:"POST",
          url:`/shops/order-change-status/${lang}`,
          data:data
      })
          .then(()=>{
              let currency_text = typeof Cookies.get("currency_id") === "undefined" ? "" : `currency=${Cookies.get("currency_id")}`
              axios(`/shops/orders-list/${user?.data?.is_shops ? user?.data?.is_shops : "" }/${lang}${typeof page=== "undefined" ? "?" : `?page=${page}&`}${currency_text}`)
                  .then(response=>{
                      setdata(response.data)
                  })
                  .catch(()=>{
                      return null;
                  })

          })
          .catch(()=>null)
  }
  const getColor = (status) => {
    switch (status) {
      case 1:
        return "danger";
      case 2:
        return "warning";
      case 3:
        return "success";
      case 4:
        return "secondary"
      default:
        return "";
    }
  };
  if(width < 770){
    return (
       <TableRow  className="text-transformation-none shop_orders_table"  my="1rem" padding="6px 18px">
           <H5 fontSize="14px"  m="6px" ml="0px" textAlign="left" className="order_h5">
               {item.orderId}
           </H5>
           <Box ml="-30px"  mr="25px" textAlign="left" className="" >
               <Chip  p="0.25rem 1rem" style={{width:"180px"}} className={`bg2-${getColor(item.status)} ${!detail ? "order_class4 text-center" : " text-center d-flex justify-content-center"}`}>
                   <Small >{item.statusName}</Small>
               </Chip>
           </Box>
           <Typography className="flex-grow pre " m="6px" textAlign="left">
               {item.delivery_time}
           </Typography>

           <Typography m="6px" ml="30px" textAlign="left" className="whitespace-nowrap" style={{wordBreak:"break-word"}}>
               {item.total_price}
           </Typography>


           <div
               className="d-flex flex-nowrap align-items-center justify-content-end align-content-end"
           >
               <Typography textAlign="center" color="">
                   <Dropdown className="d-inline" size="small" isOpen={open} toggle={()=>setopen(!open)}>
                       
                        <DropdownToggle
                            tag="span"
                            onClick={()=>setopen(!open)}
                            data-toggle="dropdown"
                        >
                           <IconButton   size="too_small">
                               <MoreHoriz style={{fontSize:"15px"}}/>
                           </IconButton>
                       </DropdownToggle>
                       
                       <DropdownMenu right>
                           {statuses.map((val,ind)=><DropdownItem disabled={ind+1 <= item.status } onClick={()=>handleStatusChange(ind+1)}>{val}</DropdownItem>)}

                       </DropdownMenu>
                   
                   </Dropdown>

                   <Link  href={detail ? `/vendor/orders/${item.orderId}` : `/orders/${item?.orderId}`}>
                       <IconButton

                           size="too_small"
                       >
                           <ArrowForward style={{fontSize:"15px"}}/>
                       </IconButton>
                   </Link>

               </Typography>
           </div>
       </TableRow>
    );
  }
  else{
    return (
        <TableRow  className="text-transformation-none shop_orders_table"  my="1rem" padding="6px 18px">
            <H5 fontSize="14px"  m="6px" ml="0px" textAlign="left" className="order_h5">
                {item.orderId}
            </H5>
            <Box ml="-30px"  mr="37px" textAlign="left" className="" >
                <Chip  p="0.25rem 1rem" style={{width:"180px"}} className={`bg2-${getColor(item.status)} ${!detail ? "order_class4 text-center" : " text-center d-flex justify-content-center"}`}>
                    <Small >{item.statusName}</Small>
                </Chip>
            </Box>
            <Typography className="flex-grow pre " m="6px" textAlign="left">
                {item.delivery_time}
            </Typography>

            <Typography m="0px" ml="37px" mr="0px" textAlign="left" className="whitespace-nowrap" style={{wordBreak:"break-word"}}>
                {item.total_price}
            </Typography>


            <div
                className="d-flex  flex-nowrap align-items-center justify-content-end align-content-end"
            >
                <Typography textAlign="center" color="">
                    <Dropdown className="d-inline" size="small" isOpen={open} toggle={()=>setopen(!open)}>
                        <DropdownToggle
                            tag="span"
                            onClick={()=>setopen(!open)}
                            data-toggle="dropdown"

                        >
                            <IconButton   size="too_small">
                                <MoreHoriz style={{fontSize:"15px"}}/>
                            </IconButton>
                        </DropdownToggle>
                        <DropdownMenu right>
                            {statuses.map((val,ind)=><DropdownItem disabled={ind+1 <= item.status } onClick={()=>handleStatusChange(ind+1)}>{val}</DropdownItem>)}
                        </DropdownMenu>
                    </Dropdown>
                    <Link  href={detail ? `/vendor/orders/${item.orderId}` : `/orders/${item?.orderId}`}>
                        <IconButton
                            size="too_small"
                        >
                            <ArrowForward style={{fontSize:"15px"}}/>
                        </IconButton>
                    </Link>
                </Typography>
            </div>
        </TableRow>
    );
  }

};

export default OrderRow;
