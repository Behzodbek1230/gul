"use strict";
exports.id = 8661;
exports.ids = [8661];
exports.modules = {

/***/ 8661:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _styled_system_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2038);
/* harmony import */ var _styled_system_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_styled_system_css__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5834);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(styled_system__WEBPACK_IMPORTED_MODULE_2__);



const IconButton = styled_components__WEBPACK_IMPORTED_MODULE_1___default().button(_styled_system_css__WEBPACK_IMPORTED_MODULE_0___default()({
    outline: "none",
    border: "none",
    cursor: "pointer",
    fontSize: "1rem",
    borderRadius: 500,
    padding: "1rem",
    fontWeight: 600,
    color: "inherit",
    transition: "all 150ms ease-in-out",
    bg: "body.paper",
    "&:hover": {
        bg: "gray.200"
    },
    "&:disabled": {
        bg: "text.disabled",
        color: "text.muted"
    }
}), (props)=>{
    var ref, ref1;
    return (0,styled_system__WEBPACK_IMPORTED_MODULE_2__.variant)({
        prop: "variant",
        variants: {
            text: {
                border: "none",
                color: `${props.color}.main`
            },
            outlined: {
                color: `${props.color}.main`,
                border: "2px solid",
                borderColor: `${props.color}.main`,
                "&:focus": {
                    boxShadow: `0px 1px 4px 0px ${(ref = props.theme.colors[props.color]) === null || ref === void 0 ? void 0 : ref.main}`
                }
            },
            contained: {
                border: "none",
                color: `${props.color}.text`,
                bg: `${props.color}.main`,
                "&:hover": {
                    bg: `${props.color}.main`
                },
                "&:focus": {
                    boxShadow: `0px 1px 4px 0px ${(ref1 = props.theme.colors[props.color]) === null || ref1 === void 0 ? void 0 : ref1.main}`
                }
            }
        }
    });
}, (0,styled_system__WEBPACK_IMPORTED_MODULE_2__.variant)({
    prop: "size",
    variants: {
        large: {
            padding: "1.25rem"
        },
        medium: {
            padding: "1rem"
        },
        small: {
            padding: "0.75rem",
            fontSize: 14
        },
        too_small: {
            padding: "7px",
            fontSize: 1
        }
    }
}), (0,styled_system__WEBPACK_IMPORTED_MODULE_2__.compose)(styled_system__WEBPACK_IMPORTED_MODULE_2__.color, styled_system__WEBPACK_IMPORTED_MODULE_2__.layout, styled_system__WEBPACK_IMPORTED_MODULE_2__.space, styled_system__WEBPACK_IMPORTED_MODULE_2__.border, styled_system__WEBPACK_IMPORTED_MODULE_2__.shadow));
IconButton.defaultProps = {
    size: "small"
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (IconButton);


/***/ })

};
;