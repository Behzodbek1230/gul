const Container = dynamic(()=>import("../components/Container"),{ssr:false})
import {useState} from "react"
import {FormattedMessage} from "react-intl"
import {KeyboardArrowDown, KeyboardArrowUp} from "@material-ui/icons";
import dynamic from "next/dynamic";
import { Card1 } from "../components/Card1";
import { Grid } from "@mui/material";
let SeoText = ({text})=>{
    let [overflow,setoverflow] = useState(false)
    // let text = useSelector((state:any)=>state.new.seoText)
    if(text!=="" && text){
        return (
            <div data-aos="fade-right">
                <Container data-aos="zoom-out" mb="70px" >
                <Card1  className="col-12 ">
                    <Grid className='index_seo_grid'>
                        {text.length > 300 ?
                         <>
                             {!overflow ?
                                   <>
                                       <div className={`whitespace-break ${!overflow ? "seotext_overflow" :""}`} dangerouslySetInnerHTML={{__html: text}} ></div>
                                       <button className="seotext_button" onClick={()=>setoverflow(true)} ><FormattedMessage id="full"/><KeyboardArrowDown/></button>
                                   </>
                                 :
                                     <>
                                         <div className={`whitespace-break ${!overflow ? "seotext_overflow" :""}`} dangerouslySetInnerHTML={{__html: text}} ></div>
                                         <button className="seotext_button" onClick={()=>setoverflow(false)} ><FormattedMessage id="less"/><KeyboardArrowUp/></button>
                                     </>
                                }
                         </>
                            :
                        <>
                            <div className="whitespace-break" dangerouslySetInnerHTML={{__html: text}} ></div>
                            {/*<button className="seotext_button" onClick={()=>setoverflow(false)} ><FormattedMessage id="less"/><KeyboardArrowUp/></button>*/}

                        </>
                        }
                    </Grid>
                </Card1>
            </Container>
            </div>
        )
    }
    else{
        return <></>
    }
}
export default SeoText
