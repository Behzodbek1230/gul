"use strict";
exports.id = 2519;
exports.ids = [2519];
exports.modules = {

/***/ 2519:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6022);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _hooks_useWindowSize__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(12);
/* harmony import */ var react_intl__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3126);
/* harmony import */ var react_intl__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_intl__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(1853);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var react_lazy_load_image_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(9252);
/* harmony import */ var react_lazy_load_image_component__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_lazy_load_image_component__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(2167);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _Redux_Actions_get_banner__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(9382);
/* harmony import */ var react_intersection_observer__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(9785);
/* harmony import */ var react_intersection_observer__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react_intersection_observer__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var next_dynamic__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(5218);











const Carousel = (0,next_dynamic__WEBPACK_IMPORTED_MODULE_9__["default"])(()=>Promise.all(/* import() */[__webpack_require__.e(1972), __webpack_require__.e(4154), __webpack_require__.e(8661), __webpack_require__.e(9868), __webpack_require__.e(9758)]).then(__webpack_require__.bind(__webpack_require__, 9868))
, {
    ssr: false
});
const Box = (0,next_dynamic__WEBPACK_IMPORTED_MODULE_9__["default"])(()=>__webpack_require__.e(/* import() */ 4875).then(__webpack_require__.bind(__webpack_require__, 4875))
, {
    ssr: false
});
const Typography = (0,next_dynamic__WEBPACK_IMPORTED_MODULE_9__["default"])(()=>__webpack_require__.e(/* import() */ 9374).then(__webpack_require__.bind(__webpack_require__, 9374))
, {
    ssr: false
});
const Section1 = ()=>{
    let router = (0,next_router__WEBPACK_IMPORTED_MODULE_5__.useRouter)();
    const width = (0,_hooks_useWindowSize__WEBPACK_IMPORTED_MODULE_3__/* ["default"] */ .Z)();
    let { 0: banner , 1: setbanner  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)([]);
    let dispatch = (0,react_redux__WEBPACK_IMPORTED_MODULE_2__.useDispatch)();
    let lang = router.locale;
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        axios__WEBPACK_IMPORTED_MODULE_7___default().get(`/banner/list/${lang}`).then((res1)=>{
            if (res1.data.length >= 1) {
                var ref, ref1;
                let banner_id = (res1 === null || res1 === void 0 ? void 0 : (ref = res1.data) === null || ref === void 0 ? void 0 : ref.length) !== 0 && (res1 === null || res1 === void 0 ? void 0 : res1.data) ? (ref1 = res1 === null || res1 === void 0 ? void 0 : res1.data[0]) === null || ref1 === void 0 ? void 0 : ref1.id : "";
                axios__WEBPACK_IMPORTED_MODULE_7___default()({
                    method: "GET",
                    url: `/banner/item-list/${banner_id}/${lang}`
                }).then((res)=>{
                    setbanner(res.data);
                    dispatch((0,_Redux_Actions_get_banner__WEBPACK_IMPORTED_MODULE_10__/* ["default"] */ .Z)(res.data));
                }).catch(()=>{
                    return null;
                });
            }
        }).catch(()=>null
        );
    }, []);
    if ((banner === null || banner === void 0 ? void 0 : banner.length) !== 0 && banner) {
        return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react__WEBPACK_IMPORTED_MODULE_1__.Fragment, {
            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(Box, {
                mb: "30px",
                mt: "9px",
                bg: "gray.white",
                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(Carousel, {
                    totalSlides: (banner === null || banner === void 0 ? void 0 : banner.length) || 0,
                    visibleSlides: 1,
                    infinite: true,
                    autoPlay: true,
                    showDots: true,
                    showArrow: false,
                    spacing: "0px",
                    interval: 4000,
                    dotGroupMarginTop: "-30px",
                    children: banner === null || banner === void 0 ? void 0 : banner.map((banners)=>{
                        return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_intersection_observer__WEBPACK_IMPORTED_MODULE_8__.InView, {
                            triggerOnce: true,
                            children: (props)=>{
                                return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    ref: props.ref,
                                    style: {
                                        position: "relative"
                                    },
                                    children: props.inView && /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                className: "banner_index_carousel_style",
                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_lazy_load_image_component__WEBPACK_IMPORTED_MODULE_6__.LazyLoadImage, {
                                                    style: {
                                                        width: "100%",
                                                        height: "auto",
                                                        objectFit: "cover",
                                                        objectPosition: "center"
                                                    },
                                                    src: width < 650 ? banners.img_s : banners.img,
                                                    alt: banners.title,
                                                    className: "category_image",
                                                    visibleByDefault: false,
                                                    threshold: 0,
                                                    effect: "blur",
                                                    placeholderSrc: ""
                                                })
                                            }),
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                className: "mt-1",
                                                style: width < 750 ? {
                                                    position: "absolute",
                                                    top: "10%",
                                                    left: "10%",
                                                    color: banners.text_color || "red",
                                                    fontSize: "small"
                                                } : {
                                                    position: "absolute",
                                                    top: "10%",
                                                    left: "10%",
                                                    color: banners.text_color || "red"
                                                },
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                                        className: "title fs-sm-6 banner_title banner_title_h2",
                                                        children: banners.title
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(Typography, {
                                                        className: "banner_description",
                                                        mb: "0.35rem",
                                                        children: banners.description
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                        onClick: ()=>router.push(banners.url)
                                                        ,
                                                        className: "btn btn-banner px-0",
                                                        style: width < 750 ? {
                                                            backgroundColor: banners.button_color || "red",
                                                            color: banners.text_color || "red",
                                                            position: "absolute",
                                                            width: "100px",
                                                            paddingTop: "2px",
                                                            paddingBottom: "2px",
                                                            paddingLeft: "0px",
                                                            paddingRight: "0px",
                                                            fontSize: "12px",
                                                            top: "45px"
                                                        } : {
                                                            backgroundColor: banners.button_color || "red",
                                                            color: banners.text_color || "red",
                                                            position: "absolute",
                                                            top: "100px",
                                                            width: "160px"
                                                        },
                                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_intl__WEBPACK_IMPORTED_MODULE_4__.FormattedMessage, {
                                                            id: "banner_button",
                                                            defaultMessage: "Visit Collection"
                                                        })
                                                    })
                                                ]
                                            })
                                        ]
                                    })
                                }, banners === null || banners === void 0 ? void 0 : banners.id));
                            }
                        }));
                    })
                })
            })
        }));
    } else {
        return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
        }));
    }
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Section1);


/***/ })

};
;