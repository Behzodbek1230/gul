import Button from "../../components/buttons/Button";
import { H3, H5 } from "../../components/Typography";
import { FACEBOOK_APP_ID, GOOGLE_CLIENT_ID } from "../../components/Variables";
import { FC } from "react";
import GoogleLogin from "react-google-login";
import { FormattedMessage } from "react-intl";
import InputMask from "react-input-mask";
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'

interface LoginSection2Props{
    handleSubmit5,
    responseGoogle,
    responseFacebook,
    handle222222,
    errors2,
    send_button
}

let Section2:FC<LoginSection2Props> = ({
    handleSubmit5,
    responseGoogle,
    responseFacebook,
    handle222222,
    errors2,
    send_button

})=>{
    return(
        <form
            className="content"
            onSubmit={handleSubmit5}
        >
            <H3
                textAlign="center"
                mb="0.5rem"
            >
                <FormattedMessage
                    id="welcome_login"
                />

            </H3>

            <H5
            fontWeight="600"
            fontSize="12px"
            color="gray.800"
            textAlign="center"
            mb="2.25rem"
            >
                <FormattedMessage
                    id="login_phone"
                />

            </H5>
            <div className="pt-0 mt-0 mb-2" style={{marginLeft:"-7px",marginRight:"0px"}}>
                <div className="row justify-content-between p-0">
                    <div className="col-6" >
                        <GoogleLogin
                            autoLoad={false}
                            clientId={GOOGLE_CLIENT_ID}
                            onSuccess={(r)=>responseGoogle(r)}
                            onFailure={()=>null}
                            className="w-100 py-1 shadow-none border border-muted"
                            render={renderProps=>(
                                <button
                                    type="button"
                                    className="btn w-100 rounded-0  ml-2 border border-muted"
                                    style={{paddingBottom:"9px",paddingTop:"9px"}}
                                    onClick={renderProps.onClick}
                                >
                                    <div
                                        className="d-inline text-left float-left pl-2"
                                        style={{marginTop:"-2px"}}
                                    >
                                        Google
                                    </div>
                                    <img
                                        className="float-right"
                                        style={{marginTop:"2px"}}
                                        src="/assets/images/google.svg"
                                        width={20}
                                        height={20}
                                    />
                                </button>
                            )}
                            />
                    </div>
                    <div className="col-6 ">
                        <FacebookLogin
                            appId={FACEBOOK_APP_ID}
                            fields="name,email,picture"
                            callback={(t)=>responseFacebook(t)}
                            className="m-0 p-0"
                                render={renderProps=>(
                                <button
                                    type="button"
                                    className="btn w-100 py-1 rounded-0   border border-muted"
                                    onClick={renderProps.onClick}
                                >
                                    <div
                                        className="d-inline text-left float-left "
                                        style={{marginTop:"3px"}}
                                    >
                                        Facebook
                                    </div>
                                    <img
                                        className="float-right"
                                        style={{marginTop:"2px"}}
                                        src="/assets/images/Facebook-rounded.svg"
                                        width={30}
                                        height={30}
                                    />
                                </button>
                            )}
                        />
                    </div>
                </div>
            </div>
            <label
                htmlFor="number"
            >
                <FormattedMessage
                    id="phone_number"
                />
            </label>
            <InputMask
            mask="+\9\9\8-99-999-99-99"
            onChange={handle222222}
            >
            {()=>(
                <input
                    data-testid='login_input_desktop'
                    type="text"
                    className="form-control"
                    placeholder={"+998-__-___-__-__"}
                    name="number"
                />
            )}
            </InputMask>

            {errors2 === " " ? "" : (
                <div
                    style={{ color: "red" }}
                >
                    {errors2}
                </div>)}
            <br />
            <Button
                disabled={errors2 !== "" || send_button!==false}
                mb="1.65rem"
                data-testid='login_number_input_desktop_submit'
                variant="contained"
                color="primary"
                type="submit"
                fullwidth
            >
                <FormattedMessage
                    id="send_message"
                />


            </Button>
            <br/>

        </form>
    )
}

export default Section2;
