"use strict";
exports.id = 1972;
exports.ids = [1972];
exports.modules = {

/***/ 1972:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "gh": () => (/* binding */ getTheme),
/* harmony export */   "Q5": () => (/* binding */ convertHexToRGB)
/* harmony export */ });
/* unused harmony export getDateDifference */
/* harmony import */ var _styled_system_theme_get__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(9099);
/* harmony import */ var _styled_system_theme_get__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_styled_system_theme_get__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4146);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(date_fns__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6517);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_2__);



const getTheme = (query, fallback)=>(0,_styled_system_theme_get__WEBPACK_IMPORTED_MODULE_0__.themeGet)(query, fallback)
;
const convertHexToRGB = (hex)=>{
    // check if it's a rgba
    if (hex.match("rgba")) {
        let triplet = hex.slice(5).split(",").slice(0, -1).join(",");
        return triplet;
    }
    let c;
    if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
        c = hex.substring(1).split("");
        if (c.length === 3) {
            c = [
                c[0],
                c[0],
                c[1],
                c[1],
                c[2],
                c[2]
            ];
        }
        c = "0x" + c.join("");
        return [
            c >> 16 & 255,
            c >> 8 & 255,
            c & 255
        ].join(",");
    }
};
const getDateDifference = (date)=>{
    let diff = differenceInMinutes(new Date(), new Date(date));
    if (diff < 60) return diff + " minutes ago";
    diff = ceil(diff / 60);
    if (diff < 24) return `${diff} hour${diff === 0 ? "" : "s"} ago`;
    diff = ceil(diff / 24);
    if (diff < 30) return `${diff} day${diff === 0 ? "" : "s"} ago`;
    diff = ceil(diff / 30);
    if (diff < 12) return `${diff} month${diff === 0 ? "" : "s"} ago`;
    diff = diff / 12;
    return `${diff.toFixed(1)} year${ceil(diff) === 0 ? "" : "s"} ago`;
};


/***/ })

};
;