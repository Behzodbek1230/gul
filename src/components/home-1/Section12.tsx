const Card = dynamic(()=>import("../Card"),{ssr:false})
const Container = dynamic(()=>import("../Container"),{ssr:false})
const Grid = dynamic(()=>import("../grid/Grid"))
const H4 = dynamic(()=>import("../Typography").then((res)=>res.H4),{ssr:false})
const SemiSpan = dynamic(()=>import("../Typography").then((res)=>res.SemiSpan),{ssr:false})
import React, { useEffect, useState } from "react"
import { LazyLoadImage } from "react-lazy-load-image-component";
import { useRouter } from "next/router";
import axios from "axios";
import dynamic from "next/dynamic";
import FlexBox from "../FlexBox"
const Section12: React.FC = () => {
  let [serviceList,setservice] = useState([])
  let lang = useRouter().locale
  useEffect(()=>{
    axios.get(`/references/services/${lang}`)
            .then(res=>{
                setservice(res.data)
            })
            .catch(()=>null)
  },[lang])
  return (
    <Container data-aos="zoom-in" mb="70px" >

        <Grid
            container
            className="p-0 m-0 justify-content-between"
            spacing={2}

        >
        {serviceList?.map((item, ind) => (
          <Grid
              item
              lg={3}
              sm={6}
              xs={6}
              key={ind}
          >
            <div data-aos={ind%2==0 ? 'fade-right' : "fade-left"}>
                <FlexBox
                  as={Card}
                  flexDirection="column"
                  alignItems="center"
                  p="0rem"
                  className="py-3"
                  height="170px"
                  borderRadius={8}
                  boxShadow="border"
                  hoverEffect
            >
              <FlexBox
                justifyContent="center"
                alignItems="center"
                borderRadius="300px"
                // bg="gray.200"
                size="64px"

              >
                <LazyLoadImage
                  src={item.icon_name}
                  alt={item?.title}
                  width={28}
                  height={28}
                  visibleByDefault={false}
                  threshold={-10}
                  effect="blur"
                  placeholderSrc=""
                />

              </FlexBox>
              <H4
                  mt="10px"
                  mb="15px"
                  fontSize="14px"
                  textAlign="center"
                  height="20px"
              >
                {item.title}
              </H4>
              <SemiSpan
                  className="p-0 m-0"
                  fontSize="12px"
                  textAlign="center"
              >
                {item.description}
              </SemiSpan>
            </FlexBox>
            </div>
          </Grid>
        ))}
        </Grid>

    </Container>
  );
};



export default Section12;
