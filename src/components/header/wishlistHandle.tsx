import FlexBox from "../../components/FlexBox";
import Icon from "../../components/icon/Icon";
import { Tiny } from "../../components/Typography";
import { FormattedMessage } from "react-intl";
import IconButton from "../../components/buttons/IconButton";
import { FC } from "react";


interface WishlistHandleProps{
    router:any;
    isloggedin:string;
    user2:any,
    wishlist:any,
}  


let WishlistHandle:FC<WishlistHandleProps> = ({
    router,
    isloggedin,
    wishlist,
    user2
})=>(
    <FlexBox ml="20px" alignItems="flex-start">
        <IconButton
            style={{
                zIndex: 0,
                background: "rgba(255, 255, 255, 0)",
                marginLeft:"-12px"
            }}
            color="text.muted"
            bg="white"
            onClick={()=>router.push("/wish-list")}
        >


            <Icon className='d-flex justify-content-center' style={{fontSize:"25px",color:"#ae99d2"}}>
                heart-filled
            </Icon>
            <div style={{fontSize:"small",fontWeight:"lighter",}}>
                <FormattedMessage
                    id="wishlist"
                    defaultMessage="Sevimlilar"
                />
            </div>
        </IconButton>
        {isloggedin !== "true" &&(
            <FlexBox
                borderRadius="300px"
                bg="error.main"
                px="5px"
                py="2px"
                alignItems="center"
                justifyContent="center"
                ml="-1rem"
                mt="3px"
            >
                <Tiny color="white" test-id='desktop_navigation_unauthenticated_wishlist_count'  fontWeight="600">
                    {wishlist.length}
                </Tiny>
            </FlexBox>
        )}
      {!!user2?.data?.favoritesCount  && (
        <FlexBox
          borderRadius="300px"
          bg="error.main"
          px="5px"
          py="2px"
          alignItems="center"
          justifyContent="center"
          ml="-1rem"
          mt="3px"
        >
          <Tiny color="white" data-test='desktop_navigation_authenticated_wishlist_count' fontWeight="600">
           {user2?.data?.favoritesCount}
          </Tiny>
        </FlexBox>
      )}
    </FlexBox>
)

export default WishlistHandle
