import useWindowSize from "../hooks/useWindowSize";
import React from "react";
import Box from "./Box";
import CategorySectionHeader from "./CategorySectionHeader";
import Container from "./Container";

export interface CategorySectionCreatorProps {
  iconName?: string;
  title?: string;
  seeMoreLink?: string;
  id?:string
}

const CategorySectionCreator: React.FC<CategorySectionCreatorProps> = ({
  iconName,
  seeMoreLink,
  title,
  children,
  id
}) => {
  let width = useWindowSize()
  return (
    <Box id={id} mb={width < 650 ? '1.75rem' :"3.75rem"}>
      <Container pb="1rem">
        {title && (
          <div>
            <CategorySectionHeader
                title={title}
                seeMoreLink={seeMoreLink}
                iconName={iconName}
            />
          </div>
        )}

        {children}
      </Container>
    </Box>
  );
};

export default CategorySectionCreator;
