import React, { useRef } from "react";
// import ReactDOM from "react-dom";
import {YMaps, Map, Placemark, GeolocationControl, ZoomControl,FullscreenControl} from "react-yandex-maps";
// import {useDispatch, useSelector} from "react-redux";
// import getcoordinates from "../../Redux/Actions/MapCoordinates"
// import {log} from "util";


const Map_Shop = (props) =>  {
    const coordinate = props.coordinate === null ? [41.316441,69.294861] : props.coordinate
    const mapState = { center: coordinate, zoom: 15 };
    const mapRef = useRef(null);
    return (
        <div className="App">
            <YMaps version="2.1.77"
                   query={{
                       lang:'en_RU',
                       apikey:'7acd9319-78e1-4abe-a6af-d64c62828777',

                   }}>
                <Map
                    width="100%"
                    height={props.height || 450}
                    modules={[
                        "multiRouter.MultiRoute",
                        "coordSystem.geo",
                        "geocode",
                        "util.bounds"
                    ]}
                    state={mapState}
                    instanceRef={(ref) => {
                        if (ref) {
                            mapRef.current = ref;
                        }
                    }}
                >
                    {coordinate !== undefined ? <Placemark geometry={coordinate} /> : ""}

                    <ZoomControl/>
                    <GeolocationControl />
                    <FullscreenControl />
                </Map>
            </YMaps>

        </div>
    );
}
export default Map_Shop;