import FlexBox from '../../components/FlexBox'
import Icon from '../../components/icon/Icon'
import { Tiny } from '../../components/Typography'
import IconButton from "../../components/buttons/IconButton";
import { FormattedMessage } from 'react-intl'

let cartHandle = (cartList) =>(
        <FlexBox ml="20px" alignItems="flex-start">
            <IconButton 
            style={{
                zIndex: 0,
                background: "rgba(255, 255, 255, 0)",
                marginLeft:"-12px"
            }}
            color="text.muted"
            bg="white"
        >
            <Icon className="d-flex justify-content-center header_cart_style2">
                grocery-store (2)
            </Icon>
            <div style={{fontSize:"small",fontWeight:"lighter",}}>
                <FormattedMessage id="cart" defaultMessage="Savatcha"  />
            </div>
            </IconButton>

            {!!cartList.length && (
            <FlexBox
                borderRadius="300px"
                bg="error.main"
                px="5px"
                py="2px"
                alignItems="center"
                justifyContent="center"
                ml="-1rem"
                mt="3px"
            >
                <Tiny color="white" fontWeight="600">
                {cartList.length}
                </Tiny>
            </FlexBox>
            )}
        </FlexBox>
    )

export default cartHandle
