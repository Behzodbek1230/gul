import Box from "../../../../../components/Box";
import IconButton from "../../../../../components/buttons/IconButton";
import Card from "../../../../../components/Card";
import FlexBox from "../../../../../components/FlexBox";
import Grid from "../../../../../components/grid/Grid";
import Hidden from "../../../../../components/hidden/Hidden";
import Icon from "../../../../../components/icon/Icon";
import NavbarLayout from "../../../../../components/layout/NavbarLayout";
import ProductCard4List from "../../../../../components/products/ProductCard4List";
import SearchProductFilterCard from "../../../../../components/products/SearchProductFilterCard";
import Sidenav from "../../../../../components/sidenav/Sidenav";
import { H1, Paragraph } from "../../../../../components/Typography";
import React, { useEffect, useState} from "react";
import axios from "axios";
import {useDispatch, useSelector} from "react-redux";
import {useRouter} from "next/router";
import Cookies from "js-cookie"
import {FormattedMessage, useIntl} from "react-intl";
import useWindowSize from "../../../../../hooks/useWindowSize";
import get_search_results from "../../../../../../Redux/Actions/get_search_results";
import { NextSeo } from "next-seo";
import get_seo_text from "../../../../../../Redux/Actions/get_seo_text";
import SeoText from "../../../../../components/SeoText";

const ProductSearchResult_Page = () => {
  let intl = useIntl()
  const dispatch = useDispatch()
  const view = "grid";
  let info = useSelector((state:any)=>state.new.search_results)
  const width = useWindowSize();
  const isTablet = width < 1025;
  const [from_price,setfrom_price] = useState("")
  const [to_price,setto_price] = useState("")
  const [cat_id,setcat_id] = useState("")
  const [sort,setsort] = useState(null)
  const router = useRouter();
  const { id,page } = router.query;
  let lang = router.locale;
  let current_currency = useSelector((state:any)=>state.token.current_currency)
  useEffect(()=>{
    let currency_id = Cookies.get("currency_id");
    let currency_text = typeof currency_id !== "undefined" ?  `&currency=${currency_id}` : ""
    let url = `/flowers/search/${lang}?page=${page}&from_price=${from_price}&to_price=${to_price}&keyword=${cat_id}&sort_type=${sort}&text=${id}${currency_text}`
    axios(encodeURI(url))
          .then(res=>{
              dispatch(get_seo_text(res?.data?.seo.seotext))
            dispatch(get_search_results(res.data))
          })
          .catch(()=>null)
  },[from_price,to_price,cat_id,sort,id,current_currency,lang,page])
 const sortOptions = [
        {label:intl.formatMessage({ id:"Sort By"}),value: ""},
        { label: intl.formatMessage({id:"low_high"}), value: 1 },
        { label: intl.formatMessage({id:"high_low"}), value: 2 },

    ];

  return (
    <NavbarLayout>
      <NextSeo
          title={info?.seo?.length !==0 && info?.seo ?  info?.seo?.mtitle : ""} 
          description={info?.seo?.length !==0 && info?.seo ? info?.seo?.mdescription : ""} 
          additionalMetaTags={[{
          name: 'keyword',
          content: info?.seo?.length !==0 && info?.seo ? info?.seo.mkeywords :""
          }, ]}
          openGraph={{
            type:"product",
          }}
      />
      <Box pt="20px" pb="20px">
        <FlexBox
          p="1.25rem"
          flexWrap="wrap"
          justifyContent="space-between"
          alignItems="center"
          mb="55px"
          elevation={5}
          as={Card}
        >
          <div>
            <H1 fontSize="15px" style={{marginBottom:"10px",textTransform:"capitalize"}} ><FormattedMessage id="search_for" /> "{id}"</H1>
            <Paragraph color="text.muted">{info?.datas?.total} <FormattedMessage id="results_found" /></Paragraph>
          </div>
          <FlexBox alignItems="right" flexWrap="wrap">

            <Box style={{marginTop:"10px"}} flex="1 1 0" mr="0.6rem" mb="0.4rem" marginRight="-2px" minWidth="190px">
                <select
                    value={sort || sortOptions[0]}
                    className="form-control"
                    id="exampleFormControlSelect1"
                    onChange={(r)=>setsort(r.target.value)}
                >
                  {sortOptions.map(option=> <option key={option.value} value={option.value} >{option.label}</option>)}


                </select>
            </Box>


            {isTablet && (
              <Sidenav
                position="left"
                scroll={true}
                handle={
                  <IconButton size="small" className="ml-2">
                    <Icon>options</Icon>
                  </IconButton>
                }
              >
                <SearchProductFilterCard  setfrom_price={(e)=>setfrom_price(e)}  setto_price={(e)=>setto_price(e)}  setcat_id={(g)=>setcat_id(g)} />
              </Sidenav>
            )}
          </FlexBox>
        </FlexBox>

        <Grid container spacing={6}>
          <Hidden as={Grid} item lg={3} xs={12} down={1024}>
            <SearchProductFilterCard  setfrom_price={(e)=>setfrom_price(e)}  setto_price={(e)=>setto_price(e)}  setcat_id={(g)=>setcat_id(g)} />
          </Hidden>

          <Grid item lg={9} xs={12}>
            {view === "grid" ? <ProductCard4List  /> :  <ProductCard4List  />}
          </Grid>
        </Grid>
      </Box>
        <SeoText text={info?.seo?.seotext}/>
    </NavbarLayout>
  );
};



export default ProductSearchResult_Page;
