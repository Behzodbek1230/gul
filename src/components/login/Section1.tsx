import Button from "../../components/buttons/Button"
import { H3 } from "../../components/Typography"
import { FormattedMessage } from "react-intl"
import InputMask from "react-input-mask";
import { FC } from "react";

interface LoginSection1Props{
    handleSubmit4:any,
    handlecode:any,
    errors3,
    error,
    code,
    button,
    setchange,
    handleSubmit5


}


let Section1:FC<LoginSection1Props> = ({
    handleSubmit4,
    handlecode,
    errors3,
    error,
    code,
    button,
    setchange,
    handleSubmit5
})=>{
    return(
        <div>
              <form
                  className="content"
                  onSubmit={handleSubmit4}
              >
                  <H3
                      textAlign="center"
                      mb="0.5rem"
                  >
                     <FormattedMessage
                         id="welcome_login"
                     />
                  </H3>
                  <H3
                    fontWeight="600"
                    fontSize="15px"
                    color="green"
                    textAlign="center"
                    mb="2.25rem"
                  >
                      <FormattedMessage
                          id='input_sms'
                          defaultMessage="Kodni kiriting"
                      />
                  </H3>
                  <label
                      htmlFor="number"
                  >
                      <FormattedMessage
                          id="kod"
                      />
                  </label>
                  <InputMask
                      mask="9999"
                      onChange={handlecode}
                  >
                    {(inputProp)=>(
                       <input
                           {...inputProp}
                           type="tel"
                           className="form-control"
                           placeholder="____"
                           name="kod"
                       />
                    )}
                  </InputMask>

                  {errors3 === " " ? "" : (
                  <div
                      style={{ color: "red" }}
                  >
                      {errors3}
                  </div>)}
                  {error!== " " ?
                          <div
                              style={{color:"red"}}
                          >
                              {error}
                          </div>
                      :
                        ""
                  }
                  <br />
                  <Button
                      disabled={code?.toString()?.length < 4 && code}
                      mb="0.6rem"
                      variant="contained"
                      color="primary"
                      type="submit"
                      fullwidth
                  >
                      <FormattedMessage
                          id="send"
                          defaultMessage="Jo'natish"
                      />
                  </Button>

                {button ? (
                            <div className="mb-2">
                                <FormattedMessage
                                    id="send_ask_again"
                                    defaultMessage="Kodni qaytadan jo'natishni 1 minutdan so'ng so'rashingiz mukin " />
                            </div>
                          )
                        :
                        ""
                }
                <div className="d-flex justify-content-between">
                    <Button
                      onClick={()=>setchange(false)}

                      mb="1.65rem"
                      variant="contained"
                      className="bg-success text-white w-50 mr-1"
                      type="submit"

                  >
                    <FormattedMessage
                        id="go_back"
                    />
                  </Button>
                  <Button
                      className="w-50 ml-1 px-0"
                      onClick={handleSubmit5}
                      disabled={button === true}
                      mb="1.65rem"
                      variant="contained"
                      color="primary"
                      type="submit"

                  >
                    <FormattedMessage
                        id="send_again"
                    />
                  </Button>
                </div>
                </form>

            </div>
    )
}


export default Section1
