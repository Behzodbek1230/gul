import FlexBox from "../../../components/FlexBox";
import Grid from "../../../components/grid/Grid";
import Hidden from "../../../components/hidden/Hidden";
import Icon from "../../../components/icon/Icon";
import ShopIntroCard from "../../../components/shop/ShopIntroCard";
import Sidenav from "../../../components/sidenav/Sidenav";
import { useEffect, useState} from "react";
import axios from "axios";
import {useDispatch, useSelector} from "react-redux";
import {H5} from "../../../components/Typography";
import Box from "../../../components/Box";
import Shop_Review from "../../../components/Shop_Review";
import ProductCard3List from "../../../components/products/ProductCard3List";
import {useRouter} from "next/router";
import Cookies from "js-cookie"
import ProductFilterCard3 from "../../../components/products/ProductFilterCard3";
import {FormattedMessage} from "react-intl";
import useWindowSize from "../../../hooks/useWindowSize";
import { BASE_URL } from "../../../components/Variables";
import AppLayout from "../../../components/layout/AppLayout";
import Container from "../../../components/Container";
import { NextSeo } from "next-seo";
import get_search_results from "../../../../Redux/Actions/get_search_results";
import get_one_shop_products from "../../../../Redux/Actions/get_one_shop_products";
import get_shop_info from "../../../../Redux/Actions/get_shop_info";
const Shop_page = ({shop_data,shop_products}) => {
    const router = useRouter()
    const data4 = useSelector((state:any)=>state.new.shop)
    const dispatch = useDispatch()
    const [from_price,setfrom_price] = useState("")
    const [shop,setshop] = useState(shop_data)
    const [to_price,setto_price] = useState("")
    const [cat_id,setcat_id] = useState("")
    let [reviews,setreviews] = useState({data:[],last_page:"0"})
    let [ReviewPage,setReviewPage] = useState(0)
    let [addedStatus,setaddedStatus] = useState("")
    const sort = ""
    const width = useWindowSize();
    const isTablet = width < 1025;
    const [selectedOption, setSelectedOption] = useState("description");
    let lang = useSelector((state:any)=>state.new.lang) || Cookies.get("lang");
    let current_currency = useSelector((state:any)=>state.token.current_currency)
    const { id } = router.query
    useEffect(()=> {
        setreviews(data4)
    },[data4])
    dispatch(get_one_shop_products(shop_products))
    useEffect(()=>{
        dispatch(get_shop_info(shop_data))
        dispatch(get_search_results(shop_products))
    },[])
    useEffect(()=>{
        axios(`/shops/show/${id}/${lang}`)
            .then((res)=>{
                setshop(res.data)
                dispatch(get_shop_info(res.data))

            })
            .catch(()=>null)
    },[lang])
    useEffect(()=>{
        axios.get(`/shops/rating-list/${id}/${lang}?page=${ReviewPage+1}`)
            .then(res=>{
                setreviews(res.data)
            })
            .catch(()=>null)
    },[ReviewPage,addedStatus])
    useEffect(()=>{
        let currency_id = Cookies.get("currency_id");
        let currency_text = typeof currency_id !== "undefined" ?  `&currency=${currency_id}` : ""
            let sort_type =  "";
            let url = `/flowers/search/${lang}?from_price=${from_price}&to_price=${to_price}&keyword=${cat_id}&sort_type=${sort_type}&shop_keyword=${id}${currency_text}`
            axios(url)
                .then(res=>{
                    dispatch(get_search_results(res.data))
                })
                .catch(()=>{
                    return null;
                })

    },[from_price,to_price,cat_id,sort,id,current_currency])

    const handleOptionClick = (opt) => () => {
        setSelectedOption(opt);
    }
    return (
           <AppLayout >
               <NextSeo
                   title={shop?.seo?.view_mtitle}
                   description={ shop_data?.seo?.view_mdescription}
                   additionalMetaTags={[{
                       name: 'keyword',
                       content: shop_data?.seo?.view_mkeywords
                   }, ]}
                   openGraph={{
                       title:shop_data?.seo?.view_share_title,
                       description:shop_data?.seo?.view_share_description,
                       site_name:shop_data?.seo?.view_share_sitename,
                       profile: {
                           firstName:shop_data?.data?.name,
                           username: shop_data?.data?.keyword,
                       },
                       images:[
                           {
                               url:shop_data?.data?.logo,
                               alt:shop_data?.data?.name,
                               width:400,
                               height:400
                           }
                       ],

                   }}
               />
               <Container className="mt-50">
                   <div>
                       <ShopIntroCard   />

                       <FlexBox
                           borderBottom="1px solid"
                           borderColor="gray.400"
                           mt="80px"
                           mb="26px"
                           className="whitespace-break word-break"
                       >
                           <H5
                               className="cursor-pointer"
                               mr="10px"
                               p="4px 10px"
                               color={
                                   selectedOption === "description" ? "primary.main" : "text.muted"
                               }
                               borderBottom={selectedOption === "description" && "2px solid"}
                               borderColor="primary.main"
                               onClick={handleOptionClick("description")}
                           >
                               <FormattedMessage
                                   id="Products"
                                   defaultMessage="Mahsulotlar"
                               />
                           </H5>
                           <H5
                               className="cursor-pointer"
                               p="4px 10px"
                               mr="10px"
                               color={selectedOption === "review" ? "primary.main" : "text.muted"}
                               onClick={handleOptionClick("review")}
                               borderBottom={selectedOption === "review" && "2px solid"}
                               borderColor="primary.main"
                           >
                               <FormattedMessage
                                   id="Review"
                                   defaultMessage="Tavsif"
                               />
                           </H5>
                           <H5
                               className="cursor-pointer"
                               p="4px 10px"

                               color={selectedOption === "contact" ? "primary.main" : "text.muted"}
                               onClick={handleOptionClick("contact")}
                               borderBottom={selectedOption === "contact" && "2px solid"}
                               borderColor="primary.main"
                           >
                               <FormattedMessage
                                   id="description"
                                   defaultMessage="Tavsif"
                               />
                           </H5>
                       </FlexBox>
                       <Box mb="50px">
                           {
                               selectedOption === "description"
                               &&
                               <Grid container spacing={6}>
                                   <Hidden as={Grid} item md={3} xs={12} down={1024}>
                                       <ProductFilterCard3
                                           setfrom_price={(e)=>setfrom_price(e)}
                                           setto_price={(e)=>setto_price(e)}
                                           setcat_id={(g)=>setcat_id(g)}
                                       />
                                   </Hidden>

                                   <Grid item md={9} xs={12}>
                                       {isTablet && (
                                           <Sidenav
                                               position="left"
                                               scroll={true}
                                               handle={
                                                   <FlexBox justifyContent="flex-end" mb="12px">
                                                       <Icon>options</Icon>
                                                   </FlexBox>
                                               }
                                           >
                                               <ProductFilterCard3
                                                   setfrom_price={(e)=>setfrom_price(e)}
                                                   setto_price={(e)=>setto_price(e)}
                                                   setcat_id={(g)=>setcat_id(g)}
                                               />
                                           </Sidenav>
                                       )}
                                       <ProductCard3List />
                                   </Grid>
                               </Grid>
                           }
                           {selectedOption === "review" &&
                               <div
                                   style=
                                       {{
                                           backgroundColor:"white",
                                           borderRadius:"15px",
                                           padding:"20px",
                                       }}
                               >
                                   <Shop_Review setaddedstatus={(e)=>setaddedStatus(e)}   reviews={reviews} setreviews={(r)=>setreviews(r)} setReviewPage={(e)=>setReviewPage(e)} ReviewPage={ReviewPage} />
                               </div>
                           }
                           {
                               selectedOption === "contact" &&
                               <div className="shop_description_style ">
                                   <div className="whitespace-break" dangerouslySetInnerHTML={{__html:shop_data?.data?.description}} ></div>
                               </div>
                           }
                       </Box>

                   </div>
               </Container>
           </AppLayout>
    );
};


export async function getStaticPaths() {
    let paths=[{params:{id:"safia"}}]
    return { paths, fallback: 'blocking' }
}


export async function getStaticProps(ctx){
        let {id} = ctx.params
        let lang = ctx.locale
        try{
            const shop_request = await axios.get(`${BASE_URL}/shops/show/${id}/${lang}`)
            return {
                props:{
                    shop_data:shop_request.data,
                    shop_products:[]
                },
                revalidate:2
            }
        }
        catch{
            return {
                notFound:true
            }
        }

}
export default Shop_page;
