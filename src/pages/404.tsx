import Button from "../components/buttons/Button";
import FlexBox from "../components/FlexBox";
import Image from "../components/Image";
import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";
import {FormattedMessage, useIntl} from "react-intl";
import {NextSeo} from "next-seo";

const Error404 = () => {
  const router = useRouter()
  let intl = useIntl()
  // console.log(router.asPath.split('location=')[1])
  const handleGoBack = async () => {
    router.back();
  };

  return (
    <FlexBox
      flexDirection="column"
      minHeight="100vh"
      justifyContent="center"
      alignItems="center"
      px="1rem"
    >
      <NextSeo
          title={intl.formatMessage({id:"not_found"})}
          description={intl.formatMessage({id:"not_found"})}
          noindex={true}
          nofollow={true}
          additionalMetaTags={[
            {
              name: 'keyword',
              content:intl.formatMessage({id:"not_found"})
            },

          ]}

      />
      <Image
        src="/assets/images/illustrations/404.svg"
        maxWidth="320px"
        width="100%"
        mb="2rem"
      />
      <FlexBox flexWrap="wrap">
        <Button
          variant="outlined"
          color="primary"
          m="0.5rem"
          onClick={handleGoBack}
        >
          <FormattedMessage
            id="go_back"
            defaultMessage="Qaytish"
          />
        </Button>
        <Link href="/">
          <Button variant="contained" color="primary" m="0.5rem">
            <FormattedMessage
              id="go_home"
              defaultMessage="Asosiy sahifaga o'tish"
            />
          </Button>
        </Link>
      </FlexBox>
    </FlexBox>
  );
};


// export async function getStaticProps(ctx){
//
// }

export default Error404;
