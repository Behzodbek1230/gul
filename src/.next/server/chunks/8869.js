"use strict";
exports.id = 8869;
exports.ids = [8869];
exports.modules = {

/***/ 8869:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_intl__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3126);
/* harmony import */ var react_intl__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_intl__WEBPACK_IMPORTED_MODULE_1__);


let DeliveryTimeOptions = ({ time , setTime  })=>{
    let intl = (0,react_intl__WEBPACK_IMPORTED_MODULE_1__.useIntl)();
    let min = intl.formatMessage({
        id: "min"
    });
    let hour = intl.formatMessage({
        id: "hour"
    });
    let timeOptions = [
        {
            value: "30",
            text: `30 ${min}`
        },
        {
            value: "40",
            text: `40 ${min}`
        },
        {
            value: "50",
            text: `50 ${min}`
        },
        {
            value: "60",
            text: `1 ${hour}`
        },
        {
            value: "70",
            text: `1 ${hour} 10 ${min}`
        },
        {
            value: "80",
            text: `1 ${hour} 20 ${min}`
        },
        {
            value: "90",
            text: `1 ${hour} 30 ${min}`
        },
        {
            value: "100",
            text: `1 ${hour} 40 ${min}`
        },
        {
            value: "110",
            text: `1 ${hour} 50 ${min}`
        },
        {
            value: "120",
            text: `2 ${hour}`
        },
        {
            value: "150",
            text: `2 ${hour} 30 ${min}`
        },
        {
            value: "180",
            text: `3 ${hour}`
        }, 
    ];
    const handleTimeChange = (e)=>{
        setTime(e.target.value);
    };
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("select", {
            required: true,
            className: "form-control",
            value: time,
            onChange: (e)=>handleTimeChange(e)
            ,
            name: "DeliveryTimeOptions",
            id: "DeliveryTimeOptions",
            children: [
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("option", {
                    hidden: true,
                    children: intl.formatMessage({
                        id: "choose_delivery_time"
                    })
                }),
                timeOptions.map((one)=>/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("option", {
                        value: one.value,
                        children: [
                            " ",
                            one.text,
                            " "
                        ]
                    })
                )
            ]
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (DeliveryTimeOptions);


/***/ })

};
;