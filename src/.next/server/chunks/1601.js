"use strict";
exports.id = 1601;
exports.ids = [1601,4875,4660];
exports.modules = {

/***/ 4875:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5834);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_system__WEBPACK_IMPORTED_MODULE_1__);


const Box = styled_components__WEBPACK_IMPORTED_MODULE_0___default().div(({ shadow , cursor , transition , theme  })=>({
        boxShadow: theme.shadows[shadow],
        cursor,
        transition
    })
, (0,styled_system__WEBPACK_IMPORTED_MODULE_1__.compose)(styled_system__WEBPACK_IMPORTED_MODULE_1__.layout, styled_system__WEBPACK_IMPORTED_MODULE_1__.space, styled_system__WEBPACK_IMPORTED_MODULE_1__.color, styled_system__WEBPACK_IMPORTED_MODULE_1__.position, styled_system__WEBPACK_IMPORTED_MODULE_1__.flexbox, styled_system__WEBPACK_IMPORTED_MODULE_1__.flex, styled_system__WEBPACK_IMPORTED_MODULE_1__.border, styled_system__WEBPACK_IMPORTED_MODULE_1__.typography));
Box.defaultProps = {
    shadow: 0,
    cursor: "unset"
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Box);


/***/ }),

/***/ 4660:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5834);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_system__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _utils_utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1972);
/* harmony import */ var _Box__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(4875);




const Card = styled_components__WEBPACK_IMPORTED_MODULE_0___default()(_Box__WEBPACK_IMPORTED_MODULE_3__["default"])`
  background-color: ${(0,_utils_utils__WEBPACK_IMPORTED_MODULE_2__/* .getTheme */ .gh)("colors.body.paper")};
  box-shadow: ${(props)=>(0,_utils_utils__WEBPACK_IMPORTED_MODULE_2__/* .getTheme */ .gh)(`shadows.${props.boxShadow}`, `shadows.${props.elevation}`)
};

  :hover {
    box-shadow: ${(props)=>props.hoverEffect && (0,_utils_utils__WEBPACK_IMPORTED_MODULE_2__/* .getTheme */ .gh)("shadows.large")
};
  }

  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.border}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.color}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.space}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.layout}
`;
Card.defaultProps = {
    boxShadow: "small",
    borderRadius: 8,
    hoverEffect: false
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Card);


/***/ }),

/***/ 9778:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "A": () => (/* binding */ Chip),
/* harmony export */   "b": () => (/* binding */ ChipwithoutRounded)
/* harmony export */ });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5834);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_system__WEBPACK_IMPORTED_MODULE_1__);


const Chip = (styled_components__WEBPACK_IMPORTED_MODULE_0___default().div)`
  display: inline-flex;
  cursor: ${(props)=>props.cursor || "unset"
};
  box-shadow: ${(props)=>props.boxShadow || "unset"
};
  transition: all 150ms ease-in-out;
  border-radius:15px;
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.space}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.color}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.position}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.typography}
`;
const ChipwithoutRounded = (styled_components__WEBPACK_IMPORTED_MODULE_0___default().div)`
  display: inline-flex;
  cursor: ${(props)=>props.cursor || "unset"
};
  box-shadow: ${(props)=>props.boxShadow || "unset"
};
  transition: all 150ms ease-in-out;
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.space}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.color}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.position}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.typography}
`;


/***/ }),

/***/ 1601:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ product_cards_ProductCard6)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: ./components/Card.tsx
var Card = __webpack_require__(4660);
// EXTERNAL MODULE: ./components/Chip.tsx
var Chip = __webpack_require__(9778);
// EXTERNAL MODULE: external "styled-components"
var external_styled_components_ = __webpack_require__(7518);
var external_styled_components_default = /*#__PURE__*/__webpack_require__.n(external_styled_components_);
// EXTERNAL MODULE: ./components/Box.tsx
var Box = __webpack_require__(4875);
;// CONCATENATED MODULE: ./components/HoverBox.tsx


const HoverBox = external_styled_components_default()(Box["default"])`
  position: relative;
  :after {
    position: absolute;
    content: " ";
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    z-index: 1;
    transition: all 250ms ease-in-out;
  }
  :hover:after {
    background: rgba(0, 0, 0, 0.3);
  }
`;
HoverBox.defaultProps = {
    overflow: "hidden"
};
/* harmony default export */ const components_HoverBox = (HoverBox);

// EXTERNAL MODULE: ./hooks/useWindowSize.js
var useWindowSize = __webpack_require__(12);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
;// CONCATENATED MODULE: ./components/product-cards/ProductCard6.tsx




// import LazyImage from "../../components/LazyImage";


const ProductCard6 = ({ title , imgUrl , s_imgUrl  })=>{
    const size = (0,useWindowSize/* default */.Z)();
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(Card["default"], {
        position: "relative",
        style: {
            marginLeft: "20px",
            cursor: "pointer"
        },
        className: "d-flex flex-column align-items-center bg-transparent shadow-none",
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx(components_HoverBox, {
                position: "relative",
                className: "productcard6_product_Style",
                children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                    className: "category_image ",
                    src: size < 750 ? s_imgUrl : imgUrl,
                    alt: title
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                style: {
                    textAlign: "center"
                },
                children: /*#__PURE__*/ jsx_runtime_.jsx(Chip/* ChipwithoutRounded */.b, {
                    className: "productcard6_chip text-wrap font-weight-normal",
                    color: "white",
                    fontWeight: "600",
                    p: "7px 20px",
                    top: "6.375rem",
                    left: "1.5rem",
                    zIndex: 2,
                    children: title
                })
            })
        ]
    }));
};
/* harmony default export */ const product_cards_ProductCard6 = (ProductCard6);


/***/ })

};
;