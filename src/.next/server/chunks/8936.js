"use strict";
exports.id = 8936;
exports.ids = [8936];
exports.modules = {

/***/ 8936:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "wI": () => (/* binding */ AppProvider),
  "bp": () => (/* binding */ useAppContext)
});

// UNUSED EXPORTS: default

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "js-cookie"
var external_js_cookie_ = __webpack_require__(6734);
var external_js_cookie_default = /*#__PURE__*/__webpack_require__.n(external_js_cookie_);
;// CONCATENATED MODULE: ./reducers/cartReducer.ts

const CHANGE_CART_AMOUNT = "CHANGE_CART_AMOUNT";
let isLoggedIn = external_js_cookie_default().get("IsLoggedIn");
const cartInitialState = {
    cartList: []
};
const cartReducer = (state, action)=>{
    switch(action.type){
        case CHANGE_CART_AMOUNT:
            let cartList = state.cartList;
            let cartItem = action.payload;
            let exist = cartList.find((item)=>item.id === cartItem.id
            );
            if (cartItem.qty < 1) return {
                cartList: cartList.filter((item)=>item.id !== cartItem.id
                )
            };
            else if (exist) {
                if (isLoggedIn === "true") {
                    return {
                        cartList: cartList.map((item)=>{
                            if (item.id === cartItem.id) return {
                                ...item,
                                qty: cartItem.qty
                            };
                            else return item;
                        })
                    };
                } else {
                    return {
                        cartList: cartList.map((item)=>{
                            if (item.id === cartItem.id) return {
                                ...item,
                                qty: cartItem.qty
                            };
                            else return item;
                        })
                    };
                }
            } else return {
                cartList: [
                    ...cartList,
                    cartItem
                ]
            };
        default:
            {
            }
    }
};

;// CONCATENATED MODULE: ./reducers/combineReducers.ts
const combineReducers = (reducers)=>{
    return (state = {
    }, action)=>{
        const newState = {
        };
        for(let key in reducers){
            newState[key] = reducers[key](state[key], action);
        }
        return newState;
    };
};
/* harmony default export */ const reducers_combineReducers = (combineReducers);

;// CONCATENATED MODULE: ./reducers/layoutReducer.ts
const layoutInitialState = {
    isHeaderFixed: false
};
const layoutReducer = (state, action)=>{
    switch(action.type){
        case "TOGGLE_HEADER":
            return {
                ...state,
                isHeaderFixed: action.payload
            };
        default:
            {
            }
    }
};

;// CONCATENATED MODULE: ./reducers/rootReducer.ts



const initialState = {
    layout: layoutInitialState,
    cart: cartInitialState
};
const rootReducer = reducers_combineReducers({
    layout: layoutReducer,
    cart: cartReducer
});

;// CONCATENATED MODULE: ./contexts/app/AppContext.tsx


// import { ContextDevTool } from "react-context-devtool";

const AppContext = /*#__PURE__*/ (0,external_react_.createContext)(null);
const AppProvider = ({ children  })=>{
    const { 0: state , 1: dispatch  } = (0,external_react_.useReducer)(rootReducer, initialState);
    const contextValue = (0,external_react_.useMemo)(()=>{
        return {
            state,
            dispatch
        };
    }, [
        state,
        dispatch
    ]);
    return(/*#__PURE__*/ jsx_runtime_.jsx(AppContext.Provider, {
        value: contextValue,
        children: children
    }));
};
const useAppContext = ()=>(0,external_react_.useContext)(AppContext)
;
/* harmony default export */ const app_AppContext = ((/* unused pure expression or super */ null && (AppContext)));


/***/ })

};
;