import React, {useState} from "react";
import Button from "../components/buttons/Button";
import {DialogActions, DialogContent} from "@mui/material";
import {FormattedMessage, useIntl} from "react-intl";
import axios from "axios";
import {useRouter} from "next/router";

export type BalanceTypes = {
    setopen2?:any
}

let Balance:React.FC<BalanceTypes> = ({setopen2})=>{
    let intl = useIntl()
    let router = useRouter()
    let [amount,setamount] = useState(null)
    let [service_name,setservice] = useState(undefined)
    let [error,seterror] = useState("")
    let [amounterror,setamounterror] = useState("")
    let lang = useRouter().locale
    let data = new FormData()
    data.append("price",amount)
    data.append("service_name",service_name)
    let cards = [
        {
            value:"click2",
            img:"/assets/images/payments_now/paymeee.svg"
        },
    ]
    let handlePayment = ()=>{
       if(typeof service_name === "undefined"){
            seterror(intl.formatMessage({id:"choose_payment"}))
           setTimeout(()=>{
               seterror("")
           },3000)
       }
       else if(amount < 1000) {
           setamounterror(intl.formatMessage({id: "more_than_1000"}))
           setTimeout(() => {
               setamounterror("")
           }, 3000)
       }
       else{
           axios({
               method:"POST",
               url:`/profile/payment-balance/${lang}`,
               data:data
           })
               .then(res=>{
                   router.push(res.data)
               })
               .catch(()=>null)
       }
    }
    const handleAmountChange = (w) =>{
        setamount(w.target.value)
    }
    return(<>
            <DialogContent>
                <div className="mb-2" >
                    <FormattedMessage id="budget_fulfill" />
                </div>
                <div className="input-group mb-3">
                    <input 
                        type="number" 
                        value={amount}  
                        required={true} 
                        onChange={(w)=>handleAmountChange(w)} 
                        className="form-control"
                        aria-label="`Balance` payment" 
                        aria-describedby="basic-addon2" 
                    />

                        <div className="input-group-append">
                            <span className="input-group-text" id="basic-addon2"><FormattedMessage id="sum" /></span>
                        </div>
                </div>
                {amounterror === "" ? "" : <div className="text-danger mb-3" >{amounterror}</div>}
                <>
                    <div><FormattedMessage id="payment_methods" /></div>
                    <div className="flex flex-wrap pb-3 justify-content-between">
                    {cards?.map((card)=>card.value === service_name ?
                        <div className="positon-relative">
                            <div className="check_order_payment">
                                <img src="/assets/images/check.svg" width="25px" height="25px" alt="check" />
                            </div>
                            <button

                                type="button"
                                onClick={()=>setservice(card.value)}
                                className="btn border border-black mt-2"
                            >
                                <img
                                    src={card.img}
                                    alt={card.value}
                                    width="60"
                                    height="60"
                                />
                            </button>
                        </div>
                        :
                        (

                            <button

                                type="button"
                                onClick={()=>setservice(card.value)}
                                className="btn border border-black mt-2"
                            >
                                <img
                                    src={card.img}
                                    width="60"
                                    height="60"
                                />
                            </button>
                        )
                    )}
                    {error === "" ? "" : <div className="text-danger">{error}</div> }
                </div></>
            </DialogContent>
            <DialogActions>
                <Button onClick={()=>setopen2(false)}><FormattedMessage id="cancel" /></Button>
                <Button onClick={()=>handlePayment()}><FormattedMessage id="pay" /></Button>
            </DialogActions>
        </>
    )

}
export default Balance;
