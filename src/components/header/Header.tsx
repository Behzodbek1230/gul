import IconButton from '../../components/buttons/IconButton'
import Image from '../../components/Image'
import Container from '../../components/Container'
import MiniCart from '../../components/mini-cart/MiniCart'
import Login from '../../components/sessions/Login'
import UserLoginDialog from './UserLoginDialog'
import CheckStatus from '../../components/OrderStatus/CheckStatus'
import { ModalBody } from 'reactstrap'
import Button from '../../components/buttons/Button'
import SearchBox from '../../components/search-box/SearchBox'
import Box from '../../components/Box'
import FlexBox from '../../components/FlexBox'
import Sidenav from '../../components/sidenav/Sidenav'
import Icon from '../../components/icon/Icon'
import WishlistHandle from './wishlistHandle'
import StyledHeader  from './HeaderStyle'
import LoggedInUserMenuList from "./loggedInUserMenuItems"
import Paper from "@material-ui/core/Paper"
import Grow from '@material-ui/core/Grow'
import Popper from '@material-ui/core/Popper'
import cartHandle from './cartHandle'
import Cookies from 'js-cookie'
import {Modal,ModalHeader} from 'reactstrap'
import { useAppContext } from "../../contexts/app/AppContext";
import Link from "next/link";
import React, {useEffect, useState} from "react";
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import { makeStyles } from '@material-ui/core/styles';
import {useDispatch, useSelector} from "react-redux";
import logout from "../../../Redux/Actions/LogoutAction";
import {useRouter} from "next/router";
import axios from "axios";
import {fetch_user_info} from "../../../Redux/Actions/Action";
import {FormattedMessage} from "react-intl";
import change_modal_status from "../../../Redux/Actions/change_modal_status";

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  paper: {
    marginRight: theme.spacing(2),
  },
}));



type HeaderProps = {
  isFixed?: boolean;
  className?: string;
};

const Header: React.FC<HeaderProps> = ({  className }) => {
  const router = useRouter()
  const [open2, setOpen2] = useState(false);
  const anchorRef = React.useRef(null);
  const info = useSelector((state:any)=>state.new.company_info)
  const dispatch2 = useDispatch()
  const [open, setOpen] = useState(false);
  const toggleSidenav = () => setOpen(!open);
  const { state } = useAppContext();
  const { cartList } = state.cart;
  const user2 = useSelector((state:any)=>state.token.user)
  const wishlist = useSelector((state:any)=>state.new.wishlist)
  const [user,setuser] = useState(user2)
  let [isloggedin,setloggedin] = useState("false");
  let modal = useSelector((state:any)=>state.new.modal)
  let loggedIn = Cookies.get("isLoggedIn")
  let lang = router.locale
  useEffect(()=>{
      setloggedin(Cookies.get("isLoggedIn"))
  },[loggedIn])

  let userInfoFetch = () =>{
    axios(`/profile/max-value/${lang}`)
    .then(response2=>{
        if(response2.data?.data?.id){
            dispatch2(fetch_user_info(response2.data))
            setuser(response2.data)
            Cookies.remove("isLoggedIn")
            Cookies.set("isLoggedIn","true",{expires:1})
        }
        else if(response2.data?.errors){
            Cookies.remove("isLoggedIn")
            Cookies.remove("token")
            Cookies.set("isLoggedIn","false",{expires:1})
            setloggedin("false")
        }
    })
  }

  useEffect(()=>{
      userInfoFetch()
  },[])

  useEffect(() => {
    userInfoFetch()
  }, [loggedIn]);


  //-----------------------------------------------------
  let classes = useStyles();
  let shop2;
  try {
      shop2 = user2.data?.is_shops
  }
  catch (e){
      try{
          let loggedin = Cookies.get("isLoggedIn") || "false";
          if(loggedin === "true" && user.errors){
              dispatch2(logout())
              Cookies.remove("isLoggedIn")
              Cookies.set("isLoggedIn","false",{expires:1})
              shop2=null
          }
      }
      catch{return  null}
  }


  const handleToggle = () => {
    setOpen2((prevOpen) => !prevOpen);
  };

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return  null;
    }

    setOpen2(false);
  };

 

  return (
  <StyledHeader className={className}>
      <Container
        display="flex"
        alignItems="center"
        justifyContent="space-between"
        height='100%'
      >
        <FlexBox className="logo" alignItems="center" mr="1rem">
          <Link href="/">
            <a>
              <Image
                  src={info.logo}
                  className="logo_image"
                  alt="logo"
              />
            </a>
          </Link>
        </FlexBox>

        <FlexBox  flex="1 1 0">
          <SearchBox />
        </FlexBox>

        <FlexBox
            className="header-right"
            alignItems="right"
        >
            <IconButton
                onClick={()=>dispatch2(change_modal_status(!modal))}
                style=
                    {{
                        zIndex:0,
                        background: "rgba(255, 255, 255, 0)",
                        marginLeft:"-5px",
                        width:"112px",
                        overflowX:"hidden",
                        borderRadius:"0px",
                        wordBreak:"keep-all",
                        wordWrap:"revert"
                    }}
                color="text.muted"
                bg="white"
            >
                <Icon style={{fontSize:"25px",marginLeft:"2px",left:"40%",position:"relative",marginBottom:"1px"}}>
                    order_s
                </Icon>
                <div
                    style={{
                        fontSize:"small",
                        fontWeight:"lighter",
                        marginLeft:"2px",
                        whiteSpace:"nowrap",
                    }}
                >
                    <FormattedMessage
                        id="order_status"
                        defaultMessage="Buyurtma xolati"
                    />
                </div>

            </IconButton>

            <Modal  isOpen={modal} toggle={()=>dispatch2(change_modal_status(!modal))} className="modal-dialog-centered">
                <ModalHeader toggle={()=>dispatch2(change_modal_status(!modal))}><FormattedMessage id="order_status" defaultMessage="Buyurtma holati" /></ModalHeader>
                <ModalBody >
                    <CheckStatus />
                </ModalBody>
            </Modal>
                <>
                <WishlistHandle router={router} isloggedin={isloggedin} wishlist={wishlist} user2={user2}  />
                </>
            <Sidenav
                handle={cartHandle(cartList)}
                position="right"
                open={open}
                width={380}
                toggleSidenav={toggleSidenav}
            >
                <MiniCart toggleSidenav={toggleSidenav}  />
            </Sidenav>
            {isloggedin === "true" ?
                (
                    <div className={classes.root}>
                        <div>
                            <Button
                                ref={anchorRef}
                                aria-controls={open2 ? 'menu-list-grow' : undefined}
                                aria-haspopup="true"
                                className='hover_button_akkount'
                                onClick={()=>handleToggle()}
                            >
                                <IconButton
                                    className="hover_button_user"
                                    bg="white" p='0px'>
                                    <Icon
                                     size="24px"
                                     className="header_icon_style d-flex m-auto justify-content-center"
                                    >
                                         round-account-button-with-user-inside
                                    </Icon>
                                    <div 
                                        className="header_icon_text_Style"
                                        color="text.muted" 
                                    >
                                      <div
                                          className="text-secondary akkount_margin"
                                      >
                                          <FormattedMessage
                                              id="desktop_navigation_cabinet"
                                              defaultMessage="Kabinet"
                                          />
                                      </div>

                                    </div>
                                </IconButton>

                            </Button>
                            <Popper
                                open={open2}
                                anchorEl={anchorRef.current}
                                role={undefined}
                                transition
                                disablePortal
                            >
                                {({ TransitionProps, placement }) => (
                                    <Grow
                                        {...TransitionProps}
                                        style=
                                            {{
                                                transformOrigin: placement === 'bottom'
                                                    ?
                                                        'center top'
                                                    :
                                                        'center bottom'
                                            }}
                                    >
                                        <Paper>
                                            <ClickAwayListener
                                                onClickAway={handleClose}
                                            >
                                                <LoggedInUserMenuList
                                                    shop2={shop2}
                                                    setloggedin={(e)=>setloggedin(e)}
                                                    setOpen2={(e)=>setOpen2(e)}
                                                 />
                                            </ClickAwayListener>
                                        </Paper>
                                    </Grow>
                                )}
                            </Popper>
                        </div>
                    </div>

                ) :  
                (
                <UserLoginDialog
                    handle={
                        <div>
                            <IconButton
                                bg="white"
                                color="text.muted"
                                className="background-rgb"
                            >
                                <Icon className="header_icon_style d-flex justify-content-center">round-account-button-with-user-inside</Icon>
                                <div className="header_icon_text_Style2">
                                    <div
                                        className="text-secondary"
                                    >
                                        <FormattedMessage
                                            id="user"
                                            defaultMessage="Kirish"
                                        />
                                    </div>
                                </div>
                            </IconButton>
                        </div>
                    }
                >
                    <Box>
                        <Login/>
                    </Box>
                </UserLoginDialog>
                
                )}
            <Link href="/help">
                <IconButton
                    className="header_help_icon_container background-rgb"
                    color="text.muted"
                    bg="white"
                >
                    <Icon className="header_help_icon_style d-flex justify-content-center">help-web-button</Icon>
                    <div className="fs-small fw-lighter ml-2px">
                        <FormattedMessage
                            id="help"
                            defaultMessage="Yordam"
                        />
                    </div>
                </IconButton>
            </Link>
        </FlexBox>
      </Container>
    </StyledHeader>
  );
};

export default Header;
