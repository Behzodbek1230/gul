import React from "react";
import Link from "next/link"
export type SocialLinksType = {
    facebookUrl?:string,
    instagramUrl?:string,
    telegramUrl?:string,
    tiktokUrl?:string,
    youtubeUrl?:string,
    siteUrl?:string,
}
let SocialLinks:React.FC<SocialLinksType>= ({
    facebookUrl,
    instagramUrl,
    telegramUrl,
    tiktokUrl,
    youtubeUrl,
    siteUrl
})=>{
    let images = []
    instagramUrl && images.push({
        imgUrl:"/assets/images/social/instagram.png",
        value:"instagram",
        url:instagramUrl
    })
    telegramUrl && images.push({
        imgUrl:"/assets/images/social/telegram2.png",
        value:"telegram",
        url:telegramUrl
    })
    facebookUrl && images.push({
        imgUrl:"/assets/images/social/facebook.png",
        value:"facebook",
        url:facebookUrl
    })
    siteUrl && images.push({
        imgUrl:"/assets/images/social/worldwide.png",
        value:"site",
        url:siteUrl
    })
    youtubeUrl && images.push({
        imgUrl:"/assets/images/social/youtube.png",
        value:"youtube",
        url:youtubeUrl
    })
    tiktokUrl && images.push({
        imgUrl:"/assets/images/social/tiktok.png",
        value:"tiktok",
        url:tiktokUrl
    })

    return(<div className="d-flex flex-wrap justify-content-lg-end justify-content-xl-end social-links ml-0 pl-0">
        {images?.map((one,ind)=>{
                if(ind===0){
                    return(<Link href={one?.url}>
                        <div className="py-1 pr-1">
                            <img
                                className="cursor-pointer2"
                                src={one?.imgUrl}
                                width="18px"
                                height="18px"
                                alt={one?.value}
                            />
                        </div>
                    </Link>)
                }
                else{
                    return(<Link href={one?.url}>
                        <div className="p-1">
                            <img
                                className="cursor-pointer2"
                                src={one?.imgUrl}
                                width="18px"
                                height="18px"
                                alt={one?.value}
                            />
                        </div>
                    </Link>)
                }
            }
        )}
    </div>
    )
}
export default SocialLinks;