import React, {useState} from "react";
import ReactPaginate from "react-paginate";
import { useFormik } from "formik";
import Box from "../components/Box";
import {H2, H5} from "../components/Typography";
import FlexBox from "../components/FlexBox";
import Rating from "../components/rating/Rating";
import Button from "../components/buttons/Button";
import axios from "axios";
import Cookies from "js-cookie"
import { useDispatch, useSelector} from "react-redux";
import {useRouter} from "next/router";
import {FormattedMessage, useIntl} from "react-intl";
import ProductComment from "../components/products/ProductComment";
import {StyledPagination} from "../components/pagination/PaginationStyle";
import Icon from "../components/icon/Icon";
import dynamic from "next/dynamic";
import useWindowSize from "../hooks/useWindowSize";
import { open_login } from "../../Redux/Actions/LoginModel";

export interface ProductReviewProps {
    reviews?:any,
    ReviewPage?:any,
    setReviewPage?:any,
    setaddedstatus?:any,
    setreviews?:any
}

if(typeof window !== "undefined"){
    const Quill = require("react-quill").Quill
    const Image = Quill.import('formats/image');
    Image.className = 'quill_image';
    Quill.register(Image, true);
}


const ReactQuill = dynamic(
    () => import('react-quill'),
    { ssr: false }
);


const modules = {
    toolbar: [
        [{ header: '1' }, { header: '2' }, { font: [] }],
        [{ size: [] }],
        ['bold', 'italic', 'underline', 'strike', 'blockquote'],
        [
            { list: 'ordered' },
            { list: 'bullet' },
            { indent: '-1' },
            { indent: '+1' },
        ],
        ['link', 'image', 'video'],
        ['clean'],
    ],
    clipboard: {
        matchVisual: false,
    },
}

const Shop_Review: React.FC<ProductReviewProps> = ({
   reviews,
   ReviewPage,
   setReviewPage,
   setaddedstatus,
    setreviews,
}) => {
    const info = useSelector((state:any)=>state.new.shop)
    const [msg,setmsg] = useState("")
    let dispatch = useDispatch()
    let width = useWindowSize()
    let [rating_error,setrating_error] = useState("")
    let [comment2,setcomment2] = useState("")
    let intl = useIntl()
    const router = useRouter()
    let {id} = router.query
    const handleFormSubmit = async (values, { resetForm }) => {
        let f = router.locale
        let formData = new FormData()
        formData.append("ball",values.rating)
        formData.append('message',comment2)
        formData.append("keyword",info.data.keyword)
        const loggedin = Cookies.get("isLoggedIn")
        setrating_error("")
        setmsg("")
        if(!values?.rating){
            setrating_error(intl.formatMessage({id:"rating_required"}))
        }
        else if(comment2==="" && comment2){
            setmsg(intl.formatMessage({id:"text_required"}))
        }
        else if(loggedin === "true"){

            axios({
                method:"POST",
                url:`/shops/create-comment/en`,
                data:formData
            })
                .then((response)=>{
                    if(response?.data?.errors){
                        setmsg("Error")
                    }
                    else {
                        setcomment2("")
                        axios.get(`/shops/rating-list/${id}/${f}?page=${ReviewPage+1}`)
                            .then((res)=>{
                                setreviews(res.data)
                                setaddedstatus(info.data.keyword+values.comment + values.rating)
                                setmsg(intl.formatMessage({id:"add_comment_success"}))
                                setTimeout(() => {
                                   setmsg("") 
                                }, 2000);

                            })
                            .catch(()=>{
                                setmsg("Error")
                            })
                        resetForm();
                    }
                })
                .catch(()=>null)

        }
        else{
            if(width < 650){
                router.push('/register')
            }
            else{
                dispatch(open_login())
            }
        }
    };

    const {
        values,
        handleSubmit,
        setFieldValue,
    } = useFormik({
        initialValues: initialValues,
        // validationSchema: reviewSchema,
        onSubmit: handleFormSubmit,
    });

    return (
        <Box>
            {reviews?.data?.length >= 1 && reviews.data ?
                <div className="p-1 mb-4">
                    {reviews?.data?.map((rating,ind) => {
                        return <ProductComment
                            id={ind}
                            key={rating.id}
                            imgUrl={rating.userAvatar}
                            date={rating.date}
                            name={rating.userFio}
                            rating={rating.ball}
                            comment={rating.comment}
                            is_shop={true}
                            extended={rating?.extend}
                            reviews={reviews}
                            setreviews = {(e)=>setreviews(e)}
                        />
                    })}
                    {reviews.last_page !== 1 ?
                        <FlexBox
                            flexWrap="wrap"
                            justifyContent="space-between"
                            alignItems="center"
                            mt="32px"
                        >
                            <StyledPagination>
                                <ReactPaginate
                                    initialPage={ReviewPage}
                                    previousLabel={
                                        <Button
                                            style={{cursor: "pointer"}}
                                            className="control-button"
                                            color="primary"
                                            overflow="hidden"
                                            height="auto"
                                            padding="6px"
                                            borderRadius="50%"
                                        >
                                            <Icon defaultcolor="currentColor" variant="small">
                                                chevron-left
                                            </Icon>
                                        </Button>

                                    }
                                    nextLabel={
                                        <Button
                                            style={{cursor: "pointer"}}
                                            className="control-button"
                                            color="primary"
                                            overflow="hidden"
                                            height="auto"
                                            padding="6px"
                                            borderRadius="50%"
                                        >
                                            <Icon defaultcolor="currentColor" variant="small">
                                                chevron-right
                                            </Icon>
                                        </Button>
                                    }
                                    breakLabel={
                                        <Icon defaultcolor="currentColor" variant="small">
                                            triple-dot
                                        </Icon>
                                    }
                                    pageCount={reviews.last_page}
                                    marginPagesDisplayed={true}
                                    pageRangeDisplayed={false}
                                    onPageChange={(r) => {
                                        setReviewPage(r.selected + 1)
                                    }
                                    }
                                    containerClassName="pagination"
                                    subContainerClassName="pages pagination"
                                    activeClassName="active"
                                    disabledClassName="disabled"
                                />
                            </StyledPagination>
                        </FlexBox>
                    :
                        ""
                    }


                </div>
                :
                ""
            }
            <H2 fontWeight="600" mt="-10px" mb="20">
                <FormattedMessage
                    id="shop_review"
                    defaultMessage="Izoh qoldiring"
                />
            </H2>

            <form onSubmit={handleSubmit}>
                <Box mb="20px">
                    <FlexBox mb="12px">
                        <H5 color="gray.700">
                            <FormattedMessage id="Your Rating" />
                        </H5>
                        <H5 color="error.main">*</H5>
                    </FlexBox>

                    <Rating
                        outof={5}
                        color="warn"
                        size="medium"
                        readonly={false}
                        value={values.rating || 0}
                        onChange={(value) => setFieldValue("rating", value)}
                    />
                    {rating_error==="" ? "" : <div className="text-danger">{rating_error}</div>}
                </Box>

                <Box mb="24px">
                    <FlexBox mb="12px">
                        <H5 color="gray.700">
                            <FormattedMessage id="Your Review" />
                        </H5>
                        <H5 color="error.main">*</H5>
                    </FlexBox>
                    <div className="react-quil-container432">
                        <ReactQuill
                            modules={modules}
                            className="reactquill-height2 my-editing-area"
                            value={comment2}
                            onChange={(e)=>{setcomment2(e)}
                            }
                        />
                    </div>
                    {msg==="" ? "" : <div className="text-danger">{msg}</div>}
                </Box>

                <Button
                    variant="contained"
                    color="primary"
                    size="small"
                    type="submit"
                >
                    <FormattedMessage
                        id="send(jonatish)"
                        defaultMessage="Jo'natish"
                    />
                </Button>
            </form>
        </Box>
    );
};


const initialValues = {
    rating: "",
    comment: "",
    date: new Date().toISOString(),
};


export default Shop_Review;
