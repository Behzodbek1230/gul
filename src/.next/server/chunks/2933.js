"use strict";
exports.id = 2933;
exports.ids = [2933,4875,4660];
exports.modules = {

/***/ 4875:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5834);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_system__WEBPACK_IMPORTED_MODULE_1__);


const Box = styled_components__WEBPACK_IMPORTED_MODULE_0___default().div(({ shadow , cursor , transition , theme  })=>({
        boxShadow: theme.shadows[shadow],
        cursor,
        transition
    })
, (0,styled_system__WEBPACK_IMPORTED_MODULE_1__.compose)(styled_system__WEBPACK_IMPORTED_MODULE_1__.layout, styled_system__WEBPACK_IMPORTED_MODULE_1__.space, styled_system__WEBPACK_IMPORTED_MODULE_1__.color, styled_system__WEBPACK_IMPORTED_MODULE_1__.position, styled_system__WEBPACK_IMPORTED_MODULE_1__.flexbox, styled_system__WEBPACK_IMPORTED_MODULE_1__.flex, styled_system__WEBPACK_IMPORTED_MODULE_1__.border, styled_system__WEBPACK_IMPORTED_MODULE_1__.typography));
Box.defaultProps = {
    shadow: 0,
    cursor: "unset"
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Box);


/***/ }),

/***/ 4660:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5834);
/* harmony import */ var styled_system__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_system__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _utils_utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1972);
/* harmony import */ var _Box__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(4875);




const Card = styled_components__WEBPACK_IMPORTED_MODULE_0___default()(_Box__WEBPACK_IMPORTED_MODULE_3__["default"])`
  background-color: ${(0,_utils_utils__WEBPACK_IMPORTED_MODULE_2__/* .getTheme */ .gh)("colors.body.paper")};
  box-shadow: ${(props)=>(0,_utils_utils__WEBPACK_IMPORTED_MODULE_2__/* .getTheme */ .gh)(`shadows.${props.boxShadow}`, `shadows.${props.elevation}`)
};

  :hover {
    box-shadow: ${(props)=>props.hoverEffect && (0,_utils_utils__WEBPACK_IMPORTED_MODULE_2__/* .getTheme */ .gh)("shadows.large")
};
  }

  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.border}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.color}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.space}
  ${styled_system__WEBPACK_IMPORTED_MODULE_1__.layout}
`;
Card.defaultProps = {
    boxShadow: "small",
    borderRadius: 8,
    hoverEffect: false
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Card);


/***/ }),

/***/ 2933:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "s": () => (/* binding */ Card1)
/* harmony export */ });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Card__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4660);


const Card1 = styled_components__WEBPACK_IMPORTED_MODULE_0___default()(_Card__WEBPACK_IMPORTED_MODULE_1__["default"])`
  position: relative;
  padding: 1.5rem 1.75rem;
  @media only screen and (max-width: 678px) {
    padding: 1rem;
  }
`;


/***/ })

};
;