import dynamic from "next/dynamic";
const Section1 = dynamic(()=>import('../components/home-1/Section1'),{ssr:false})
const Section12 = dynamic(()=>import('../components/home-1/Section12'),{ssr:false})
const Section15 = dynamic(()=>import('../components/home-1/Section15'),{ssr:false})
const Section2 = dynamic(()=>import('../components/home-1/Section2'),{ssr:false})
const Section3 = dynamic(()=>import('../components/home-1/Section3'),{ssr:false})
const AppLayout = dynamic(()=>import('../components/layout/AppLayout'),{ssr:false})
const SeoText = dynamic(()=>import("../components/SeoText"),{ssr:false})
const Loading = dynamic(()=>import("../components/Loading"),{ssr:false})
import axios from "axios"
import {useDispatch} from "react-redux"
import React,{useCallback, useEffect, useState} from "react"
import Cookies from "js-cookie"
import useWindowSize from "../hooks/useWindowSize"
import get_category_products from "../../Redux/Actions/get_category_products"
import {fetch_user_info} from "../../Redux/Actions/Action"
import get_banner from "../../Redux/Actions/get_banner"
import {NextSeo} from "next-seo"
import { useRouter } from "next/router"
import get_seo_text from "../../Redux/Actions/get_seo_text"
import cookies from "next-cookies";
import { InView } from 'react-intersection-observer'
import { BASE_URL } from "../components/Variables";

const IndexPage = ({banner,category_products,shop_list,setcategory,data2,setbanner,category_products2,home_seo}) => {
    const dispatch = useDispatch()
    useEffect(()=>{
      setcategory("")
      dispatch(get_banner(banner))
      dispatch(fetch_user_info(data2))
      dispatch(get_category_products(category_products2))
      setseo(home_seo)
      console.log(shop_list,setbanner,category_products)
    },[])
    const size = useWindowSize()
    let router = useRouter()
    let lang = router.locale
    let [seo,setseo] = useState(home_seo)
    let [loading2,setloading2] = useState(false)
    let [categoryProducts,setcategoryProducts] = useState(category_products2)
    let currency_id = Cookies.get("currency_id");
    let currency_text = typeof currency_id !== "undefined" ?  `?currency=${currency_id}` : ""
    let CategoryFetch = useCallback(()=>{
        axios.get(`/flowers/category-item/${router.locale}${currency_text}`,)
            .then(res=>{
                dispatch(get_category_products(res.data))
                setcategoryProducts(res.data)
                setloading2(false)
            })
            .catch(()=>setloading2(false))
    },[currency_id,lang])
    useEffect(() => {
        CategoryFetch()
    }, [currency_id,lang]);
    let PageDataChangeWhenChanged=useCallback(()=>{
        axios.get(`/seo/home-page/${lang}`)
            .then((res)=>{
                dispatch(get_seo_text((res?.data?.site_settings_main_seotext)))
                setseo(res.data)
            })
            .catch(()=>null)
    },[lang])
    useEffect(()=>{
        PageDataChangeWhenChanged()
    },[lang])
    if(loading2){
        return <>
             <NextSeo
              title={seo?.site_settings_main_title}
              description={seo?.site_settings_main_description}
              additionalMetaTags={[
                {
                name: 'keyword',
                content: seo?.site_settings_main_keyword
              },
             ]}
             openGraph={{

                 images:[
                     {
                         url:seo?.logo,
                         alt:"Dana.uz",
                         width:600,
                         height:200
                     }
                 ]
             }}
              
           />
            <Loading />
        </>
    }
    else{
        return (
                <AppLayout>
                    <NextSeo
                        title={seo?.site_settings_main_title}
                        description={seo?.site_settings_main_description}
                        additionalMetaTags={[
                            {
                                name: 'keyword',
                                content: seo?.site_settings_main_keyword
                            },
                        ]}
                        openGraph={{

                            images:[
                                {
                                    url:seo?.logo,
                                    alt:"Dana.uz",
                                    width:600,
                                    height:200
                                }
                            ]
                        }}

                    />
                    <div>
                        {size < 650
                            ?
                            <>
                                <InView  triggerOnce={true}>
                                    {({ref,inView})=>(
                                        <div ref={ref}>{inView && <Section3 />}</div>
                                    )}
                                    
                                </InView>
                                <InView  triggerOnce={true}>
                                    {({ref,inView})=>(
                                        <div ref={ref}>{inView && <Section1 />}</div>
                                    )}
                                    
                                </InView>
                            </>
                            :

                            <>
                                <InView  triggerOnce={true}>
                                    {({ref,inView})=>(
                                        <div ref={ref}>{inView && <Section1 />}</div>
                                    )}
                                    
                                </InView>
                                <InView  triggerOnce={true}>
                                    {({ref,inView})=>(
                                        <div ref={ref}>{inView && <Section3 />}</div>
                                    )}
                                    
                                </InView>
                            </>
                        }

                        <main className="container">
                            
                            <div className='category_flower_height'>
                                <InView  triggerOnce={true}>
                                {({ref,inView})=>(
                                    <div ref={ref}>{inView && <Section2  data={categoryProducts}/>}</div>
                                )}
                                    
                                </InView>
                            </div>
                            <InView triggerOnce={true}>
                                {({ref,inView})=>(
                                    <div ref={ref}>{inView && <Section15 />}</div>
                                )}
                            </InView>
                            <InView triggerOnce={true}>
                                {({ref,inView})=>(
                                    <div ref={ref}>{inView && <Section12 />}</div>
                                )}
                            </InView>
                            <InView triggerOnce={true}>
                                {({ref,inView})=>(
                                    <div ref={ref}>{inView && <SeoText  text={seo?.site_settings_main_seotext}/>}</div>
                                )}
                            </InView>
                        </main>

                    </div>
                </AppLayout>
    )
    }

};

export default IndexPage;


export async function getStaticProps(ctx){
    let lang= ctx.locale
    let {currency_id} = cookies(ctx)
    let data = await axios.get(`${BASE_URL}/flowers/category-item/${lang}currency_id=${currency_id}`)
    let seo_request = await axios.get(`${BASE_URL}/seo/home-page/${lang}`)
    return{
        props:{
            category_products2:data.data,
            home_seo:seo_request.data
        },
        revalidate:1
    }

}
