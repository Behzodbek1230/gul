"use strict";
exports.id = 7272;
exports.ids = [7272];
exports.modules = {

/***/ 7272:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ components_CategorySectionCreator)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: ./hooks/useWindowSize.js
var useWindowSize = __webpack_require__(12);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: ./components/Box.tsx
var Box = __webpack_require__(4875);
// EXTERNAL MODULE: ../node_modules/next/link.js
var next_link = __webpack_require__(9894);
// EXTERNAL MODULE: ./components/FlexBox.tsx
var FlexBox = __webpack_require__(5359);
// EXTERNAL MODULE: ./components/icon/Icon.tsx + 1 modules
var Icon = __webpack_require__(4154);
// EXTERNAL MODULE: ./components/Typography.tsx
var Typography = __webpack_require__(9374);
// EXTERNAL MODULE: external "react-intl"
var external_react_intl_ = __webpack_require__(3126);
;// CONCATENATED MODULE: ./components/CategorySectionHeader.tsx







const CategorySectionHeader = ({ title , seeMoreLink , iconName ,  })=>{
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(FlexBox["default"], {
        justifyContent: "space-between",
        alignItems: "center",
        mb: "1.5rem",
        children: [
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(FlexBox["default"], {
                alignItems: "center",
                children: [
                    iconName && /*#__PURE__*/ jsx_runtime_.jsx(Icon["default"], {
                        defaultcolor: "auto",
                        mr: "0.5rem",
                        children: iconName
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx(Typography.H1, {
                        fontWeight: "bold",
                        className: "title_category_Section_header",
                        lineHeight: "1",
                        children: title
                    })
                ]
            }),
            seeMoreLink && /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                href: seeMoreLink,
                children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                    children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)(FlexBox["default"], {
                        alignItems: "center",
                        ml: "0.5rem",
                        color: "text.muted",
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx(Typography.SemiSpan, {
                                className: "more_category_section_header",
                                mr: "0.5rem",
                                children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                                    id: "view_all",
                                    defaultMessage: "View all"
                                })
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx(Icon["default"], {
                                size: "12px",
                                defaultcolor: "currentColor",
                                children: "right-arrow"
                            })
                        ]
                    })
                })
            })
        ]
    }));
};
/* harmony default export */ const components_CategorySectionHeader = (CategorySectionHeader);

// EXTERNAL MODULE: ./components/Container.tsx
var Container = __webpack_require__(9221);
;// CONCATENATED MODULE: ./components/CategorySectionCreator.tsx






const CategorySectionCreator = ({ iconName , seeMoreLink , title , children , id  })=>{
    let width = (0,useWindowSize/* default */.Z)();
    return(/*#__PURE__*/ jsx_runtime_.jsx(Box["default"], {
        id: id,
        mb: width < 650 ? '1.75rem' : "3.75rem",
        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)(Container["default"], {
            pb: "1rem",
            children: [
                title && /*#__PURE__*/ jsx_runtime_.jsx("div", {
                    children: /*#__PURE__*/ jsx_runtime_.jsx(components_CategorySectionHeader, {
                        title: title,
                        seeMoreLink: seeMoreLink,
                        iconName: iconName
                    })
                }),
                children
            ]
        })
    }));
};
/* harmony default export */ const components_CategorySectionCreator = (CategorySectionCreator);


/***/ })

};
;