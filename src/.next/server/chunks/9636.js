"use strict";
exports.id = 9636;
exports.ids = [9636];
exports.modules = {

/***/ 9636:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ shop_ShopCard1)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: ./components/avatar/Avatar.tsx + 1 modules
var Avatar = __webpack_require__(3392);
// EXTERNAL MODULE: ./components/Box.tsx
var Box = __webpack_require__(4875);
// EXTERNAL MODULE: ./components/buttons/IconButton.tsx
var IconButton = __webpack_require__(8661);
// EXTERNAL MODULE: ./components/FlexBox.tsx
var FlexBox = __webpack_require__(5359);
// EXTERNAL MODULE: ./components/icon/Icon.tsx + 1 modules
var Icon = __webpack_require__(4154);
// EXTERNAL MODULE: ./components/Typography.tsx
var Typography = __webpack_require__(9374);
// EXTERNAL MODULE: external "styled-components"
var external_styled_components_ = __webpack_require__(7518);
var external_styled_components_default = /*#__PURE__*/__webpack_require__.n(external_styled_components_);
// EXTERNAL MODULE: ./utils/themeColors.ts
var themeColors = __webpack_require__(8970);
// EXTERNAL MODULE: ./utils/utils.ts
var utils = __webpack_require__(1972);
// EXTERNAL MODULE: ./components/Card.tsx
var Card = __webpack_require__(4660);
;// CONCATENATED MODULE: ./components/shop/ShopCardStyle.tsx




const ShopCard1Wrapper = external_styled_components_default()(Card["default"])`
  .black-box {
    background-image: linear-gradient(
        to bottom,
        rgba(${(0,utils/* convertHexToRGB */.Q5)(themeColors/* colors.gray.900 */.O.gray[900])}, 0.8),
        rgba(${(0,utils/* convertHexToRGB */.Q5)(themeColors/* colors.gray.900 */.O.gray[900])}, 0.8)
      ),
      url(${(props)=>props.coverImgUrl || "/assets/images/banners/cycle.png"
});
    background-size: cover;
    background-position: center;
    color: white;
    height:190px;
    width:100%;   
  }
`;

// EXTERNAL MODULE: ./components/rating/Rating.tsx + 2 modules
var Rating = __webpack_require__(197);
// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(1853);
// EXTERNAL MODULE: external "react-intl"
var external_react_intl_ = __webpack_require__(3126);
;// CONCATENATED MODULE: ./components/shop/ShopCard1.tsx












const ShopCard1 = ({ name , logo , rating , keyword , address , background_image  })=>{
    const router = (0,router_.useRouter)();
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(ShopCard1Wrapper, {
        overflow: "hidden",
        coverImgUrl: background_image || logo,
        onClick: ()=>router.push(`/shop/${keyword}`)
        ,
        children: [
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(Box["default"], {
                className: "black-box",
                p: "17px 30px 56px",
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx(Typography.H3, {
                        fontWeight: "600",
                        mb: "8px",
                        style: {
                            width: '370px',
                            overflow: "hidden",
                            textOverflow: "ellipsis",
                            paddingRight: "50px"
                        },
                        children: name
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx(Box["default"], {
                        mb: "5px",
                        children: /*#__PURE__*/ jsx_runtime_.jsx(Rating/* default */.Z, {
                            size: "small",
                            value: rating || 0,
                            outof: 5,
                            color: "warn"
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx(FlexBox["default"], {
                        mb: "3px",
                        children: /*#__PURE__*/ jsx_runtime_.jsx(Typography.SemiSpan, {
                            style: {
                                height: "40px",
                                wordBreak: "break-all",
                                overflowY: "hidden",
                                textOverflow: "ellipsis"
                            },
                            color: "white",
                            ml: "0px",
                            children: address
                        })
                    })
                ]
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(FlexBox["default"], {
                pl: "30px",
                pr: "18px",
                justifyContent: "space-between",
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx(Avatar/* default */.Z, {
                        src: logo,
                        alt: name,
                        size: 64,
                        mt: "-32px",
                        border: "4px solid",
                        borderColor: "gray.100"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        style: {
                            position: "relative"
                        },
                        children: /*#__PURE__*/ jsx_runtime_.jsx("label", {
                            className: "visit_shop_style",
                            children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_intl_.FormattedMessage, {
                                id: "visit_Shop"
                            })
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("a", {
                        children: /*#__PURE__*/ jsx_runtime_.jsx(IconButton/* default */.Z, {
                            size: "small",
                            my: "0.25rem",
                            children: /*#__PURE__*/ jsx_runtime_.jsx(Icon["default"], {
                                defaultcolor: "auto",
                                children: "arrow-long-right"
                            })
                        })
                    })
                ]
            })
        ]
    }));
};
/* harmony default export */ const shop_ShopCard1 = (ShopCard1);


/***/ })

};
;